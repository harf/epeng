
select
asa.id_satuan as asa_id_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.id_lokasi as ake_id_lokasi, mlok_ake.nama_lokasi as ake_nama_lokasi, ake.id_kontainer as ake_id_kontainer, mkon_ake.nama_kontainer as ake_nama_kontainer, ake.id_posisi_kontainer as ake_posisi_kontainer, mpos_ake.label_posisi_kontainer as ake_label_posisi_kontainer, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, ake.penerima_aset as ake_penerima_aset, ake.keterangan_aset_keluar as ake_keterangan_aset_keluar
 from aset_satuan as asa
left join ms_lokasi as mlok_asa on asa.id_lokasi = mlok_asa.id_lokasi
left join ms_kontainer as mkon_asa on asa.id_kontainer = mkon_asa.id_kontainer
left join ms_posisi_kontainer as mpos_asa on asa.id_posisi_kontainer = mpos_asa.id_posisi_kontainer
left join aset_keluar as ake on asa.id_satuan = ake.id_satuan
left join ms_lokasi as mlok_ake on ake.id_lokasi = mlok_ake.id_lokasi
left join ms_kontainer as mkon_ake on ake.id_kontainer = mkon_ake.id_kontainer
left join ms_posisi_kontainer as mpos_ake on ake.id_posisi_kontainer = mpos_ake.id_posisi_kontainer
left join ms_kantor_imigrasi as kanim on ake.kode_kanim = kanim.kode_kanim
where asa.id_aset = 1 and asa.kode_pengadaan = 49