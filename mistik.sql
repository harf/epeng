-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 08 Mar 2019 pada 08.47
-- Versi server: 10.1.32-MariaDB
-- Versi PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mistik`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `alamat`
--

CREATE TABLE `alamat` (
  `id_alamat` int(10) UNSIGNED NOT NULL,
  `src_tabel_alamat` varchar(100) DEFAULT '0',
  `src_kolom_alamat` varchar(100) DEFAULT '0',
  `src_fk_alamat` int(10) DEFAULT '0',
  `nama_alamat` varchar(200) NOT NULL,
  `subdomain` varchar(100) DEFAULT NULL,
  `ip_address` varchar(100) DEFAULT NULL,
  `port` varchar(50) DEFAULT NULL,
  `keterangan_alamat` varchar(500) DEFAULT NULL,
  `alamat_utama` enum('Y','N') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alamat`
--

INSERT INTO `alamat` (`id_alamat`, `src_tabel_alamat`, `src_kolom_alamat`, `src_fk_alamat`, `nama_alamat`, `subdomain`, `ip_address`, `port`, `keterangan_alamat`, `alamat_utama`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'aplikasi', 'id_aplikasi', 1, 'Main Connection', 'helpdesk.imigrasi.go.id', '272.68.192.4', '8570', '-', 'Y', '2018-11-11 13:54:45', '18', NULL, NULL, NULL),
(2, 'aplikasi', 'id_aplikasi', 2, 'WHV Production', 'whv.imigrasi.go.id', '10.1.4.40', '8000', '-', 'Y', '2018-11-11 17:55:58', '18', NULL, NULL, NULL),
(3, 'aplikasi', 'id_aplikasi', 2, 'WHV Development', 'whv.imigrasi.go.id:8000', '10.1.4.40', '8000', NULL, 'N', '2018-11-11 17:56:29', '18', NULL, NULL, NULL),
(4, 'aplikasi', 'id_aplikasi', 6, 'MISTIK Dev', 'localhost', 'localhost', '8000', '-', 'N', '2018-11-11 18:18:45', '18', NULL, NULL, NULL),
(5, 'aplikasi', 'id_aplikasi', 6, 'MISTIK Prod', '172.31.98.105', '172.31.98.105', '8000', '-', 'Y', '2018-11-11 18:19:14', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aplikasi`
--

CREATE TABLE `aplikasi` (
  `id_aplikasi` int(10) UNSIGNED NOT NULL,
  `kode_pengadaan` int(10) UNSIGNED DEFAULT NULL,
  `id_jenis_pengembangan` int(10) UNSIGNED NOT NULL,
  `nama_aplikasi` varchar(200) NOT NULL,
  `singkatan_aplikasi` varchar(100) DEFAULT NULL,
  `deskripsi_aplikasi` varchar(500) DEFAULT NULL,
  `bahasa_pemrograman` varchar(100) DEFAULT NULL,
  `framework_pemrograman` varchar(100) DEFAULT NULL,
  `sistem_operasi` varchar(100) DEFAULT NULL,
  `tanggal_dev` datetime DEFAULT NULL,
  `tanggal_live` datetime DEFAULT NULL,
  `keterangan_aplikasi` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aplikasi`
--

INSERT INTO `aplikasi` (`id_aplikasi`, `kode_pengadaan`, `id_jenis_pengembangan`, `nama_aplikasi`, `singkatan_aplikasi`, `deskripsi_aplikasi`, `bahasa_pemrograman`, `framework_pemrograman`, `sistem_operasi`, `tanggal_dev`, `tanggal_live`, `keterangan_aplikasi`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, NULL, 1, 'Aplikasi Helpdesk IMIGRASI', 'HD', 'Aplikasi yang dipergunakan untuk report permasalahan mengenai SIMKIM untuk seluruh satuan kerja Imigrasi di Indonesia maupun di Luar Negeri', 'PHP', 'CodeIgniter', 'Red Hat Enterprise Linux (RHEL)', '2016-04-04 00:00:00', '2016-10-10 00:00:00', NULL, '2018-11-11 13:45:30', '18', '2018-11-11 13:46:44', '18', NULL),
(2, NULL, 1, 'Aplikasi Pelayanan Surat Rekomendasi Pemerintah Indonesia untuk Program Work and Holiday Visa (WHV)', 'WHV', 'Aplikasi Layanan Surat Rekomendasi Pemerintah Indonesia adalah aplikasi untuk membantu masyarakat Indonesia mendapatkan Surat Rekomendasi Pemerintah Indonesia yang ditujukan ke Kedutaan Besar Australia untuk mengajukan Visa Australia tipe \"Work and Holiday\"', 'PHP', 'Laravel', 'Red Hat Enterprise Linux (RHEL)', '2018-04-16 00:00:00', '2018-08-28 00:00:00', NULL, '2018-11-11 14:03:36', '18', '2018-11-11 17:46:02', '18', NULL),
(3, NULL, 3, 'BCM Immigration on Shipping (Android)', 'IoS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11 14:04:03', '18', '2018-11-11 14:04:20', '18', NULL),
(4, NULL, 1, 'Aplikasi Pendaftaran Antrian Paspor secara Online (APAPO) - Website', 'APAPO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11 14:04:46', '18', NULL, NULL, NULL),
(5, NULL, 3, 'Aplikasi Pendaftaran Antrian Paspor secara Online (APAPO) - Android', 'APAPO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11 14:05:08', '18', '2018-11-11 14:05:18', '18', NULL),
(6, NULL, 1, 'Aplikasi Manajemen Informasi Direktorat SISTIK', 'MISTIK', 'Aplikasi Manajamen Informasi Direktorat SISTIK atau MISTIK merupakan aplikasi yang dipakai di Subdirektorat Perencanaan dan Pengembangan (Subdit Renbang) di Direktorat Sistem dan Teknologi Informasi Keimigrasian (SISTIK), Direktorat Jenderal Imigrasi, sebagai tempat inventaris seluruh dokumentasi terkait SIMKIM mulai dari aplikasi SIMKIM, source code SIMKIM, sampai perangkat SIMKIM. Aplikasi ini dapat berjalan pada platform atau sistem operasi apa saja yang mendukung aplikasi berbasis website.', 'PHP', 'Laravel', 'Windows 7', '2018-08-02 00:00:00', '2018-10-31 00:00:00', NULL, '2018-11-11 14:07:14', '18', '2018-11-11 18:18:17', '18', NULL),
(7, NULL, 1, 'Aplikasi Penerbitan Dokumen Perjalanan Republik Indonesia', 'DPRI', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11 14:07:44', '18', NULL, NULL, NULL),
(8, NULL, 1, 'Aplikasi Maintenance Server 178', '178', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11 14:08:13', '18', NULL, NULL, NULL),
(9, NULL, 1, 'Aplikasi Izin Tinggal Keimigrasian (WNA)', 'INTAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-11-11 14:08:48', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aplikasi_admin`
--

CREATE TABLE `aplikasi_admin` (
  `id_administrator` int(10) UNSIGNED NOT NULL,
  `id_aplikasi` int(10) UNSIGNED NOT NULL,
  `id_koneksi` int(10) UNSIGNED DEFAULT NULL,
  `user_administrator` varchar(100) NOT NULL,
  `password_administrator` varchar(100) NOT NULL,
  `keterangan_administrator` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aplikasi_sourcecode`
--

CREATE TABLE `aplikasi_sourcecode` (
  `id_sourcecode` int(10) UNSIGNED NOT NULL,
  `id_aplikasi` int(10) UNSIGNED NOT NULL,
  `id_versi` int(10) UNSIGNED NOT NULL,
  `id_lokasi` int(10) UNSIGNED DEFAULT NULL,
  `id_kontainer` int(10) UNSIGNED DEFAULT NULL,
  `id_posisi_kontainer` int(10) UNSIGNED DEFAULT NULL,
  `nama_sourcecode` varchar(200) NOT NULL,
  `ukuran_sourcecode` int(10) UNSIGNED NOT NULL,
  `link_sourcecode` varchar(200) NOT NULL,
  `keterangan_sourcecode` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aplikasi_sourcecode`
--

INSERT INTO `aplikasi_sourcecode` (`id_sourcecode`, `id_aplikasi`, `id_versi`, `id_lokasi`, `id_kontainer`, `id_posisi_kontainer`, `nama_sourcecode`, `ukuran_sourcecode`, `link_sourcecode`, `keterangan_sourcecode`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 1, NULL, NULL, NULL, 'sourcecode-helpdesk-v1.0.zip', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\sourcecode\\20181111135215 - sourcecode-helpdesk-v1.0.zip', 'initial version', '2018-11-11 13:52:15', '18', NULL, NULL),
(2, 1, 2, NULL, NULL, NULL, 'sourcecode-helpdesk-v2.0.zip', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\sourcecode\\20181111135529 - sourcecode-helpdesk-v2.0.zip', '-', '2018-11-11 13:55:29', '18', NULL, NULL),
(3, 2, 3, NULL, NULL, NULL, 'sourcecode-whv-v1.0.zip', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\sourcecode\\20181111180130 - sourcecode-whv-v1.0.zip', NULL, '2018-11-11 18:01:30', '18', NULL, NULL),
(4, 6, 4, NULL, NULL, NULL, 'sourcecode-mistik-v1.0.zip', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\sourcecode\\20181111182111 - sourcecode-mistik-v1.0.zip', NULL, '2018-11-11 18:21:11', '18', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aplikasi_versi`
--

CREATE TABLE `aplikasi_versi` (
  `id_versi` int(10) UNSIGNED NOT NULL,
  `id_aplikasi` int(10) UNSIGNED NOT NULL,
  `nomor_versi` varchar(100) NOT NULL,
  `tanggal_versi` datetime NOT NULL,
  `rincian_versi` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aplikasi_versi`
--

INSERT INTO `aplikasi_versi` (`id_versi`, `id_aplikasi`, `nomor_versi`, `tanggal_versi`, `rincian_versi`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 1, '1.0', '2016-03-22 00:00:00', 'Initial Version', '2018-11-11 13:52:15', '18', NULL, NULL, NULL),
(2, 1, '2.0', '2018-11-11 00:00:00', 'Dapat melakukan penyelesaian tiket dari administrator dengan client', '2018-11-11 13:55:29', '18', NULL, NULL, NULL),
(3, 2, '1.0', '2018-08-28 00:00:00', 'Production', '2018-11-11 18:01:30', '18', NULL, NULL, NULL),
(4, 6, '1.0', '2018-10-31 00:00:00', 'Mistik Initial Version', '2018-11-11 18:21:11', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset`
--

CREATE TABLE `aset` (
  `id_aset` int(10) UNSIGNED NOT NULL,
  `id_jenis_aset` int(10) UNSIGNED NOT NULL,
  `id_merek` int(10) UNSIGNED NOT NULL,
  `tipe_aset` varchar(500) NOT NULL,
  `keterangan_aset` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset`
--

INSERT INTO `aset` (`id_aset`, `id_jenis_aset`, `id_merek`, `tipe_aset`, `keterangan_aset`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 10, 2, 'QRadar SIEM Security', NULL, '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(2, 10, 2, 'QRadar High Availability', 'SIEM (Security Information and Event Management) IBM Security QRadar mencakup solusi High Availability (HA) terutama untuk perangkat SIEM nya sehingga apabila SIEM utama mengalami kegagalan maka perangkat HA-nya secara otomatis mengambil alih fungsi SIEM. Perangkat SIEM untuk High Availability (HA) identik dengan perangkat SIEM utama, yaitu menggunakan IBM Security QRadar SIEM xx29 appliance.', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(3, 10, 2, 'Security QRadar Network Insights 1901 Layer-7 Network Flow', 'Perangkat Layer-7 Network Flow (IBM Security QRadar Network Insights) mampu secara mandiri (tidak tergantung solusi dari third party / principal lainnya) untuk menangkap Layer-7 Network Flow menggunakan deep packet inspection.', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(4, 10, 2, 'QRadar Network Packet Capture and Incident Forensics', 'IBM Security QRadar SIEM memiliki kemampuan Network Packet Capture and Incident Forensics yang dapat ditambahkan masing-masing menggunakan perangkat terpisah dimana fitur Incident Forensics mempunyai dashboard / console platform yang sama dengan solusi SIEM, sehingga dalam melakukan manajemen operasional solusi SIEM & Network Incident Forensics menggunakan dashboard / console platform yang sama dengan solusi SIEM tanpa membutuhkan kustomisasi (sudah tersedia out-of-the-box pada console solusi SIEM nya).', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(5, 11, 6, 'Containment Server 12kw - 6 Racks', NULL, '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(6, 11, 6, 'Containment Server 12kw - 12 Racks', NULL, '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(7, 11, 6, 'Containment Rack 5kw - W800', NULL, '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(8, 6, 7, 'P670 Secure Email Gateways', 'Memiliki fitur Email Protection terhadap impostor email, phising, malware, spam, bulk mail meiliki 2 supervisor engine;\r\n- Mendukung fitur Targeted Attack Protection\r\n- Mendukung fitur Email Fraud Defense\r\n- Mendukung fitur  Domain Discover\r\n- Mendukung fitur Information Protection\r\n- Mendukung fitur tambahna Threat Response Auto Pull (TRAP)\r\n- Mendukung fitur Email Continuity\r\n- Mendukung fitur Wombat Anti-Phising Suite\r\n- Memiliki lisensi untuk 4000 user\r\n- Garansi 3 tahun\r\n- Memiliki surat dukungan prinsipal\r\n- Termasuk instalasi dan pelatihan kepada team IT maksimal 10 orang', '2018-11-11 09:01:06', '22', NULL, NULL, NULL),
(9, 6, 3, 'PowerEdge R740', 'Memiliki 2U Form Factor', '2018-11-11 09:16:06', '22', NULL, NULL, NULL),
(10, 6, 3, 'PowerEdge M640 High Performance', 'Ukuran Server Blade Half-Length', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(11, 6, 3, 'PowerEdge R740 2U', NULL, '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(12, 6, 3, 'PowerEdge R740 ArcGIS', NULL, '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(13, 6, 3, 'PowerEdge M1000e Blade Server Chassis', NULL, '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(14, 5, 8, 'Passport Scanner MRTD AT9000 MK2', '3M Passport Scanner MRTD AT9000 MK2\r\n- NON-RFID Version. Contact seller for additional options.\r\n- Standard SDK: ICAO MRZ OCR; non-ICAO MRZ; 1D barcode; 2D (Aztec, DataMatrix, PDF417, QR Code) barcodes; ePassport support for basic access control; Active Authentication\r\n- Standard features: 162 (w) x 190 (d) x 157 (h) mm; visible, infrared and ultraviolet imaging using LED technology; USB 2.0 connectivity with 2-port USB hub; IP50 rating for dust ingress protection in optical chamber; reads 2D barcodes from cell phones', '2018-11-11 09:55:17', '22', NULL, NULL, NULL),
(15, 12, 9, 'Back-UPS 1100', NULL, '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(16, 1, 5, 'SFF Desktop ProDesk 400 G3N4P96AV', 'Include CPU, Keyboard, Mouse, and cables', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(17, 13, 5, 'V194 Monitor', 'LCD Monitor HP V194', '2018-11-11 11:30:05', '22', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_informasi`
--

CREATE TABLE `aset_informasi` (
  `id_aset_informasi` int(10) NOT NULL,
  `id_aset` int(10) NOT NULL,
  `kode_aset_informasi` int(11) NOT NULL,
  `kode_grup_satuan` int(10) NOT NULL,
  `id_field_info` int(10) NOT NULL,
  `isian_field_informasi` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset_informasi`
--

INSERT INTO `aset_informasi` (`id_aset_informasi`, `id_aset`, `kode_aset_informasi`, `kode_grup_satuan`, `id_field_info`, `isian_field_informasi`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(25, 10, 1, 12, 1, '10.0.91.1', '2019-03-03 00:14:26', '18', NULL, NULL, NULL),
(26, 10, 1, 12, 3, '101001', '2019-03-03 00:14:26', '18', NULL, NULL, NULL),
(27, 10, 1, 12, 4, 'io12dnklas', '2019-03-03 00:14:26', '18', NULL, NULL, NULL),
(28, 13, 2, 9, 1, '9191991', '2019-03-03 00:18:49', '18', NULL, NULL, NULL),
(29, 13, 2, 9, 3, '11111', '2019-03-03 00:18:49', '18', NULL, NULL, NULL),
(30, 13, 2, 9, 4, 'iocnsc', '2019-03-03 00:18:49', '18', NULL, NULL, NULL),
(31, 10, 3, 10, 1, 'nksal', '2019-03-03 00:19:50', '18', NULL, NULL, NULL),
(32, 10, 3, 10, 3, 'masl', '2019-03-03 00:19:50', '18', NULL, NULL, NULL),
(33, 10, 3, 10, 4, 'as', '2019-03-03 00:19:50', '18', NULL, NULL, NULL),
(34, 11, 4, 42, 1, 'Peperony', '2019-03-05 05:52:40', '18', NULL, NULL, NULL),
(35, 11, 4, 42, 3, '12312', '2019-03-05 05:52:40', '18', NULL, NULL, NULL),
(36, 11, 4, 42, 4, '12312', '2019-03-05 05:52:40', '18', NULL, NULL, NULL),
(37, 11, 5, 45, 1, '10.2.33.17', '2019-03-05 06:50:37', '18', NULL, NULL, NULL),
(38, 11, 5, 45, 3, '9090', '2019-03-05 06:50:37', '18', NULL, NULL, NULL),
(39, 11, 5, 45, 4, '90DSCojsdoij', '2019-03-05 06:50:37', '18', NULL, NULL, NULL),
(40, 11, 5, 45, 5, '', '2019-03-05 06:50:37', '18', NULL, NULL, NULL),
(41, 11, 6, 44, 1, '10.2.95.17', '2019-03-05 06:53:51', '18', NULL, NULL, NULL),
(42, 11, 6, 44, 3, '9070', '2019-03-05 06:53:51', '18', NULL, NULL, NULL),
(43, 11, 6, 44, 4, 'ihasc98aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '2019-03-05 06:53:51', '18', NULL, NULL, NULL),
(44, 11, 6, 44, 5, '', '2019-03-05 06:53:51', '18', NULL, NULL, NULL),
(45, 12, 7, 63, 1, 'asdasd', '2019-03-05 06:58:58', '18', NULL, NULL, NULL),
(46, 12, 7, 63, 3, 'asdasdasdadsa', '2019-03-05 06:58:58', '18', NULL, NULL, NULL),
(47, 12, 7, 63, 4, '23234234234', '2019-03-05 06:58:58', '18', NULL, NULL, NULL),
(48, 12, 8, 63, 1, 'asdasd', '2019-03-05 06:59:26', '18', NULL, NULL, NULL),
(49, 12, 8, 63, 3, 'asdasdasdadsa', '2019-03-05 06:59:26', '18', NULL, NULL, NULL),
(50, 12, 8, 63, 4, '23234234234', '2019-03-05 06:59:26', '18', NULL, NULL, NULL),
(51, 12, 9, 63, 1, 'asdasd', '2019-03-05 06:59:41', '18', NULL, NULL, NULL),
(52, 12, 9, 63, 3, 'asdasdasdadsa', '2019-03-05 06:59:41', '18', NULL, NULL, NULL),
(53, 12, 9, 63, 4, '23234234234', '2019-03-05 06:59:41', '18', NULL, NULL, NULL),
(54, 12, 10, 63, 1, 'asdasd', '2019-03-05 07:00:00', '18', NULL, NULL, NULL),
(55, 12, 10, 63, 3, 'asdasdasdadsa', '2019-03-05 07:00:00', '18', NULL, NULL, NULL),
(56, 12, 10, 63, 4, '23234234234', '2019-03-05 07:00:00', '18', NULL, NULL, NULL),
(57, 12, 11, 63, 1, 'asdasd', '2019-03-05 07:00:26', '18', NULL, NULL, NULL),
(58, 12, 11, 63, 3, 'asdasdasdadsa', '2019-03-05 07:00:26', '18', NULL, NULL, NULL),
(59, 12, 11, 63, 4, '23234234234', '2019-03-05 07:00:26', '18', NULL, NULL, NULL),
(60, 12, 11, 63, 5, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20190305070026 - restart tomcat antrian imigrasi.txt', '2019-03-05 07:00:26', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_keluar`
--

CREATE TABLE `aset_keluar` (
  `id_aset_keluar` int(10) UNSIGNED NOT NULL,
  `nomor_aset_keluar` int(10) UNSIGNED NOT NULL,
  `id_stok` int(10) UNSIGNED NOT NULL,
  `id_aset` int(10) UNSIGNED NOT NULL,
  `kode_kanim` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_aset_keluar` int(10) UNSIGNED NOT NULL,
  `jenis_aset_keluar` enum('Alokasi','Peminjaman') NOT NULL,
  `tanggal_aset_keluar` datetime NOT NULL,
  `lokasi_keluar_lain` varchar(100) DEFAULT NULL,
  `tanggal_aset_kembali` datetime DEFAULT NULL,
  `keterangan_aset_keluar` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset_keluar`
--

INSERT INTO `aset_keluar` (`id_aset_keluar`, `nomor_aset_keluar`, `id_stok`, `id_aset`, `kode_kanim`, `jumlah_aset_keluar`, `jenis_aset_keluar`, `tanggal_aset_keluar`, `lokasi_keluar_lain`, `tanggal_aset_kembali`, `keterangan_aset_keluar`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 0, 15, 16, 171, 12, 'Alokasi', '2018-11-11 00:00:00', NULL, NULL, 'untuk Bapak Bagus Dwi Putra, Plh. Kakanim', '2018-11-11 13:35:40', '22', NULL, NULL, NULL),
(2, 0, 17, 17, 171, 12, 'Alokasi', '2018-11-11 00:00:00', NULL, NULL, 'untuk Bapak Bagus Dwi Putra, Plh. Kakanim', '2018-11-11 13:35:40', '22', NULL, NULL, NULL),
(3, 0, 18, 15, 171, 12, 'Alokasi', '2018-11-11 00:00:00', NULL, NULL, 'untuk Bapak Bagus Dwi Putra, Plh. Kakanim', '2018-11-11 13:35:40', '22', NULL, NULL, NULL),
(4, 0, 16, 14, 171, 12, 'Alokasi', '2018-11-11 00:00:00', NULL, NULL, 'untuk Bapak Bagus Dwi Putra, Plh. Kakanim', '2018-11-11 13:35:40', '22', NULL, NULL, NULL),
(5, 1, 10, 7, 1, 2, 'Alokasi', '2018-11-11 00:00:00', NULL, NULL, 'Alokasi Kontainmen untuk Data Center Keimigrasian Pusat Lantai 2, Gedung Direktorat Jenderal Imigrasi', '2018-11-11 13:37:46', '22', NULL, NULL, NULL),
(6, 1, 9, 6, 1, 1, 'Alokasi', '2018-11-11 00:00:00', NULL, NULL, 'Alokasi Kontainmen untuk Data Center Keimigrasian Pusat Lantai 2, Gedung Direktorat Jenderal Imigrasi', '2018-11-11 13:37:46', '22', NULL, NULL, NULL),
(7, 1, 8, 5, 1, 2, 'Alokasi', '2018-11-11 00:00:00', NULL, NULL, 'Alokasi Kontainmen untuk Data Center Keimigrasian Pusat Lantai 2, Gedung Direktorat Jenderal Imigrasi', '2018-11-11 13:37:46', '22', NULL, NULL, NULL),
(8, 2, 3, 13, 109, 1, 'Alokasi', '2019-02-11 00:00:00', NULL, NULL, '-', '2019-02-11 11:27:59', '18', NULL, NULL, NULL),
(9, 2, 4, 10, 109, 3, 'Alokasi', '2019-02-11 00:00:00', NULL, NULL, '-', '2019-02-11 11:27:59', '18', NULL, NULL, NULL),
(10, 2, 5, 11, 109, 2, 'Alokasi', '2019-02-11 00:00:00', NULL, NULL, '-', '2019-02-11 11:27:59', '18', NULL, NULL, NULL),
(11, 3, 3, 13, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, 'aaaa', '2019-02-14 10:51:57', '18', NULL, NULL, NULL),
(12, 4, 3, 13, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, 'aaaa', '2019-02-14 10:52:18', '18', NULL, NULL, NULL),
(13, 5, 3, 13, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, 'aaaa', '2019-02-14 11:15:42', '18', NULL, NULL, NULL),
(14, 6, 3, 13, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, 'aaaa', '2019-02-14 11:16:12', '18', NULL, NULL, NULL),
(15, 7, 3, 13, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, 'aaa', '2019-02-14 11:17:58', '18', NULL, NULL, NULL),
(16, 8, 3, 13, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, 'aaa', '2019-02-14 11:18:36', '18', NULL, NULL, NULL),
(17, 9, 3, 13, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, 'aaa', '2019-02-14 11:19:03', '18', NULL, NULL, NULL),
(18, 10, 10, 7, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, '9888', '2019-02-14 11:22:29', '18', NULL, NULL, NULL),
(19, 11, 10, 7, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, '9888', '2019-02-14 11:23:22', '18', NULL, NULL, NULL),
(20, 12, 10, 7, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, '9888', '2019-02-14 11:24:13', '18', NULL, NULL, NULL),
(21, 13, 10, 7, 101, 1, 'Alokasi', '2019-02-14 00:00:00', NULL, NULL, '9888', '2019-02-14 11:26:46', '18', NULL, NULL, NULL),
(22, 14, 5, 11, 101, 2, 'Alokasi', '2019-02-25 00:00:00', NULL, NULL, 'AABBCC', '2019-02-25 15:32:47', '18', NULL, NULL, NULL),
(23, 15, 6, 12, 148, 3, 'Alokasi', '2019-03-21 00:00:00', NULL, NULL, 'sdsdfsdfsd', '2019-03-05 06:55:05', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_masuk`
--

CREATE TABLE `aset_masuk` (
  `id_aset_masuk` int(10) UNSIGNED NOT NULL,
  `nomor_aset_masuk` int(10) UNSIGNED NOT NULL,
  `id_stok` int(10) UNSIGNED NOT NULL,
  `id_aset` int(10) UNSIGNED NOT NULL,
  `kode_pengadaan` int(10) UNSIGNED DEFAULT NULL,
  `jumlah_aset_masuk` int(10) UNSIGNED NOT NULL,
  `tanggal_aset_masuk` datetime NOT NULL,
  `keterangan_aset_masuk` varchar(500) DEFAULT NULL,
  `harga_satuan` decimal(50,2) DEFAULT '0.00',
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset_masuk`
--

INSERT INTO `aset_masuk` (`id_aset_masuk`, `nomor_aset_masuk`, `id_stok`, `id_aset`, `kode_pengadaan`, `jumlah_aset_masuk`, `tanggal_aset_masuk`, `keterangan_aset_masuk`, `harga_satuan`, `created_at`, `created_by`) VALUES
(1, 1, 1, 8, 4, 1, '2018-11-11 12:18:01', NULL, '65000000.00', '2018-11-11 12:18:01', '22'),
(2, 1, 2, 9, 4, 6, '2018-11-11 12:18:01', NULL, '115000000.00', '2018-11-11 12:18:01', '22'),
(3, 1, 3, 13, 4, 2, '2018-11-11 12:18:01', NULL, '195000000.00', '2018-11-11 12:18:01', '22'),
(4, 1, 4, 10, 4, 32, '2018-11-11 12:18:01', NULL, '125000000.00', '2018-11-11 12:18:01', '22'),
(5, 2, 5, 11, 5, 21, '2018-11-11 12:36:22', NULL, '119000000.00', '2018-11-11 12:36:22', '22'),
(6, 2, 6, 12, 5, 1, '2018-11-11 12:36:22', NULL, '250000000.00', '2018-11-11 12:36:22', '22'),
(7, 3, 8, 5, 7, 2, '2018-11-11 12:57:45', NULL, '700000000.00', '2018-11-11 12:57:45', '22'),
(8, 3, 9, 6, 7, 1, '2018-11-11 12:57:45', NULL, '900000000.00', '2018-11-11 12:57:45', '22'),
(9, 3, 10, 7, 7, 2, '2018-11-11 12:57:45', NULL, '400000000.00', '2018-11-11 12:57:45', '22'),
(10, 4, 11, 1, 8, 1, '2018-11-11 13:07:28', NULL, '180000000.00', '2018-11-11 13:07:28', '22'),
(11, 4, 12, 2, 8, 1, '2018-11-11 13:07:28', NULL, '250000000.00', '2018-11-11 13:07:28', '22'),
(12, 4, 13, 3, 8, 1, '2018-11-11 13:07:28', NULL, '255000000000.00', '2018-11-11 13:07:28', '22'),
(13, 4, 14, 4, 8, 2, '2018-11-11 13:07:28', NULL, '495700000000.00', '2018-11-11 13:07:28', '22'),
(14, 5, 15, 16, NULL, 12, '2018-11-11 13:15:30', NULL, '9000000.00', '2018-11-11 13:15:30', '22'),
(15, 6, 16, 14, NULL, 12, '2018-11-11 13:19:32', NULL, '27000000.00', '2018-11-11 13:19:32', '22'),
(16, 7, 17, 17, NULL, 12, '2018-11-11 13:28:05', NULL, '1500000.00', '2018-11-11 13:28:05', '22'),
(17, 8, 18, 15, NULL, 12, '2018-11-11 13:30:36', NULL, '11000000.00', '2018-11-11 13:30:36', '22'),
(18, 9, 17, 17, NULL, 1, '2019-02-12 08:02:42', NULL, '2500000.00', '2019-02-12 08:02:42', '18'),
(19, 10, 2, 9, NULL, 2, '2019-02-12 08:04:32', NULL, '22000.00', '2019-02-12 08:04:32', '18'),
(20, 11, 1, 8, NULL, 1, '2019-02-12 08:07:01', NULL, '5555.00', '2019-02-12 08:07:01', '18'),
(21, 12, 1, 8, NULL, 1, '2019-02-12 08:07:13', NULL, '5555.00', '2019-02-12 08:07:13', '18'),
(22, 13, 4, 10, NULL, 1, '2019-02-12 08:08:02', NULL, '515516.00', '2019-02-12 08:08:02', '18'),
(23, 14, 4, 10, NULL, 1, '2019-02-12 08:08:11', NULL, '515516.00', '2019-02-12 08:08:11', '18'),
(24, 15, 10, 7, NULL, 2, '2019-02-12 08:13:10', NULL, '202.00', '2019-02-12 08:13:10', '18'),
(25, 16, 10, 7, NULL, 2, '2019-02-12 08:13:23', NULL, '202.00', '2019-02-12 08:13:23', '18'),
(26, 17, 3, 13, NULL, 2, '2019-02-14 08:36:06', NULL, '5000.00', '2019-02-14 08:36:06', '18'),
(27, 18, 3, 13, NULL, 4, '2019-02-14 08:38:45', NULL, '85200.00', '2019-02-14 08:38:45', '18'),
(28, 19, 6, 12, 9, 2, '2019-03-03 10:41:18', 'Aset masuk berasal dari Paket Pengadaan Sewa Jaringan Komunikasi Saluran II', '9000000.00', '2019-03-03 10:41:18', '18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_satuan`
--

CREATE TABLE `aset_satuan` (
  `id_satuan` int(10) UNSIGNED NOT NULL,
  `kode_grup_satuan` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `kode_pengadaan` int(10) UNSIGNED DEFAULT NULL,
  `id_item` int(10) UNSIGNED DEFAULT NULL,
  `id_aset` int(10) UNSIGNED DEFAULT NULL,
  `id_aset_masuk` int(10) UNSIGNED DEFAULT NULL,
  `id_aset_keluar` int(10) UNSIGNED DEFAULT NULL,
  `kode_aset_informasi` int(10) UNSIGNED DEFAULT NULL,
  `id_lokasi` int(10) UNSIGNED DEFAULT NULL,
  `id_kontainer` int(10) UNSIGNED DEFAULT NULL,
  `id_posisi_kontainer` int(10) UNSIGNED DEFAULT NULL,
  `field_aset` varchar(100) DEFAULT NULL,
  `isian_aset` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset_satuan`
--

INSERT INTO `aset_satuan` (`id_satuan`, `kode_grup_satuan`, `kode_pengadaan`, `id_item`, `id_aset`, `id_aset_masuk`, `id_aset_keluar`, `kode_aset_informasi`, `id_lokasi`, `id_kontainer`, `id_posisi_kontainer`, `field_aset`, `isian_aset`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 1, 4, 1, 8, 1, NULL, NULL, 4, NULL, NULL, 'Product ID', 'PFP1', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(2, 2, 4, 2, 9, 2, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7401', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(3, 3, 4, 2, 9, 2, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7402', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(4, 4, 4, 2, 9, 2, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7403', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(5, 5, 4, 2, 9, 2, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7404', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(6, 6, 4, 2, 9, 2, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7405', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(7, 7, 4, 2, 9, 2, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7406', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(8, 8, 4, 3, 13, 3, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPEM1000E1', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(9, 9, 4, 3, 13, 3, 8, 2, NULL, NULL, NULL, 'Product ID', 'DPEM1000E2', '2018-11-11 12:18:01', '22', '2019-03-03 00:18:49', '18', NULL),
(10, 10, 4, 4, 10, 4, 9, 3, NULL, NULL, NULL, 'Product ID', 'DPEM6401', '2018-11-11 12:18:01', '22', '2019-03-03 00:19:50', '18', NULL),
(11, 11, 4, 4, 10, 4, 9, NULL, NULL, NULL, NULL, 'Product ID', 'DPEM6402', '2018-11-11 12:18:01', '22', '2019-03-02 23:54:55', '18', NULL),
(12, 12, 4, 4, 10, 4, 9, 1, NULL, NULL, NULL, 'Product ID', 'DPEM6403', '2018-11-11 12:18:01', '22', '2019-03-03 00:14:26', '18', NULL),
(13, 13, 4, 4, 10, 4, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPEM6404', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(14, 14, 4, 4, 10, 4, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPEM6405', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(15, 15, 4, 4, 10, 4, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPEM6406', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(16, 16, 4, 4, 10, 4, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPEM6407', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(17, 17, 4, 4, 10, 4, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPEM6408', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(18, 18, 4, 4, 10, 4, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPEM6409', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(19, 19, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64010', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(20, 20, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64011', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(21, 21, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64012', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(22, 22, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64013', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(23, 23, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64014', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(24, 24, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64015', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(25, 25, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64016', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(26, 26, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64017', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(27, 27, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64018', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(28, 28, 4, 4, 10, 4, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPEM64019', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(29, 29, 4, 4, 10, 4, NULL, NULL, 3, NULL, NULL, 'Product ID', 'DPEM64020', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(30, 30, 4, 4, 10, 4, NULL, NULL, 3, NULL, NULL, 'Product ID', 'DPEM64021', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(31, 31, 4, 4, 10, 4, NULL, NULL, 3, NULL, NULL, 'Product ID', 'DPEM64022', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(32, 32, 4, 4, 10, 4, NULL, NULL, 3, NULL, NULL, 'Product ID', 'DPEM64023', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(33, 33, 4, 4, 10, 4, NULL, NULL, 3, NULL, NULL, 'Product ID', 'DPEM64024', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(34, 34, 4, 4, 10, 4, NULL, NULL, 3, NULL, NULL, 'Product ID', 'DPEM64025', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(35, 35, 4, 4, 10, 4, NULL, NULL, 3, NULL, NULL, 'Product ID', 'DPEM64026', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(36, 36, 4, 4, 10, 4, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPEM64027', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(37, 37, 4, 4, 10, 4, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPEM64028', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(38, 38, 4, 4, 10, 4, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPEM64029', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(39, 39, 4, 4, 10, 4, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPEM64030', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(40, 40, 4, 4, 10, 4, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPEM64031', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(41, 41, 4, 4, 10, 4, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPEM64032', '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(42, 42, 5, 5, 11, 5, 10, 4, NULL, NULL, NULL, 'Product ID', 'DPER7402U1', '2018-11-11 12:36:22', '22', '2019-03-05 05:52:40', '18', NULL),
(43, 43, 5, 5, 11, 5, 10, NULL, NULL, NULL, NULL, 'Product ID', 'DPER7402U2', '2018-11-11 12:36:22', '22', '2019-02-11 11:27:59', '18', NULL),
(44, 44, 5, 5, 11, 5, 22, 6, NULL, NULL, NULL, 'Product ID', 'DPER7402U3', '2018-11-11 12:36:22', '22', '2019-03-05 06:53:51', '18', NULL),
(45, 45, 5, 5, 11, 5, 22, 5, NULL, NULL, NULL, 'Product ID', 'DPER7402U4', '2018-11-11 12:36:22', '22', '2019-03-05 06:50:37', '18', NULL),
(46, 46, 5, 5, 11, 5, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7402U5', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(47, 47, 5, 5, 11, 5, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7402U6', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(48, 48, 5, 5, 11, 5, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7402U7', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(49, 49, 5, 5, 11, 5, NULL, NULL, 4, NULL, NULL, 'Product ID', 'DPER7402U8', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(50, 50, 5, 5, 11, 5, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPER7402U9', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(51, 51, 5, 5, 11, 5, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPER7402U10', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(52, 52, 5, 5, 11, 5, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPER7402U11', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(53, 53, 5, 5, 11, 5, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPER7402U12', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(54, 54, 5, 5, 11, 5, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPER7402U13', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(55, 55, 5, 5, 11, 5, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPER7402U14', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(56, 56, 5, 5, 11, 5, NULL, NULL, 2, NULL, NULL, 'Product ID', 'DPER7402U15', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(57, 57, 5, 5, 11, 5, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPER7402U16', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(58, 58, 5, 5, 11, 5, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPER7402U17', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(59, 59, 5, 5, 11, 5, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPER7402U18', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(60, 60, 5, 5, 11, 5, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPER7402U19', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(61, 61, 5, 5, 11, 5, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPER7402U20', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(62, 62, 5, 5, 11, 5, NULL, NULL, 1, NULL, NULL, 'Product ID', 'DPER7402U21', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(63, 63, 5, 6, 12, 6, 23, 11, NULL, NULL, NULL, 'Product ID', 'DPER740AG1', '2018-11-11 12:36:22', '22', '2019-03-05 07:00:26', '18', NULL),
(64, 64, 7, 8, 5, 7, 7, NULL, NULL, NULL, NULL, 'Product ID', '128037640929823761', '2018-11-11 12:57:45', '22', '2018-11-11 13:37:46', '22', NULL),
(65, 65, 7, 8, 5, 7, 7, NULL, NULL, NULL, NULL, 'Product ID', '128037640929823762', '2018-11-11 12:57:45', '22', '2018-11-11 13:37:46', '22', NULL),
(66, 66, 7, 9, 6, 8, 6, NULL, NULL, NULL, NULL, 'Product ID', '1340947259298200238', '2018-11-11 12:57:45', '22', '2018-11-11 13:37:46', '22', NULL),
(67, 67, 7, 10, 7, 9, 5, NULL, NULL, NULL, NULL, 'Product ID', '1734871999900024512', '2018-11-11 12:57:45', '22', '2018-11-11 13:37:46', '22', NULL),
(68, 68, 7, 10, 7, 9, 5, NULL, NULL, NULL, NULL, 'Product ID', '1734871999900024512', '2018-11-11 12:57:45', '22', '2018-11-11 13:37:46', '22', NULL),
(69, 69, 8, 11, 1, 10, NULL, NULL, 1, NULL, NULL, 'S/N', 'IBMQSS18920109333', '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(70, 70, 8, 12, 2, 11, NULL, NULL, 1, NULL, NULL, 'S/N', 'IBMQHA458030023001', '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(71, 71, 8, 13, 3, 12, NULL, NULL, 1, NULL, NULL, 'S/N', 'IBMQNIL7N24357710008', '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(72, 72, 8, 14, 4, 13, NULL, NULL, 4, NULL, NULL, 'S/N', 'IBMQPCIF203025405893101', '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(73, 73, 8, 14, 4, 13, NULL, NULL, NULL, NULL, NULL, 'S/N', 'IBMQPCIF203025405893102', '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(74, 74, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5451', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(75, 75, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5452', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(76, 76, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5453', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(77, 77, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5454', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(78, 78, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5455', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(79, 79, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5456', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(80, 80, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5457', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(81, 81, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5458', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(82, 82, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5459', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(83, 83, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5460', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(84, 84, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5461', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(85, 85, NULL, NULL, 16, 14, 1, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.001.5462', '2018-11-11 13:15:30', '22', '2018-11-11 13:35:40', '22', NULL),
(86, 86, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.851', '2018-11-11 13:19:32', '22', '2019-03-04 20:43:15', '18', NULL),
(87, 87, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.852', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(88, 88, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.853', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(89, 89, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.854', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(90, 90, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.855', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(91, 91, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.856', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(92, 92, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.857', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(93, 93, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.858', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(94, 94, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.859', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(95, 95, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.860', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(96, 96, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.861', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(97, 97, NULL, NULL, 14, 15, 4, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.02.03.004.862', '2018-11-11 13:19:32', '22', '2018-11-11 13:35:40', '22', NULL),
(98, 98, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5451', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(99, 99, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5452', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(100, 100, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5453', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(101, 101, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5454', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(102, 102, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5455', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(103, 103, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5456', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(104, 104, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5457', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(105, 105, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5458', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(106, 106, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5459', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(107, 107, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5460', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(108, 108, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5461', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(109, 109, NULL, NULL, 17, 16, 2, NULL, NULL, NULL, NULL, 'No. BMN', '3.10.01.02.003.5462', '2018-11-11 13:28:05', '22', '2018-11-11 13:35:40', '22', NULL),
(110, 110, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1551', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(111, 111, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1552', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(112, 112, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1553', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(113, 113, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1554', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(114, 114, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1555', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(115, 115, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1556', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(116, 116, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1557', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(117, 117, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1558', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(118, 118, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1559', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(119, 119, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1560', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(120, 120, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1561', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(121, 121, NULL, NULL, 15, 17, 3, NULL, NULL, NULL, NULL, 'No. BMN', '3.06.01.01.048.1562', '2018-11-11 13:30:36', '22', '2018-11-11 13:35:40', '22', NULL),
(122, 122, NULL, NULL, 9, 19, NULL, NULL, NULL, NULL, NULL, 'UID', 'q3oijwelknois894', '2019-02-12 08:04:32', '18', NULL, NULL, NULL),
(123, 122, NULL, NULL, 9, 19, NULL, NULL, NULL, NULL, NULL, 'PID', 'oaj89iok', '2019-02-12 08:04:32', '18', NULL, NULL, NULL),
(124, 122, NULL, NULL, 9, 19, NULL, NULL, NULL, NULL, NULL, 'XID', 'AJSIOUCH LS', '2019-02-12 08:04:32', '18', NULL, NULL, NULL),
(126, 123, NULL, NULL, 9, 19, NULL, NULL, NULL, NULL, NULL, 'BMN', 'sjkdicuhijnk', '2019-02-12 08:04:32', '18', NULL, NULL, NULL),
(127, 123, NULL, NULL, 9, 19, NULL, NULL, NULL, NULL, NULL, 'SN', 'nod89hacslkjn m', '2019-02-12 08:04:32', '18', NULL, NULL, NULL),
(129, 124, NULL, NULL, 8, 20, NULL, NULL, NULL, NULL, NULL, 'OPID', 'YGT7HBJW NM', '2019-02-12 08:07:01', '18', NULL, NULL, NULL),
(131, 125, NULL, NULL, 8, 21, NULL, NULL, NULL, NULL, NULL, 'OPID', 'YGT7HBJW NM', '2019-02-12 08:07:13', '18', NULL, NULL, NULL),
(133, 126, NULL, NULL, 10, 22, NULL, NULL, NULL, NULL, NULL, 'ICON', '365', '2019-02-12 08:08:02', '18', NULL, NULL, NULL),
(134, 126, NULL, NULL, 10, 22, NULL, NULL, NULL, NULL, NULL, 'SERIAL', 'OIASCIJKN', '2019-02-12 08:08:02', '18', NULL, NULL, NULL),
(136, 127, NULL, NULL, 10, 23, NULL, NULL, NULL, NULL, NULL, 'ICON', '365', '2019-02-12 08:08:11', '18', NULL, NULL, NULL),
(137, 127, NULL, NULL, 10, 23, NULL, NULL, NULL, NULL, NULL, 'SERIAL', 'OIASCIJKN', '2019-02-12 08:08:11', '18', NULL, NULL, NULL),
(139, 128, NULL, NULL, 7, 25, NULL, NULL, NULL, NULL, NULL, 'PRODUCT ID', 'aosm', '2019-02-12 08:13:23', '18', NULL, NULL, NULL),
(140, 128, NULL, NULL, 7, 25, NULL, NULL, NULL, NULL, NULL, 'SERIAL ID', 'ausu', '2019-02-12 08:13:23', '18', NULL, NULL, NULL),
(141, 129, NULL, NULL, 7, 25, 21, NULL, NULL, NULL, NULL, 'AAA', 'AAA#', '2019-02-12 08:13:23', '18', '2019-02-14 11:26:46', '18', NULL),
(142, 129, NULL, NULL, 7, 25, 21, NULL, NULL, NULL, NULL, 'BBB', 'BBB#', '2019-02-12 08:13:23', '18', '2019-02-14 11:26:46', '18', NULL),
(143, 130, NULL, NULL, 13, 26, NULL, NULL, 4, 3, 10, 'BMN Number', 'ANC9A98CO', '2019-02-14 08:36:06', '18', NULL, NULL, NULL),
(144, 131, NULL, NULL, 13, 26, NULL, NULL, 1, 1, 2, 'NOMOR BMN', '63472UYIOP', '2019-02-14 08:36:06', '18', NULL, NULL, NULL),
(145, 131, NULL, NULL, 13, 26, NULL, NULL, 1, 1, 2, 'PRODUCT ID', 'LKASCIUH', '2019-02-14 08:36:06', '18', NULL, NULL, NULL),
(146, 132, NULL, NULL, 13, 27, NULL, NULL, 1, 1, 1, 'BIBI', 'ASDASDASDASD', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(147, 132, NULL, NULL, 13, 27, NULL, NULL, 1, 1, 1, 'BMNNN', 'ZZZ', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(148, 133, NULL, NULL, 13, 27, NULL, NULL, 2, 5, 4, 'CICI', 'ASCASCAS ASC', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(149, 133, NULL, NULL, 13, 27, NULL, NULL, 2, 5, 4, 'BMNMM', '323', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(150, 134, NULL, NULL, 13, 27, NULL, NULL, 3, 10, 12, 'BMNAIUS', 'ADASDASD', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(151, 135, NULL, NULL, 13, 27, NULL, NULL, 2, 5, 6, 'PRODID', 'A66666', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(152, 135, NULL, NULL, 13, 27, NULL, NULL, 2, 5, 6, 'S/N', '4ASD4', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(153, 135, NULL, NULL, 13, 27, NULL, NULL, 2, 5, 6, 'CID', '4AS', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(154, 135, NULL, NULL, 13, 27, NULL, NULL, 2, 5, 6, 'RFID', 'ASDASDASDAS', '2019-02-14 08:38:45', '18', NULL, NULL, NULL),
(155, 136, 9, 15, 12, 28, 23, NULL, NULL, NULL, NULL, 'BMN', '1123456789', '2019-03-03 10:41:18', '18', '2019-03-05 06:55:05', '18', NULL),
(156, 136, 9, 15, 12, 28, 23, NULL, NULL, NULL, NULL, 'PID', '2234567890', '2019-03-03 10:41:18', '18', '2019-03-05 06:55:05', '18', NULL),
(157, 137, 9, 15, 12, 28, 23, NULL, NULL, NULL, NULL, 'BMN', '334567890', '2019-03-03 10:41:18', '18', '2019-03-05 06:55:05', '18', NULL),
(158, 137, 9, 15, 12, 28, 23, NULL, NULL, NULL, NULL, 'BMN', '44567890', '2019-03-03 10:41:18', '18', '2019-03-05 06:55:05', '18', NULL),
(159, 137, 9, 15, 12, 28, 23, NULL, NULL, NULL, NULL, 'BMN', '556789012', '2019-03-03 10:41:18', '18', '2019-03-05 06:55:05', '18', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_satuan_log`
--

CREATE TABLE `aset_satuan_log` (
  `id_log` int(100) NOT NULL,
  `id_satuan` int(10) NOT NULL,
  `log` text NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_spesifikasi`
--

CREATE TABLE `aset_spesifikasi` (
  `id_spesifikasi` int(10) NOT NULL,
  `id_aset` int(10) NOT NULL,
  `id_field` int(10) NOT NULL,
  `isian_field` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset_spesifikasi`
--

INSERT INTO `aset_spesifikasi` (`id_spesifikasi`, `id_aset`, `id_field`, `isian_field`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 1, 25, '1600', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(2, 1, 26, '128 GB', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(3, 1, 27, '72 TB', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(4, 1, 28, '4 x 10/100/1000 Base-T Ethernet, 2 x 10 Gbps SFP, 2 x 10G SR SFP Transceiver Module', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(5, 1, 29, '15000', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(6, 1, 30, '300000', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(7, 2, 25, '1600', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(8, 2, 26, '128 GB', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(9, 2, 27, '72 TB', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(10, 2, 28, '4 x 10/100/1000 Base-T Ethernet, 2 x 10 Gbps SFP, 2 x 10G SR SFP Transceiver Module', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(11, 2, 29, '15000', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(12, 2, 30, '300000', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(13, 3, 25, '-', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(14, 3, 26, '64 GB', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(15, 3, 27, '400 GB SSD (2 x 200 GB SSD)', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(16, 3, 28, '4 x 1 Gbps SFP, 2 x 1G SX SFP Transceiver Module, 2 x 1G TX SFP Transceiver Module', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(17, 3, 29, '-', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(18, 3, 30, '200000', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(19, 4, 25, '-', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(20, 4, 26, '128 GB', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(21, 4, 27, '72 TB (12 x 6 TB)', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(22, 4, 28, '3 x 10/100/1000 Base-T Ethernet, 4 x 10 Gbps SFP, 4 x 10G SR SFP Transceiver Module / 4 x 1G SX SFP Transceiver Module', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(23, 4, 29, '-', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(24, 4, 30, '-', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(25, 5, 31, '6 Racks', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(26, 5, 32, '6 Units, SSG 42U/45U, SBRSUHPANELVRT2-42/45', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(27, 5, 33, '6 Units, 600mm wide cabinet set of 2 pcs, SURBRUSH600', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(28, 5, 34, '120 units, 1U-Toolless Mounting Plastic Blank Panel, SP1UPANEL', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(29, 5, 35, '6 units, 42U-SSG, SCABLEMNVRT42-SSG', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(30, 5, 36, '6 Units, Power Shielding Trough for Width 600mm Rack, SMCABLETRAY600', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(31, 5, 37, '6 units, Data Shielding Partition for W600mm Rack, set of 2 pcs, SNCABLETRAY600', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(32, 5, 38, '1 unit, 2000mm height motorized sliding door for 1200mm width aisle (two ends), 02100080', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(33, 5, 39, '2 units, 300mm width fixed  roof plate for 1200mm width aisle, 02100082', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(34, 5, 40, '6 units, 600mm width openable roof plate for 1200mm width aisle, 02100085', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(35, 5, 41, '1 unit, RDUMI-RDU-M-I, 02311603', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(36, 5, 42, '1 unit, 4COM Extension Card, 02311676', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(37, 5, 43, '1 unit, SmartAisle2 RDU Management Package (0210010)', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(38, 5, 44, '1 unit,  IRS-SAB-CB Open Controller', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(39, 5, 45, '1 unit, RDU-A G2, 02311675', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(40, 5, 46, '1 unit, 8DOAO Extension Card, 02311678', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(41, 5, 47, '2 units, 12VDC/70mA/RJ45 10-60 degress of Celcius, 33010571', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(42, 5, 48, '24 VDC/RJ45/12m 10-50 degrees of Celcius, 33040014', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(43, 5, 49, '12V-24VDC / Normal Low / 10m, 10-60 degrees of Celcius, RDU-A-S01W 10m, 33010328', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(44, 5, 50, 'Black, Normal Close, RJ45 - 500mA / 40-70mm / 10-51 degrees of Celcius, 33010469', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(45, 5, 51, '2 units, IRS-CF-CS', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(46, 5, 52, '2 units, IRS-CF-CC', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(47, 5, 53, '1 unit, IRS-SA-SW', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(48, 5, 54, 'CHD601S, 02070044', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(49, 5, 55, '1 unit, 02070060', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(50, 5, 56, '10 units, RFID01A ID Card, 02070050', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(51, 6, 31, '12 Racks', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(52, 6, 32, '12 units, SSG 42U/45U set of 2 pcs, SBRUSHPANELVRT2-42/45', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(53, 6, 33, '12 units, for 600mm wide cabinet, set of 2 pcs, SURBRUSH600', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(54, 6, 34, '120 units, 1U-Toolless Mounting Plastic Blank Panel, SP1UPANEL', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(55, 6, 35, '12 units, 42U-SSG, SCABLEMNVRT42-SSG', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(56, 6, 36, '12 Units, Power Shielding Trough for Width 600mm Rack, SMCABLETRAY600', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(57, 6, 37, '12 units, Data Shielding Partition for W600mm Rack, set of 2 pcs, SNCABLETRAY600', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(58, 6, 38, '1 unit, 2000mm height motorized sliding door for 1200mm width aisle (two ends), 02100080', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(59, 6, 39, '4 units, 300mm width fixed  roof plate for 1200mm width aisle, 02100082', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(60, 6, 40, '6 units, 600mm width openable roof plate for 1200mm width aisle, 02100085', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(61, 6, 41, '1 unit, RDUMI-RDU-M-I, 02311603', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(62, 6, 42, '1 unit, 4COM Extension Card, 02311676', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(63, 6, 43, '1 unit, SmartAisle2 RDU Management Package (0210010)', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(64, 6, 44, '1 unit,  IRS-SAB-CB Open Controller', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(65, 6, 45, '1 unit, RDU-A G2, 02311675', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(66, 6, 46, '1 unit, 8DOAO Extension Card, 02311678', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(67, 6, 47, '2 units, 12VDC/70mA/RJ45 10-60 degress of Celcius, 33010571', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(68, 6, 48, '24 VDC/RJ45/12m 10-50 degrees of Celcius, 33040014', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(69, 6, 49, '12V-24VDC / Normal Low / 10m, 10-60 degrees of Celcius, RDU-A-S01W 10m, 33010328', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(70, 6, 50, 'Black, Normal Close, RJ45 - 500mA / 40-70mm / 10-51 degrees of Celcius, 33010469', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(71, 6, 51, '2 units, IRS-CF-CS', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(72, 6, 52, '2 units, IRS-CF-CC', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(73, 6, 53, '1 unit, IRS-SA-SW', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(74, 6, 54, 'CHD601S, 02070044', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(75, 6, 55, '1 unit, 02070060', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(76, 6, 56, '10 units, RFID01A ID Card, 02070050', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(77, 7, 31, '5 Rack', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(78, 7, 32, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(79, 7, 33, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(80, 7, 34, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(81, 7, 35, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(82, 7, 36, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(83, 7, 37, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(84, 7, 38, 'F Series II Rack 42 U 1290 C/W Perforated doors with lock and key, split side panels, EIA mounting rails, leveling feet, black in color, Curved front door', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(85, 7, 39, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(86, 7, 40, '2 x 800  x 1200 x 250 open roof (oversea)', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(87, 7, 41, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(88, 7, 42, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(89, 7, 43, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(90, 7, 44, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(91, 7, 45, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(92, 7, 46, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(93, 7, 47, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(94, 7, 48, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(95, 7, 49, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(96, 7, 50, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(97, 7, 51, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(98, 7, 52, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(99, 7, 53, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(100, 7, 54, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(101, 7, 55, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(102, 7, 56, '-', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(103, 8, 1, 'Intel Xeon E5-2630', '2018-11-11 09:01:06', '22', NULL, NULL, NULL),
(104, 8, 2, '16 GB, 2 x 300 GB', '2018-11-11 09:01:06', '22', NULL, NULL, NULL),
(105, 8, 3, '2.4 GHz processor with 8 cores', '2018-11-11 09:01:06', '22', NULL, NULL, NULL),
(106, 8, 4, 'Red Hat Enterprise Linux (RHEL)', '2018-11-11 09:01:06', '22', NULL, NULL, NULL),
(107, 8, 5, 'iDRAC', '2018-11-11 09:01:06', '22', NULL, NULL, NULL),
(108, 9, 1, 'Intel Xeon Scalable Process, 2 x Processor Intel Xeon Gold 6154', '2018-11-11 09:16:06', '22', NULL, NULL, NULL),
(109, 9, 2, '3 TB', '2018-11-11 09:16:06', '22', NULL, NULL, NULL),
(110, 9, 3, '4 x 32GB Memory', '2018-11-11 09:16:06', '22', NULL, NULL, NULL),
(111, 9, 4, 'Red Hat Enterprise Linux (RHEL)', '2018-11-11 09:16:06', '22', NULL, NULL, NULL),
(112, 9, 5, 'Quadrant Leader Gartner Modular Server 2016', '2018-11-11 09:16:06', '22', NULL, NULL, NULL),
(113, 10, 1, 'Intel Xeon Scalable Process, 2 x Processor Intel Xeon Gold 5118 2.3 G, 12C', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(114, 10, 2, '16 Slot Memory, hingga 2 TB', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(115, 10, 3, '8 x32 GB RDIMM Memory', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(116, 10, 4, 'Ubuntu, Windows, RHEL, Virtual Machine', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(117, 10, 5, 'Quadrant Leader Gartner Modular Server 2016', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(118, 11, 1, 'Intel Xeon Scalable Process, 2 x Processor Intel Xeon Gold 6154', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(119, 11, 2, '3 TB', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(120, 11, 3, '4 x 32GB Memory', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(121, 11, 4, 'Ubuntu, Windows, RHEL, Virtual Machine', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(122, 11, 5, 'Quadrant Leader Gartner Modular Server 2016', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(123, 12, 1, 'Intel Xeon Scalable Process, 2 x Processor Intel Xeon Gold 6154', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(124, 12, 2, '6 x 1.2 TB 10k RPM SAS 12Gbps 2.5in', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(125, 12, 3, '4 x 32GB Memory', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(126, 12, 4, 'Ubuntu, Windows, RHEL, Virtual Machine', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(127, 12, 5, 'Quadrant Leader Gartner Modular Server 2016', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(128, 13, 1, 'Intel Processor', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(129, 13, 2, '-', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(130, 13, 3, '-', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(131, 13, 4, '-', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(132, 13, 5, 'Quadrant Leader Gartner Modular Server 2016', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(133, 14, 20, '162 (w) x 190 (d) x 157 (h) mm', '2018-11-11 09:55:17', '22', NULL, NULL, NULL),
(134, 14, 21, '1 paspor / 3 detik', '2018-11-11 09:55:17', '22', NULL, NULL, NULL),
(135, 15, 57, '1100 KwH', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(136, 15, 58, '500 Watts', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(137, 15, 59, '6 Outlets', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(138, 15, 60, 'up to 73 minutes saving', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(139, 15, 61, '2 years', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(140, 15, 62, 'AVR', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(141, 16, 6, '4 GB DDR4-2133 DIMM (1x4GB) RAM', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(142, 16, 7, '500 GB 7200 RPM SATA 6G 3.5 HDD', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(143, 16, 8, 'Intel Inside Core i7 SFF Label', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(144, 16, 9, 'Intel Core i7-6700 3.4G 8M 2133 4C CPU', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(145, 17, 63, '18.5 inch', '2018-11-11 11:30:05', '22', NULL, NULL, NULL),
(146, 17, 64, '100 Watt', '2018-11-11 11:30:05', '22', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_stok`
--

CREATE TABLE `aset_stok` (
  `id_stok` int(10) UNSIGNED NOT NULL,
  `id_aset` int(10) UNSIGNED NOT NULL,
  `jumlah_stok` int(10) UNSIGNED NOT NULL,
  `tanggal_stok` datetime NOT NULL,
  `satuan_stok` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset_stok`
--

INSERT INTO `aset_stok` (`id_stok`, `id_aset`, `jumlah_stok`, `tanggal_stok`, `satuan_stok`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 8, 3, '2019-02-12 08:07:13', 'paket', '2019-02-12 08:07:13', '22', '2019-02-12 08:07:13', '18'),
(2, 9, 8, '2019-02-12 08:04:32', 'unit', '2019-02-12 08:04:32', '22', '2019-02-12 08:04:32', '18'),
(3, 13, 7, '2019-02-14 11:19:03', 'unit', '2019-03-01 10:08:23', '22', '2019-02-14 11:19:03', '18'),
(4, 10, 31, '2019-02-12 08:08:11', 'unit', '2019-02-12 08:08:11', '22', '2019-02-12 08:08:11', '18'),
(5, 11, 17, '2019-02-25 15:32:47', 'unit', '2019-02-25 15:32:48', '22', '2019-02-25 15:32:47', '18'),
(6, 12, 0, '2019-03-05 06:55:05', 'unit', '2019-03-05 06:55:05', '22', '2019-03-05 06:55:05', '18'),
(8, 5, 0, '2018-11-11 13:37:46', 'set', '2018-11-11 13:37:46', '22', '2018-11-11 13:37:46', '22'),
(9, 6, 0, '2018-11-11 13:37:46', 'set', '2018-11-11 13:37:46', '22', '2018-11-11 13:37:46', '22'),
(10, 7, 0, '2019-02-14 11:26:46', 'set', '2019-02-14 11:26:46', '22', '2019-02-14 11:26:46', '18'),
(11, 1, 1, '2018-11-11 13:07:28', 'Unit', '2018-11-11 13:07:28', '22', NULL, NULL),
(12, 2, 1, '2018-11-11 13:07:28', 'unit', '2018-11-11 13:07:28', '22', NULL, NULL),
(13, 3, 1, '2018-11-11 13:07:28', 'unit', '2018-11-11 13:07:28', '22', NULL, NULL),
(14, 4, 2, '2018-11-11 13:07:28', 'unit', '2018-11-11 13:07:28', '22', NULL, NULL),
(15, 16, 0, '2018-11-11 13:35:40', 'unit', '2018-11-11 13:35:40', '22', '2018-11-11 13:35:40', '22'),
(16, 14, 0, '2018-11-11 13:35:40', 'unit', '2018-11-11 13:35:40', '22', '2018-11-11 13:35:40', '22'),
(17, 17, 1, '2019-02-12 08:02:42', 'unit', '2019-02-12 08:02:42', '22', '2019-02-12 08:02:42', '18'),
(18, 15, 0, '2018-11-11 13:35:40', 'unit', '2018-11-11 13:35:40', '22', '2018-11-11 13:35:40', '22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `aset_stok_riwayat`
--

CREATE TABLE `aset_stok_riwayat` (
  `id_riwayat` int(10) NOT NULL,
  `id_stok` int(10) NOT NULL,
  `id_aset` int(10) NOT NULL,
  `jumlah_stok` int(10) NOT NULL,
  `tanggal_stok` datetime NOT NULL,
  `keterangan_stok` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aset_stok_riwayat`
--

INSERT INTO `aset_stok_riwayat` (`id_riwayat`, `id_stok`, `id_aset`, `jumlah_stok`, `tanggal_stok`, `keterangan_stok`, `created_at`, `created_by`) VALUES
(1, 1, 8, 1, '2018-11-11 12:18:01', NULL, '2018-11-11 12:18:01', '22'),
(2, 2, 9, 6, '2018-11-11 12:18:01', NULL, '2018-11-11 12:18:01', '22'),
(3, 3, 13, 2, '2018-11-11 12:18:01', NULL, '2018-11-11 12:18:01', '22'),
(4, 4, 10, 32, '2018-11-11 12:18:01', NULL, '2018-11-11 12:18:01', '22'),
(5, 5, 11, 21, '2018-11-11 12:36:22', NULL, '2018-11-11 12:36:22', '22'),
(6, 6, 12, 1, '2018-11-11 12:36:22', NULL, '2018-11-11 12:36:22', '22'),
(8, 8, 5, 2, '2018-11-11 12:57:45', NULL, '2018-11-11 12:57:45', '22'),
(9, 9, 6, 1, '2018-11-11 12:57:45', NULL, '2018-11-11 12:57:45', '22'),
(10, 10, 7, 2, '2018-11-11 12:57:45', NULL, '2018-11-11 12:57:45', '22'),
(11, 11, 1, 1, '2018-11-11 13:07:28', NULL, '2018-11-11 13:07:28', '22'),
(12, 12, 2, 1, '2018-11-11 13:07:28', NULL, '2018-11-11 13:07:28', '22'),
(13, 13, 3, 1, '2018-11-11 13:07:28', NULL, '2018-11-11 13:07:28', '22'),
(14, 14, 4, 2, '2018-11-11 13:07:28', NULL, '2018-11-11 13:07:28', '22'),
(15, 15, 16, 12, '2018-11-11 13:15:30', NULL, '2018-11-11 13:15:30', '22'),
(16, 16, 14, 12, '2018-11-11 13:19:32', NULL, '2018-11-11 13:19:32', '22'),
(17, 17, 17, 12, '2018-11-11 13:28:05', NULL, '2018-11-11 13:28:05', '22'),
(18, 18, 15, 12, '2018-11-11 13:30:36', NULL, '2018-11-11 13:30:36', '22'),
(19, 15, 16, 0, '2018-11-11 13:35:40', NULL, '2018-11-11 13:35:40', '22'),
(20, 17, 17, 0, '2018-11-11 13:35:40', NULL, '2018-11-11 13:35:40', '22'),
(21, 18, 15, 0, '2018-11-11 13:35:40', NULL, '2018-11-11 13:35:40', '22'),
(22, 16, 14, 0, '2018-11-11 13:35:40', NULL, '2018-11-11 13:35:40', '22'),
(23, 10, 7, 0, '2018-11-11 13:37:46', NULL, '2018-11-11 13:37:46', '22'),
(24, 9, 6, 0, '2018-11-11 13:37:46', NULL, '2018-11-11 13:37:46', '22'),
(25, 8, 5, 0, '2018-11-11 13:37:46', NULL, '2018-11-11 13:37:46', '22'),
(26, 3, 13, 1, '2019-02-11 11:27:59', NULL, '2019-02-11 11:27:59', '18'),
(27, 4, 10, 29, '2019-02-11 11:27:59', NULL, '2019-02-11 11:27:59', '18'),
(28, 5, 11, 19, '2019-02-11 11:27:59', NULL, '2019-02-11 11:27:59', '18'),
(29, 17, 17, 1, '2019-02-12 08:02:42', NULL, '2019-02-12 08:02:42', '18'),
(30, 2, 9, 8, '2019-02-12 08:04:32', NULL, '2019-02-12 08:04:32', '18'),
(31, 1, 8, 2, '2019-02-12 08:07:01', NULL, '2019-02-12 08:07:01', '18'),
(32, 1, 8, 3, '2019-02-12 08:07:13', NULL, '2019-02-12 08:07:13', '18'),
(33, 4, 10, 30, '2019-02-12 08:08:02', NULL, '2019-02-12 08:08:02', '18'),
(34, 4, 10, 31, '2019-02-12 08:08:11', NULL, '2019-02-12 08:08:11', '18'),
(35, 10, 7, 2, '2019-02-12 08:13:10', NULL, '2019-02-12 08:13:10', '18'),
(36, 10, 7, 4, '2019-02-12 08:13:23', NULL, '2019-02-12 08:13:23', '18'),
(37, 3, 13, 3, '2019-02-14 08:36:06', NULL, '2019-02-14 08:36:06', '18'),
(38, 3, 13, 7, '2019-02-14 08:38:45', NULL, '2019-02-14 08:38:45', '18'),
(39, 3, 13, 6, '2019-02-14 10:51:57', NULL, '2019-02-14 10:51:57', '18'),
(40, 3, 13, 5, '2019-02-14 10:52:18', NULL, '2019-02-14 10:52:18', '18'),
(41, 3, 13, 4, '2019-02-14 11:15:42', NULL, '2019-02-14 11:15:42', '18'),
(42, 3, 13, 3, '2019-02-14 11:16:12', NULL, '2019-02-14 11:16:12', '18'),
(43, 3, 13, 2, '2019-02-14 11:17:58', NULL, '2019-02-14 11:17:58', '18'),
(44, 3, 13, 1, '2019-02-14 11:18:36', NULL, '2019-02-14 11:18:36', '18'),
(45, 3, 13, 0, '2019-02-14 11:19:03', NULL, '2019-02-14 11:19:03', '18'),
(46, 10, 7, 3, '2019-02-14 11:22:29', NULL, '2019-02-14 11:22:29', '18'),
(47, 10, 7, 2, '2019-02-14 11:23:22', NULL, '2019-02-14 11:23:22', '18'),
(48, 10, 7, 1, '2019-02-14 11:24:13', NULL, '2019-02-14 11:24:13', '18'),
(49, 10, 7, 0, '2019-02-14 11:26:46', NULL, '2019-02-14 11:26:46', '18'),
(50, 5, 11, 17, '2019-02-25 15:32:47', NULL, '2019-02-25 15:32:48', '18'),
(51, 6, 12, 3, '2019-03-03 10:41:18', NULL, '2019-03-03 10:41:18', '18'),
(52, 6, 12, 0, '2019-03-05 06:55:05', NULL, '2019-03-05 06:55:05', '18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokumentasi`
--

CREATE TABLE `dokumentasi` (
  `id_dokumentasi` int(10) UNSIGNED NOT NULL,
  `src_tabel_dokumentasi` varchar(100) DEFAULT '0',
  `src_kolom_dokumentasi` varchar(100) DEFAULT '0',
  `src_fk_dokumentasi` int(10) DEFAULT '0',
  `id_lokasi` int(10) UNSIGNED DEFAULT NULL,
  `id_kontainer` int(10) UNSIGNED DEFAULT NULL,
  `id_posisi_kontainer` int(10) UNSIGNED DEFAULT NULL,
  `nama_dokumentasi` varchar(200) DEFAULT NULL,
  `tanggal_dokumentasi` datetime DEFAULT NULL,
  `keterangan_dokumentasi` text,
  `ukuran_dokumentasi` int(10) UNSIGNED DEFAULT NULL,
  `link_dokumentasi` varchar(200) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokumentasi`
--

INSERT INTO `dokumentasi` (`id_dokumentasi`, `src_tabel_dokumentasi`, `src_kolom_dokumentasi`, `src_fk_dokumentasi`, `id_lokasi`, `id_kontainer`, `id_posisi_kontainer`, `nama_dokumentasi`, `tanggal_dokumentasi`, `keterangan_dokumentasi`, `ukuran_dokumentasi`, `link_dokumentasi`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'aset', 'id_aset', 17, 4, 3, 10, 'User Manual Book', '2018-11-11 00:00:00', 'User Manual Book', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111113005 - surat_418613.pdf', '2018-11-11 11:30:05', '22', NULL, NULL),
(10, 'pengadaan', 'kode_pengadaan', 4, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 1', '2018-11-12 00:00:00', 'Dokumen Spektek 1', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111121801 - 6_7-PDF_SSKK 2.pdf', '2018-11-11 12:18:01', '22', NULL, NULL),
(11, 'pengadaan', 'kode_pengadaan', 4, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 2', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 2', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111121801 - 7_7-PDF_SSKK 2.pdf', '2018-11-11 12:18:01', '22', NULL, NULL),
(12, 'pengadaan', 'kode_pengadaan', 4, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 3', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 3', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111121801 - 8_7-PDF_SSKK 2.pdf', '2018-11-11 12:18:01', '22', NULL, NULL),
(13, 'pengadaan', 'kode_pengadaan', 4, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 4', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 4', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111121801 - 9_7-PDF_SSKK 2.pdf', '2018-11-11 12:18:01', '22', NULL, NULL),
(14, 'pengadaan', 'kode_pengadaan', 5, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 1', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 1', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111123622 - 6_7-PDF_SSKK 2.pdf', '2018-11-11 12:36:22', '22', NULL, NULL),
(15, 'pengadaan', 'kode_pengadaan', 5, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 2', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 2', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111123622 - 7_7-PDF_SSKK 2.pdf', '2018-11-11 12:36:22', '22', NULL, NULL),
(16, 'pengadaan', 'kode_pengadaan', 5, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 3', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 3', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111123622 - 8_7-PDF_SSKK 2.pdf', '2018-11-11 12:36:22', '22', NULL, NULL),
(17, 'pengadaan', 'kode_pengadaan', 5, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 4', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 4', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111123622 - 9_7-PDF_SSKK 2.pdf', '2018-11-11 12:36:22', '22', NULL, NULL),
(23, 'pengadaan', 'kode_pengadaan', 7, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 1', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 1', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111125745 - 11_7-PDF_SSKK 1.pdf', '2018-11-11 12:57:45', '22', NULL, NULL),
(24, 'pengadaan', 'kode_pengadaan', 7, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 2', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 2', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111125745 - 12_7-PDF_SSKK 1.pdf', '2018-11-11 12:57:45', '22', NULL, NULL),
(25, 'pengadaan', 'kode_pengadaan', 7, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 3', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 3', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111125745 - 13_7-PDF_SSKK 1.pdf', '2018-11-11 12:57:45', '22', NULL, NULL),
(26, 'pengadaan', 'kode_pengadaan', 7, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 4', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 4', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111125745 - 14_7-PDF_SSKK 1.pdf', '2018-11-11 12:57:45', '22', NULL, NULL),
(27, 'pengadaan', 'kode_pengadaan', 7, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis 5', '2018-11-11 00:00:00', 'Dokumen Spesifikasi Teknis 5', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111125745 - 15_7-PDF_SSKK 1.pdf', '2018-11-11 12:57:45', '22', NULL, NULL),
(28, 'pengadaan', 'kode_pengadaan', 8, NULL, NULL, NULL, 'Dokumen Detail Jenis Barang / Jasa 1', '2018-11-11 00:00:00', 'Dokumen Detail Jenis Barang / Jasa 1', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111130728 - 9_7-PDF_SSKK.pdf', '2018-11-11 13:07:28', '22', NULL, NULL),
(29, 'pengadaan', 'kode_pengadaan', 8, NULL, NULL, NULL, 'Dokumen Detail Jenis Barang / Jasa 2', '2018-11-11 00:00:00', 'Dokumen Detail Jenis Barang / Jasa 2', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111130728 - 10_7-PDF_SSKK.pdf', '2018-11-11 13:07:28', '22', NULL, NULL),
(30, 'pengadaan', 'kode_pengadaan', 8, NULL, NULL, NULL, 'Dokumen Detail Jenis Barang / Jasa 3', '2018-11-11 00:00:00', 'Dokumen Detail Jenis Barang / Jasa 3', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111130728 - 11_7-PDF_SSKK.pdf', '2018-11-11 13:07:28', '22', NULL, NULL),
(31, 'aset_keluar', 'nomor_aset_keluar', 0, NULL, NULL, NULL, 'Dokumen BA', '2018-11-11 00:00:00', 'Dokumen Berita Acara Aset Keluar untuk dialokasikan ke Kanim Kelas II Atambua', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111133540 - Dokumen BA.pdf', '2018-11-11 13:35:40', '22', NULL, NULL),
(32, 'aset_keluar', 'nomor_aset_keluar', 1, NULL, NULL, NULL, 'BA', '2018-11-11 00:00:00', 'Dokumen Berita Acara', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111133746 - Dokumen BA.pdf', '2018-11-11 13:37:46', '22', NULL, NULL),
(33, 'aplikasi', 'id_aplikasi', 1, 4, 3, 10, 'Dokumen User Manual', NULL, 'User Manual', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111135641 - sample-doc.pdf', '2018-11-11 13:56:41', '18', NULL, NULL),
(34, 'aplikasi', 'id_aplikasi', 2, NULL, NULL, NULL, 'Dokumen Spesifikasi Teknis', '2018-04-16 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111180333 - Technical Design Document new.docx', '2018-11-11 18:03:33', '18', NULL, NULL),
(35, 'aplikasi', 'id_aplikasi', 2, NULL, NULL, NULL, 'User Manual', '2018-08-28 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111180420 - WHV USER MANUAL.docx', '2018-11-11 18:04:20', '18', NULL, NULL),
(36, 'aplikasi', 'id_aplikasi', 2, NULL, NULL, NULL, 'SOP Pemeliharaan Aplikasi', '2018-08-28 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111180452 - SOP Pemeliharaan.docx', '2018-11-11 18:04:52', '18', NULL, NULL),
(37, 'aplikasi', 'id_aplikasi', 2, NULL, NULL, NULL, 'Laporan Kegiatan Wawancara', '2018-08-28 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111180552 - WHV - Laporan Kegiatan.pdf', '2018-11-11 18:05:52', '18', NULL, NULL),
(38, 'aplikasi_versi', 'id_versi', 4, 4, 3, 10, 'Dokumen Inital', NULL, '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111182111 - 0. MISTIK_Dokumen_Blueprint_Spesifikasi_Kebutuhan_Aplikasi.pdf', '2018-11-11 18:21:11', '18', NULL, NULL),
(39, 'aplikasi', 'id_aplikasi', 6, NULL, NULL, NULL, 'Blue Print Spesifikasi Teknis Aplikasi', '2018-08-03 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111182139 - 0. MISTIK_Dokumen_Blueprint_Spesifikasi_Kebutuhan_Aplikasi.pdf', '2018-11-11 18:21:39', '18', NULL, NULL),
(40, 'aplikasi', 'id_aplikasi', 6, NULL, NULL, NULL, 'Dokumen SRS', '2018-08-06 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111182201 - 1. MISTIK_Dokumen_SRS.pdf', '2018-11-11 18:22:01', '18', NULL, NULL),
(41, 'aplikasi', 'id_aplikasi', 6, NULL, NULL, NULL, 'Dokumen Testing', '2018-10-17 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111182232 - 2. MISTIK_Dokumen_Testing.pdf', '2018-11-11 18:22:32', '18', NULL, NULL),
(42, 'aplikasi', 'id_aplikasi', 6, NULL, NULL, NULL, 'Dokumen Hasil Perbaikan Aplikasi', '2018-10-31 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111182254 - 3. MISTIK_Daftar Hasil Perbaikan Aplikasi.pdf', '2018-11-11 18:22:54', '18', NULL, NULL),
(43, 'aplikasi', 'id_aplikasi', 6, NULL, NULL, NULL, 'Dokumen UservManual', '2018-11-09 00:00:00', '-', 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20181111182318 - 4. MISTIK_Dokumen_User_Manual.pdf', '2018-11-11 18:23:18', '18', NULL, NULL),
(44, 'aset_keluar', 'nomor_aset_keluar', 2, NULL, NULL, NULL, 'BA', NULL, NULL, 0, 'C:\\xampp\\htdocs\\laravel\\mistik\\storage\\app\\public\\files\\dokumentasi\\20190211112759 - APAPO v.2.0 MPP Banyuwangi.xlsx', '2019-02-11 11:27:59', '18', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar`
--

CREATE TABLE `gambar` (
  `id_gambar` int(10) UNSIGNED NOT NULL,
  `src_tabel_gambar` varchar(50) DEFAULT NULL,
  `src_kolom_gambar` varchar(50) DEFAULT NULL,
  `src_fk_gambar` int(10) UNSIGNED DEFAULT NULL,
  `nama_gambar` varchar(100) DEFAULT NULL,
  `ukuran_gambar` int(10) UNSIGNED NOT NULL,
  `link_gambar` varchar(200) NOT NULL,
  `keterangan_gambar` varchar(500) DEFAULT NULL,
  `gambar_utama` enum('Y','N') DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gambar`
--

INSERT INTO `gambar` (`id_gambar`, `src_tabel_gambar`, `src_kolom_gambar`, `src_fk_gambar`, `nama_gambar`, `ukuran_gambar`, `link_gambar`, `keterangan_gambar`, `gambar_utama`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'aset', 'id_aset', 1, 'Logo', 0, 'public/files/gambar/20181111072326 - IBM-QRADAR.jpg', 'Logo of IBM QRadar Software Security', 'Y', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(2, 'aset', 'id_aset', 1, 'Dashboard', 0, 'public/files/gambar/20181111072326 - offering_db52d151-aa0f-4d88-8b27-27bbf2d71efe.jpg', 'Viewing charts and graphs of the current controlling system', 'N', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(3, 'aset', 'id_aset', 1, 'Report', 0, 'public/files/gambar/20181111072326 - docview.wss.png', 'The report feature of the analytical processing', 'N', '2018-11-11 07:23:26', '22', NULL, NULL, NULL),
(4, 'aset', 'id_aset', 2, 'Illustration', 0, 'public/files/gambar/20181111073036 - journey_sec_qrad_siem_admin.jpg', 'Illustration of the IBM QRadar HA Security Software', 'Y', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(5, 'aset', 'id_aset', 2, 'Dashboard', 0, 'public/files/gambar/20181111073036 - offering_c948c9df-4d0f-4cfa-857c-8d1dfd91c28d.jpg', 'Dashboard of IBM Security QRadar HA', 'N', '2018-11-11 07:30:36', '22', NULL, NULL, NULL),
(6, 'aset', 'id_aset', 3, 'Architecture', 0, 'public/files/gambar/20181111073545 - nowhere-to-hide-expose-threats-in-realtime-with-ibm-qradar-network-insights-11-638.jpg', 'Architecture Diagram of the Layer-7 Network Flow (IBM Security QRadar Network Insights)', 'Y', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(7, 'aset', 'id_aset', 3, 'Application', 0, 'public/files/gambar/20181111073545 - qni_1901_back_panel.jpg', 'The Application oft Network Insights 1901 Appliance (Management Interface Port using  RJ-45 1GbE)', 'N', '2018-11-11 07:35:45', '22', NULL, NULL, NULL),
(8, 'aset', 'id_aset', 4, 'Installation', 0, 'public/files/gambar/20181111074153 - NW_PCAP_avago_installed.jpg', 'Fiber installed and Four SR SFP+ module applicated', 'Y', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(9, 'aset', 'id_aset', 4, 'Rear Panel', 0, 'public/files/gambar/20181111074153 - NW_PCAP_rear_panel.jpg', 'The Rear panel of Intel X520 Socket', 'N', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(10, 'aset', 'id_aset', 4, 'Architecture', 0, 'public/files/gambar/20181111074153 - forensics_2a.jpg', 'The Architecture of  IBM QRadar Console Network', 'N', '2018-11-11 07:41:53', '22', NULL, NULL, NULL),
(11, 'aset', 'id_aset', 5, 'Vertif SmartAisle', 0, 'public/files/gambar/20181111081008 - smartaisle-left-side-angle.png', 'Vertif SmartAisle', 'Y', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(12, 'aset', 'id_aset', 5, 'Illustration', 0, 'public/files/gambar/20181111081008 - 1600x636-datacenter-banner_174538_0.jpg', 'Illustration of Vertif SmartAisle', 'N', '2018-11-11 08:10:08', '22', NULL, NULL, NULL),
(13, 'aset', 'id_aset', 6, '12 Racks', 0, 'public/files/gambar/20181111083946 - 1600x636-101166_237086_0.jpg', NULL, 'Y', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(14, 'aset', 'id_aset', 6, 'Top View of the Aisle', 0, 'public/files/gambar/20181111083946 - images.jpg', NULL, 'N', '2018-11-11 08:39:46', '22', NULL, NULL, NULL),
(15, 'aset', 'id_aset', 7, '5kw - W800', 0, 'public/files/gambar/20181111085350 - doubleprorack-network.png', NULL, 'Y', '2018-11-11 08:53:50', '22', NULL, NULL, NULL),
(16, 'aset', 'id_aset', 8, 'Proofpoint P670 Secure Email Gateways', 0, 'public/files/gambar/20181111090106 - INFOSEC mock up web4-edit-12.png', 'Image from the website', 'Y', '2018-11-11 09:01:06', '22', NULL, NULL, NULL),
(17, 'aset', 'id_aset', 9, 'Dell PowerEdge R740 1', 0, 'public/files/gambar/20181111091606 - index2.jpg', NULL, 'Y', '2018-11-11 09:16:07', '22', NULL, NULL, NULL),
(18, 'aset', 'id_aset', 9, 'Dell PowerEdge R740 2 (Backside)', 0, 'public/files/gambar/20181111091606 - index.jpg', NULL, 'N', '2018-11-11 09:16:07', '22', NULL, NULL, NULL),
(19, 'aset', 'id_aset', 9, 'Dell PowerEdge R740 3', 0, 'public/files/gambar/20181111091606 - the-poweredge-r740-rack-server-en.jpg', NULL, 'N', '2018-11-11 09:16:07', '22', NULL, NULL, NULL),
(20, 'aset', 'id_aset', 10, 'Dell PowerEdge M640 High Performance', 0, 'public/files/gambar/20181111091948 - enterprise-servers-poweredge-dellemc-pem640-pdp-module-01.jpg', 'Tampak Depan', 'Y', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(21, 'aset', 'id_aset', 10, 'PowerEdge M640 High Performance', 0, 'public/files/gambar/20181111091948 - enterprise-servers-poweredge-dell-pem640-left-hero-685x350-ng.psd.jpg', NULL, 'N', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(22, 'aset', 'id_aset', 10, 'PowerEdge M640 High Performance', 0, 'public/files/gambar/20181111091948 - enterprise-servers-poweredge-m640-module-vide0-no-play-button-pdp-module.jpg', NULL, 'N', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(23, 'aset', 'id_aset', 10, 'PowerEdge M640 High Performance', 0, 'public/files/gambar/20181111091948 - m640-5.png', 'Tampak Atas', 'N', '2018-11-11 09:19:48', '22', NULL, NULL, NULL),
(24, 'aset', 'id_aset', 11, 'Dell PowerEdge R740 2U', 0, 'public/files/gambar/20181111094347 - index.jpg', NULL, 'Y', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(25, 'aset', 'id_aset', 11, 'Dell PowerEdge R740 2U', 0, 'public/files/gambar/20181111094347 - dell-outlet-poweredge-r740-rackmount-2-x-xeon-silver-4110-cpus-64gb-ram-2-x-240gb-4-x-1.2tb-[3]-285-p.jpg', NULL, 'N', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(26, 'aset', 'id_aset', 11, 'Dell PowerEdge R740 2U Purpose', 0, 'public/files/gambar/20181111094347 - R740-slide-1.jpg', NULL, 'N', '2018-11-11 09:43:47', '22', NULL, NULL, NULL),
(27, 'aset', 'id_aset', 12, 'Dell PowerEdge R740 ArcGIS', 0, 'public/files/gambar/20181111094537 - maxresdefault.jpg', NULL, 'Y', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(28, 'aset', 'id_aset', 12, 'Dell PowerEdge R740 ArcGIS', 0, 'public/files/gambar/20181111094537 - R740R.jpg', 'Tampak Belakang', 'N', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(29, 'aset', 'id_aset', 12, 'Dell PowerEdge R740 ArcGIS', 0, 'public/files/gambar/20181111094537 - dell-emc-poweredge-r740xd-front.png', NULL, 'N', '2018-11-11 09:45:37', '22', NULL, NULL, NULL),
(30, 'aset', 'id_aset', 13, 'Dell PowerEdge M1000e Blade Server Chassis', 0, 'public/files/gambar/20181111094839 - s-l1000.jpg', NULL, 'Y', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(31, 'aset', 'id_aset', 13, 'Dell PowerEdge M1000e Blade Server Chassis', 0, 'public/files/gambar/20181111094839 - dell-poweredge-m1000e-16-slot-blade-server-chassis-centre-psu-s-hexarootsolution-1704-02-hexarootsolution@57.jpg', NULL, 'N', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(32, 'aset', 'id_aset', 13, 'Dell PowerEdge M1000e Blade Server Chassis', 0, 'public/files/gambar/20181111094839 - 3-37.jpg', NULL, 'N', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(33, 'aset', 'id_aset', 13, 'Dell PowerEdge M1000e Blade Server Chassis', 0, 'public/files/gambar/20181111094839 - pedge_m1000e_overview1.jpg', NULL, 'N', '2018-11-11 09:48:39', '22', NULL, NULL, NULL),
(34, 'aset', 'id_aset', 14, '3M Passport Scanner MRTD AT9000 MK2', 0, 'public/files/gambar/20181111095517 - WhatsApp Image 2018-11-08 at 10.07.57(5).jpeg', 'Tampak Depan', 'Y', '2018-11-11 09:55:17', '22', NULL, NULL, NULL),
(35, 'aset', 'id_aset', 14, '3M Passport Scanner MRTD AT9000 MK2', 0, 'public/files/gambar/20181111095517 - WhatsApp Image 2018-11-08 at 10.07.57(6).jpeg', 'Tampak Belakang', 'N', '2018-11-11 09:55:17', '22', NULL, NULL, NULL),
(36, 'aset', 'id_aset', 14, '3M Passport Scanner MRTD AT9000 MK2', 0, 'public/files/gambar/20181111095517 - WhatsApp Image 2018-11-08 at 10.07.57(8).jpeg', 'Tampak Samping', 'N', '2018-11-11 09:55:17', '22', NULL, NULL, NULL),
(37, 'aset', 'id_aset', 14, '3M Passport Scanner MRTD AT9000 MK2', 0, 'public/files/gambar/20181111095517 - WhatsApp Image 2018-11-08 at 10.07.57(4).jpeg', 'Kelengkapan Unit', 'N', '2018-11-11 09:55:17', '22', NULL, NULL, NULL),
(38, 'aset', 'id_aset', 15, 'APC Back-UPS 1100', 0, 'public/files/gambar/20181111100507 - WhatsApp Image 2018-11-08 at 10.07.55(5).jpeg', 'Tampak Depan', 'Y', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(39, 'aset', 'id_aset', 15, 'APC Back-UPS 1100', 0, 'public/files/gambar/20181111100507 - WhatsApp Image 2018-11-08 at 10.07.55(4).jpeg', 'Tampak Samping', 'N', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(40, 'aset', 'id_aset', 15, 'APC Back-UPS 1100', 0, 'public/files/gambar/20181111100507 - WhatsApp Image 2018-11-08 at 10.07.56(1).jpeg', 'Tampak Belakang', 'N', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(41, 'aset', 'id_aset', 15, 'APC Back-UPS 1100', 0, 'public/files/gambar/20181111100507 - WhatsApp Image 2018-11-08 at 10.07.57(1).jpeg', 'Kardus', 'N', '2018-11-11 10:05:07', '22', NULL, NULL, NULL),
(42, 'aset', 'id_aset', 16, 'HP SFF Desktop ProDesk 400 G3N4P96AV', 0, 'public/files/gambar/20181111111207 - WhatsApp Image 2018-11-08 at 10.07.56(4).jpeg', 'PC set', 'Y', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(43, 'aset', 'id_aset', 16, 'HP SFF Desktop ProDesk 400 G3N4P96AV', 0, 'public/files/gambar/20181111111207 - WhatsApp Image 2018-11-08 at 10.07.55.jpeg', 'CPU', 'N', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(44, 'aset', 'id_aset', 16, 'HP SFF Desktop ProDesk 400 G3N4P96AV', 0, 'public/files/gambar/20181111111207 - WhatsApp Image 2018-11-08 at 10.07.55(1).jpeg', 'Tampak Belakang', 'N', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(45, 'aset', 'id_aset', 16, 'HP SFF Desktop ProDesk 400 G3N4P96AV', 0, 'public/files/gambar/20181111111207 - WhatsApp Image 2018-11-08 at 10.07.56(5).jpeg', 'Kardus', 'N', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(46, 'aset', 'id_aset', 16, 'HP SFF Desktop ProDesk 400 G3N4P96AV', 0, 'public/files/gambar/20181111111207 - WhatsApp Image 2018-11-08 at 10.07.56(6).jpeg', 'Kardus 2', 'N', '2018-11-11 11:12:07', '22', NULL, NULL, NULL),
(47, 'aset', 'id_aset', 17, 'HP V194 Monitor', 0, 'public/files/gambar/20181111113005 - WhatsApp Image 2018-11-08 at 10.07.55(2).jpeg', 'Tampak Depan', 'Y', '2018-11-11 11:30:05', '22', NULL, NULL, NULL),
(48, 'aset', 'id_aset', 17, 'HP V194 Monitor', 0, 'public/files/gambar/20181111113005 - WhatsApp Image 2018-11-08 at 10.07.55(3).jpeg', 'Tampak Belakang', 'N', '2018-11-11 11:30:05', '22', NULL, NULL, NULL),
(49, 'aset', 'id_aset', 17, 'HP V194 Monitor', 0, 'public/files/gambar/20181111113005 - WhatsApp Image 2018-11-08 at 10.07.56(7).jpeg', 'Kardus', 'N', '2018-11-11 11:30:05', '22', NULL, NULL, NULL),
(50, 'aplikasi', 'id_aplikasi', 1, 'Login Page', 0, 'public/files/gambar/20181111135739 - Capture.PNG', 'Login Page Aplikasi Helpdesk', 'N', '2018-11-11 13:57:39', '18', NULL, NULL, NULL),
(51, 'aplikasi', 'id_aplikasi', 1, 'Dashboar Aplikasi Helpdesk', 0, 'public/files/gambar/20181111135815 - Capture2.PNG', 'Helpdesk Dashboard', 'N', '2018-11-11 13:58:15', '18', NULL, NULL, NULL),
(52, 'aplikasi', 'id_aplikasi', 1, 'Membuat Tiket Pengaduan Baru', 0, 'public/files/gambar/20181111135834 - Capture3.PNG', 'Membuat Tiket Pengaduan Baru', 'N', '2018-11-11 13:58:34', '18', NULL, NULL, NULL),
(53, 'aplikasi', 'id_aplikasi', 1, 'Daftar Pengaduan yang Open', 0, 'public/files/gambar/20181111135851 - Capture4.PNG', 'Daftar Pengaduan yang Open', 'N', '2018-11-11 13:58:51', '18', NULL, NULL, NULL),
(54, 'aplikasi', 'id_aplikasi', 1, 'Daftar Pengaduan yang Close', 0, 'public/files/gambar/20181111135907 - Capture5.PNG', 'Daftar Pengaduan yang Close', 'N', '2018-11-11 13:59:07', '18', NULL, NULL, NULL),
(55, 'aplikasi', 'id_aplikasi', 1, 'Daftar Pengaduan', 0, 'public/files/gambar/20181111135932 - Capture6.PNG', 'Daftar Pengaduan', 'N', '2018-11-11 13:59:32', '18', NULL, NULL, NULL),
(56, 'aplikasi', 'id_aplikasi', 1, 'Rincian Tiket Pengaduan', 0, 'public/files/gambar/20181111135948 - Capture7.PNG', 'Rincian Tiket Pengaduan', 'N', '2018-11-11 13:59:48', '18', NULL, NULL, NULL),
(57, 'aplikasi', 'id_aplikasi', 1, 'Rekap & Laporan', 0, 'public/files/gambar/20181111140007 - Capture8.PNG', 'Fitur Rekap & Laporan', 'N', '2018-11-11 14:00:07', '18', NULL, NULL, NULL),
(58, 'aplikasi', 'id_aplikasi', 1, 'Reopen Pengaduan', 0, 'public/files/gambar/20181111140029 - Capture9.PNG', 'Reopen Pengaduan', 'N', '2018-11-11 14:00:29', '18', NULL, NULL, NULL),
(59, 'aplikasi', 'id_aplikasi', 1, 'Ubah Profile Timtik', 0, 'public/files/gambar/20181111140058 - Capture10.PNG', 'Fitur Profile', 'N', '2018-11-11 14:00:58', '18', NULL, NULL, NULL),
(60, 'aplikasi', 'id_aplikasi', 2, 'Homepage', 0, 'public/files/gambar/20181111181218 - 1.png', '-', 'N', '2018-11-11 18:12:18', '18', NULL, NULL, NULL),
(61, 'aplikasi', 'id_aplikasi', 2, 'Login Page', 0, 'public/files/gambar/20181111181238 - 2.png', '-', 'N', '2018-11-11 18:12:38', '18', NULL, NULL, NULL),
(62, 'aplikasi', 'id_aplikasi', 2, 'Reservasi', 0, 'public/files/gambar/20181111181253 - 3.png', '-', 'N', '2018-11-11 18:12:53', '18', NULL, NULL, NULL),
(63, 'aplikasi', 'id_aplikasi', NULL, 'Pemilihan Tanggal Wawancara', 0, 'public/files/gambar/20181111181311 - 4.png', '-', 'N', '2018-11-11 18:13:11', '18', NULL, NULL, NULL),
(64, 'aplikasi', 'id_aplikasi', 2, 'Pemilihan Tanggal Wawancara', 0, 'public/files/gambar/20181111181336 - 4.png', '-', 'N', '2018-11-11 18:13:36', '18', NULL, NULL, NULL),
(65, 'aplikasi', 'id_aplikasi', 2, 'Isi Formulir', 0, 'public/files/gambar/20181111181357 - 5.png', '-', 'N', '2018-11-11 18:13:57', '18', NULL, NULL, NULL),
(66, 'aplikasi', 'id_aplikasi', NULL, 'Riwayat', 0, 'public/files/gambar/20181111181410 - 6.png', '-', 'N', '2018-11-11 18:14:10', '18', NULL, NULL, NULL),
(67, 'aplikasi', 'id_aplikasi', 2, 'Riwayat', 0, 'public/files/gambar/20181111181436 - 6.png', '-', 'N', '2018-11-11 18:14:36', '18', NULL, NULL, NULL),
(68, 'aplikasi', 'id_aplikasi', 2, 'Penolakan Permohonan', 0, 'public/files/gambar/20181111181501 - 7.png', '-', 'N', '2018-11-11 18:15:01', '18', NULL, NULL, NULL),
(69, 'aplikasi', 'id_aplikasi', 2, 'Permohonan Total Ditolak', 0, 'public/files/gambar/20181111181527 - 8.png', '-', 'N', '2018-11-11 18:15:27', '18', NULL, NULL, NULL),
(70, 'aplikasi', 'id_aplikasi', 2, 'Login Petugas', 0, 'public/files/gambar/20181111181545 - 9.png', '-', 'N', '2018-11-11 18:15:45', '18', NULL, NULL, NULL),
(71, 'aplikasi', 'id_aplikasi', 2, 'Manajemen Akses User Petugas', 0, 'public/files/gambar/20181111181615 - 10.png', '-', 'N', '2018-11-11 18:16:15', '18', NULL, NULL, NULL),
(72, 'aplikasi', 'id_aplikasi', 2, 'Pengaturan Aplikasi WHV', 0, 'public/files/gambar/20181111181632 - 11.png', '-', 'N', '2018-11-11 18:16:32', '18', NULL, NULL, NULL),
(73, 'aplikasi', 'id_aplikasi', 2, 'Pengaturan TTD', 0, 'public/files/gambar/20181111181647 - 12.png', '-', 'N', '2018-11-11 18:16:48', '18', NULL, NULL, NULL),
(74, 'aplikasi', 'id_aplikasi', 6, 'Login Page', 0, 'public/files/gambar/20181111182348 - 1.PNG', NULL, 'N', '2018-11-11 18:23:48', '18', NULL, NULL, NULL),
(75, 'aplikasi', 'id_aplikasi', 6, 'Homepage', 0, 'public/files/gambar/20181111182402 - 2.PNG', '-', 'N', '2018-11-11 18:24:02', '18', NULL, NULL, NULL),
(76, 'aplikasi', 'id_aplikasi', 6, 'Page Aplikasi SIMKIM', 0, 'public/files/gambar/20181111182420 - 3.PNG', '-', 'N', '2018-11-11 18:24:20', '18', NULL, NULL, NULL),
(77, 'aplikasi', 'id_aplikasi', 6, 'Rincian Aplikasi SIMKIM', 0, 'public/files/gambar/20181111182442 - 5.PNG', '-', 'N', '2018-11-11 18:24:42', '18', NULL, NULL, NULL),
(78, 'aplikasi', 'id_aplikasi', 6, 'Page Aset SIMKIM', 0, 'public/files/gambar/20181111182459 - 12.PNG', '-', 'N', '2018-11-11 18:24:59', '18', NULL, NULL, NULL),
(79, 'aplikasi', 'id_aplikasi', 6, 'Rincian Aset SIMKIM', 0, 'public/files/gambar/20181111182520 - 12_2.PNG', '-', 'N', '2018-11-11 18:25:20', '18', NULL, NULL, NULL),
(80, 'aplikasi', 'id_aplikasi', 6, 'Paket Pengadaan SIMKIM', 0, 'public/files/gambar/20181111182546 - 15.PNG', '-', 'N', '2018-11-11 18:25:46', '18', NULL, NULL, NULL),
(81, 'aplikasi', 'id_aplikasi', 6, 'Manajemen Aset', 0, 'public/files/gambar/20181111182604 - 19.PNG', '-', 'N', '2018-11-11 18:26:04', '18', NULL, NULL, NULL),
(82, 'aplikasi', 'id_aplikasi', 6, 'Catat Aset Masuk', 0, 'public/files/gambar/20181111182628 - 22.PNG', '-', 'N', '2018-11-11 18:26:28', '18', NULL, NULL, NULL),
(83, 'aplikasi', 'id_aplikasi', 6, 'Catat Aset Keluar', 0, 'public/files/gambar/20181111182647 - 25.PNG', '-', 'N', '2018-11-11 18:26:47', '18', NULL, NULL, NULL),
(84, 'aplikasi', 'id_aplikasi', 6, 'Laporan Aset Masuk', 0, 'public/files/gambar/20181111182715 - 38.PNG', '-', 'N', '2018-11-11 18:27:15', '18', NULL, NULL, NULL),
(85, 'aplikasi', 'id_aplikasi', 6, 'Laporan Aset Keluar', 0, 'public/files/gambar/20181111182732 - 40.PNG', '-', 'N', '2018-11-11 18:27:32', '18', NULL, NULL, NULL),
(86, 'aplikasi', 'id_aplikasi', 6, 'Laporan Persebaran Aset', 0, 'public/files/gambar/20181111182759 - 42.PNG', '-', 'N', '2018-11-11 18:27:59', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_08_21_020508_create_products_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_datatype`
--

CREATE TABLE `ms_datatype` (
  `id_datatype` int(10) NOT NULL,
  `nama_datatype` varchar(100) NOT NULL,
  `kode_html` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_datatype`
--

INSERT INTO `ms_datatype` (`id_datatype`, `nama_datatype`, `kode_html`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(2, 'text', '<input type=\"text\" class=\"form-control\" name=\"<<name>>\" placeholder=\"\'<<placeholder>>\">', '2019-03-05 00:58:41', '18', '2019-03-05 01:28:57', NULL, '2019-03-05 01:28:57'),
(3, 'file', '<div class=\"input-group\"><input type=\"text\" class=\"form-control\" readonly=\"\" style=\"background-color: white;\" placeholder=\"Ukuran maksimum: 100 MB\"><div class=\"input-group-append\"><div class=\"btn btn-primary btn-sm\"><span><i class=\"fa fa-upload\"></i></span><input type=\"file\" rel=\"tooltip\" name=\"<<name>>\" data-original-title=\"Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB.\" title=\"Ukuran maksimum: 100 MB.\" class=\"upload\" onChange=\"$(this).parent().parent().prev(\'input\').val(this.value.replace(/C:\\\\fakepath\\\\/i, \'\'));\" /></div></div></div>', '2019-03-05 01:21:45', '', '2019-03-05 06:12:17', NULL, '2019-03-05 06:12:17'),
(4, 'date', '<div class=\"input-group\"><input type=\"date\" name=\"<<name>>\" class=\"form-control\"><div class=\"input-group-append\"><div class=\"text-primary\">&emsp;<i class=\"fa fa-calendar\"></i></div></div></div>', '2019-03-05 01:26:16', '', '2019-03-05 01:29:47', NULL, '2019-03-05 01:29:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_datatype_parameter`
--

CREATE TABLE `ms_datatype_parameter` (
  `id_parameter` int(10) NOT NULL,
  `id_datatype` int(10) NOT NULL,
  `src_tabel_parameter` varchar(150) NOT NULL,
  `src_kolom_parameter` varchar(150) NOT NULL,
  `src_fk_parameter` varchar(150) NOT NULL,
  `parameter` text NOT NULL,
  `replacement` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) NOT NULL,
  `deleted_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_datatype_parameter`
--

INSERT INTO `ms_datatype_parameter` (`id_parameter`, `id_datatype`, `src_tabel_parameter`, `src_kolom_parameter`, `src_fk_parameter`, `parameter`, `replacement`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(2, 2, 'ms_field_aset_informasi', 'id_field_info', '1', '<<name>>', 'isian_field_informasi~<<kode_grup_satuan>>_<<id_field_info>>', '2019-03-05 01:45:04', '18', '2019-03-05 02:01:03', '', '2019-03-05 02:01:03'),
(3, 2, 'ms_field_aset_informasi', 'id_field_info', '1', '<<placeholder>>', '<<keterangan_field_info>>', '2019-03-05 01:48:07', '18', '2019-03-05 03:48:09', '', '2019-03-05 03:48:09'),
(4, 2, 'ms_field_aset_informasi', 'id_field_info', '3', '<<name>>', 'isian_field_informasi~<<kode_grup_satuan>>_<<id_field_info>>', '2019-03-05 03:47:16', '18', '2019-03-05 03:50:27', '', '2019-03-05 03:50:27'),
(5, 2, 'ms_field_aset_informasi', 'id_field_info', '3', '<<placeholder>>', '<<keterangan_field_info>>', '2019-03-05 03:47:53', '18', '2019-03-05 03:50:28', '', '2019-03-05 03:50:28'),
(6, 2, 'ms_field_aset_informasi', 'id_field_info', '4', '<<name>>', 'isian_field_informasi~<<kode_grup_satuan>>_<<id_field_info>>', '2019-03-05 03:51:28', '18', '2019-03-05 03:51:41', '', '2019-03-05 03:51:41'),
(7, 2, 'ms_field_aset_informasi', 'id_field_info', '4', '<<placeholder>>', '<<keterangan_field_info>>', '2019-03-05 03:51:48', '18', '2019-03-05 03:51:59', '', '2019-03-05 03:51:59'),
(8, 3, 'ms_field_aset_informasi', 'id_field_info', '5', '<<name>>', 'isian_field_informasi~<<kode_grup_satuan>>_<<id_field_info>>', '2019-03-05 06:03:05', '18', '2019-03-05 06:04:31', '', '2019-03-05 06:04:31');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_datatype_parameter_mapping`
--

CREATE TABLE `ms_datatype_parameter_mapping` (
  `id_mapping` int(10) NOT NULL,
  `id_parameter` int(10) NOT NULL,
  `replacement` text NOT NULL,
  `src_tabel_mapping` text NOT NULL,
  `src_kolom_mapping` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_datatype_parameter_mapping`
--

INSERT INTO `ms_datatype_parameter_mapping` (`id_mapping`, `id_parameter`, `replacement`, `src_tabel_mapping`, `src_kolom_mapping`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 2, '<<kode_grup_satuan>>', 'aset_satuan', 'kode_grup_satuan', '2019-03-05 01:55:54', '18', '2019-03-05 02:07:34', NULL, NULL),
(2, 2, '<<id_field_info>>', 'ms_field_aset_informasi', 'id_field_info', '2019-03-05 02:05:34', '18', '2019-03-05 02:07:51', NULL, '2019-03-05 02:07:51'),
(3, 3, '<<keterangan_field_info>>', 'ms_field_aset_informasi', 'keterangan_field_info', '2019-03-05 02:06:30', '18', '2019-03-05 02:08:03', NULL, NULL),
(4, 4, '<<kode_grup_satuan>>', 'aset_satuan', 'kode_grup_satuan', '2019-03-05 03:48:59', '18', '2019-03-05 03:49:08', NULL, NULL),
(5, 4, '<<id_field_info>>', 'ms_field_aset_informasi', 'id_field_info', '2019-03-05 03:49:19', '18', '2019-03-05 03:49:31', NULL, NULL),
(6, 5, '<<keterangan_field_info>>', 'ms_field_aset_informasi', 'keterangan_field_info', '2019-03-05 03:49:36', '18', '2019-03-05 03:49:44', NULL, NULL),
(7, 6, '<<kode_grup_satuan>>', 'aset_satuan', 'kode_grup_satuan', '2019-03-05 03:52:20', '18', '2019-03-05 03:52:28', NULL, NULL),
(8, 6, '<<id_field_info>>', 'ms_field_aset_informasi', 'id_field_info', '2019-03-05 03:52:36', '18', '2019-03-05 03:52:44', NULL, NULL),
(9, 7, '<<keterangan_field_info>>', 'ms_field_aset_informasi', 'keterangan_field_info', '2019-03-05 03:52:49', '18', '2019-03-05 03:52:56', NULL, NULL),
(10, 8, '<<kode_grup_satuan>>', 'aset_satuan', 'kode_grup_satuan', '2019-03-05 06:05:05', '18', '2019-03-05 06:05:15', NULL, NULL),
(11, 8, '<<id_field_info>>', 'ms_field_aset_informasi', 'id_field_info', '2019-03-05 06:05:21', '18', '2019-03-05 06:05:38', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_field_aset_informasi`
--

CREATE TABLE `ms_field_aset_informasi` (
  `id_field_info` int(10) NOT NULL,
  `id_jenis_aset` int(10) NOT NULL DEFAULT '0',
  `nama_field_info` varchar(50) NOT NULL,
  `keterangan_field_info` varchar(500) DEFAULT NULL,
  `id_datatype` int(10) NOT NULL,
  `src_tabel_field_info` varchar(150) NOT NULL,
  `src_kolom_field_info` varchar(150) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_field_aset_informasi`
--

INSERT INTO `ms_field_aset_informasi` (`id_field_info`, `id_jenis_aset`, `nama_field_info`, `keterangan_field_info`, `id_datatype`, `src_tabel_field_info`, `src_kolom_field_info`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 6, 'IP Address', 'Masukkan IP Address', 2, 'ms_field_aset_informasi', 'id_field_info', '2019-03-02 16:04:21', '18', '2019-03-05 02:23:01', NULL, '2019-03-05 02:23:01'),
(3, 6, 'Port', 'Masukkan Port', 2, 'ms_field_aset_informasi', 'id_field_info', '2019-03-02 16:04:50', '18', '2019-03-05 02:23:01', NULL, '2019-03-05 02:23:01'),
(4, 6, 'MAC Address', 'Masukkan MAC Address', 2, 'ms_field_aset_informasi', 'id_field_info', '2019-03-02 16:05:30', '18', '2019-03-05 02:23:02', NULL, '2019-03-05 02:23:02'),
(5, 6, 'Server Certificate', 'Masukkan Server Certificate', 3, 'ms_field_aset_informasi', 'id_field_info', '2019-03-05 06:02:44', '', '2019-03-05 06:02:47', NULL, '2019-03-05 06:02:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_field_spesifikasi_aset`
--

CREATE TABLE `ms_field_spesifikasi_aset` (
  `id_field` int(10) NOT NULL,
  `id_jenis_aset` int(10) NOT NULL DEFAULT '0',
  `nama_field_spek` varchar(50) NOT NULL,
  `keterangan_field_spek` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_field_spesifikasi_aset`
--

INSERT INTO `ms_field_spesifikasi_aset` (`id_field`, `id_jenis_aset`, `nama_field_spek`, `keterangan_field_spek`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 6, 'Processor', NULL, '2018-10-02 09:49:45', '', NULL, NULL, NULL),
(2, 6, 'Memory', NULL, '2018-10-02 09:49:55', '', NULL, NULL, NULL),
(3, 6, 'RAM', NULL, '2019-02-13 11:10:02', '', NULL, NULL, NULL),
(4, 6, 'Operating System', NULL, '2018-10-02 09:51:19', '', NULL, NULL, NULL),
(5, 6, 'BIOS', NULL, '2018-10-02 09:51:31', '', NULL, NULL, NULL),
(6, 1, 'RAM', NULL, '2018-10-15 10:27:19', '', NULL, NULL, NULL),
(7, 1, 'Memory', NULL, '2018-10-15 10:27:39', '', NULL, NULL, NULL),
(8, 1, 'VGA', NULL, '2018-10-15 10:27:52', '', NULL, NULL, NULL),
(9, 1, 'Processor', NULL, '2018-11-11 11:07:13', '', NULL, NULL, NULL),
(10, 2, 'RAM', NULL, '2018-10-15 10:28:49', '', NULL, NULL, NULL),
(11, 2, 'Memory', NULL, '2018-10-15 10:29:01', '', NULL, NULL, NULL),
(12, 2, 'VGA', NULL, '2018-10-15 10:29:10', '', NULL, NULL, NULL),
(13, 2, 'Ukuran Layar', NULL, '2018-10-15 10:29:21', '', NULL, NULL, NULL),
(14, 3, 'Kecepatan Putaran', NULL, '2018-10-15 10:29:47', '', NULL, NULL, NULL),
(15, 3, 'Bahan', NULL, '2018-10-15 10:29:55', '', NULL, NULL, NULL),
(16, 4, 'Dimensi', NULL, '2018-10-15 10:31:58', '', NULL, NULL, NULL),
(17, 4, 'Jenis Tinta', NULL, '2018-10-15 10:32:09', '', NULL, NULL, NULL),
(18, 4, 'Jenis Cartridge', NULL, '2018-10-15 10:32:22', '', NULL, NULL, NULL),
(19, 4, 'Jenis Printhead', NULL, '2018-10-15 10:32:38', '', NULL, NULL, NULL),
(20, 5, 'Dimensi', NULL, '2018-10-15 10:32:56', '', NULL, NULL, NULL),
(21, 5, 'Kecepatan Scan', NULL, '2018-10-15 10:33:07', '', NULL, NULL, NULL),
(22, 7, 'Megapixel', NULL, '2018-10-15 10:34:48', '', NULL, NULL, NULL),
(23, 7, 'Sensor', NULL, '2018-10-15 10:34:55', '', NULL, NULL, NULL),
(24, 7, 'Stabilizer', NULL, '2018-10-15 10:35:01', '', NULL, NULL, NULL),
(25, 10, 'Events per Second', NULL, '2018-11-11 07:16:15', '', NULL, NULL, NULL),
(26, 10, 'Memory', NULL, '2018-11-11 07:16:32', '', NULL, NULL, NULL),
(27, 10, 'Storage', NULL, '2018-11-11 07:16:53', '', NULL, NULL, NULL),
(28, 10, 'Network Interface', NULL, '2018-11-11 07:17:16', '', NULL, NULL, NULL),
(29, 10, 'License Event per Second', NULL, '2018-11-11 07:17:50', '', NULL, NULL, NULL),
(30, 10, 'Flows per Minute', NULL, '2018-11-11 07:18:17', '', NULL, NULL, NULL),
(31, 11, 'Total Rack', NULL, '2018-11-11 07:50:36', '', NULL, NULL, NULL),
(32, 11, 'Vertical Rails Brush Panel', NULL, '2018-11-11 07:50:58', '', NULL, NULL, NULL),
(33, 11, 'Bottom Brush Panel', NULL, '2018-11-11 07:51:09', '', NULL, NULL, NULL),
(34, 11, 'Mounting', NULL, '2018-11-11 07:51:18', '', NULL, NULL, NULL),
(35, 11, 'Vertical Cable', NULL, '2018-11-11 07:51:32', '', NULL, NULL, NULL),
(36, 11, 'Power Shielding Trough', NULL, '2018-11-11 07:51:47', '', NULL, NULL, NULL),
(37, 11, 'Data Shielding Partition', NULL, '2018-11-11 07:51:59', '', NULL, NULL, NULL),
(38, 11, 'Sliding Door', NULL, '2018-11-11 07:52:12', '', NULL, NULL, NULL),
(39, 11, 'Fixed Roof Plate', NULL, '2018-11-11 07:52:24', '', NULL, NULL, NULL),
(40, 11, 'Openable Roof Plate', NULL, '2018-11-11 07:52:35', '', NULL, NULL, NULL),
(41, 11, 'RDUMI', NULL, '2018-11-11 07:52:45', '', NULL, NULL, NULL),
(42, 11, 'COM Extension Card', NULL, '2018-11-11 07:54:05', '', NULL, NULL, NULL),
(43, 11, 'Management Package', NULL, '2018-11-11 07:53:16', '', NULL, NULL, NULL),
(44, 11, 'Open Controller', NULL, '2018-11-11 07:53:28', '', NULL, NULL, NULL),
(45, 11, 'RDU', NULL, '2018-11-11 07:53:38', '', NULL, NULL, NULL),
(46, 11, 'Other Extension Card', NULL, '2018-11-11 07:54:11', '', NULL, NULL, NULL),
(47, 11, 'Sound and Flash LED Alarm', NULL, '2018-11-11 07:54:31', '', NULL, NULL, NULL),
(48, 11, 'Microwave and Infrared', NULL, '2018-11-11 07:54:43', '', NULL, NULL, NULL),
(49, 11, 'Water Sensor', NULL, '2018-11-11 07:54:50', '', NULL, NULL, NULL),
(50, 11, 'Door Status Sensore', NULL, '2018-11-11 07:55:00', '', NULL, NULL, NULL),
(51, 11, 'Top Button Open', NULL, '2018-11-11 07:55:14', '', NULL, NULL, NULL),
(52, 11, 'Protective Cover', NULL, '2018-11-11 07:55:26', '', NULL, NULL, NULL),
(53, 11, 'Cable Set Top', NULL, '2018-11-11 07:55:36', '', NULL, NULL, NULL),
(54, 11, 'ID Card Reader', NULL, '2018-11-11 07:55:43', '', NULL, NULL, NULL),
(55, 11, 'Door Access Controller Box', NULL, '2018-11-11 07:55:57', '', NULL, NULL, NULL),
(56, 11, 'RFID', NULL, '2018-11-11 07:56:12', '', NULL, NULL, NULL),
(57, 12, 'Capacity', NULL, '2018-11-11 09:57:26', '', NULL, NULL, NULL),
(58, 12, 'Electricity', NULL, '2018-11-11 09:58:04', '', NULL, NULL, NULL),
(59, 12, 'Total Outlets', NULL, '2018-11-11 09:58:28', '', NULL, NULL, NULL),
(60, 12, 'Saving Time', NULL, '2018-11-11 09:58:35', '', NULL, NULL, NULL),
(61, 12, 'Warranty', NULL, '2018-11-11 09:58:53', '', NULL, NULL, NULL),
(62, 12, 'Technology', NULL, '2018-11-11 09:59:00', '', NULL, NULL, NULL),
(63, 13, 'Size (inch)', NULL, '2018-11-11 11:24:21', '', NULL, NULL, NULL),
(64, 13, 'Electricity', NULL, '2018-11-11 11:24:29', '', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jenis_aset`
--

CREATE TABLE `ms_jenis_aset` (
  `id_jenis_aset` int(10) UNSIGNED NOT NULL,
  `id_kategori_aset` int(10) UNSIGNED NOT NULL,
  `nama_jenis_aset` varchar(100) NOT NULL,
  `keterangan_jenis_aset` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_jenis_aset`
--

INSERT INTO `ms_jenis_aset` (`id_jenis_aset`, `id_kategori_aset`, `nama_jenis_aset`, `keterangan_jenis_aset`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 1, 'PC', NULL, '0000-00-00 00:00:00', '', '2018-10-01 18:21:16', NULL, NULL),
(2, 1, 'Laptop', NULL, '0000-00-00 00:00:00', '', '2018-10-01 18:21:14', NULL, NULL),
(3, 1, 'Harddisk', NULL, '0000-00-00 00:00:00', '', '2018-10-01 18:21:11', NULL, NULL),
(4, 1, 'Printer', NULL, '0000-00-00 00:00:00', '', '2018-10-01 18:21:18', NULL, NULL),
(5, 1, 'Scanner', NULL, '0000-00-00 00:00:00', '', '2018-10-01 18:21:20', NULL, NULL),
(6, 1, 'Server', NULL, '0000-00-00 00:00:00', '', '2018-10-01 18:21:22', NULL, NULL),
(7, 1, 'Kamera', NULL, '0000-00-00 00:00:00', '', '2018-10-15 10:33:26', NULL, NULL),
(8, 2, 'License', NULL, '2018-08-23 15:49:37', '18', '2018-10-01 18:21:31', '18', '2018-08-23 08:50:42'),
(9, 2, 'Software', NULL, '2018-10-01 18:12:13', '18', '2018-10-01 18:21:39', NULL, NULL),
(10, 2, 'Security', NULL, '2018-11-11 07:15:16', '18', '2018-11-11 07:15:25', NULL, NULL),
(11, 1, 'Containment', NULL, '2018-11-11 07:49:12', '18', '2018-11-11 07:49:22', NULL, NULL),
(12, 1, 'Battery / UPS', NULL, '2018-11-11 09:57:02', NULL, NULL, NULL, NULL),
(13, 1, 'Monitor', NULL, '2018-11-11 11:23:23', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jenis_kontainer`
--

CREATE TABLE `ms_jenis_kontainer` (
  `id_jenis_kontainer` int(10) UNSIGNED NOT NULL,
  `nama_jenis_kontainer` varchar(100) NOT NULL,
  `keterangan_jenis_kontainer` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_jenis_kontainer`
--

INSERT INTO `ms_jenis_kontainer` (`id_jenis_kontainer`, `nama_jenis_kontainer`, `keterangan_jenis_kontainer`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Lemari', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(2, 'Brankas', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(3, 'Rak Server', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(4, 'tes edit', 'tesss', '2018-08-16 12:16:31', '18', '2018-10-16 10:43:23', '18', '2018-10-16 03:43:23'),
(5, 'aaq', 'aae', '2018-08-16 14:46:54', '18', '2018-08-21 09:15:28', '18', '2018-08-20 19:15:28'),
(6, 'tes', 'dtsy', '2018-08-21 08:09:44', '18', '2018-08-21 09:14:22', NULL, '2018-08-20 19:14:22'),
(8, 'Kantin', 'beli makan', '2018-08-21 10:53:59', '18', '2018-08-21 10:54:29', NULL, '2018-08-20 20:54:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jenis_pengembangan`
--

CREATE TABLE `ms_jenis_pengembangan` (
  `id_jenis_pengembangan` int(10) UNSIGNED NOT NULL,
  `nama_jenis_pengembangan` varchar(100) NOT NULL,
  `keterangan_jenis_pengembangan` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_jenis_pengembangan`
--

INSERT INTO `ms_jenis_pengembangan` (`id_jenis_pengembangan`, `nama_jenis_pengembangan`, `keterangan_jenis_pengembangan`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Website', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(2, 'Desktop', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(3, 'Mobile Android', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(4, 'Mobile iOS', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(5, 'Web Service', NULL, '2018-08-24 09:27:53', NULL, NULL, NULL, NULL),
(6, 'aaf', 'aaf', '2018-08-24 09:37:16', '18', '2018-08-24 09:38:12', '18', '2018-08-24 02:38:12');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_kantor_imigrasi`
--

CREATE TABLE `ms_kantor_imigrasi` (
  `kode_kanim` int(10) UNSIGNED NOT NULL,
  `nama_kanim` varchar(200) NOT NULL,
  `kode_kanwil` int(10) UNSIGNED DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_kantor_imigrasi`
--

INSERT INTO `ms_kantor_imigrasi` (`kode_kanim`, `nama_kanim`, `kode_kanwil`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Direktorat Jenderal Imigrasi', 0, '0000-00-00 00:00:00', '', '2018-08-24 10:12:09', '18', NULL),
(101, 'Kanim Kelas I Khusus Batam', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(102, 'Kanim Kelas I Khusus Medan', 1002, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(103, 'Kanim Kelas I Khusus Jakarta Barat', 1012, '0000-00-00 00:00:00', '', '2018-10-13 22:02:21', NULL, NULL),
(104, 'Kanim Kelas I Khusus Jakarta Selatan', 1012, '0000-00-00 00:00:00', '', '2018-10-13 22:02:29', NULL, NULL),
(105, 'Kanim Kelas I Khusus Ngurah Rai', 1021, '0000-00-00 00:00:00', '', '2018-10-13 22:02:32', NULL, NULL),
(106, 'Kanim Kelas I Khusus Soekarno Hatta', 1011, '0000-00-00 00:00:00', '', '2018-10-13 22:02:37', NULL, NULL),
(107, 'Kanim Kelas I Khusus Surabaya', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(108, 'Kanim Kelas I Banda Aceh', 1001, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(109, 'Kanim Kelas I Jambi', 1006, '0000-00-00 00:00:00', '', '2018-10-01 18:13:32', '18', NULL),
(110, 'Kanim Kelas I Bengkulu', 1008, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(111, 'Kanim Kelas I Bandar Lampung', 1010, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(112, 'Kanim Kelas I Jakarta Pusat', 1012, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(113, 'Kanim Kelas I Jakarta Timur', 1012, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(114, 'Kanim Kelas I Jakarta Utara', 1012, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(115, 'Kanim Kelas I Bandung', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(116, 'Kanim Kelas I Banjarmasin', 1019, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(117, 'Kanim Kelas I Balikpapan', 1020, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(118, 'Kanim Kelas I Gorontalo', 1025, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(119, 'Kanim Kelas I Denpasar', 1021, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(120, 'Kanim Kelas I Ambon', 1031, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(121, 'Kanim Kelas I Jayapura', 1032, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(122, 'Kanim Kelas I Kendari', 1029, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(123, 'Kanim Kelas I Kupang', 1023, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(124, 'Kanim Kelas I Makasar', 1028, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(125, 'Kanim Kelas I Manado', 1024, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(126, 'Kanim Kelas I Mataram', 1022, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(127, 'Kanim Kelas I Padang', 1005, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(128, 'Kanim Kelas I Palangkaraya', 1018, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(129, 'Kanim Kelas I Palembang', 1009, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(130, 'Kanim Kelas I Palu', 1026, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(131, 'Kanim Kelas I Pangkal Pinang', 1007, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(132, 'Kanim Kelas I Pekanbaru', 1003, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(133, 'Kanim Kelas I Pontianak', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(134, 'Kanim Kelas I Samarinda', 1020, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(135, 'Kanim Kelas I Semarang', 1014, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(136, 'Kanim Kelas I Serang', 1011, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(137, 'Kanim Kelas I Tanjung Perak', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(138, 'Kanim Kelas I Tanjung Pinang', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(139, 'Kanim Kelas I Tanjung Priok', 1012, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(140, 'Kanim Kelas I Ternate', 1030, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(141, 'Kanim Kelas I Yogyakarta', 1015, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(142, 'Kanim Kelas II Langsa', 1001, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(143, 'Kanim Kelas II Lhokseumawe', 1001, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(144, 'Kanim Kelas II Meulaboh', 1001, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(145, 'Kanim Kelas II Sabang', 1001, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(146, 'Kanim Kelas II Polonia', 1002, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(147, 'Kanim Kelas II Pematang Siantar', 1002, '0000-00-00 00:00:00', '', '2018-10-13 22:02:55', NULL, NULL),
(148, 'Kanim Kelas I Tangerang', 1011, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(149, 'Kanim Kelas II Bogor', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(150, 'Kanim Kelas II Sukabumi', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(151, 'Kanim Kelas II Tasikmalaya', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(152, 'Kanim Kelas II Cirebon', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(153, 'Kanim Kelas II Karawang', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(154, 'Kanim Kelas II Cilegon', 1011, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(155, 'Kanim Kelas II Wonosobo', 1014, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(156, 'Kanim Kelas II Cilacap', 1014, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(157, 'Kanim Kelas II Pemalang', 1014, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(158, 'Kanim Kelas II Pati', 1014, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(159, 'Kanim Kelas I Surakarta', 1014, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(160, 'Kanim Kelas II Madiun', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(161, 'Kanim Kelas II Blitar', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(162, 'Kanim Kelas II Jember', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(163, 'Kanim Kelas II Malang', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(164, 'Kanim Kelas II Sambas', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(165, 'Kanim Kelas II Entikong', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(166, 'Kanim Kelas II Singaraja', 1021, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(167, 'Kanim Kelas II Sumbawa Besar', 1022, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(168, 'Kanim Kelas II Merauke', 1032, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(169, 'Kanim Kelas II Tg. Balai Karim', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(170, 'Kanim Kelas II Ranai', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(171, 'Kanim Kelas II Atambua', 1023, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(172, 'Kanim Kelas II Bagan Siapi-Api', 1003, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(173, 'Kanim Kelas II Belakang Padang', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(174, 'Kanim Kelas II Belawan', 1002, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(175, 'Kanim Kelas II Bengkalis', 1003, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(176, 'Kanim Kelas II Biak', 1032, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(177, 'Kanim Kelas II Bitung', 1024, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(178, 'Kanim Kelas II Bukit Tinggi', 1005, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(179, 'Kanim Kelas II Dumai', 1003, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(180, 'Kanim Kelas II Kota Baru', 1019, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(181, 'Kanim Kelas II Kuala Tungkal', 1006, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(182, 'Kanim Kelas II Maumere', 1023, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(183, 'Kanim Kelas II Muara Enim', 1009, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(184, 'Kanim Kelas II Nunukan', 1020, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(185, 'Kanim Kelas II Pare Pare', 1028, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(186, 'Kanim Kelas II Sampit', 1018, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(187, 'Kanim Kelas II Sanggau', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(188, 'Kanim Kelas II Selat Panjang', 1003, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(189, 'Kanim Kelas II Siak', 1003, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(190, 'Kanim Kelas II Sibolga', 1002, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(191, 'Kanim Kelas II Singkawang', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(192, 'Kanim Kelas II Sorong', 1033, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(193, 'Kanim Kelas II Tahuna', 1024, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(194, 'Kanim Kelas II Tg. Balai Asaha', 1002, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(195, 'Kanim Kelas II Tanjung Pandan', 1007, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(196, 'Kanim Kelas II Tanjung Uban TPI', 1004, '0000-00-00 00:00:00', '', '2018-10-13 22:03:06', NULL, NULL),
(197, 'Kanim Kelas II Tarakan', 1020, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(198, 'Kanim Kelas II Tembaga Pura', 1032, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(199, 'Kanim Kelas II Tembilahan', 1003, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(200, 'Kanim Kelas II Tual', 1031, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(201, 'Kanim Kelas III Dabo Singkep', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(202, 'Kanim Kelas III Panjang', 1010, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(203, 'Kanim Kelas III Tarempa', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(204, 'Kanim Kelas II Mamuju', 1027, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(205, 'Kanim Kelas II Polewali', 1027, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(206, 'Kanim Kelas II Depok', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(207, 'Kanim Kelas II Manokwari', 1033, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(210, 'Kanim Kelas II Tobelo', 1030, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(211, 'Kanim Kelas III Tg Redep', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(212, 'Kanim Kelas III Bekasi', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(213, 'Kanim Kelas III Kotabumi', 1010, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(214, 'Kanim Kelas II Kalianda', 1010, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(215, 'Kanim Kelas III Kediri', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(216, 'Kanim Kelas III Pamekasan', 1016, '0000-00-00 00:00:00', '', '2018-10-13 22:03:34', NULL, NULL),
(217, 'Kanim Kelas III Labuan Bajo', 1016, '0000-00-00 00:00:00', '', '2018-10-13 22:03:45', NULL, NULL),
(218, 'Kanim Kelas III Takengon', 1016, '0000-00-00 00:00:00', '', '2018-10-13 22:03:54', NULL, NULL),
(219, 'ULP Wilayah I Pondok Pinang', 1012, '0000-00-00 00:00:00', '', '2018-10-13 22:04:18', NULL, NULL),
(220, 'ULP Wilayah II Karang Tengah', 1012, '0000-00-00 00:00:00', '', '2018-10-13 22:04:12', NULL, NULL),
(221, 'Unit Pelayanan Paspor Surabaya', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(222, 'Unit Pelayanan Paspor Jakarta ', 1012, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(223, 'Unit Pelayanan Paspor Bandung', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(224, 'Kanim Kelas III Putussibau', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(225, 'Kanim Kelas III Wakatobi', 1029, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(226, 'Kanim Kelas III Baubau', 1029, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(227, 'Kanim Klas III Kotamobagu', 1024, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(228, 'Kanim Kelas III Banggai', 1026, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(229, 'LTSP Nunukan', 1020, '0000-00-00 00:00:00', '', '2018-10-13 22:05:00', NULL, NULL),
(230, 'Unit Layanan Paspor Kualanamu', 1002, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(231, 'Unit Layanan Paspor Tangerang', 1011, '0000-00-00 00:00:00', '', '2018-10-13 22:05:16', NULL, NULL),
(232, 'Unit Layanan Paspor Alauddin Makassar', 1028, '0000-00-00 00:00:00', '', '2018-10-13 22:05:20', NULL, NULL),
(233, 'Unit Layanan Paspor Banjarmasin', 1019, '0000-00-00 00:00:00', '', '2018-10-13 22:05:23', NULL, NULL),
(234, 'Unit Layanan Paspor Semarang', 1014, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(235, 'Unit Layanan Paspor Angke', 1012, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(236, 'Kanim Kelas III Ponorogo', 1016, '0000-00-00 00:00:00', '', '2018-10-13 22:05:36', NULL, NULL),
(237, 'Kanim Kelas III Palopo', 1028, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(238, 'Kanim Kelas III Kerinci', 1006, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(239, 'Kanim Kelas III Bima', 1022, '0000-00-00 00:00:00', '', '2018-10-13 22:05:42', NULL, NULL),
(240, 'Kanim Kelas III Ketapang', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(241, 'Unit Layanan Paspor Tanjung Perak', 1016, '0000-00-00 00:00:00', '', '2018-10-13 22:05:46', NULL, NULL),
(242, 'Unit Layanan Paspor Mataram', 1022, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(243, 'Unit Layanan Paspor Samarinda', 1020, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(244, 'Unit Layanan Paspor Jember', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(245, 'Unit Layanan Paspor Pematang Siantar', 1002, '0000-00-00 00:00:00', '', '2018-10-13 22:05:58', NULL, NULL),
(246, 'LTSP Karawang', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(247, 'LTSP Indramayu', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(248, 'LTSP Sukabumi', 1013, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(249, 'LTSP Lombok Tengah', 1022, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(250, 'LTSP Tanjung Pinang', 1004, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(251, 'LTSP Entikong', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(252, 'LTSP Sambas', 1017, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(253, 'LTSP Lombok Timur', 1022, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(254, 'LTSP Sumbawa', 1022, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(255, 'LTSP Surabaya', 1016, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(509, 'KBRI BANDAR SERI BEGAWAN', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(510, 'KBRI BANGKOK', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(511, 'KBRI BEIJING', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(514, 'KBRI BERLIN', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(523, 'KBRI CANBERRA', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(531, 'KBRI DARWIN', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(532, 'KJRI DAVAO CITY', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(533, 'KBRI DEN HAAG', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(535, 'KBRI DILI', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(538, 'KBRI DUBAI', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(541, 'KJRI GUANGZHOU', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(543, 'KBRI HANOI', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(548, 'KJRI HONGKONG', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(552, 'KJRI JEDDAH', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(553, 'KJRI JOHOR BAHRU', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(555, 'KBRI KAIRO', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(561, 'KBRI KUALA LUMPUR', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(562, 'KJRI KUCHING', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(568, 'KJRI LOS ANGELES', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(570, 'KBRI MANILA', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(572, 'KBRI MELBOURNE', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(586, 'KBRI PENANG', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(587, 'KBRI PERTH', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(596, 'KBRI RIYADH', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(602, 'KBRI SINGAPURA', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(607, 'KJRI SYDNEY', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(608, 'KDEI TAIPEI', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(610, 'KJRI TAWAO', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(612, 'KBRI TOKYO', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(622, 'KBRI WELLINGTON', 0, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_kategori_aset`
--

CREATE TABLE `ms_kategori_aset` (
  `id_kategori_aset` int(10) NOT NULL,
  `nama_kategori_aset` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_kategori_aset`
--

INSERT INTO `ms_kategori_aset` (`id_kategori_aset`, `nama_kategori_aset`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Aset Berwujud', '2018-10-01 15:32:11', '0', '2018-10-01 15:41:26', NULL, NULL),
(2, 'Aset Tak Berwujud', '2018-10-01 15:32:32', '0', '2018-10-01 15:41:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_kategori_pengadaan`
--

CREATE TABLE `ms_kategori_pengadaan` (
  `id_kategori_pengadaan` int(10) UNSIGNED NOT NULL,
  `nama_kategori_pengadaan` varchar(100) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_kategori_pengadaan`
--

INSERT INTO `ms_kategori_pengadaan` (`id_kategori_pengadaan`, `nama_kategori_pengadaan`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Lelang Umum', '2018-09-25 11:22:38', NULL, NULL, NULL, NULL),
(2, 'Lelang Cepat', '2018-09-27 10:00:12', NULL, NULL, NULL, NULL),
(3, 'e-Purchasing', '2018-09-27 10:00:29', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_kontainer`
--

CREATE TABLE `ms_kontainer` (
  `id_kontainer` int(10) UNSIGNED NOT NULL,
  `id_lokasi` int(10) UNSIGNED NOT NULL,
  `id_jenis_kontainer` int(10) UNSIGNED NOT NULL,
  `nama_kontainer` varchar(100) NOT NULL,
  `jumlah_slot` int(10) DEFAULT NULL,
  `keterangan_kontainer` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_kontainer`
--

INSERT INTO `ms_kontainer` (`id_kontainer`, `id_lokasi`, `id_jenis_kontainer`, `nama_kontainer`, `jumlah_slot`, `keterangan_kontainer`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 1, 2, 'Rack_Astagfirullah', NULL, '111', '0000-00-00 00:00:00', '', '2018-10-01 18:00:52', '18', NULL),
(2, 1, 3, 'Rack_Istiqomah', 2, '111', '0000-00-00 00:00:00', '', '2018-08-21 09:22:57', '18', NULL),
(3, 4, 1, 'Lemari Dekat Pak Cipto', NULL, NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(5, 2, 3, 'Rack Descartes', 4, 'pojokan', '2018-08-21 09:16:39', '18', NULL, NULL, NULL),
(6, 2, 3, 'Rack Galileo', 5, 'tengah2', '2018-08-21 09:17:00', '18', NULL, NULL, NULL),
(7, 1, 2, 'coba', 1, 'tempat duit', '2018-08-23 09:56:56', '18', NULL, NULL, NULL),
(8, 1, 2, 'yeyeyeye', NULL, 'abc', '2018-08-23 10:44:06', '18', '2018-08-23 11:36:53', '18', '2018-08-23 04:36:53'),
(9, 1, 1, 'tes', NULL, 'a', '2018-08-23 11:48:47', '18', NULL, NULL, NULL),
(10, 3, 1, 'tessss', NULL, 'dhadb', '2018-08-23 14:01:21', '18', NULL, NULL, NULL),
(11, 3, 2, 'yayaya', NULL, 'ay', '2018-08-23 14:01:34', '18', NULL, NULL, NULL),
(12, 1, 4, 'RACK_ARJUNA', NULL, 'YAS', '2018-10-01 18:04:36', '18', '2018-10-01 18:04:53', NULL, '2018-10-01 11:04:53'),
(13, 13, 2, '123123', NULL, 'TESSSSST', '2018-10-01 18:05:37', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_lokasi`
--

CREATE TABLE `ms_lokasi` (
  `id_lokasi` int(10) UNSIGNED NOT NULL,
  `nama_lokasi` varchar(100) NOT NULL,
  `keterangan_lokasi` varchar(500) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_lokasi`
--

INSERT INTO `ms_lokasi` (`id_lokasi`, `nama_lokasi`, `keterangan_lokasi`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Pusdakim Lt 9', 'gedung ki', '0000-00-00 00:00:00', '', '2018-08-23 09:24:05', '18', NULL),
(2, 'DC Lt 2', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(3, 'DRC', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(4, 'Sistik Lt 2', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(7, 'kantin aa', 'beli makan minum', '2018-08-21 10:59:44', '18', '2018-08-21 11:00:01', '18', '2018-08-21 04:00:01'),
(8, 'tess', 'tesssss', '2018-08-21 14:11:17', '18', '2018-08-21 15:13:26', NULL, '2018-08-21 08:13:26'),
(9, 'tes back edit', 'asa', '2018-08-21 14:57:06', '18', '2018-08-21 14:58:35', '18', '2018-08-21 07:58:35'),
(10, 'arhgads', 'arhdh', '2018-08-21 14:58:41', '18', '2018-08-21 15:12:57', NULL, '2018-08-21 08:12:57'),
(11, 'tess', 'aSEGA', '2018-08-21 15:34:44', '18', '2018-08-23 08:40:22', NULL, '2018-08-23 01:40:22'),
(12, 'AAAAB', 'aaa', '2018-08-23 11:04:36', '18', '2018-08-23 11:04:56', '18', '2018-08-23 04:04:56'),
(13, 'TEST', 'TESSSSST', '2018-10-01 18:05:02', '18', '2018-10-01 18:05:56', NULL, '2018-10-01 11:05:56'),
(14, '1111', '1111', '2018-10-01 18:06:07', '18', '2018-10-16 10:42:42', '18', '2018-10-16 10:42:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_menu`
--

CREATE TABLE `ms_menu` (
  `id_menu` bigint(20) NOT NULL,
  `title` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  `route` varchar(1000) CHARACTER SET utf8 DEFAULT '#',
  `deskripsi` varchar(8000) CHARACTER SET utf8 DEFAULT NULL,
  `icon` varchar(100) CHARACTER SET utf8 NOT NULL,
  `atribut` varchar(4000) CHARACTER SET utf8 DEFAULT NULL,
  `id_parent` bigint(20) DEFAULT '0',
  `posisi` enum('0','1','2') CHARACTER SET utf8 NOT NULL DEFAULT '0' COMMENT '0 = Left Menu, 1 = Top Right Menu',
  `auth` enum('0','1','2') CHARACTER SET utf8 DEFAULT NULL COMMENT '0 = Both (Login/Non Login dapat diakses), 1 = Harus Login untuk mengakses, 2 = Dapat diakses jika tidak Login',
  `status` enum('0','1') CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '0 = Non aktif, 1 = Aktif',
  `urutan` int(11) DEFAULT NULL,
  `role` enum('All','Admin','PPK','Timtik') DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_menu`
--

INSERT INTO `ms_menu` (`id_menu`, `title`, `route`, `deskripsi`, `icon`, `atribut`, `id_parent`, `posisi`, `auth`, `status`, `urutan`, `role`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Aplikasi SIMKIM', 'aplikasi', NULL, 'fa fa-globe', NULL, 12, '0', '0', '1', 21, 'Timtik', '2018-08-24 11:30:24', NULL, '2018-10-29 18:58:42', '18', NULL),
(2, 'Aset SIMKIM', 'aset', NULL, 'fa fa-laptop', NULL, 27, '0', '0', '1', 17, 'PPK', '2018-08-24 11:30:24', NULL, '2018-10-29 18:58:31', '18', NULL),
(3, 'License SIMKIM', 'lisensi', NULL, 'fa fa-id-card', NULL, 12, '0', '0', '0', 0, '', '2018-08-24 11:30:24', NULL, '2018-10-29 18:51:54', '18', NULL),
(4, 'Pengaturan', '#', NULL, 'fa fa-cogs', NULL, 0, '0', '0', '1', 10, 'Admin', '2018-08-24 11:30:24', NULL, '2018-10-29 18:53:16', NULL, NULL),
(5, 'Dashboard Kontainer', 'dashboard_kontainer', NULL, 'fa fa-building', NULL, 4, '0', '0', '1', 11, 'Admin', '2018-08-24 11:30:24', NULL, '2018-10-29 18:53:26', NULL, NULL),
(6, 'Jenis Aset', 'jenis_aset', NULL, 'fa fa-podcast', NULL, 4, '0', '0', '1', 12, 'Admin', '2018-08-24 11:30:24', NULL, '2018-10-29 18:53:29', NULL, NULL),
(7, 'Jenis Pengembangan', 'jenis_pengembangan', NULL, 'fa fa-desktop', NULL, 4, '0', '0', '1', 13, 'Admin', '2018-08-24 11:30:24', NULL, '2018-10-29 18:53:37', NULL, NULL),
(8, 'Pengadaan', 'pengadaan', NULL, 'fa fa-clipboard', NULL, 0, '0', '0', '1', 18, 'PPK', '2018-08-24 11:30:24', NULL, '2018-10-29 18:58:35', NULL, NULL),
(9, 'Kantor', 'kantor', NULL, 'fa fa-building-o', NULL, 4, '0', '0', '1', 14, 'Admin', '2018-08-24 11:30:24', NULL, '2018-10-29 18:53:51', NULL, NULL),
(10, 'Konfigurasi Menu', 'menu', NULL, 'fa fa-sliders', NULL, 4, '0', '0', '1', 15, 'Admin', '2018-08-24 11:30:24', NULL, '2018-10-29 18:53:58', NULL, NULL),
(11, 'Manajemen Aset', 'manajemenAset', NULL, 'fa fa-stack-overflow', NULL, 0, '0', NULL, '1', 19, 'PPK', '2018-09-04 09:49:35', '17', '2018-10-29 18:58:38', '17', NULL),
(12, 'Dokumentasi', '#', NULL, 'fa fa-book', NULL, 0, '0', NULL, '1', 20, 'Timtik', '2018-09-27 08:41:52', '18', '2018-10-29 18:58:39', NULL, NULL),
(13, 'Laporan', '#', NULL, 'fa fa-bar-chart', NULL, 0, '0', NULL, '1', 23, 'Timtik', '2018-10-12 09:50:39', '18', '2018-10-29 18:58:45', NULL, NULL),
(14, 'Laporan Aset Masuk', 'laporanAsetMasuk', NULL, 'fa fa-archive', NULL, 13, '0', NULL, '1', 24, 'Timtik', '2018-10-12 09:51:56', '18', '2018-10-29 18:58:49', NULL, NULL),
(15, 'Laporan Aset Keluar', 'laporanAsetKeluar', NULL, 'fa fa-paper-plane', NULL, 13, '0', NULL, '1', 25, 'Timtik', '2018-10-12 09:53:28', '18', '2018-10-29 18:58:51', NULL, NULL),
(16, 'Laporan Aset Stok', 'laporanAsetStok', NULL, 'fa fa-window-restore ', NULL, 13, '0', NULL, '0', 0, '', '2018-10-12 09:53:57', '18', '2018-10-29 18:54:31', NULL, NULL),
(17, 'Laporan Persebaran Aset', 'laporanPersebaranAset', NULL, 'fa fa-building ', NULL, 13, '0', NULL, '1', 26, 'Timtik', '2018-10-12 09:54:51', '18', '2018-10-29 18:58:55', NULL, NULL),
(18, 'Aplikasi SIMKIM', 'aplikasi', NULL, 'fa fa-globe', NULL, 28, '0', NULL, '1', 2, 'Admin', '2018-10-29 18:00:34', NULL, '2018-10-29 18:52:07', NULL, NULL),
(19, 'Pengadaan', 'pengadaan', NULL, 'fa fa-clipboard', NULL, 0, '0', NULL, '1', 4, 'Admin', '2018-10-29 18:32:43', NULL, '2018-10-29 18:52:19', NULL, NULL),
(20, 'Manajemen Aset', 'manajemenAset', NULL, 'fa fa-stack-overflow', NULL, 0, '0', NULL, '1', 5, 'Admin', '2018-10-29 18:33:24', NULL, '2018-10-29 18:52:28', NULL, NULL),
(21, 'Aset SIMKIM', 'aset', NULL, 'fa fa-laptop', NULL, 28, '0', NULL, '1', 3, 'Admin', '2018-10-29 18:34:01', NULL, '2018-10-29 18:52:15', NULL, NULL),
(22, 'Laporan Aset Masuk', 'laporanAsetMasuk', NULL, 'fa fa-archive', NULL, 30, '0', NULL, '1', 7, 'Admin', '2018-10-29 18:34:53', NULL, '2018-10-29 18:58:04', NULL, NULL),
(23, 'Laporan Aset Keluar', 'laporanAsetKeluar', NULL, 'fa fa-paper-plane', NULL, 30, '0', NULL, '1', 8, 'Admin', '2018-10-29 18:35:25', NULL, '2018-10-29 18:58:09', NULL, NULL),
(25, 'Manajemen Aset', 'manajemenAset', NULL, 'fa fa-stack-overflow', NULL, 0, '0', NULL, '1', 22, 'Timtik', '2018-10-29 18:36:53', NULL, '2018-10-31 11:58:59', NULL, NULL),
(26, 'Laporan Persebaran Aset', 'laporanPersebaranAset', NULL, 'fa fa-building', NULL, 30, '0', NULL, '1', 9, 'Admin', '2018-10-29 18:37:29', NULL, '2018-10-29 18:58:14', NULL, NULL),
(27, 'Dokumentasi', '#', NULL, 'fa fa-book', NULL, 0, '0', NULL, '1', 16, 'PPK', '2018-10-29 18:43:21', NULL, '2018-10-29 18:58:29', NULL, NULL),
(28, 'Dokumentasi', '#', NULL, 'fa fa-book', NULL, 0, '0', NULL, '1', 1, 'Admin', '2018-10-29 18:43:38', NULL, '2018-10-29 18:52:04', NULL, NULL),
(30, 'Laporan', '#', NULL, 'fa fa-bar-chart', NULL, 0, '0', NULL, '1', 6, 'Admin', '2018-10-29 18:44:18', NULL, '2018-10-29 18:58:02', NULL, NULL),
(31, 'Manajemen Akun', 'akun', NULL, 'fa fa-address-book', NULL, 4, '0', NULL, '0', 16, 'Admin', '2018-10-30 16:46:01', '18', '2018-11-11 17:58:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_merek_aset`
--

CREATE TABLE `ms_merek_aset` (
  `id_merek` int(10) UNSIGNED NOT NULL,
  `nama_merek` varchar(100) NOT NULL,
  `keterangan_merek` varchar(500) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_merek_aset`
--

INSERT INTO `ms_merek_aset` (`id_merek`, `nama_merek`, `keterangan_merek`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'Lenovo', '', '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(2, 'IBM', '', '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(3, 'Dell', '', '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(4, 'aaaa', '', '2018-08-23 16:12:01', '18', '2018-11-11 07:57:06', '18', '2018-08-23 09:12:09'),
(5, 'HP', '', '2018-10-01 18:13:13', '18', '2018-11-11 07:57:08', NULL, NULL),
(6, 'Vertiv', '', '2018-11-11 07:57:17', '18', NULL, NULL, NULL),
(7, 'Proofpoint', '', '2018-11-11 08:56:32', NULL, NULL, NULL, NULL),
(8, '3M', '', '2018-11-11 09:50:19', NULL, NULL, NULL, NULL),
(9, 'APC', '', '2018-11-11 09:59:21', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_penyedia`
--

CREATE TABLE `ms_penyedia` (
  `id_penyedia` int(10) UNSIGNED NOT NULL,
  `nama_penyedia` varchar(100) NOT NULL,
  `alamat_penyedia` varchar(500) DEFAULT NULL,
  `notelp_penyedia` varchar(50) DEFAULT NULL,
  `email_penyedia` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_penyedia`
--

INSERT INTO `ms_penyedia` (`id_penyedia`, `nama_penyedia`, `alamat_penyedia`, `notelp_penyedia`, `email_penyedia`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 'PT. Docotel Teknologi', 'Jl. KH. Hasyim Ashari No.26, RT.1/RW.4,\r\nPetojo Utara, Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10130', '+62216302626', 'info@docotel.com', '0000-00-00 00:00:00', '', '2018-11-11 13:42:12', '18', NULL),
(2, 'PT. Pro Sistimatika Automasi', 'Komp Ruko Roxy Mas Blok D2 No.28, Jl. KH. Hasyim Ashari, RW.8, Cideng, Gambir, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10150', '(021) 63860306', 'customerservice@prosia.com', '0000-00-00 00:00:00', '', '2018-10-16 10:51:58', '19', NULL),
(3, 'aa', 'aa', 'a34', '', '2018-08-24 10:25:57', '18', '2018-08-24 10:31:38', NULL, '2018-08-24 03:31:38'),
(4, 'PT. Jaya Teknik Indonesia', 'Jl. Johar, RT.5/RW.3, Kb. Sirih, Menteng, Kota Jakarta Pusat, Daerah Khusus Ibukota Jakarta 10340', '(021) 23555999', 'jayateknikindonesia@gmail.com', '2018-10-16 10:52:47', '19', NULL, NULL, NULL),
(5, 'PT. Telekomunikasi Indonesia', 'Jl. Prof Dr Soepomo SH No. 139 Selong Kebayoran Baru Jakarta Selatan DKI Jakarta, RT.4/RW.1, Karet, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12920', '(021) 83701300', 'info@telkom.co.id', '2018-10-16 10:53:25', '19', '2018-11-11 13:42:02', NULL, NULL),
(6, 'PT. Bagus Harapan Tritunggal', NULL, NULL, NULL, '2018-11-11 13:42:09', NULL, '2018-11-11 13:42:28', NULL, NULL),
(7, 'PT. Signet Pratama', NULL, NULL, NULL, '2018-11-11 13:42:35', NULL, NULL, NULL, NULL),
(8, 'PT. Nusantara Compnet Integrator', NULL, NULL, NULL, '2018-11-11 13:43:02', NULL, NULL, NULL, NULL),
(9, 'PT. IMP', NULL, NULL, NULL, '2018-11-11 13:43:09', NULL, NULL, NULL, NULL),
(10, 'PT. Bertiga Mitra Solusi', NULL, NULL, NULL, '2018-11-11 13:43:32', NULL, NULL, NULL, NULL),
(11, 'PT. LEN (Persero)', NULL, NULL, NULL, '2018-11-11 13:43:38', NULL, '2018-11-11 13:43:43', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_posisi_kontainer`
--

CREATE TABLE `ms_posisi_kontainer` (
  `id_posisi_kontainer` int(10) UNSIGNED NOT NULL,
  `id_kontainer` int(10) UNSIGNED NOT NULL,
  `label_posisi_kontainer` varchar(50) NOT NULL,
  `keterangan_posisi_kontainer` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ms_posisi_kontainer`
--

INSERT INTO `ms_posisi_kontainer` (`id_posisi_kontainer`, `id_kontainer`, `label_posisi_kontainer`, `keterangan_posisi_kontainer`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 1, 'Slot 1', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(2, 1, 'Slot 2', NULL, '0000-00-00 00:00:00', '', '2018-08-21 08:47:15', NULL, NULL),
(3, 2, 'Sekat Ab', 'ww', '0000-00-00 00:00:00', '', '2018-08-21 10:25:03', '18', '2018-08-21 03:25:03'),
(4, 5, 'Atas', 'qq', '2018-08-21 10:24:27', '18', NULL, NULL, NULL),
(5, 5, 'Tengah', 'qq', '2018-08-21 10:24:37', '18', NULL, NULL, NULL),
(6, 5, 'Bawah', 'qq', '2018-08-21 10:24:47', '18', NULL, NULL, NULL),
(7, 9, 'slot a', 'a', '2018-08-23 11:59:17', '18', NULL, NULL, NULL),
(8, 9, 'slot c', 'dfg', '2018-08-23 12:01:35', '18', '2018-08-23 12:07:08', '18', '2018-08-23 05:07:08'),
(9, 9, 'slot b', 'EGFQ', '2018-08-23 12:08:18', '18', '2018-08-23 12:08:24', NULL, '2018-08-23 05:08:24'),
(10, 3, 'aaa', NULL, '2018-08-23 14:10:20', '18', NULL, NULL, NULL),
(11, 9, 'gg', NULL, '2018-08-23 14:24:09', '18', '2018-08-23 14:24:15', NULL, '2018-08-23 07:24:15'),
(12, 10, 'fbzdg', NULL, '2018-08-23 15:00:07', '18', NULL, NULL, NULL),
(13, 1, 'Slot 3', 'aaaa', '2018-10-01 18:02:07', '18', '2018-10-01 18:04:47', '18', '2018-10-01 11:04:47');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengadaan`
--

CREATE TABLE `pengadaan` (
  `kode_pengadaan` int(10) UNSIGNED NOT NULL,
  `id_penyedia` int(10) UNSIGNED NOT NULL,
  `id_kategori_pengadaan` int(10) UNSIGNED NOT NULL,
  `nama_pengadaan` varchar(500) NOT NULL,
  `nomor_pengadaan` varchar(100) DEFAULT NULL,
  `tahun_pengadaan` int(10) UNSIGNED NOT NULL,
  `kode_anggaran` varchar(100) DEFAULT NULL,
  `nilai_anggaran` decimal(50,2) UNSIGNED NOT NULL,
  `nama_ppk` varchar(50) DEFAULT NULL,
  `tanggal_mulai_kontrak` datetime DEFAULT NULL,
  `tanggal_selesai_kontrak` datetime DEFAULT NULL,
  `keterangan_pengadaan` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengadaan`
--

INSERT INTO `pengadaan` (`kode_pengadaan`, `id_penyedia`, `id_kategori_pengadaan`, `nama_pengadaan`, `nomor_pengadaan`, `tahun_pengadaan`, `kode_anggaran`, `nilai_anggaran`, `nama_ppk`, `tanggal_mulai_kontrak`, `tanggal_selesai_kontrak`, `keterangan_pengadaan`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(4, 5, 3, 'Paket Pengadaan Perangkat Pendukung Data Discovery Center (DRC)', '1.9191.238.0023.IMI.4939', 2018, 'UG.932.IASJ.KEMENKUMHAM/2018', '8900000000.00', 'IMAN SYAFRIZAL', '2018-11-11 00:00:00', '2019-11-11 00:00:00', NULL, '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(5, 5, 3, 'Paket Pengadaan Jaringan Pendukung Data Discovery Center (DRC)', '1.9191.238.0023.IMI.4939', 2018, 'UG.932.IASJ.KEMENKUMHAM/2018', '8900000000.00', 'IMAN SYAFRIZAL', '2018-11-11 00:00:00', '2019-11-11 00:00:00', 'Paket Pengadaan Jaringan Pendukung Data Discovery Center (DRC)', '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(7, 4, 1, 'Paket Pengadaan Kontainmen Data Center Keimigrasian', 'IMI.IMI.18.10.11-UM/2018/LU', 2018, '12009121092', '5000000000000000.00', 'IMAN SYAFRIZAL', '2018-11-11 00:00:00', '2019-11-11 00:00:00', NULL, '2018-11-11 12:57:45', '22', NULL, NULL, NULL),
(8, 2, 2, 'Paket Pengadaan Sistem Pengamanan Teknologi Informasi', 'IMI.1.PL.06.02-19492', 2018, '12893410122', '7500000000.00', 'IMAN SYAFRIZAL', '2018-11-11 00:00:00', '2019-11-11 00:00:00', NULL, '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(9, 5, 1, 'Paket Pengadaan Sewa Jaringan Komunikasi Saluran II', '90909090990090', 2019, 'asc23', '919191919.00', 'HANDWIYUTO', '2019-03-03 00:00:00', '2019-08-03 00:00:00', 'ASASASA', '2019-03-03 10:41:18', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengadaan_item`
--

CREATE TABLE `pengadaan_item` (
  `id_item` int(10) NOT NULL,
  `kode_pengadaan` int(10) NOT NULL,
  `id_aset` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengadaan_item`
--

INSERT INTO `pengadaan_item` (`id_item`, `kode_pengadaan`, `id_aset`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 4, 8, '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(2, 4, 9, '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(3, 4, 13, '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(4, 4, 10, '2018-11-11 12:18:01', '22', NULL, NULL, NULL),
(5, 5, 11, '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(6, 5, 12, '2018-11-11 12:36:22', '22', NULL, NULL, NULL),
(7, 6, 5, '2018-11-11 12:43:27', '22', NULL, NULL, NULL),
(8, 7, 5, '2018-11-11 12:57:45', '22', NULL, NULL, NULL),
(9, 7, 6, '2018-11-11 12:57:45', '22', NULL, NULL, NULL),
(10, 7, 7, '2018-11-11 12:57:45', '22', NULL, NULL, NULL),
(11, 8, 1, '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(12, 8, 2, '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(13, 8, 3, '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(14, 8, 4, '2018-11-11 13:07:28', '22', NULL, NULL, NULL),
(15, 9, 12, '2019-03-03 10:41:18', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tim_pengembang`
--

CREATE TABLE `tim_pengembang` (
  `id_pengembang` int(10) UNSIGNED NOT NULL,
  `id_aplikasi` int(10) UNSIGNED NOT NULL,
  `nama_pengembang` varchar(100) NOT NULL,
  `nik_pengembang` varchar(50) NOT NULL,
  `jabatan_pengembang` varchar(200) NOT NULL,
  `nohp_pengembang` varchar(50) NOT NULL,
  `email_pengembang` varchar(100) NOT NULL,
  `keterangan_pengembang` varchar(500) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(50) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tim_pengembang`
--

INSERT INTO `tim_pengembang` (`id_pengembang`, `id_aplikasi`, `nama_pengembang`, `nik_pengembang`, `jabatan_pengembang`, `nohp_pengembang`, `email_pengembang`, `keterangan_pengembang`, `created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`) VALUES
(1, 1, 'Bagus Kurniadi', '3275671200293440', 'Project Manager', '091546789099', 'bagus.kurniadi@imp.net', '-', '2018-11-11 13:47:40', '18', NULL, NULL, NULL),
(2, 1, 'Vincent Abimanyu', '6701890921009231', 'Programmer', '087890335432', 'abi.v.89@imp.net', '-', '2018-11-11 13:48:25', '18', NULL, NULL, NULL),
(3, 1, 'Tata Arsyidanta', '15750617125601901', 'Network Engineer', '089576800430', 'tata.arsyidanta@imp.net', '-', '2018-11-11 13:53:51', '18', NULL, NULL, NULL),
(4, 2, 'Berthi Mustika', '3275910271008101', 'Project Manager', '081283839092', 'berthi.mustika@gmail.com', '-', '2018-11-11 17:57:24', '18', NULL, NULL, NULL),
(5, 2, 'Tasril Shiddiq', '458193602839017371', 'System Analyst', '0856819011231', 'tasril.shiddiq@gmail.com', '-', '2018-11-11 17:58:06', '18', NULL, NULL, NULL),
(6, 2, 'Maulina Damayanti', '61527397460919001', 'System Analyst', '08785362819101', 'maulina.damayanti@gmail.com', '-', '2018-11-11 17:59:29', '18', NULL, NULL, NULL),
(7, 2, 'Rina Rahmawati', '3819123912301209342', 'Programmer', '08181917492010', 'rinrina.rahma@gmail.com', NULL, '2018-11-11 18:00:05', '18', NULL, NULL, NULL),
(8, 2, 'Hendy Dwi Harfianto', '327506160395001', 'Programmer', '08571158540', 'hendydwih@gmail.com', '-', '2018-11-11 18:00:30', '18', NULL, NULL, NULL),
(9, 6, 'Hendy Dwi Harfianto', '3275061603950010', 'Programmer', '08571158540', 'hendydwih@gmail.com', '-', '2018-11-11 18:19:46', '18', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('Admin','PPK','Timtik') COLLATE utf8mb4_unicode_ci NOT NULL,
  `provider` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_login` enum('Y','N') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_code` varchar(1024) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `provider`, `provider_id`, `remember_token`, `allow_login`, `activation_code`, `status`, `created_at`, `updated_at`) VALUES
(17, 'Rina Rahmawati', 'rinrina.rahma@gmail.com', '$2y$10$vqY6/KF1IKKNN7i.SSTebOnLWOEFf28hzc/1iTM6esWElDbtvqGpG', 'Admin', NULL, NULL, 'LX4BBAjxqJuD6VR8SfiJfSkRbWYuGu0w4egfzLO7JpKAbegwPjCXh1PNUs0w', NULL, NULL, '0', '2018-08-14 21:20:21', '2018-10-31 09:55:13'),
(18, 'admin', 'admin@gmail.com', '$2y$10$GFr3SQq/ddQQlpsubypFkeLY0JJKjc9DJaS8SONBogbqT4cJxX5mK', 'Admin', NULL, NULL, 'wCTwXloQPF5kNYwo5W0F17Mp2OrfeDDVsV4KGymRo4g0zGGEK5vjb6c3LbZh', NULL, NULL, '0', '2018-08-15 19:22:21', '2018-08-15 19:22:21'),
(19, 'Hendy Dwi Harfianto', 'hendyharf@gmail.com', '$2y$10$vf/YwYb.Vj4tN73yaoeUC.Q..P.x.j/kezVjbrAOPbNJ5Lib4gKJe', 'Admin', NULL, NULL, '0JVZQlOCmBFLlNDoPU2TzeYP5glUGF6ynLXPnzojZvHiYdiVL5n7DeWPRebP', NULL, NULL, '0', '2018-10-16 01:40:52', '2018-10-16 01:40:52'),
(20, 'Nadiah Indriyani', 'nadiah@gmail.com', '$2y$10$G9jrXzAsIn4X4PKmPdhcQeFBd/6g3iybrss4ABx1Ufb.Enq0fLYke', 'Admin', NULL, NULL, 'TUFoSIOdJ19QC9JkZg2sMyMIYS7D0PlL4WuQwMAIYHlt6NBfgeuRC1ndLBX9', NULL, NULL, '0', '2018-10-31 04:08:31', '2018-10-31 04:08:31'),
(21, 'Tasril Shiddiq', 'tasril@gmail.com', '$2y$10$MeUwDgbT8b05DuVzsPzCIuc0YQZLSGi.9QiHv4r4BYJcDUt8p79j6', 'Timtik', NULL, NULL, '2Ggd29hqQIAMrBdUhKS8QvEcUI0KRippkL5HZhKkIHS5sUgBvHIqLnVMPcxh', NULL, NULL, '0', '2018-10-31 04:18:22', '2018-10-31 04:56:02'),
(22, 'Marni Indriyati', 'marni@gmail.com', '$2y$10$YkNpxfoPaRviRcvkK/A4tOvLLjRTC853KzrtBqzi3ETyoHiMV6Gsu', 'PPK', NULL, NULL, 'qaVVVlqVdOAvASOwBadkABETw8w3lBqiEpvtN48hmAMPfOzwcgdhwEQSuiBu', NULL, NULL, '0', '2018-10-31 04:20:50', '2018-10-31 04:20:50'),
(23, 'Rudini', 'rudini@gmail.com', '$2y$10$8E/ek90g342EQlHi6lSI0uxJrhY/6wess06tYZcJklsuqjS9/w9pe', 'Timtik', NULL, NULL, 'HF9KGe9KAVE93effuVFq2bFnaxsbxiJiRdrSvT49EbOi0c2OeDk9wIZizInF', NULL, NULL, '0', '2018-10-31 04:48:46', '2018-10-31 04:48:46');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `alamat`
--
ALTER TABLE `alamat`
  ADD PRIMARY KEY (`id_alamat`);

--
-- Indeks untuk tabel `aplikasi`
--
ALTER TABLE `aplikasi`
  ADD PRIMARY KEY (`id_aplikasi`);

--
-- Indeks untuk tabel `aplikasi_admin`
--
ALTER TABLE `aplikasi_admin`
  ADD PRIMARY KEY (`id_administrator`);

--
-- Indeks untuk tabel `aplikasi_sourcecode`
--
ALTER TABLE `aplikasi_sourcecode`
  ADD PRIMARY KEY (`id_sourcecode`);

--
-- Indeks untuk tabel `aplikasi_versi`
--
ALTER TABLE `aplikasi_versi`
  ADD PRIMARY KEY (`id_versi`);

--
-- Indeks untuk tabel `aset`
--
ALTER TABLE `aset`
  ADD PRIMARY KEY (`id_aset`);

--
-- Indeks untuk tabel `aset_informasi`
--
ALTER TABLE `aset_informasi`
  ADD PRIMARY KEY (`id_aset_informasi`);

--
-- Indeks untuk tabel `aset_keluar`
--
ALTER TABLE `aset_keluar`
  ADD PRIMARY KEY (`id_aset_keluar`);

--
-- Indeks untuk tabel `aset_masuk`
--
ALTER TABLE `aset_masuk`
  ADD PRIMARY KEY (`id_aset_masuk`);

--
-- Indeks untuk tabel `aset_satuan`
--
ALTER TABLE `aset_satuan`
  ADD PRIMARY KEY (`id_satuan`);

--
-- Indeks untuk tabel `aset_satuan_log`
--
ALTER TABLE `aset_satuan_log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indeks untuk tabel `aset_spesifikasi`
--
ALTER TABLE `aset_spesifikasi`
  ADD PRIMARY KEY (`id_spesifikasi`);

--
-- Indeks untuk tabel `aset_stok`
--
ALTER TABLE `aset_stok`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indeks untuk tabel `aset_stok_riwayat`
--
ALTER TABLE `aset_stok_riwayat`
  ADD PRIMARY KEY (`id_riwayat`);

--
-- Indeks untuk tabel `dokumentasi`
--
ALTER TABLE `dokumentasi`
  ADD PRIMARY KEY (`id_dokumentasi`);

--
-- Indeks untuk tabel `gambar`
--
ALTER TABLE `gambar`
  ADD PRIMARY KEY (`id_gambar`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `ms_datatype`
--
ALTER TABLE `ms_datatype`
  ADD PRIMARY KEY (`id_datatype`);

--
-- Indeks untuk tabel `ms_datatype_parameter`
--
ALTER TABLE `ms_datatype_parameter`
  ADD PRIMARY KEY (`id_parameter`);

--
-- Indeks untuk tabel `ms_datatype_parameter_mapping`
--
ALTER TABLE `ms_datatype_parameter_mapping`
  ADD PRIMARY KEY (`id_mapping`);

--
-- Indeks untuk tabel `ms_field_aset_informasi`
--
ALTER TABLE `ms_field_aset_informasi`
  ADD PRIMARY KEY (`id_field_info`);

--
-- Indeks untuk tabel `ms_field_spesifikasi_aset`
--
ALTER TABLE `ms_field_spesifikasi_aset`
  ADD PRIMARY KEY (`id_field`);

--
-- Indeks untuk tabel `ms_jenis_aset`
--
ALTER TABLE `ms_jenis_aset`
  ADD PRIMARY KEY (`id_jenis_aset`);

--
-- Indeks untuk tabel `ms_jenis_kontainer`
--
ALTER TABLE `ms_jenis_kontainer`
  ADD PRIMARY KEY (`id_jenis_kontainer`);

--
-- Indeks untuk tabel `ms_jenis_pengembangan`
--
ALTER TABLE `ms_jenis_pengembangan`
  ADD PRIMARY KEY (`id_jenis_pengembangan`);

--
-- Indeks untuk tabel `ms_kantor_imigrasi`
--
ALTER TABLE `ms_kantor_imigrasi`
  ADD PRIMARY KEY (`kode_kanim`);

--
-- Indeks untuk tabel `ms_kategori_aset`
--
ALTER TABLE `ms_kategori_aset`
  ADD PRIMARY KEY (`id_kategori_aset`);

--
-- Indeks untuk tabel `ms_kategori_pengadaan`
--
ALTER TABLE `ms_kategori_pengadaan`
  ADD PRIMARY KEY (`id_kategori_pengadaan`);

--
-- Indeks untuk tabel `ms_kontainer`
--
ALTER TABLE `ms_kontainer`
  ADD PRIMARY KEY (`id_kontainer`);

--
-- Indeks untuk tabel `ms_lokasi`
--
ALTER TABLE `ms_lokasi`
  ADD PRIMARY KEY (`id_lokasi`);

--
-- Indeks untuk tabel `ms_menu`
--
ALTER TABLE `ms_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indeks untuk tabel `ms_merek_aset`
--
ALTER TABLE `ms_merek_aset`
  ADD PRIMARY KEY (`id_merek`);

--
-- Indeks untuk tabel `ms_penyedia`
--
ALTER TABLE `ms_penyedia`
  ADD PRIMARY KEY (`id_penyedia`);

--
-- Indeks untuk tabel `ms_posisi_kontainer`
--
ALTER TABLE `ms_posisi_kontainer`
  ADD PRIMARY KEY (`id_posisi_kontainer`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pengadaan`
--
ALTER TABLE `pengadaan`
  ADD PRIMARY KEY (`kode_pengadaan`);

--
-- Indeks untuk tabel `pengadaan_item`
--
ALTER TABLE `pengadaan_item`
  ADD PRIMARY KEY (`id_item`);

--
-- Indeks untuk tabel `tim_pengembang`
--
ALTER TABLE `tim_pengembang`
  ADD PRIMARY KEY (`id_pengembang`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `alamat`
--
ALTER TABLE `alamat`
  MODIFY `id_alamat` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `aplikasi`
--
ALTER TABLE `aplikasi`
  MODIFY `id_aplikasi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `aplikasi_admin`
--
ALTER TABLE `aplikasi_admin`
  MODIFY `id_administrator` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `aplikasi_sourcecode`
--
ALTER TABLE `aplikasi_sourcecode`
  MODIFY `id_sourcecode` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `aplikasi_versi`
--
ALTER TABLE `aplikasi_versi`
  MODIFY `id_versi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `aset`
--
ALTER TABLE `aset`
  MODIFY `id_aset` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `aset_informasi`
--
ALTER TABLE `aset_informasi`
  MODIFY `id_aset_informasi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT untuk tabel `aset_keluar`
--
ALTER TABLE `aset_keluar`
  MODIFY `id_aset_keluar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT untuk tabel `aset_masuk`
--
ALTER TABLE `aset_masuk`
  MODIFY `id_aset_masuk` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT untuk tabel `aset_satuan`
--
ALTER TABLE `aset_satuan`
  MODIFY `id_satuan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT untuk tabel `aset_satuan_log`
--
ALTER TABLE `aset_satuan_log`
  MODIFY `id_log` int(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `aset_spesifikasi`
--
ALTER TABLE `aset_spesifikasi`
  MODIFY `id_spesifikasi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=147;

--
-- AUTO_INCREMENT untuk tabel `aset_stok`
--
ALTER TABLE `aset_stok`
  MODIFY `id_stok` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `aset_stok_riwayat`
--
ALTER TABLE `aset_stok_riwayat`
  MODIFY `id_riwayat` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT untuk tabel `dokumentasi`
--
ALTER TABLE `dokumentasi`
  MODIFY `id_dokumentasi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT untuk tabel `gambar`
--
ALTER TABLE `gambar`
  MODIFY `id_gambar` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ms_datatype`
--
ALTER TABLE `ms_datatype`
  MODIFY `id_datatype` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `ms_datatype_parameter`
--
ALTER TABLE `ms_datatype_parameter`
  MODIFY `id_parameter` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `ms_datatype_parameter_mapping`
--
ALTER TABLE `ms_datatype_parameter_mapping`
  MODIFY `id_mapping` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `ms_field_aset_informasi`
--
ALTER TABLE `ms_field_aset_informasi`
  MODIFY `id_field_info` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `ms_field_spesifikasi_aset`
--
ALTER TABLE `ms_field_spesifikasi_aset`
  MODIFY `id_field` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT untuk tabel `ms_jenis_aset`
--
ALTER TABLE `ms_jenis_aset`
  MODIFY `id_jenis_aset` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `ms_jenis_kontainer`
--
ALTER TABLE `ms_jenis_kontainer`
  MODIFY `id_jenis_kontainer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `ms_jenis_pengembangan`
--
ALTER TABLE `ms_jenis_pengembangan`
  MODIFY `id_jenis_pengembangan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `ms_kantor_imigrasi`
--
ALTER TABLE `ms_kantor_imigrasi`
  MODIFY `kode_kanim` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=623;

--
-- AUTO_INCREMENT untuk tabel `ms_kategori_aset`
--
ALTER TABLE `ms_kategori_aset`
  MODIFY `id_kategori_aset` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `ms_kategori_pengadaan`
--
ALTER TABLE `ms_kategori_pengadaan`
  MODIFY `id_kategori_pengadaan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `ms_kontainer`
--
ALTER TABLE `ms_kontainer`
  MODIFY `id_kontainer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `ms_lokasi`
--
ALTER TABLE `ms_lokasi`
  MODIFY `id_lokasi` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `ms_menu`
--
ALTER TABLE `ms_menu`
  MODIFY `id_menu` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `ms_merek_aset`
--
ALTER TABLE `ms_merek_aset`
  MODIFY `id_merek` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `ms_penyedia`
--
ALTER TABLE `ms_penyedia`
  MODIFY `id_penyedia` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `ms_posisi_kontainer`
--
ALTER TABLE `ms_posisi_kontainer`
  MODIFY `id_posisi_kontainer` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `pengadaan`
--
ALTER TABLE `pengadaan`
  MODIFY `kode_pengadaan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `pengadaan_item`
--
ALTER TABLE `pengadaan_item`
  MODIFY `id_item` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tim_pengembang`
--
ALTER TABLE `tim_pengembang`
  MODIFY `id_pengembang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
