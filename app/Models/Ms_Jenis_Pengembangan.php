<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Jenis_Pengembangan extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

  protected $table = 'ms_jenis_pengembangan';
  public $timestamps = false;

  protected $guarded = ['id_jenis_pengembangan'];


}
