<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;

class Aset_Keluar extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'aset_keluar';
	public $timestamps = false;

	protected $guarded = ['id_aset_keluar'];


	public static function getLastNomorAsetKeluar()
	{
		$last_nomor_aset_keluar = DB::table('aset_keluar')
			->select('nomor_aset_keluar')
			->orderBy('nomor_aset_keluar', 'desc')
			->first();
		// dd($last_nomor_aset_keluar);
	    if ($last_nomor_aset_keluar) return $last_nomor_aset_keluar;
	    else return 0;
	}

	public static function getAsetKeluar($periode_dari, $periode_sampai, $nomor_aset_keluar, $skip, $take)
	{
		$list_aset_keluar = DB::table('aset_keluar as ake')
			->leftJoin('ms_kantor_imigrasi as msk', 'msk.kode_kanim', '=', 'ake.kode_kanim')
			->selectRaw('ake.nomor_aset_keluar, ake.jenis_aset_keluar, sum(ake.jumlah_aset_keluar) as jumlah_aset_keluar, ake.tanggal_aset_keluar, ake.lokasi_keluar_lain, ake.tanggal_aset_kembali, ake.keterangan_aset_keluar, msk.kode_kanim, msk.nama_kanim')
			->groupBy('ake.nomor_aset_keluar', 'ake.jenis_aset_keluar', 'ake.tanggal_aset_keluar', 'ake.lokasi_keluar_lain', 'ake.tanggal_aset_kembali', 'ake.keterangan_aset_keluar', 'msk.kode_kanim', 'msk.nama_kanim')
			->orderBy('ake.nomor_aset_keluar', 'desc');

		if ($periode_dari != null) $list_aset_keluar->whereBetween('ake.tanggal_aset_keluar', [$periode_dari, $periode_sampai]);
		if ($nomor_aset_keluar) $list_aset_keluar->where('ake.nomor_aset_keluar', '=', $nomor_aset_keluar);

		$list_aset_keluar = $list_aset_keluar->distinct()->get()->toArray();
		$count = count($list_aset_keluar);
		$list_aset_keluar = array_slice($list_aset_keluar, $skip, $take);

		if ($count != 0) return [$count, $list_aset_keluar];
		else return null;
	}

	public static function getItemAsetKeluar($nomor_aset_keluar)
	{
		$list_item_aset_keluar = DB::table('aset_keluar as ake')
			->leftJoin('aset as a', 'ake.id_aset', '=', 'a.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->leftJoin('gambar as g', function($join){
				$join->on('a.id_aset', '=', 'g.src_fk_gambar')
					 ->where('g.src_tabel_gambar', '=', 'aset')
					 ->where('g.src_kolom_gambar', '=', 'id_aset')
					 ->where('g.gambar_utama', '=', 'Y');
			})
			->leftJoin('aset_stok as aso', 'ake.id_stok', '=', 'aso.id_stok')
			->selectRaw('ake.id_aset_keluar, ake.id_aset, ake.jumlah_aset_keluar, a.tipe_aset, mma.nama_merek, mja.nama_jenis_aset, mka.nama_kategori_aset, g.nama_gambar, g.link_gambar, g.keterangan_gambar , aso.satuan_stok')
			->where('ake.nomor_aset_keluar', '=', $nomor_aset_keluar)
			->orderBy('mka.nama_kategori_aset', 'asc')
			->orderBy('mma.nama_merek', 'asc')
			->orderBy('a.tipe_aset', 'asc')
			->get();

	    if ($list_item_aset_keluar) return $list_item_aset_keluar;
	    else return null;
	}


	public static function getSatuanAsetKeluar($id_aset_keluar, $id_aset)
	{
	    $list_satuan_aset_keluar = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')	
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->where('ake.id_aset_keluar', '=', $id_aset_keluar)
			->where('asa.id_aset', '=', $id_aset)
			->selectRaw('asa.id_satuan as asa_id_satuan, asa.kode_grup_satuan as asa_kode_grup_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.kode_aset_informasi as asa_kode_aset_informasi, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, ake.keterangan_aset_keluar as ake_keterangan_aset_keluar, coalesce(asm.harga_satuan, "-") as asm_harga_satuan')
			->get();
	    if ($list_satuan_aset_keluar) return $list_satuan_aset_keluar;
	    else return null;
	}

	public static function getPersebaranAset($kanim, $skip, $take)
	{
		$result_kanim = DB::table('ms_kantor_imigrasi as mki')
			->leftJoin('aset_keluar as ake', 'mki.kode_kanim', '=', 'ake.kode_kanim')
			->selectRaw('coalesce(mki.kode_kanim, null) as kode_kanim, coalesce(mki.nama_kanim, "Lain-lain") as nama_kanim, coalesce(sum(ake.jumlah_aset_keluar), 0) as jumlah_persebaran_aset')
			->groupBy('mki.kode_kanim', 'mki.nama_kanim')
			->orderBy('mki.nama_kanim', 'asc');

		if ($kanim != null) $result_kanim->where('mki.nama_kanim', 'like', '%' . $kanim . '%');

		$result_lokasi_lain = DB::table('aset_keluar')
			->selectRaw('coalesce(kode_kanim, 0) as kode_kanim, "Lain-lain" as nama_kanim, coalesce(sum(jumlah_aset_keluar), 0) as jumlah_persebaran_aset')
			->where('kode_kanim', '=', null)
			->groupBy('kode_kanim');

		$list_persebaran_aset = $result_kanim->union($result_lokasi_lain)->get()->toArray();

		$count = count($list_persebaran_aset);
		$list_persebaran_aset = array_slice($list_persebaran_aset, $skip, $take);
		if ($count != 0) return [$count, $list_persebaran_aset];
		else return null;
	}

	public static function getPersebaranAsetDetail($kode_kanim)
	{
		$list_persebaran_aset = '';
		if($kode_kanim != 0)
		{
			$list_persebaran_aset = DB::table('ms_kantor_imigrasi as mki')
				->leftJoin('aset_keluar as ake', 'mki.kode_kanim', '=', 'ake.kode_kanim')
				->where('mki.kode_kanim', '=', $kode_kanim)
				->selectRaw('coalesce(mki.kode_kanim, null) as kode_kanim, coalesce(mki.nama_kanim, "Lain-lain") as nama_kanim, coalesce(sum(ake.jumlah_aset_keluar), 0) as jumlah_persebaran_aset, ake.nomor_aset_keluar')
				->groupBy('mki.kode_kanim', 'mki.nama_kanim', 'ake.nomor_aset_keluar')
				->orderBy('mki.nama_kanim', 'asc')
				->distinct()
				->get();
		}
		else
		{
			$list_persebaran_aset = DB::table('aset_keluar')
				->where('kode_kanim', '=', null)
				->selectRaw('nomor_aset_keluar, lokasi_keluar_lain as nama_kanim, coalesce(sum(jumlah_aset_keluar), 0) as jumlah_persebaran_aset')
				->groupBy('nomor_aset_keluar', 'lokasi_keluar_lain')
				->distinct()
				->get();
		}

	    if ($list_persebaran_aset) return $list_persebaran_aset;
	    else return null;
	}

	public static function getItemPersebaranAsetDetail($kode_kanim)
	{
		$list_item_persebaran_aset = '';
		if($kode_kanim != 0)
		{
			$list_item_persebaran_aset = DB::table('aset_keluar as ake')
				->leftJoin('aset as a', 'ake.id_aset', '=', 'a.id_aset')
				->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
				->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
				->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
				->leftJoin('gambar as g', function($join){
					$join->on('a.id_aset', '=', 'g.src_fk_gambar')
						 ->where('g.src_tabel_gambar', '=', 'aset')
						 ->where('g.src_kolom_gambar', '=', 'id_aset')
						 ->where('g.gambar_utama', '=', 'Y');
				})
				->leftJoin('aset_stok as aso', 'ake.id_stok', '=', 'aso.id_stok')
				->selectRaw('a.id_aset, ake.id_aset_keluar, ake.nomor_aset_keluar, ake.jumlah_aset_keluar, a.tipe_aset, mma.nama_merek, mja.nama_jenis_aset, mka.nama_kategori_aset, g.nama_gambar, g.link_gambar, g.keterangan_gambar, aso.satuan_stok, ake.kode_kanim, ake.jenis_aset_keluar, ake.tanggal_aset_keluar')
				->where('ake.kode_kanim', '=', $kode_kanim)
				->orderBy('mka.nama_kategori_aset', 'asc')
				->orderBy('mma.nama_merek', 'asc')
				->orderBy('a.tipe_aset', 'asc')
				->get();
		}
		else
		{
			$list_item_persebaran_aset = DB::table('aset_keluar as ake')
				->leftJoin('aset as a', 'ake.id_aset', '=', 'a.id_aset')
				->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
				->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
				->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
				->leftJoin('gambar as g', function($join){
					$join->on('a.id_aset', '=', 'g.src_fk_gambar')
						 ->where('g.src_tabel_gambar', '=', 'aset')
						 ->where('g.src_kolom_gambar', '=', 'id_aset')
						 ->where('g.gambar_utama', '=', 'Y');
				})
				->leftJoin('aset_stok as aso', 'ake.id_stok', '=', 'aso.id_stok')
				->selectRaw('a.id_aset, ake.id_aset_keluar, ake.nomor_aset_keluar, ake.jumlah_aset_keluar, a.tipe_aset, mma.nama_merek, mja.nama_jenis_aset, mka.nama_kategori_aset, g.nama_gambar, g.link_gambar, g.keterangan_gambar, aso.satuan_stok, ake.kode_kanim, ake.jenis_aset_keluar, ake.tanggal_aset_keluar')
				->where('ake.kode_kanim', '=', null)
				->orderBy('mka.nama_kategori_aset', 'asc')
				->orderBy('mma.nama_merek', 'asc')
				->orderBy('a.tipe_aset', 'asc')
				->get();
		}

	    if ($list_item_persebaran_aset) return $list_item_persebaran_aset;
	    else return null;
	}

	public static function getSatuanAsetPersebaran($id_aset_keluar, $id_aset)
	{
		//HERE
		$list_satuan_aset_persebaran = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')	
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->where('ake.id_aset_keluar', '=', $id_aset_keluar)
			->where('asa.id_aset', '=', $id_aset)
			->selectRaw('asa.id_satuan as asa_id_satuan, asa.kode_grup_satuan as asa_kode_grup_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.kode_aset_informasi as asa_kode_aset_informasi, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, ake.keterangan_aset_keluar as ake_keterangan_aset_keluar, coalesce(asm.harga_satuan, "-") as asm_harga_satuan')
			->get();
	    if ($list_satuan_aset_persebaran) return $list_satuan_aset_persebaran;
	    else return null;
	}

	public static function getDownloadItemAsetKeluar($nomor_aset_keluar)
	{	
		$list_download_item_aset_keluar = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')	
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset as a', 'asa.id_aset', '=', 'a.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->where('ake.nomor_aset_keluar', '=', $nomor_aset_keluar)
			->select('asa.id_satuan as asa_id_satuan', 'asa.kode_pengadaan as asa_kode_pengadaan', 'asa.id_item as asa_id_item', 'asa.id_aset as asa_id_aset', 'asa.id_aset_masuk as asa_id_aset_masuk', 'asa.id_aset_keluar as asa_id_aset_keluar', 'asa.id_lokasi as asa_id_lokasi', 'mlok_asa.nama_lokasi as asa_nama_lokasi', 'asa.id_kontainer as asa_id_kontainer', 'mkon_asa.nama_kontainer as asa_nama_kontainer', 'asa.id_posisi_kontainer as asa_id_posisi_kontainer', 'mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer', 'asa.field_aset as asa_field_aset', 'asa.isian_aset as asa_isian_aset', 'ake.id_aset_keluar as ake_id_aset_keluar', 'ake.kode_kanim as ake_kode_kanim', 'kanim.nama_kanim as ake_nama_kanim', 'ake.jumlah_aset_keluar as ake_jumlah_aset_keluar', 'ake.jenis_aset_keluar as ake_jenis_aset_keluar', 'ake.tanggal_aset_keluar as ake_tanggal_aset_keluar', 'ake.tanggal_aset_kembali as ake_tanggal_aset_kembali', 'ake.lokasi_keluar_lain as ake_lokasi_keluar_lain', 'ake.keterangan_aset_keluar as ake_keterangan_aset_keluar', 'a.tipe_aset', 'mja.nama_jenis_aset', 'mka.nama_kategori_aset', 'mma.nama_merek')
			->get();
	    if ($list_download_item_aset_keluar) return $list_download_item_aset_keluar;
	    else return null;
	}

	public static function getDownloadItemPersebaranAset($kode_kanim)
	{	
		$list_item_persebaran_aset = '';
		if($kode_kanim != 0)
		{
			$list_item_persebaran_aset = DB::table('aset_keluar as ake')
				->leftJoin('aset as a', 'ake.id_aset', '=', 'a.id_aset')
				->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
				->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
				->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
				->leftJoin('aset_stok as aso', 'ake.id_stok', '=', 'aso.id_stok')
				->leftJoin('aset_satuan as ast', 'ake.id_aset_keluar', '=', 'ast.id_aset_keluar')
				->leftJoin('ms_kantor_imigrasi as mki', 'ake.kode_kanim', '=', 'mki.kode_kanim')
				->selectRaw('a.id_aset, ake.id_aset_keluar, ake.nomor_aset_keluar, a.tipe_aset, mma.nama_merek, mja.nama_jenis_aset, mka.nama_kategori_aset, aso.satuan_stok, ake.kode_kanim, mki.nama_kanim, ake.jenis_aset_keluar, ake.tanggal_aset_keluar, ast.field_aset, ast.isian_aset')
				->where('ake.kode_kanim', '=', $kode_kanim)
				->orderBy('mka.nama_kategori_aset', 'asc')
				->orderBy('mma.nama_merek', 'asc')
				->orderBy('a.tipe_aset', 'asc')
				->distinct()
				->get();
		}
		else
		{
			$list_item_persebaran_aset = DB::table('aset_keluar as ake')
				->leftJoin('aset as a', 'ake.id_aset', '=', 'a.id_aset')
				->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
				->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
				->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
				->leftJoin('aset_stok as aso', 'ake.id_stok', '=', 'aso.id_stok')
				->leftJoin('aset_satuan as ast', 'ake.id_aset_keluar', '=', 'ast.id_aset_keluar')
				->selectRaw('a.id_aset, ake.id_aset_keluar, ake.nomor_aset_keluar, a.tipe_aset, mma.nama_merek, mja.nama_jenis_aset, mka.nama_kategori_aset, aso.satuan_stok, ake.kode_kanim, ake.jenis_aset_keluar, ake.lokasi_keluar_lain, ake.tanggal_aset_keluar, ast.field_aset, ast.isian_aset')
				->where('ake.kode_kanim', '=', null)
				->orderBy('ake.lokasi_keluar_lain', 'asc')
				->orderBy('mka.nama_kategori_aset', 'asc')
				->orderBy('mma.nama_merek', 'asc')
				->orderBy('a.tipe_aset', 'asc')
				->distinct()
				->get();
		}

	    if ($list_item_persebaran_aset) return $list_item_persebaran_aset;
	    else return null;
	}
}
