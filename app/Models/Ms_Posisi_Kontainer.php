<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Posisi_Kontainer extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];
    
	protected $table = 'ms_posisi_kontainer';
	public $timestamps = false;

	protected $guarded = ['id_posisi_kontainer'];

  	public static function getPosisiKontainer($id_kontainer)
	{
		$list_posisi_kontainer = DB::table('ms_posisi_kontainer')->where('id_kontainer', '=', $id_kontainer)
						->orderBy('id_posisi_kontainer', 'asc')->get();

		if($list_posisi_kontainer) return $list_posisi_kontainer;
		else return null; 
	}
}
