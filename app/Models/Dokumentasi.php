<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Dokumentasi extends Model
{

	protected $table = 'dokumentasi';
	public $timestamps = false;

	protected $guarded = ['id_dokumentasi'];

	public static function getDokumentasiList($src_tabel_gambar, $src_kolom_gambar, $src_fk_gambar)
	{
		$list_dokumentasi = DB::table('dokumentasi as d')
			->leftJoin('ms_lokasi as msl', 'd.id_lokasi', '=', 'msl.id_lokasi')
			->leftJoin('ms_kontainer as msk', 'd.id_kontainer', '=', 'msk.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpk', 'd.id_posisi_kontainer', '=', 'mpk.id_posisi_kontainer')
			->selectRaw('d.id_dokumentasi, d.nama_dokumentasi, d.link_dokumentasi, d.tanggal_dokumentasi, d.keterangan_dokumentasi, d.ukuran_dokumentasi, msl.id_lokasi, msl.nama_lokasi, msk.id_kontainer, msk.nama_kontainer, mpk.id_posisi_kontainer, mpk.label_posisi_kontainer')
			->where('d.src_tabel_dokumentasi', '=', $src_tabel_gambar)
			->where('d.src_kolom_dokumentasi', '=', $src_kolom_gambar)
			->where('d.src_fk_dokumentasi', '=', $src_fk_gambar)
			->orderBy('d.nama_dokumentasi', 'asc')
			->get();

		if($list_dokumentasi) return $list_dokumentasi;
		else return null;
	}

	public static function getDokumentasi($id_dokumentasi)
	{
		$dokumentasi = DB::table('dokumentasi')
			->where('id_dokumentasi', '=', $id_dokumentasi)
			->first();

		if($dokumentasi) return $dokumentasi;
		else return null;
	}
}
