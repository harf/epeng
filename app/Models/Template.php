<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Template extends Model
{
  use SoftDeletes;
  protected $dates = ['deleted_at'];
  public $table = 'ms_template';
  
  protected $guarded = [];

   
  public static function getMsTemplateIsi()
  {
     $data = DB::table('ms_template_isi')
       ->orderBy('id', 'asc')->get();

      return $data;
  }

  public static function getMsTemplateByName($nama)
  {
     $data = DB::table('ms_template')
        ->where('template_nama','$nama')
        ->orderBy('id', 'asc')->get();

      return $data;
  }

  public static function getTemplateByIdWorkflow($id_workflow, $status_workflow)
  {
    $template = DB::table('ms_template')
      ->where('id_workflow', '=', $id_workflow)
      ->where('status_workflow', '=', $status_workflow)
      ->first();

    return $template;
  }
   
}
