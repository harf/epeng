<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Aset_Satuan extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'aset_satuan';
	public $timestamps = false;

	protected $guarded = ['id_satuan'];


	public static function getSatuanAset($id_aset, $id_jenis_aset)
	{
		$list_satuan_aset = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->leftJoin('pengadaan as p', 'asa.kode_pengadaan', '=', 'p.kode_pengadaan')
			->where('asa.id_aset', '=', $id_aset)
			->selectRaw('asa.id_satuan as asa_id_satuan, asa.kode_grup_satuan as asa_kode_grup_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, ake.keterangan_aset_keluar as ake_keterangan_aset_keluar, coalesce(asm.harga_satuan, "-") as asm_harga_satuan, p.kode_pengadaan as p_kode_pengadaan, p.nama_pengadaan as p_nama_pengadaan, p.tahun_pengadaan as p_tahun_pengadaan, p.nomor_pengadaan as p_nomor_pengadaan, p.kode_anggaran as p_kode_anggaran, p.nilai_anggaran as p_nilai_anggaran, p.nama_ppk as p_nama_ppk')
			->orderBy('p.nama_pengadaan', 'desc')
			->get();

	    if ($list_satuan_aset) return $list_satuan_aset;
	    else return null;
	}


	public static function getLastKodeGrupSatuan()
	{
		$last_kode_grup_satuan = DB::table('aset_satuan')
			->select('kode_grup_satuan')
			->orderBy('kode_grup_satuan', 'desc')
			->first();
	    if ($last_kode_grup_satuan) return $last_kode_grup_satuan;
	    else return 0;
	}

	public static function getKodeGrupSatuanById($id_satuan)
	{
		$kode_grup_satuan = DB::table('aset_satuan')
			->select('kode_grup_satuan')
			->where('id_satuan', '=', $id_satuan)
			->first();

		if($kode_grup_satuan) return $kode_grup_satuan;
		else return null;
	}

	public static function getSatuanAsetGrup($id_satuan)
	{
		$kode_grup = DB::table('aset_satuan')
			->select('kode_grup_satuan')
			->where('id_satuan', '=', $id_satuan)
			->first();

		// dd($id_satuan . ' -- ' . $kode_grup->kode_grup_satuan);

		$list_satuan_grup = DB::table('aset_satuan')
			->where('kode_grup_satuan', '=', $kode_grup->kode_grup_satuan)
			->get();

		if($list_satuan_grup) return $list_satuan_grup;
		else return null;
	}

	public static function getSatuanAsetByKodeGrup($kodeGrupId)
	{
		$satuan_aset = DB::table('aset_satuan as as')
			->leftJoin('pengadaan as p', 'as.kode_pengadaan', '=', 'p.kode_pengadaan')
			->leftJoin('aset as a', 'as.id_aset', '=', 'a.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->leftJoin('aset_masuk as am', 'as.id_aset_masuk', '=', 'am.id_aset_masuk')
			->selectRaw('mka.nama_kategori_aset, mja.nama_jenis_aset, mma.nama_merek, a.tipe_aset, p.nama_pengadaan, p.tahun_pengadaan, am.harga_satuan')
			->where('as.kode_grup_satuan', '=', $kodeGrupId)
			->first();
	    if ($satuan_aset) return $satuan_aset;
	    else return 0;
	}

	public static function getSatuanFieldIsianListByKodeGrup($kodeGrupId)
	{
		$field_isian_list = DB::table('aset_satuan')
			->selectRaw('id_satuan, field_aset, isian_aset')
			->where('kode_grup_satuan', '=', $kodeGrupId)
			->orderBy('id_satuan', 'asc')
			->get();
	    if ($field_isian_list) return $field_isian_list;
	    else return null;
	}
}
