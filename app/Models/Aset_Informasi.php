<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aset_Informasi extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'aset_informasi';
	public $timestamps = false;

	protected $guarded = ['id_aset_informasi'];

  	public static function getAsetInformasiList()
	{
		$list_aset_informasi = DB::table('aset as a')
			->leftJoin('aset_satuan as as', 'a.id_aset', '=', 'as.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->selectRaw('a.id_aset, mja.id_jenis_aset, mja.nama_jenis_aset, mma.nama_merek, a.tipe_aset, 0 as jumlah_satuan_aset, null as grup_satuan')
			->where('as.id_aset_keluar', '!=', null)
			->where('as.kode_aset_informasi', '=', null)
			->distinct()
			->groupBy('a.id_aset', 'mja.id_jenis_aset', 'mja.nama_jenis_aset', 'mma.nama_merek', 'a.tipe_aset')
			->orderBy('mma.nama_merek', 'asc')
			->orderBy('a.tipe_aset', 'asc')
			->get();

		for($i = 0 ; $i < count($list_aset_informasi) ; $i++)
		{
			$grup_satuan = DB::table('aset_satuan as as')
				->leftJoin('aset_keluar as ak', 'as.id_aset_keluar', '=', 'ak.id_aset_keluar')
				->leftJoin('ms_kantor_imigrasi as mk', 'ak.kode_kanim', '=', 'mk.kode_kanim')
				->where('as.id_aset', '=', $list_aset_informasi[$i]->id_aset)
				->where('as.id_aset_keluar', '!=', null)
				->where('as.kode_aset_informasi', '=', null)
				->selectRaw('as.kode_grup_satuan, mk.nama_kanim, ak.tanggal_aset_keluar, null as satuan_aset')
				->distinct()
				->get();

			for($j = 0 ; $j < count($grup_satuan) ; $j++)
			{
				$satuan_aset = DB::table('aset_satuan as as')
					->where('as.kode_grup_satuan', '=', $grup_satuan[$j]->kode_grup_satuan)
					->selectRaw('as.field_aset, as.isian_aset')
					->distinct()
					->get();
				$grup_satuan[$j]->satuan_aset = $satuan_aset;
			}

			$list_aset_informasi[$i]->grup_satuan = $grup_satuan;
			$list_aset_informasi[$i]->jumlah_satuan_aset = count($grup_satuan);
		}
		// dd($list_aset_informasi);
	    if ($list_aset_informasi) return $list_aset_informasi;
	    else return null;
	}

	public static function getLastKodeAsetInformasi()
	{
		$last_kode_aset_informasi = DB::table('aset_informasi')
			->select('kode_aset_informasi')
			->orderBy('kode_aset_informasi', 'desc')
			->first();
	    if ($last_kode_aset_informasi) return $last_kode_aset_informasi->kode_aset_informasi;
	    else return 0;
	}

	public static function getAsetInformasiListByKodeGrup($kode_grup_satuan)
	{
		$list_aset_informasi = DB::table('aset_informasi as ai')
			->leftJoin('ms_field_aset_informasi as mfai', 'ai.id_field_info', '=', 'mfai.id_field_info')
			->leftJoin('ms_datatype as md', 'mfai.id_datatype', '=', 'md.id_datatype')
			->selectRaw('mfai.id_field_info, mfai.id_jenis_aset, mfai.src_tabel_field_info, mfai.src_kolom_field_info, mfai.nama_field_info, md.kode_html_input, md.kode_html_show, ai.id_aset_informasi, ai.isian_field_informasi, ' . $kode_grup_satuan . ' as kode_grup_satuan, mfai.keterangan_field_info, ai.keterangan_isian')
			->distinct()
			->where('ai.kode_grup_satuan', '=', $kode_grup_satuan)
			->orderBy('mfai.id_field_info', 'asc')->get();

		for($j = 0 ; $j < count($list_aset_informasi) ; $j++)
		{
			$list_parameter = DB::table('ms_datatype_parameter')
				->where('src_tabel_parameter', '=', $list_aset_informasi[$j]->src_tabel_field_info)
				->where('src_kolom_parameter', '=', $list_aset_informasi[$j]->src_kolom_field_info)
				->where('src_fk_parameter', '=', $list_aset_informasi[$j]->id_field_info)
				->selectRaw('id_parameter, parameter, replacement')
				->distinct()
				->get();

			for($k = 0 ; $k < count($list_parameter) ; $k++)
			{
				$list_aset_informasi[$j]->kode_html_input = str_replace($list_parameter[$k]->parameter, $list_parameter[$k]->replacement, $list_aset_informasi[$j]->kode_html_input);
				$list_aset_informasi[$j]->kode_html_show = str_replace($list_parameter[$k]->parameter, $list_parameter[$k]->replacement, $list_aset_informasi[$j]->kode_html_show);
				$list_mapping = DB::table('ms_datatype_parameter_mapping')
					->where('id_parameter', '=', $list_parameter[$k]->id_parameter)
					->selectRaw('replacement, src_tabel_mapping, src_kolom_mapping')
					->distinct()
					->get();

				for($l = 0 ; $l < count($list_mapping) ; $l++)
				{
					$src = $list_mapping[$l]->src_kolom_mapping;
					$list_aset_informasi[$j]->kode_html_show = str_replace($list_mapping[$l]->replacement, $list_aset_informasi[$j]->{$src}, $list_aset_informasi[$j]->kode_html_show);
					$list_aset_informasi[$j]->kode_html_input = str_replace($list_mapping[$l]->replacement, $list_aset_informasi[$j]->{$src}, $list_aset_informasi[$j]->kode_html_input);
				}
			}
		}

	    if ($list_aset_informasi) return $list_aset_informasi;
	    else return 0;
	}

	public static function getFileInfo($id_aset_info)
	{
		$aset_info = DB::table('aset_informasi')
			->where('id_aset_informasi', '=', $id_aset_info)
			->first();

		if($aset_info) return $aset_info;
		else return null;
	}

	public static function getIdFieldInfo($id_aset_informasi)
	{
		$aset_info = DB::table('aset_informasi')
			->where('id_aset_informasi', '=', $id_aset_informasi)
			->select('id_field_info')
			->first();

		if($aset_info) return $aset_info;
		else return null;
	}
}
