<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aplikasi_Admin extends Model
{

  protected $table = 'aplikasi_admin';
  public $timestamps = false;

  protected $guarded = ['id_administrator'];


}
