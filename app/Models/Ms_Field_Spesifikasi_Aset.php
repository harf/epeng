<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Field_Spesifikasi_Aset extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'ms_field_spesifikasi_aset';
	public $timestamps = false;

	protected $guarded = ['id_field_spek'];

  	public static function getFieldSpesifikasi($id_jenis_aset)
	{
		$list_field_spesifikasi = DB::table('ms_field_spesifikasi_aset')->where('id_jenis_aset', '=', $id_jenis_aset)
						->orderBy('id_field', 'asc')->get();

		if($list_field_spesifikasi) return $list_field_spesifikasi;
		else return null;
	}
}
