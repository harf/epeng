<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Pengadaan extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'pengadaan';
	public $timestamps = false;

	protected $guarded = ['kode_pengadaan'];


	public static function getAllPengadaan($filter1,$filter2,$keyword,$skip,$take)
	{
		$list_pengadaan = DB::table('pengadaan as p')
			->leftJoin('ms_penyedia as mp', 'p.id_penyedia', '=', 'mp.id_penyedia')
			->leftJoin('ms_kategori_pengadaan as mkp', 'p.id_kategori_pengadaan', '=', 'mkp.id_kategori_pengadaan')
			->whereNull('p.deleted_at')
			->select('p.kode_pengadaan', 'p.nama_pengadaan', 'p.nomor_pengadaan', 'p.tahun_pengadaan', 'p.kode_anggaran', 'p.nilai_anggaran', 'p.nama_ppk', 'mp.nama_penyedia', 'mkp.id_kategori_pengadaan', 'mkp.nama_kategori_pengadaan', 'p.tanggal_mulai_kontrak', 'p.tanggal_selesai_kontrak', 'p.keterangan_pengadaan', 'p.created_at', 'p.updated_at')
	        ->orderBy('p.kode_pengadaan', 'desc')
	        ->distinct();

	    // dd($list_pengadaan);
        if($filter1 != 0) {
			$list_pengadaan->where('mkp.id_kategori_pengadaan', '=', $filter1);
		}
		if($filter2 != 0) {
			$list_pengadaan->where('p.tahun_pengadaan', '=', $filter2);
		}
		if($keyword != null) {
			$list_pengadaan->where(function($query) use($keyword)
		    {
		        $query->where('p.nama_pengadaan', 'like', '%' . $keyword . '%')
					->orWhere('p.nomor_pengadaan', 'like', '%' . $keyword . '%')
					->orWhere('p.tahun_pengadaan', 'like', '%' . $keyword . '%')
					->orWhere('p.kode_anggaran', 'like', '%' . $keyword . '%')
					->orWhere('p.tanggal_mulai_kontrak', 'like', '%' . $keyword . '%')
					->orWhere('p.tanggal_selesai_kontrak', 'like', '%' . $keyword . '%')
					->orWhere('p.keterangan_pengadaan', 'like', '%' . $keyword . '%')
					->orWhere('mp.nama_penyedia', 'like', '%' . $keyword . '%')
					->orWhere('mkp.nama_kategori_pengadaan', 'like', '%' . $keyword . '%');
		    });	
		}

        $count = $list_pengadaan->count();
		$list_pengadaan = $list_pengadaan->take($take)->skip($skip)->get();
		if ($count != 0) return [$count, $list_pengadaan];
		else return null;
	}

	public static function getPengadaanByID($kode_pengadaan)
	{
		$pengadaan = DB:: table('pengadaan as p')
			->leftJoin('ms_penyedia as mp', 'p.id_penyedia', '=', 'mp.id_penyedia')
			->leftJoin('ms_kategori_pengadaan as mkp', 'p.id_kategori_pengadaan', '=', 'mkp.id_kategori_pengadaan')
			->where('p.kode_pengadaan', '=', $kode_pengadaan)
			->select('p.kode_pengadaan', 'p.nama_pengadaan', 'p.nomor_pengadaan', 'p.tahun_pengadaan', 'p.kode_anggaran', 'p.nilai_anggaran', 'p.nama_ppk', 'mp.id_penyedia', 'mp.nama_penyedia', 'mkp.id_kategori_pengadaan', 'mkp.nama_kategori_pengadaan', 'p.tanggal_mulai_kontrak', 'p.tanggal_selesai_kontrak', 'p.keterangan_pengadaan', 'p.created_at', 'p.updated_at')
			->first();
		if($pengadaan) return $pengadaan;
		else return null;
	}
}
