<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Kantor_Imigrasi extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

  protected $table = 'ms_kantor_imigrasi';
  public $timestamps = false;

  protected $guarded = ['kode_kanim'];


  public static function getKantorImigrasiByID($kode_kanim)
	{
		$result = DB::table('ms_kantor_imigrasi')
		  ->where('kode_kanim', '=', $kode_kanim)
		  ->first();
		if($result)
		  return $result;
		else
		  return null;
	}
}
