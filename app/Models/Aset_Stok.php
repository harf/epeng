<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aset_Stok extends Model
{

	protected $table = 'aset_stok';
	public $timestamps = false;

	protected $guarded = ['id_stok'];

	public static function getAsetStokByID($id_aset)
	{
		$aset_stok = DB::table('aset_stok')
						->where('id_aset', '=', $id_aset)
						->orderBy('id_stok', 'desc')
						->first();

		if($aset_stok) return $aset_stok;
		else return null;
	}

	public static function getAsetStokByID2($id_permintaan)
	{
		$list_aset_stok = DB::table('permintaan_item as pi')
			->leftJoin('aset_stok as ass', 'pi.id_aset', '=', 'ass.id_aset')
			->leftJoin('aset as a', 'pi.id_aset', '=', 'a.id_aset')
			->where('pi.id_permintaan', '=', $id_permintaan)
			->select('ass.id_aset','a.tipe_aset','ass.jumlah_stok')
			->orderBy('ass.id_aset', 'asc')
			->get();
	    if ($list_aset_stok) return $list_aset_stok;
	    else return null;
	}

}
