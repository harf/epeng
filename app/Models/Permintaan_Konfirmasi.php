<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;



class Permintaan_Konfirmasi extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'permintaan_item';
	public $timestamps = false;

	protected $guarded = ['id_konfirmasi'];

}
