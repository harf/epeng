<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aset_Satuan_Log extends Model
{
	protected $table = 'aset_satuan_log';
	public $timestamps = false;

	protected $guarded = ['id_satuan_log'];

	public static function getLogByKodeGrupId($kode_grup_satuan)
	{
		$list_log = DB::table('aset_satuan_log as asl')
			->leftJoin('ms_log as ml', 'asl.id_log', '=', 'ml.id_log')
			->where('asl.kode_grup_satuan', '=', $kode_grup_satuan)
			->orderBy('asl.created_at', 'asc')
			->get();

	    if ($list_log) return $list_log;
	    else return null;
	}
}