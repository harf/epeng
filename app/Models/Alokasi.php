<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Alokasi extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'alokasi';
	public $timestamps = false;

	protected $guarded = ['id_alokasi'];


	public static function getAllAlokasi($keyword,$skip,$take)
	{

		$list_alokasi = DB::table('alokasi as alo')
			->select('alo.id_alokasi', 'alo.id_permintaan', 'alo.id_prmnt_item', 'alo.id_jenis_alokasi', 'alo.kode_kanim', 'alo.nama_pejabat', 'alo.nip_pejabat', 'alo.pangkat_golongan_pejabat', 'alo.jabatan_pejabat', 'alo.tanggal_alokasi', 'alo.keterangan', 'alo.created_at', 'alo.created_by', 'alo.updated_at', 'alo.updated_by')
	        ->orderBy('alo.id_alokasi', 'asc')
	        ->distinct();

	    $count = $list_alokasi->count();
		$list_alokasi = $list_alokasi->take($take)->skip($skip)->get();
		if ($count != 0) return [$count, $list_alokasi];
		else return null;

	}


	public static function getAlokasiByID($id_alokasi)
	{

		$alokasi = DB::table('alokasi as alo')
			->select('alo.id_alokasi', 'alo.id_permintaan', 'alo.id_prmnt_item', 'alo.id_jenis_alokasi', 'alo.kode_kanim', 'alo.nama_pejabat', 'alo.nip_pejabat', 'alo.pangkat_golongan_pejabat', 'alo.jabatan_pejabat', 'alo.tanggal_alokasi', 'alo.keterangan', 'alo.created_at', 'alo.created_by', 'alo.updated_at', 'alo.updated_by')
			->where('alo.id_alokasi', '=', $id_alokasi)
			->first();
	    if ($alokasi) return $alokasi;
	    else return 0;

	}

}
