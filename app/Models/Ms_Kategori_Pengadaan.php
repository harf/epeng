<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Kategori_Pengadaan extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	protected $table = 'ms_kategori_pengadaan';
	public $timestamps = false;

	protected $guarded = ['id_kategori_pengadaan'];

	
	public static function getSingleKategoriPengadaan($id_kategori_pengadaan)
	{
		$kategori_pengadaan = DB::table('ms_kategori_pengadaan')
								->where('id_kategori_pengadaan', '=', $id_kategori_pengadaan)
								->first();
		
		if($kategori_pengadaan) return $kategori_pengadaan;
		else return null;
	}
}
