<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Jenis_Kontainer extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

  protected $table = 'ms_jenis_kontainer';
  public $timestamps = false;

  protected $guarded = ['id_jenis_kontainer'];

  public static function getbyID($id)
	{
		$result = DB::table('ms_jenis_kontainer')
		  ->where('id_jenis_kontainer', '=', $id)
		  ->first();
		if($result)
		  return $result;
		else
		  return null;
	}
}
