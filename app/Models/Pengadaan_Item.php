<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Pengadaan_Item extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'pengadaan_item';
	public $timestamps = false;

	protected $guarded = ['id_item'];

	public static function getItemPengadaan($kode_pengadaan)
	{
		$list_item_pengadaan = DB:: table('pengadaan_item as pi')
			->leftJoin('aset as a', 'pi.id_aset', '=', 'a.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('gambar as g', function($join){
				$join->on('a.id_aset', '=', 'g.src_fk_gambar')
					 ->where('g.src_tabel_gambar', '=', 'aset')
					 ->where('g.src_kolom_gambar', '=', 'id_aset');
			})
			->where('pi.kode_pengadaan', '=', $kode_pengadaan)
			->where('g.gambar_utama', '=', 'Y')
			->get();
		if($list_item_pengadaan) return $list_item_pengadaan;
		else return null;
	}

	public static function getSatuanItemPengadaan($kode_pengadaan, $id_aset)
	{
		$list_satuan_item_pengadaan = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->where('asa.kode_pengadaan', '=', $kode_pengadaan)
			->where('asa.id_aset', '=', $id_aset)
			->selectRaw('asa.id_satuan as asa_id_satuan, asa.kode_grup_satuan as asa_kode_grup_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.kode_aset_informasi as asa_kode_aset_informasi, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, ake.keterangan_aset_keluar as ake_keterangan_aset_keluar, coalesce(asm.harga_satuan, "-") as asm_harga_satuan')
			->get();

	    if ($list_satuan_item_pengadaan) return $list_satuan_item_pengadaan;
	    else return null;
	}
}
