<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aset extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'aset';
	public $timestamps = false;

  	protected $guarded = ['id_aset'];

  	public static function getTipeAset($id_jenis_aset, $id_merek)
	{
		$list_jenis_aset = DB::table('aset')->where('id_jenis_aset', '=', $id_jenis_aset)
						->where('id_merek', '=', $id_merek)
						->orderBy('id_aset', 'asc')->get();

		if($list_jenis_aset) return $list_jenis_aset;
		else return null;
	}

	public static function getAset($id_aset)
	{
		$aset = DB::table('aset as a')
				->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
				->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
				->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
				->selectRaw('a.id_aset, mja.id_jenis_aset, mma.id_merek, mka.id_kategori_aset, mma.nama_merek, a.tipe_aset, a.keterangan_aset, a.created_at, a.created_by, a.updated_at, a.updated_by, mja.nama_jenis_aset, mka.nama_kategori_aset')
				->where('a.id_aset', '=', $id_aset)
				->first();

		if($aset) return $aset;
		else return null;
	}

	public static function getManajemenAsetListIndex($filter,$keyword,$skip,$take)
	{
		$list_aset = DB::table('aset as a')
			->leftJoin('aset_stok as ast', 'a.id_aset', '=', 'ast.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->select('a.id_aset', 'a.tipe_aset', 'mja.id_jenis_aset', 'mja.nama_jenis_aset', 'mka.id_kategori_aset', 'mka.nama_kategori_aset', 'mma.id_merek', 'mma.nama_merek', 'ast.jumlah_stok', 'ast.satuan_stok', 'ast.tanggal_stok' )
			->orderBy('mma.nama_merek', 'asc')
			->orderBy('a.tipe_aset', 'asc');

		if($filter != 0) {
			$list_aset->where('mja.id_jenis_aset', '=', $filter);
		}
		if($keyword != null) {
			$list_aset_all = DB::table('aset as a')
				->leftJoin('aset_satuan as asa', 'a.id_aset', '=', 'asa.id_aset')
				->leftJoin('aset_informasi as ai', 'a.id_aset', '=', 'ai.id_aset')
				->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
				->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
				->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
				->where('asa.isian_aset', 'like', '%' . $keyword . '%')
				->orWhere('ai.isian_field_informasi', 'like', '%' . $keyword . '%')
				->select('a.id_aset')
				->distinct()
				->get()
				->toArray();

			$list_id_aset = implode(",", array_column($list_aset_all, 'id_aset'));
			$list_aset->whereIn('a.id_aset', explode(',', $list_id_aset));
		}

	    $count = $list_aset->count();
		$list_aset = $list_aset->take($take)->skip($skip)->get();
		if ($count != 0) return [$count, $list_aset];
		else return null;
	}

	public static function getAsetListIndex($filter,$keyword,$skip, $take)
	{
		$list_aset_index = DB::table('aset as a')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_merek_aset as mer', 'a.id_merek', '=', 'mer.id_merek')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('gambar as g', function($join){
				$join->on('a.id_aset', '=', 'g.src_fk_gambar')
					 ->where('g.src_tabel_gambar', '=', 'aset')
					 ->where('g.src_kolom_gambar', '=', 'id_aset');
			})
			->where('g.gambar_utama', '=', 'Y')
			->whereNull('a.deleted_at')
			->orderBy('a.created_at', 'desc');
		if($filter != 0) {
			$list_aset_index->where('mja.id_jenis_aset', '=', $filter);
		}
		if($keyword != null) {

			$list_aset_index->where(function($query) use($keyword)
		    {
		        $query->where('mer.nama_merek', 'like', '%' . $keyword . '%')
					->orWhere('mja.nama_jenis_aset', 'like', '%' . $keyword . '%')
					->orWhere('mka.nama_kategori_aset', 'like', '%' . $keyword . '%')
					->orWhere('a.tipe_aset', 'like', '%' . $keyword . '%');
		    });	
		}

		$count = $list_aset_index->count();
		$list_aset_index = $list_aset_index->take($take)->skip($skip)->get();
		if ($count != 0) return [$count, $list_aset_index];
		else return null;
	}

	public static function getSumberAset($id_aset)
	{
		$list_aset = DB::table('aset_masuk as asm')
			->leftJoin('pengadaan as p', 'p.kode_pengadaan', '=', 'asm.kode_pengadaan')
			->leftJoin('aset_stok as ast', 'asm.id_stok', '=', 'ast.id_stok')
			->leftJoin('aset as a', 'ast.id_aset', '=', 'a.id_aset')
			->where('a.id_aset', '=', $id_aset)
			->selectRaw('coalesce(p.kode_pengadaan, 0) as kode_pengadaan, coalesce(p.nama_pengadaan, "Non Pengadaan") as nama_pengadaan, coalesce(p.tahun_pengadaan, "-") as tahun_pengadaan, sum(asm.jumlah_aset_masuk) as jumlah_aset_masuk')
			->groupBy('p.kode_pengadaan', 'p.nama_pengadaan', 'p.tahun_pengadaan')
			->orderBy('p.tahun_pengadaan', 'desc')
			->get();
	    if ($list_aset) return $list_aset;
	    else return null;
	}

	//Get Satuan Aset on the Manajemen Aset page
	public static function getSatuanAsetPengadaan($id_aset, $kode_pengadaan)
	{
		$list_satuan = null;
		if ($kode_pengadaan != 0) {
			$list_satuan = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')	
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->where('asa.id_aset', '=', $id_aset)
			->where('asa.kode_pengadaan', '=', $kode_pengadaan)
			->selectRaw('asa.id_satuan as asa_id_satuan, asa.kode_grup_satuan as asa_kode_grup_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.kode_aset_informasi as asa_kode_aset_informasi, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, asm.harga_satuan as asm_harga_satuan, null as list_aset_informasi')
			->get();
		} else {
			$list_satuan = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->where('asa.id_aset', '=', $id_aset)
			->where('asa.kode_pengadaan', '=', null)
			->selectRaw('asa.id_satuan as asa_id_satuan, asa.kode_grup_satuan as asa_kode_grup_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.kode_aset_informasi as asa_kode_aset_informasi, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, coalesce(asm.harga_satuan, "-") as asm_harga_satuan, null as list_aset_informasi')
			->get();
		}

		for($i = 0 ; $i < count($list_satuan) ; $i++)
		{
			$list_aset_informasi = DB::table('aset_informasi as ai')
				->leftJoin('ms_field_aset_informasi as mfai', 'ai.id_field_info', '=', 'mfai.id_field_info')
				->leftJoin('ms_datatype as md', 'mfai.id_datatype', '=', 'md.id_datatype')
				->selectRaw('mfai.id_field_info, mfai.id_jenis_aset, mfai.src_tabel_field_info, mfai.src_kolom_field_info, mfai.nama_field_info, md.kode_html_show, ai.id_aset_informasi, ai.isian_field_informasi, ai.keterangan_isian')
				->distinct()
				->where('ai.id_aset', '=', $id_aset)
				->where('ai.kode_grup_satuan', '=', $list_satuan[$i]->asa_kode_grup_satuan)
				->orderBy('mfai.id_field_info', 'asc')->get();

			for($j = 0 ; $j < count($list_aset_informasi) ; $j++)
			{
				$list_parameter = DB::table('ms_datatype_parameter')
					->where('src_tabel_parameter', '=', $list_aset_informasi[$j]->src_tabel_field_info)
					->where('src_kolom_parameter', '=', $list_aset_informasi[$j]->src_kolom_field_info)
					->where('src_fk_parameter', '=', $list_aset_informasi[$j]->id_field_info)
					->where('src_jenis_fk_parameter', '=', 'show')
					->selectRaw('id_parameter, parameter, replacement')
					->distinct()
					->get();

				for($k = 0 ; $k < count($list_parameter) ; $k++)
				{
					$list_aset_informasi[$j]->kode_html_show = str_replace($list_parameter[$k]->parameter, $list_parameter[$k]->replacement, $list_aset_informasi[$j]->kode_html_show);
					$list_mapping = DB::table('ms_datatype_parameter_mapping')
						->where('id_parameter', '=', $list_parameter[$k]->id_parameter)
						->selectRaw('replacement, src_tabel_mapping, src_kolom_mapping')
						->distinct()
						->get();

					for($l = 0 ; $l < count($list_mapping) ; $l++)
					{
						$src = $list_mapping[$l]->src_kolom_mapping;
						$list_aset_informasi[$j]->kode_html_show = str_replace($list_mapping[$l]->replacement, $list_aset_informasi[$j]->{$src}, $list_aset_informasi[$j]->kode_html_show);
					}
				}
			}

			$list_satuan[$i]->list_aset_informasi = $list_aset_informasi;
		}

	    if ($list_satuan) return $list_satuan;
	    else return null;
	}

	public static function getAsetByJenisAset($id_jenis_aset)
	{
		$list_aset = DB::table('aset as a')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('gambar as g', function($join){
				$join->on('a.id_aset', '=', 'g.src_fk_gambar')
					 ->where('g.src_tabel_gambar', '=', 'aset')
					 ->where('g.src_kolom_gambar', '=', 'id_aset');
			})
			->leftJoin(DB::raw("(select id_aset, count(distinct kode_grup_satuan) as total_stok_aset from aset_satuan group by id_aset) as att"), 'att.id_aset', '=', 'a.id_aset')
			->leftJoin(DB::raw("(select id_aset, count(distinct kode_grup_satuan) as jumlah_stok_tersedia from aset_satuan where isNull(id_aset_keluar) group by id_aset) as ast"), 'ast.id_aset', '=', 'a.id_aset')
			->where('mja.id_jenis_aset', '=', $id_jenis_aset)
			->where('g.gambar_utama', '=', 'Y')
			->select('a.id_aset', 'a.tipe_aset', 'a.keterangan_aset', 'mja.id_jenis_aset', 'mja.nama_jenis_aset', 'mma.id_merek', 'mma.nama_merek', 'mka.id_kategori_aset', 'mka.nama_kategori_aset', 'g.id_gambar', 'g.src_tabel_gambar', 'g.src_kolom_gambar', 'g.src_fk_gambar', 'g.nama_gambar', 'g.ukuran_gambar', 'g.link_gambar', 'g.keterangan_gambar', 'g.gambar_utama', 'att.total_stok_aset', 'ast.jumlah_stok_tersedia')
			->orderby('mma.nama_merek', 'desc')
			->orderBy('a.tipe_aset', 'desc')
			->get();
	    if ($list_aset) return $list_aset;
	    else return null;
	}
}
