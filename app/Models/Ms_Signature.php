<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Ms_Signature extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
  public $table = 'ms_signature';
  
  protected $guarded = [];

   
  public static function getMsSignature()
    {
       $data = DB::table('ms_signature')
         ->orderBy('id', 'asc')->get();

        return $data;
    }
  

  public static function getListSignature()
    {
       $data = DB::table('ms_signature')
         ->orderBy('id', 'asc')
        ->paginate(5);

        if($data)
        return $data;
        else return null;
    }

  public static function getSignatureByJabatan($jabatan)
    {
       $data = DB::table('ms_signature as a')   
       ->leftJoin('ms_template as b', 'a.id', '=', 'b.signature')
        ->where('b.jabatan','=',$jabatan)
        ->first();

        if($data)
        return $data;
        else return null;
    } 

  public static function getSignatureById($id)
  {
     $data = DB::table('ms_signature as a')          
      ->where('a.id','=',$id)
      ->first();

      if($data)
      return $data;
      else return null;
  } 


  public static function getSignature($id)
    {
       $data = DB::table('ms_signature as a')   
       ->leftJoin('ms_template as b', 'a.id', '=', 'b.signature')
        ->where('b.id','=',$id)
        ->first();

        if($data)
        return $data;
        else return null;
    } 
      
}
