<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Kontainer extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'ms_kontainer';
	public $timestamps = false;

	protected $guarded = ['id_kontainer'];

	public static function getbyID($id)
	{
		$result = DB::table('ms_kontainer')
		  ->where('id_kontainer', '=', $id)
		  ->first();
		if($result)
		  return $result;
		else
		  return null;
	}

	public static function getKontainer($id_lokasi)
	{
		$list_kontainer = DB::table('ms_kontainer')->where('id_lokasi', '=', $id_lokasi)
						->orderBy('id_kontainer', 'asc')->get();

		if($list_kontainer) return $list_kontainer;
		else return null;
	}

}
