<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Log extends Model
{
	protected $table = 'ms_log';
	public $timestamps = false;

	protected $guarded = ['id_log'];

	public static function getMsLog($src_controller, $src_function)
	{
		$log = DB::table('ms_log')
			->where('src_controller', '=', $src_controller)
			->where('src_function', '=', $src_function)
			->first();
        if($log) return $log;
        else return null;
 	}
}