<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aset_Spesifikasi extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'aset_spesifikasi';
	public $timestamps = false;

	protected $guarded = ['id_spesifikasi'];

  	public static function getAsetSpesifikasi($id_aset)
	{
		$list_spesifikasi = DB::table('aset_spesifikasi as asp')
						->leftJoin('ms_field_spesifikasi_aset as mfs', 'asp.id_field', '=', 'mfs.id_field')
						->where('asp.id_aset', '=', $id_aset)
						->orderBy('asp.id_spesifikasi', 'asc')->get();

		if($list_spesifikasi) return $list_spesifikasi;
		else return null;
	}
}
