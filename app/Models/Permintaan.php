<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;



class Permintaan extends Model
{
	use SoftDeletes;
    //protected $dates = ['deleted_at'];

	protected $table = 'permintaan';
	public $timestamps = false;

	protected $guarded = ['id_permintaan'];


	public static function getAllPermintaan($skip,$take,$keyword)
	{
		$list_permintaan = DB::table('permintaan as p')
			->leftJoin('ms_kantor_imigrasi as mki', 'p.kode_kanim', '=', 'mki.kode_kanim')
			->select('p.id_permintaan','p.no_surat_permintaan', 'p.tanggal_surat_permintaan','p.tanggal_permintaan', 'p.nama_pemohon', 'p.jabatan_pemohon','mki.nama_kanim as kantor_imigrasi','p.pangkat_golongan_pemohon as pangkat_golongan','p.nip_pemohon','p.deskripsi_permintaan','p.created_by','p.created_at','p.updated_at')
	        ->orderBy('p.id_permintaan', 'desc')
	        ->distinct();

	    // dd($list_pengadaan);
        // if($filter1 != 0) {
		// 	$list_pengadaan->where('mkp.id_kategori_pengadaan', '=', $filter1);
		// }
		// if($filter2 != 0) {
		// 	$list_pengadaan->where('p.tahun_pengadaan', '=', $filter2);
		// }
		// if($keyword != null) {
			$list_permintaan->where(function($query) use($keyword)
		    {
		        $query->where('p.deskripsi_permintaan', 'like', '%' . $keyword . '%')
					->orWhere('p.no_surat_permintaan', 'like', '%' . $keyword . '%')
					->orWhere('p.nama_pemohon', 'like', '%' . $keyword . '%')
					->orWhere('mki.nama_kanim', 'like', '%' . $keyword . '%')
					->orWhere('p.deskripsi_permintaan', 'like', '%' . $keyword . '%')
					->orWhere('p.tanggal_surat_permintaan', 'like', '%' . $keyword . '%');
		    });	
		// }

        $count = $list_permintaan->count();
		$list_permintaan = $list_permintaan->take($take)->skip($skip)->get();
		if ($count != 0) return [$count, $list_permintaan];
		else return null;
	}

	public static function getSumberPermintaan($id_permintaan)
	{
		$list_permintaan_item = DB::table('permintaan_item as pi')
			->leftJoin('permintaan as p', 'p.id_permintaan', '=', 'pi.id_permintaan')
			->leftJoin('aset as a', 'pi.id_aset', '=', 'a.id_jenis_aset')
			->where('pi.id_permintaan', '=', $id_permintaan)
			->selectRaw('coalesce(pi.id_aset) as id_aset,coalesce(a.tipe_aset) as nama_aset, coalesce(pi.keterangan_prmnt_item, "-") as keterangan, qty_prmnt_item as jumlah')
			->orderBy('pi.id_prmnt_item', 'desc')
			->get();
	    if ($list_permintaan_item) return $list_permintaan_item;
	    else return null;
	}

	public static function getSumberPermintaanbyID($id_permintaan)
	{
		$list_permintaan = DB::table('permintaan as p')
			->leftJoin('ms_kantor_imigrasi as mki', 'p.kode_kanim', '=', 'mki.kode_kanim')
			->where('p.id_permintaan', '=', $id_permintaan)
			->select('p.id_permintaan','p.no_surat_permintaan', 'p.tanggal_surat_permintaan','p.tanggal_permintaan', 'p.nama_pemohon', 'p.jabatan_pemohon','mki.nama_kanim as kantor_imigrasi','p.pangkat_golongan_pemohon as pangkat_golongan','p.nip_pemohon','p.nama_pemohon','p.deskripsi_permintaan','p.created_by','p.created_at','p.updated_at')
			->orderBy('p.id_permintaan', 'desc')
			->first();
	    if ($list_permintaan) return $list_permintaan;
	    else return 0;
	}

	public static function getAllPermintaanAlokasi($skip,$take,$keyword)
	{
		$list_permintaan = DB::table('permintaan as p')
			->leftJoin('ms_kantor_imigrasi as mki', 'p.kode_kanim', '=', 'mki.kode_kanim')
			->where('p.status_permintaan', '=', SUDAH_DIKONFIRMASI)
			->select('p.id_permintaan','p.no_surat_permintaan', 'p.tanggal_surat_permintaan','p.tanggal_permintaan', 'p.nama_pemohon', 'p.jabatan_pemohon','mki.nama_kanim as kantor_imigrasi','p.pangkat_golongan_pemohon as pangkat_golongan','p.nip_pemohon','p.deskripsi_permintaan','p.created_by','p.created_at','p.updated_at')
	        ->orderBy('p.id_permintaan', 'desc')
	        ->distinct();

	    // dd($list_pengadaan);
        // if($filter1 != 0) {
		// 	$list_pengadaan->where('mkp.id_kategori_pengadaan', '=', $filter1);
		// }
		// if($filter2 != 0) {
		// 	$list_pengadaan->where('p.tahun_pengadaan', '=', $filter2);
		// }
		// if($keyword != null) {
			$list_permintaan->where(function($query) use($keyword)
		    {
		        $query->where('p.deskripsi_permintaan', 'like', '%' . $keyword . '%')
					->orWhere('p.no_surat_permintaan', 'like', '%' . $keyword . '%')
					->orWhere('p.nama_pemohon', 'like', '%' . $keyword . '%')
					->orWhere('mki.nama_kanim', 'like', '%' . $keyword . '%')
					->orWhere('p.deskripsi_permintaan', 'like', '%' . $keyword . '%')
					->orWhere('p.tanggal_surat_permintaan', 'like', '%' . $keyword . '%');
		    });	
		// }

        $count = $list_permintaan->count();
		$list_permintaan = $list_permintaan->take($take)->skip($skip)->get();
		if ($count != 0) return [$count, $list_permintaan];
		else return null;
	}
}
