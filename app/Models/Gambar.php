<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Gambar extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'gambar';
	public $timestamps = false;

	protected $guarded = ['id_gambar'];

	public static function getGambarAset($src_tabel_gambar, $src_kolom_gambar, $src_fk_gambar)
	{
		$list_gambar = DB::table('gambar')
						->where('src_tabel_gambar', '=', $src_tabel_gambar)
						->where('src_kolom_gambar', '=', $src_kolom_gambar)
						->where('src_fk_gambar', '=', $src_fk_gambar)
						->orderBy('id_gambar', 'asc')
						->take(4)
						->get();

		if($list_gambar) return $list_gambar;
		else return null;
	}

	public static function getGambarAsetList($src_tabel_gambar, $src_kolom_gambar, $src_fk_gambar)
	{
		$list_gambar = DB::table('gambar')
						->where('src_tabel_gambar', '=', $src_tabel_gambar)
						->where('src_kolom_gambar', '=', $src_kolom_gambar)
						->where('src_fk_gambar', '=', $src_fk_gambar)
						->orderBy('gambar_utama', 'asc')
						->get();

		// dd($list_gambar);

		if($list_gambar) return $list_gambar;
		else return null;
	}

	public static function getGambarAplikasiList($src_tabel_gambar, $src_kolom_gambar, $src_fk_gambar)
	{
		$list_gambar = DB::table('gambar')
						->where('src_tabel_gambar', '=', $src_tabel_gambar)
						->where('src_kolom_gambar', '=', $src_kolom_gambar)
						->where('src_fk_gambar', '=', $src_fk_gambar)
						->orderBy('gambar_utama', 'asc')
						->get();
						
		if($list_gambar) return $list_gambar;
		else return null;
	}

	public static function getGambarByKodeGrupSatuan($kode_grup_satuan)
	{
		$aset = DB::table('aset_satuan')
						->where('kode_grup_satuan', '=', $kode_grup_satuan)
						->select('id_aset')
						->first();

		$gambar = DB::table('gambar')
						->where('src_tabel_gambar', '=', 'aset')
						->where('src_kolom_gambar', '=', 'id_aset')
						->where('src_fk_gambar', '=', $aset->id_aset)
						->orderBy('gambar_utama', 'asc')
						->take(1)
						->first();

		if($gambar) return $gambar;
		else return null;
	}
}
