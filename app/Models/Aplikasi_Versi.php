<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Aplikasi_Versi extends Model
{

    use SoftDeletes;
	protected $table = 'aplikasi_versi';
	public $timestamps = false;

	protected $guarded = ['id_versi'];
	protected $dates = ['deleted_at'];

	public static function getAplikasiVersi($aplikasiId)
	{
		$list_aplikasi_versi = DB::table('aplikasi_versi as av')
			->leftJoin('aplikasi_sourcecode as as', 'av.id_versi', '=', 'as.id_versi')
			->leftJoin('dokumentasi as d', function($join){
				$join->on('av.id_versi', '=', 'd.src_fk_dokumentasi')
					 ->where('d.src_tabel_dokumentasi', '=', 'aplikasi_versi')
					 ->where('d.src_kolom_dokumentasi', '=', 'id_versi');
			})
			->leftJoin('ms_lokasi as mlok_src', 'as.id_lokasi', '=', 'mlok_src.id_lokasi')
			->leftJoin('ms_kontainer as mkon_src', 'as.id_kontainer', '=', 'mkon_src.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_src', 'as.id_posisi_kontainer', '=', 'mpos_src.id_posisi_kontainer')
			->leftJoin('ms_lokasi as mlok_dok', 'd.id_lokasi', '=', 'mlok_dok.id_lokasi')
			->leftJoin('ms_kontainer as mkon_dok', 'd.id_kontainer', '=', 'mkon_dok.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_dok', 'd.id_posisi_kontainer', '=', 'mpos_dok.id_posisi_kontainer')
			->selectRaw('av.id_versi, av.id_aplikasi, av.nomor_versi, av.tanggal_versi, av.rincian_versi, as.id_sourcecode, as.id_lokasi as as_id_lokasi, mlok_src.nama_lokasi as as_nama_lokasi, as.id_kontainer as as_id_kontainer, mkon_src.nama_kontainer as as_nama_kontainer, as.id_posisi_kontainer as as_id_posisi_kontainer, mpos_src.label_posisi_kontainer as as_label_posisi_kontainer, as.nama_sourcecode, as.link_sourcecode, as.keterangan_sourcecode, d.id_dokumentasi, d.id_lokasi as d_id_lokasi, mlok_dok.nama_lokasi as d_nama_lokasi, d.id_kontainer as d_id_kontainer, mkon_dok.nama_kontainer as d_nama_kontainer, d.id_posisi_kontainer as d_id_posisi_kontainer, mpos_dok.label_posisi_kontainer as d_label_posisi_kontainer, d.nama_dokumentasi, d.tanggal_dokumentasi, d.keterangan_dokumentasi, d.link_dokumentasi')
			->where('av.id_aplikasi', '=', $aplikasiId)
			->orderBy('av.id_versi', 'asc')
			->get();
	    if ($list_aplikasi_versi) return $list_aplikasi_versi;
	    else return 0;
	}
}
