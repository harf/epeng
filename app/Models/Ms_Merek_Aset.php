<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Merek_Aset extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

  protected $table = 'ms_merek_aset';
  public $timestamps = false;

  protected $guarded = ['id_merek'];

  public static function getMerekAsetbyId($id_merek)
	{
		$list_merek_aset_byid = DB:: table('ms_merek_aset')
			->select('ms_jenis_aset.id_jenis_aset', 'ms_jenis_aset.nama_jenis_aset')
			->join('ms_jenis_aset', 'ms_jenis_aset.id_jenis_aset','ms_jenis_aset.id_jenis_aset')
			->where('ms_merek_aset.id_merek', $id_merek)->get();
			
		if($list_merek_aset_byid) 
			return $list_merek_aset_byid;
		else 
			return null;
	}

	public static function getAllJenisAset()
	{
		$list_merek_aset = DB::table('ms_merek_aset')
						->select('ms_merek_aset.id_merek','ms_merek_aset.nama_merek','ms_merek_aset.keterangan_merek','ms_jenis_aset.nama_jenis_aset','ms_jenis_aset.id_jenis_aset')
						->join('ms_jenis_aset', 'ms_merek_aset.id_jenis_aset', 'ms_jenis_aset.id_jenis_aset')
						->orderBy('ms_jenis_aset.nama_jenis_aset', 'desc')
						->paginate(10);
		if($list_merek_aset) return $list_merek_aset;
		else return null;
	}
}
