<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Alamat extends Model
{
  use SoftDeletes;
  protected $table = 'alamat';
  public $timestamps = false;

  protected $guarded = ['id_alamat'];
    protected $dates = ['deleted_at'];


}
