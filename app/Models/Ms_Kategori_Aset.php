<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Kategori_Aset extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	protected $table = 'ms_kategori_aset';
	public $timestamps = false;

	protected $guarded = ['id_kategori_aset'];

	public static function getbyID($id)
	{
		$result = DB::table('ms_kategori_aset')
		  ->where('id_kategori_aset', '=', $id)
		  ->first();
		if($result)
		  return $result;
		else
		  return null;
	}
}
