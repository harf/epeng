<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aplikasi_Sourcecode extends Model
{

  protected $table = 'aplikasi_sourcecode';
  public $timestamps = false;

  protected $guarded = ['id_sourcecode'];

	public static function getSourceCode($id_sourcecode)
	{
		$sourcecode = DB::table('aplikasi_sourcecode')
			->where('id_sourcecode', '=', $id_sourcecode)
			->first();

		if($sourcecode) return $sourcecode;
		else return null;
	}

}
