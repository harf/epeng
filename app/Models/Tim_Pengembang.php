<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Tim_Pengembang extends Model
{
    use SoftDeletes;

	protected $table = 'tim_pengembang';
	public $timestamps = false;

	protected $guarded = ['id_pengembang'];
    protected $dates = ['deleted_at'];


}
