<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Field_Aset_Informasi extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'ms_field_aset_informasi';
	public $timestamps = false;

	protected $guarded = ['id_field_info'];

  	public static function getFieldAsetInformasi($id_jenis_aset, $kode_grup_satuan)
	{
		$list_field_aset_informasi = DB::table('ms_field_aset_informasi as mfai')
			->leftJoin('ms_datatype as md', 'mfai.id_datatype', '=', 'md.id_datatype')
			->selectRaw('mfai.id_field_info, mfai.id_jenis_aset, mfai.nama_field_info, mfai.keterangan_field_info, mfai.src_tabel_field_info, mfai.src_kolom_field_info, md.id_datatype, md.nama_datatype, md.kode_html_input, ' . $kode_grup_satuan . ' as kode_grup_satuan, null as isian_field_informasi, null as keterangan_isian')
			->distinct()
			->where('mfai.id_jenis_aset', '=', $id_jenis_aset)
			->orderBy('mfai.id_field_info', 'asc')->get();

		for($i = 0 ; $i < count($list_field_aset_informasi) ; $i++)
		{
			$list_parameter = DB::table('ms_datatype_parameter')
				->where('src_tabel_parameter', '=', $list_field_aset_informasi[$i]->src_tabel_field_info)
				->where('src_kolom_parameter', '=', $list_field_aset_informasi[$i]->src_kolom_field_info)
				->where('src_fk_parameter', '=', $list_field_aset_informasi[$i]->id_field_info)
				->selectRaw('id_parameter, parameter, replacement')
				->distinct()
				->get();

			for($j = 0 ; $j < count($list_parameter) ; $j++)
			{
				$list_field_aset_informasi[$i]->kode_html_input = str_replace($list_parameter[$j]->parameter, $list_parameter[$j]->replacement, $list_field_aset_informasi[$i]->kode_html_input);
				$list_mapping = DB::table('ms_datatype_parameter_mapping')
					->where('id_parameter', '=', $list_parameter[$j]->id_parameter)
					->selectRaw('replacement, src_tabel_mapping, src_kolom_mapping')
					->distinct()
					->get();

				for($k = 0 ; $k < count($list_mapping) ; $k++)
				{
					$src = $list_mapping[$k]->src_kolom_mapping;
					$list_field_aset_informasi[$i]->kode_html_input = str_replace($list_mapping[$k]->replacement, $list_field_aset_informasi[$i]->{$src}, $list_field_aset_informasi[$i]->kode_html_input);
				}
			}
		}

		if($list_field_aset_informasi) return $list_field_aset_informasi;
		else return null;
	}

	public static function getFieldAsetInformasiCatat($id_jenis_aset, $kode_grup_satuan)
	{
		$list_field_aset_informasi = DB::table('ms_field_aset_informasi as mfai')
			->leftJoin('ms_datatype as md', 'mfai.id_datatype', '=', 'md.id_datatype')
			->selectRaw('mfai.id_field_info, mfai.id_jenis_aset, mfai.nama_field_info, mfai.keterangan_field_info, mfai.src_tabel_field_info, mfai.src_kolom_field_info, md.id_datatype, md.nama_datatype, md.kode_html_input, ' . $kode_grup_satuan . ' as kode_grup_satuan, null as isian_field_informasi, null as keterangan_isian')
			->distinct()
			->where('mfai.id_jenis_aset', '=', $id_jenis_aset)
			->orderBy('mfai.id_field_info', 'asc')->get();

		for($i = 0 ; $i < count($list_field_aset_informasi) ; $i++)
		{
			$list_parameter = DB::table('ms_datatype_parameter')
				->where('src_tabel_parameter', '=', $list_field_aset_informasi[$i]->src_tabel_field_info)
				->where('src_kolom_parameter', '=', $list_field_aset_informasi[$i]->src_kolom_field_info)
				->where('src_fk_parameter', '=', $list_field_aset_informasi[$i]->id_field_info)
				->where('src_jenis_fk_parameter', '=', 'input')
				->selectRaw('id_parameter, parameter, replacement')
				->distinct()
				->get();

			for($j = 0 ; $j < count($list_parameter) ; $j++)
			{
				$list_field_aset_informasi[$i]->kode_html_input = str_replace($list_parameter[$j]->parameter, $list_parameter[$j]->replacement, $list_field_aset_informasi[$i]->kode_html_input);
				$list_mapping = DB::table('ms_datatype_parameter_mapping')
					->where('id_parameter', '=', $list_parameter[$j]->id_parameter)
					->selectRaw('replacement, src_tabel_mapping, src_kolom_mapping')
					->distinct()
					->get();

				for($k = 0 ; $k < count($list_mapping) ; $k++)
				{
					$src = $list_mapping[$k]->src_kolom_mapping;
					$list_field_aset_informasi[$i]->kode_html_input = str_replace($list_mapping[$k]->replacement, $list_field_aset_informasi[$i]->{$src}, $list_field_aset_informasi[$i]->kode_html_input);
				}
			}
		}

		if($list_field_aset_informasi) return $list_field_aset_informasi;
		else return null;
	}

	public static function getDataTypeFieldAsetInformasi($id_field_info)
	{
		$datatype = DB::table('ms_field_aset_informasi as mfai')
			->leftJoin('ms_datatype as md', 'mfai.id_datatype', '=', 'md.id_datatype')
			->where('mfai.id_field_info', '=', $id_field_info)
			->first();

		if($datatype) return $datatype;
		else return null;
	}
}
