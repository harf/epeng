<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Jenis_Aset extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

  protected $table = 'ms_jenis_aset';
  public $timestamps = false;

  protected $guarded = ['id_jenis_aset'];

  	public static function getJenisAset($id_kategori_aset)
	{
		$list_jenis_aset = DB::table('ms_jenis_aset')->where('id_kategori_aset', '=', $id_kategori_aset)
						->orderBy('nama_jenis_aset', 'asc')
						->get();

		if($list_jenis_aset) return $list_jenis_aset;
		else return null;
	}

	public static function getSingleJenisAset($id_jenis_aset)
	{
		// dd($id_jenis_aset);
		$jenis_aset = DB::table('ms_jenis_aset')
								->where('id_jenis_aset', '=', $id_jenis_aset)
								->first();
		
		if($jenis_aset) return $jenis_aset;
		else return null;
	}	

	public static function getAllJenisAset()
	{
		$list_jenis_aset = DB::table('ms_jenis_aset')
						->select('ms_jenis_aset.id_jenis_aset','ms_jenis_aset.nama_jenis_aset','ms_jenis_aset.keterangan_jenis_aset','ms_kategori_aset.nama_kategori_aset','ms_kategori_aset.id_kategori_aset')
						->join('ms_kategori_aset', 'ms_jenis_aset.id_kategori_aset', 'ms_kategori_aset.id_kategori_aset')
						->orderBy('ms_kategori_aset.nama_kategori_aset', 'desc')
						->paginate(10);
		if($list_jenis_aset) return $list_jenis_aset;
		else return null;
	}

	public static function getJenisAsetPermintaan($id_kategori_aset,$id_permintaan)
	{
		$list_jenis_aset = DB::table('ms_jenis_aset')->where('id_kategori_aset', '=', $id_kategori_aset, 'id_permintaan', '=', $id_permintaan)
						->orderBy('nama_jenis_aset', 'asc')
						->get();

		if($list_jenis_aset) return $list_jenis_aset;
		else return null;
	}


	public static function getJenisAsetbyId($id_jenis_aset)
	{
		$list_jenis_aset_byid = DB:: table('ms_jenis_aset')
			->select('ms_kategori_aset.id_kategori_aset', 'ms_kategori_aset.nama_kategori_aset')
			->join('ms_kategori_aset', 'ms_jenis_aset.id_kategori_aset','ms_kategori_aset.id_kategori_aset')
			->where('ms_jenis_aset.id_jenis_aset', $id_jenis_aset)->get();
			
		if($list_jenis_aset_byid) 
			return $list_jenis_aset_byid;
		else 
			return null;
	}

	public static function getbyID($id)
	{
		$result = DB::table('ms_jenis_aset')
		  ->where('id_jenis_aset', '=', $id)
		  ->first();
		if($result)
		  return $result;
		else
		  return null;
	}
}
