<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Aplikasi extends Model
{
    use SoftDeletes;

	protected $table = 'aplikasi';
	public $timestamps = false;

	protected $guarded = ['id_aplikasi'];
	protected $dates = ['deleted_at'];

  
}
