<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Aset_Stok_Riwayat extends Model
{

	protected $table = 'aset_stok_riwayat';
	public $timestamps = false;

	protected $guarded = ['id_riwayat'];
}
