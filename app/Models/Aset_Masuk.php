<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class Aset_Masuk extends Model
{
	
	protected $table = 'aset_masuk';
	public $timestamps = false;

	protected $guarded = ['id_aset_masuk'];

	public static function getLastNomorAsetMasuk()
	{
		$last_nomor_aset_masuk = DB::table('aset_masuk')
			->select('nomor_aset_masuk')
			->orderBy('nomor_aset_masuk', 'desc')
			->first();
		// dd($last_nomor_aset_keluar);
	    if ($last_nomor_aset_masuk) return $last_nomor_aset_masuk;
	    else return 0;
	}

	public static function getAsetMasuk($periode_dari, $periode_sampai, $nomor_aset_masuk, $skip, $take)
	{
		$list_aset_masuk = DB::table('aset_masuk as asm')
			->leftJoin('pengadaan as p', 'asm.kode_pengadaan', '=', 'p.kode_pengadaan')
			->leftJoin('aset_stok as aso', 'asm.id_stok', '=', 'aso.id_stok')
			->selectRaw('asm.nomor_aset_masuk, asm.tanggal_aset_masuk, sum(asm.jumlah_aset_masuk) as jumlah_aset_masuk, p.kode_pengadaan, p.nama_pengadaan, p.tahun_pengadaan, asm.keterangan_aset_masuk')
			->groupBy('asm.nomor_aset_masuk', 'asm.tanggal_aset_masuk', 'p.kode_pengadaan', 'p.nama_pengadaan', 'p.tahun_pengadaan', 'asm.keterangan_aset_masuk')
			->orderBy('asm.nomor_aset_masuk', 'desc');

		// dd($periode_dari);
		if ($periode_dari != null) $list_aset_masuk->whereBetween('asm.tanggal_aset_masuk', [$periode_dari, $periode_sampai]);
		if ($nomor_aset_masuk) $list_aset_masuk->where('asm.nomor_aset_masuk', '=', $nomor_aset_masuk);

		$list_aset_masuk = $list_aset_masuk->distinct()->get()->toArray();
		$count = count($list_aset_masuk);
		$list_aset_masuk = array_slice($list_aset_masuk, $skip, $take);
		if ($count != 0) return [$count, $list_aset_masuk];
		else return null;
	}

	public static function getItemAsetMasuk($nomor_aset_masuk)
	{
		// dd($periode_dari);
		$list_item_aset_masuk = DB::table('aset_masuk as asm')
			->leftJoin('aset as a', 'asm.id_aset', '=', 'a.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->leftJoin('gambar as g', function($join){
				$join->on('a.id_aset', '=', 'g.src_fk_gambar')
					 ->where('g.src_tabel_gambar', '=', 'aset')
					 ->where('g.src_kolom_gambar', '=', 'id_aset')
					 ->where('g.gambar_utama', '=', 'Y');
			})
			->leftJoin('aset_stok as aso', 'asm.id_stok', '=', 'aso.id_stok')
			->selectRaw('asm.id_aset_masuk, asm.jumlah_aset_masuk, a.tipe_aset, a.id_aset, mma.nama_merek, mja.nama_jenis_aset, mka.nama_kategori_aset, g.nama_gambar, g.link_gambar, g.keterangan_gambar , aso.satuan_stok')
			->where('asm.nomor_aset_masuk', '=', $nomor_aset_masuk)
			->orderBy('mka.nama_kategori_aset', 'asc')
			->orderBy('mma.nama_merek', 'asc')
			->orderBy('a.tipe_aset', 'asc')
			->get();

	    if ($list_item_aset_masuk) return $list_item_aset_masuk;
	    else return null;
	}

	public static function getDownloadItemAsetMasuk($nomor_aset_masuk)
	{	
		$list_download_item_aset_masuk = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')	
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->leftJoin('aset as a', 'asa.id_aset', '=', 'a.id_aset')
			->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			->where('asm.nomor_aset_masuk', '=', $nomor_aset_masuk)
			->select('asa.id_satuan as asa_id_satuan', 'asa.kode_pengadaan as asa_kode_pengadaan', 'asa.id_item as asa_id_item', 'asa.id_aset as asa_id_aset', 'asa.id_aset_masuk as asa_id_aset_masuk', 'asa.id_aset_keluar as asa_id_aset_keluar', 'asa.id_lokasi as asa_id_lokasi', 'mlok_asa.nama_lokasi as asa_nama_lokasi', 'asa.id_kontainer as asa_id_kontainer', 'mkon_asa.nama_kontainer as asa_nama_kontainer', 'asa.id_posisi_kontainer as asa_id_posisi_kontainer', 'mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer', 'asa.field_aset as asa_field_aset', 'asa.isian_aset as asa_isian_aset', 'ake.id_aset_keluar as ake_id_aset_keluar', 'ake.kode_kanim as ake_kode_kanim', 'kanim.nama_kanim as ake_nama_kanim', 'ake.jumlah_aset_keluar as ake_jumlah_aset_keluar', 'ake.jenis_aset_keluar as ake_jenis_aset_keluar', 'ake.tanggal_aset_keluar as ake_tanggal_aset_keluar', 'ake.tanggal_aset_kembali as ake_tanggal_aset_kembali', 'ake.lokasi_keluar_lain as ake_lokasi_keluar_lain', 'ake.keterangan_aset_keluar as ake_keterangan_aset_keluar', 'asm.harga_satuan as asm_harga_satuan', 'a.tipe_aset', 'mja.nama_jenis_aset', 'mka.nama_kategori_aset', 'mma.nama_merek')
			->get();
	    if ($list_download_item_aset_masuk) return $list_download_item_aset_masuk;
	    else return null;
	}

	public static function getSatuanAsetMasuk($id_aset_masuk, $id_aset)
	{
		$list_satuan_aset_masuk = DB::table('aset_satuan as asa')
			->leftJoin('ms_lokasi as mlok_asa', 'asa.id_lokasi', '=', 'mlok_asa.id_lokasi')
			->leftJoin('ms_kontainer as mkon_asa', 'asa.id_kontainer', '=', 'mkon_asa.id_kontainer')
			->leftJoin('ms_posisi_kontainer as mpos_asa', 'asa.id_posisi_kontainer', '=', 'mpos_asa.id_posisi_kontainer')
			->leftJoin('aset_keluar as ake', 'asa.id_aset_keluar', '=', 'ake.id_aset_keluar')	
			->leftJoin('ms_kantor_imigrasi as kanim', 'ake.kode_kanim', '=', 'kanim.kode_kanim')
			->leftJoin('aset_masuk as asm', 'asa.id_aset_masuk', '=', 'asm.id_aset_masuk')
			->where('asm.id_aset_masuk', '=', $id_aset_masuk)
			->where('asa.id_aset', '=', $id_aset)
			->selectRaw('asa.id_satuan as asa_id_satuan, asa.kode_grup_satuan as asa_kode_grup_satuan, asa.kode_pengadaan as asa_kode_pengadaan, asa.id_item as asa_id_item, asa.id_aset as asa_id_aset, asa.id_aset_masuk as asa_id_aset_masuk, asa.id_aset_keluar as asa_id_aset_keluar, asa.kode_aset_informasi as asa_kode_aset_informasi, asa.id_lokasi as asa_id_lokasi, mlok_asa.nama_lokasi as asa_nama_lokasi, asa.id_kontainer as asa_id_kontainer, mkon_asa.nama_kontainer as asa_nama_kontainer, asa.id_posisi_kontainer as asa_id_posisi_kontainer, mpos_asa.label_posisi_kontainer as asa_label_posisi_kontainer, asa.field_aset as asa_field_aset, asa.isian_aset as asa_isian_aset, ake.id_aset_keluar as ake_id_aset_keluar, ake.kode_kanim as ake_kode_kanim, kanim.nama_kanim as ake_nama_kanim, ake.jumlah_aset_keluar as ake_jumlah_aset_keluar, ake.jenis_aset_keluar as ake_jenis_aset_keluar, ake.tanggal_aset_keluar as ake_tanggal_aset_keluar, ake.tanggal_aset_kembali as ake_tanggal_aset_kembali, ake.lokasi_keluar_lain as ake_lokasi_keluar_lain, ake.keterangan_aset_keluar as ake_keterangan_aset_keluar, coalesce(asm.harga_satuan, "-") as asm_harga_satuan')
			->get();
	    if ($list_satuan_aset_masuk) return $list_satuan_aset_masuk;
	    else return null;
	}
}
