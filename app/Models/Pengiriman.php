<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Database\Eloquent\SoftDeletes;


class Pengiriman extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

	protected $table = 'pengiriman';
	public $timestamps = false;

	protected $guarded = ['id_pengiriman'];


	public static function getAllPengiriman($keyword,$skip,$take)
	{

		$list_pengiriman = DB::table('pengiriman as p')
			// ->leftJoin('ms_penyedia as mp', 'p.id_penyedia', '=', 'mp.id_penyedia')
			// ->leftJoin('ms_kategori_pengadaan as mkp', 'p.id_kategori_pengadaan', '=', 'mkp.id_kategori_pengadaan')
			// ->whereNull('p.deleted_at')
			->select('p.id_pengiriman', 'p.id_permintaan', 'p.id_alokasi', 'p.nama_kurir', 'p.alamat_kurir', 'p.nomor_resi', 'p.biaya_kirim', 'p.created_at', 'p.created_by', 'p.updated_at', 'p.updated_by')
	        ->orderBy('p.id_pengiriman', 'asc')
	        ->distinct();

	    $count = $list_pengiriman->count();
		$list_pengiriman = $list_pengiriman->take($take)->skip($skip)->get();
		if ($count != 0) return [$count, $list_pengiriman];
		else return null;

	}
	

	public static function getPengirimanByID($id_pengiriman)
	{

		$pengiriman = DB::table('pengiriman as pe')
			// ->leftJoin('pengadaan as p', 'as.kode_pengadaan', '=', 'p.kode_pengadaan')
			// ->leftJoin('aset as a', 'as.id_aset', '=', 'a.id_aset')
			// ->leftJoin('ms_jenis_aset as mja', 'a.id_jenis_aset', '=', 'mja.id_jenis_aset')
			// ->leftJoin('ms_kategori_aset as mka', 'mja.id_kategori_aset', '=', 'mka.id_kategori_aset')
			// ->leftJoin('ms_merek_aset as mma', 'a.id_merek', '=', 'mma.id_merek')
			// ->leftJoin('aset_masuk as am', 'as.id_aset_masuk', '=', 'am.id_aset_masuk')
			// ->selectRaw('mka.nama_kategori_aset, mja.nama_jenis_aset, mma.nama_merek, a.tipe_aset, p.nama_pengadaan, p.tahun_pengadaan, am.harga_satuan')
			->select('pe.id_pengiriman', 'pe.id_permintaan', 'pe.id_alokasi', 'pe.nama_kurir', 'pe.alamat_kurir', 'pe.nomor_resi', 'pe.biaya_kirim', 'pe.created_at', 'pe.created_by', 'pe.updated_at', 'pe.updated_by')
			->where('pe.id_pengiriman', '=', $id_pengiriman)
			->first();
	    if ($pengiriman) return $pengiriman;
	    else return 0;

	}

}
