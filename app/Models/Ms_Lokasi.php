<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Lokasi extends Model
{
	use SoftDeletes;
    protected $dates = ['deleted_at'];

  protected $table = 'ms_lokasi';
  public $timestamps = false;

  protected $guarded = ['id_lokasi'];

	public static function getbyID($id)
	{
		$result = DB::table('ms_lokasi')
		  ->where('id_lokasi', '=', $id)
		  ->first();
		if($result)
		  return $result;
		else
		  return null;
	}
}
