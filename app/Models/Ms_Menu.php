<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;


class Ms_Menu extends Model
{

  protected $table = 'ms_menu';
  public $timestamps = false;

  protected $guarded = ['id_menu'];


  public static function getMenu()
	{
  	$value = Cache::remember('ms_menu', 60, function () {
  	    return  DB::table('ms_menu')->where('id_parent','=','0')
        	                    ->where('status','=','1')
                              ->orderBy('urutan', 'asc')->get();
  	});
    	
    return $value;
 	}

  public static function getMenuByID($role)
  {
    $value = Cache::remember('ms_menu', 60, function () use($role) {
        return  DB::table('ms_menu')->where('id_parent','=','0')
                              ->where('status','=','1')
                              ->where('role', '=', $role) 
                              // ->whereIn('role', [$role, 'All'])
                              ->orderBy('urutan', 'asc')->get();
    });
    
    return $value;
  }

	public static function getChild($id_parent)
 	{
   	$value = Cache::remember('submenu_'.$id_parent, 60, function () use ($id_parent) {
	    return  DB::table('ms_menu')->where('id_parent','=',$id_parent)
      	                    ->where('status','=','1')
                              ->orderBy('urutan', 'asc')->get();
	 });
  	
    return $value;

 	}

  public static function getbyID($id)
  {
    $result = DB::table('ms_menu')
      ->where('id_menu', '=', $id)
      ->first();
    if($result)
      return $result;
    else
      return null;
  }
}
