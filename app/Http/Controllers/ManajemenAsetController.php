<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;
use App\Models\Aset;
use App\Models\Pengadaan;
use App\Models\Ms_Kategori_Pengadaan;
use App\Models\Ms_Penyedia;
use App\Models\Ms_Lokasi;
use App\Models\Dokumentasi;
use App\Models\Ms_Kategori_Aset;
use App\Models\Ms_Merek_Aset;
use App\Models\Pengadaan_Item;
use App\Models\Aset_Satuan;
use App\Models\Aset_Masuk;
use App\Models\Aset_Stok;
use App\Models\Aset_Stok_Riwayat;
use App\Models\Ms_Kantor_Imigrasi;
use App\Models\Aset_Keluar;
use App\Models\Aset_Informasi;
use App\Models\Ms_Field_Aset_Informasi;
use App\Models\Gambar;
use App\Models\Aset_Satuan_Log;
use App\Models\Ms_Log;
use App\Models\Ms_Jenis_Aset;

class ManajemenAsetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        if($request->is_filtered) {
            $is_filtered = $request->is_filtered;
            $filter = $request->filter;
            $filter_name = Ms_Jenis_Aset::getSingleJenisAset($filter)->nama_jenis_aset;
        }

        $result = Aset::getManajemenAsetListIndex($filter,$keyword,$skip,$take);
        $count = $result[0];
        $list_aset = $result[1];
        $list_jenis_aset = Ms_Jenis_Aset::getAllJenisAset();

        return view('/manajemen_aset/manajemenAsetIndex', [
            'list_aset' => $list_aset,
            'list_jenis_aset' => $list_jenis_aset,
            'count_list_aset' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function detail($asetId)
    {
        return view('/manajemen_aset/manajemenAsetDetail');
    }

    public function activity($asetId)
    {
        return view('/manajemen_aset/manajemenAsetActivity');
    }

    public function catatAsetMasuk()
    {
        $list_lokasi = Ms_Lokasi::get();
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_merek_aset = Ms_Merek_Aset::get();

        $system_datetime = \Carbon\Carbon::now();
        $current_year = $system_datetime->year;

        $idx = 0;
        for ($i = $current_year; $i >= $current_year-10; $i--)
        {
            $list_tahun_pengadaan[$idx] = $i;
            $idx++;
        }
        
        return view('/manajemen_aset/catatAsetMasuk', [
                    'list_lokasi' => $list_lokasi,
                    'list_kategori_aset' => $list_kategori_aset,
                    'list_merek_aset' => $list_merek_aset
                ]);
    }

    public function catatAsetKeluar()
    {   
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_kantor_imigrasi = Ms_Kantor_Imigrasi::get();
        $list_lokasi = Ms_Lokasi::get();

        return view('/manajemen_aset/catatAsetKeluar', [
            'list_kategori_aset' => $list_kategori_aset,
            'list_kantor_imigrasi' => $list_kantor_imigrasi,
            'list_lokasi' => $list_lokasi
        ]);
    }

    public function catatAsetInformasi()
    {   
        $list_aset_informasi = Aset_Informasi::getAsetInformasiList();
        return view('/manajemen_aset/catatAsetInformasi', [
            'list_aset_informasi' => $list_aset_informasi
        ]);
    }

    public function asetPeminjaman()
    {        
        return view('/manajemen_aset/manajemenAsetPeminjaman');
    }

    public function simpanAsetMasuk(Request $request)
    {
        // dd($request);

        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $itemIndexArray = explode('~', $request->index_item);
        $itemCount = $request->count_item;

        $last_nomor_aset_masuk = Aset_Masuk::getLastNomorAsetMasuk();
        $nomor_aset_masuk = ((int)$last_nomor_aset_masuk->nomor_aset_masuk + 1);
        for($i = 0; $i < $itemCount; $i++)
        {
            $cek_stok = Aset_Stok::getAsetStokByID($request->input('tipe_aset~'.$itemIndexArray[$i]));
            $id_stok = 0;
            $jumlah_stok = 0;
            if ($cek_stok) {
                $aset_stok = Aset_Stok::where('id_stok', $cek_stok->id_stok)
                    ->update([
                        'jumlah_stok' => ($cek_stok->jumlah_stok +  $request->input('jumlah_stok~' . $itemIndexArray[$i])),
                        'tanggal_stok' => $current_datetime,
                        'updated_at' => $current_datetime,
                        'updated_by' => $auth->id
                ]);
                $id_stok = $cek_stok->id_stok;
                $jumlah_stok = $cek_stok->jumlah_stok + $request->input('jumlah_stok~' . $itemIndexArray[$i]);
            } else {
                $aset_stok = Aset_Stok::create([
                    'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                    'jumlah_stok' => $request->input('jumlah_stok~' . $itemIndexArray[$i]),
                    'tanggal_stok' => $current_datetime,
                    'satuan_stok' => $request->input('satuan_stok~' . $itemIndexArray[$i]),
                    'created_by' => $auth->id

                ]);
                $id_stok = $aset_stok->id;
                $jumlah_stok = $request->input('jumlah_stok~' . $itemIndexArray[$i]);
            }

            $aset_stok_riwayat = Aset_Stok_Riwayat::create([
                'id_stok' => $id_stok,
                'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                'jumlah_stok' => $jumlah_stok,
                'tanggal_stok' => $current_datetime,
                'created_by' => $auth->id
            ]);

            $aset_masuk = Aset_Masuk::create([
                'nomor_aset_masuk' => $nomor_aset_masuk,
                'id_stok' => $id_stok,
                'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                'jumlah_aset_masuk' => $request->input('jumlah_stok~' . $itemIndexArray[$i]),
                'tanggal_aset_masuk' => $current_datetime,
                'keterangan_aset_masuk' => null,
                'harga_satuan' => $request->input('harga_satuan~'. $itemIndexArray[$i]),
                'created_at' => $current_datetime,
                'created_by' => $auth->id
            ]);
            $aset_masuk_insertedId = $aset_masuk->id;

            $satuanIndexArray = explode('~', $request->input('index_satuan~'.$itemIndexArray[$i]));
            $satuanCount = $request->input('count_satuan~'.$itemIndexArray[$i]);
            for ($j = 0 ; $j < $satuanCount ; $j++)
            {
                $last_kode_grup_satuan = Aset_Satuan::getLastKodeGrupSatuan();
                $kode_grup_satuan = ((int)$last_kode_grup_satuan->kode_grup_satuan + 1);

                $satuanIndexFieldIsian = explode('~', $request->input('index_field_isian_satuan~'. $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]));
                $countSatuanIndexFieldIsian = count($satuanIndexFieldIsian) - 1;
                for($k = 0 ; $k < $countSatuanIndexFieldIsian ; $k++)
                {
                    $aset_satuan = Aset_Satuan::create([
                        'kode_grup_satuan' => $kode_grup_satuan,
                        'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                        'id_aset_masuk' => $aset_masuk_insertedId,
                        'id_lokasi' => $request->input('satuan~lokasi~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]),
                        'id_kontainer' => $request->input('satuan~kontainer~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]),
                        'id_posisi_kontainer' => $request->input('satuan~posisi_kontainer~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]),
                        'field_aset' => $request->input('field_aset~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j] . '-' . $satuanIndexFieldIsian[$k]),
                        'isian_aset' => $request->input('isian_aset~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j] . '-' . $satuanIndexFieldIsian[$k]),
                        'created_at' => $current_datetime,
                        'created_by' => $auth->id
                    ]);
                }

                $log = Ms_Log::getMsLog('ManajemenAsetController', 'simpanAsetMasuk');
                // dd($log);
                $aset_satuan_log = Aset_Satuan_Log::create([
                    'kode_grup_satuan' => $kode_grup_satuan,
                    'id_log' => $log->id_log,
                    'keterangan_log' => 'Nomor aset masuk: #' . $nomor_aset_masuk,
                    'created_at' => $current_datetime,
                    'created_by' => $auth->id
                ]);
            }
        }
        return redirect('/manajemenAset');
    }

    public function simpanAsetKeluar(Request $request)
    {
        // dd($request);
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();
        $nomor_aset_keluar = 0;

        $itemIndexAsetKeluarArray = explode('~', $request->index_item_aset_keluar);
        $itemCount = $request->count_item_aset_keluar;

        if($itemCount != 0)
        {
            $itemAsetIdArray = [];
            $idxAsetId = 0;
            for($i = 0; $i < $itemCount; $i++)
            {
                $itemAset = explode('-', $itemIndexAsetKeluarArray[$i])[0];
                $itemAsetIdArray[$idxAsetId] = $itemAset;
                $idxAsetId++;
            }

            $itemAsetCountArray = array_count_values($itemAsetIdArray);

            $last_nomor_aset_keluar = Aset_Keluar::getLastNomorAsetKeluar();
            if($last_nomor_aset_keluar) {
                $nomor_aset_keluar = ((int)$last_nomor_aset_keluar->nomor_aset_keluar + 1);
            }
            foreach ($itemAsetCountArray as $key => $value)
            {
                $cek_stok = Aset_Stok::getAsetStokByID($key);
                $id_stok = $cek_stok->id_stok;
            
                $aset_stok_update = Aset_Stok::where('id_stok', $cek_stok->id_stok)
                    ->update([
                        'jumlah_stok' => ($cek_stok->jumlah_stok - $value),
                        'tanggal_stok' => $current_datetime,
                        'updated_at' => $current_datetime,
                        'updated_by' => $auth->id
                ]);
                    
                $jumlah_stok = $cek_stok->jumlah_stok - $value;

                $aset_stok_riwayat = Aset_Stok_Riwayat::create([
                    'id_stok' => $id_stok,
                    'id_aset' => $key,
                    'jumlah_stok' => $jumlah_stok,
                    'tanggal_stok' => $current_datetime,
                    'created_by' => $auth->id
                ]);

                $kode_kanim = null;
                $lokasi_keluar_lain = null;
                $tanggal_aset_kembali = null;

                if($request->jenis_aset_keluar == 'Alokasi')
                {
                    if ($request->kode_kanim_alokasi)
                    {
                        $kode_kanim = $request->kode_kanim_alokasi;
                    }
                    else
                    {
                        $lokasi_keluar_lain = $request->lokasi_keluar_lain_alokasi;
                    }
                }
                else if ($request->jenis_aset_keluar == 'Peminjaman')
                {
                    if ($request->kode_kanim_peminjaman)
                    {
                        $kode_kanim = $request->kode_kanim_peminjaman;
                    }
                    else
                    {
                        $lokasi_keluar_lain = $request->lokasi_keluar_lain_peminjaman;
                    }   
                }
                
                $aset_keluar = Aset_Keluar::create([
                    'nomor_aset_keluar' => $nomor_aset_keluar,
                    'id_stok' => $id_stok,
                    'id_aset' => $key,
                    'kode_kanim' => $kode_kanim,
                    'jumlah_aset_keluar' => $value,
                    'jenis_aset_keluar' => $request->jenis_aset_keluar,
                    'tanggal_aset_keluar' => $request->tanggal_aset_keluar,
                    'lokasi_keluar_lain' => $lokasi_keluar_lain,
                    'tanggal_aset_kembali' => $tanggal_aset_kembali,
                    'keterangan_aset_keluar' => $request->keterangan_aset_keluar,
                    'created_at' => $current_datetime,
                    'created_by' => $auth->id
                ]);

                $aset_keluar_insertedId = $aset_keluar->id;

                foreach($itemIndexAsetKeluarArray as $satuan)
                {
                    if($key == explode('-', $satuan)[0])
                    {
                        $id_sat = explode('-', $satuan)[1];
                        $list_satuan_grup = Aset_Satuan::getSatuanAsetGrup($id_sat);
                        
                        foreach($list_satuan_grup as $satuan_grup)
                        {
                            $update_satuan_aset = Aset_Satuan::where('id_satuan', $satuan_grup->id_satuan)
                            ->update([
                                'id_aset_keluar' => $aset_keluar_insertedId,
                                'id_lokasi' => null,
                                'id_kontainer' => null,
                                'id_posisi_kontainer' => null,
                                'updated_at' => $current_datetime,
                                'updated_by' => $auth->id
                            ]);
                        }

                        $log = Ms_Log::getMsLog('ManajemenAsetController', 'simpanAsetKeluar');
                        $kd_grup_satuan = Aset_Satuan::getKodeGrupSatuanById($id_sat);
                        dd($log);
                        $aset_satuan_log = Aset_Satuan_Log::create([
                            'kode_grup_satuan' => $kd_grup_satuan,
                            'id_log' => $log->id_log,
                            'keterangan_log' => 'Nomor aset keluar: #' . $nomor_aset_keluar,
                            'created_at' => $current_datetime,
                            'created_by' => $auth->id
                        ]);
                    }
                }
            }
        }

        $fileCount = count($request->file());
        if($fileCount != 0) {
            $files = $request->file();
            $filesIndex = [];
            $idx = 0;
            foreach ($files as $file) {
                $input_name = array_search($file, $files);
                $input_index = str_replace('id_dokumentasi~', '', $input_name);
                $filesIndex[$idx] = $input_index;
                $idx++;
            }
            foreach($filesIndex as $idx) {
                if($request->hasFile('id_dokumentasi~'. $idx)){
                    $uploadedFile = $request->file('id_dokumentasi~'. $idx);

                    $rootPath = '\app\public\files\dokumentasi';
                    $nama_dokumentasi = $request->input('nama_dokumentasi~'.$idx);
                    $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - ' .$uploadedFile->getClientOriginalName());

                    $id_lokasi = $request->input('dokumentasi~lokasi~'. $idx);
                    $id_kontainer = $request->input('dokumentasi~kontainer~'. $idx);
                    $id_posisi_kontainer = $request->input('dokumentasi~posisi_kontainer~'. $idx);

                    $keterangan_dokumentasi = $request->input('keterangan_dokumentasi~'.$idx);
                    $ukuran_dokumentasi = 0;
                    try {
                        $ukuran_dokumentasi = $request->file('id_dokumentasi~'.$idx)->getClientSize();
                    }
                    catch (\Exception $e) {
                        $ukuran_dokumentasi = 0;
                    }
                    
                    $tanggal_dokumentasi = $request->input('tanggal_dokumentasi~'.$idx);
                    $dokumentasi = Dokumentasi::create([
                        'src_tabel_dokumentasi' => 'aset_keluar',
                        'src_kolom_dokumentasi' => 'nomor_aset_keluar',
                        'src_fk_dokumentasi' => $nomor_aset_keluar,
                        'id_lokasi' => $id_lokasi,
                        'id_kontainer' => $id_kontainer,
                        'id_posisi_kontainer' => $id_posisi_kontainer,
                        'nama_dokumentasi' => $nama_dokumentasi,
                        'keterangan_dokumentasi' => $keterangan_dokumentasi,
                        'tanggal_dokumentasi' => $tanggal_dokumentasi,
                        'ukuran_dokumentasi' => $ukuran_dokumentasi,
                        'link_dokumentasi' => $result->getRealPath(),
                        'created_by' => $auth->id
                    ]);
                    $dokumentasi_insertedId = $dokumentasi->id;
                }
            }            
        }

        return redirect('/manajemenAset');
    }

    public function simpanAsetInformasi(Request $request)
    {   
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();
        $kode_grup_satuan = '';

        $grupSatuanFieldList = explode('~', $request->grup_satuan_field);
        for($i = 0 ; $i < count($grupSatuanFieldList)-1 ; $i++)
        {
            $grupSatuanField = explode('-', $grupSatuanFieldList[$i]);
            $fieldList = explode('_', $grupSatuanField[2]);

            $last_kode_aset_informasi = Aset_Informasi::getLastKodeAsetInformasi();
            $kode_aset_informasi = ((int)$last_kode_aset_informasi + 1);
            for($j = 0 ; $j < count($fieldList)-1 ; $j++)
            {
                $datatype = Ms_Field_Aset_Informasi::getDataTypeFieldAsetInformasi($fieldList[$j]);
                $isian_field_informasi = '';
                $keterangan_isian = '';
                
                if ($datatype->nama_datatype != 'file') {
                    $isian_field_informasi = $request->input('isian_field_informasi~' . $grupSatuanField[1] . '_' . $fieldList[$j]);
                } else {
                    if($request->hasFile('isian_field_informasi~' . $grupSatuanField[1] . '_' . $fieldList[$j])) { 
                        $uploadedFile = $request->file('isian_field_informasi~' . $grupSatuanField[1] . '_' . $fieldList[$j]);
                        
                        $rootPath = '/app/public/files/dokumentasi';
                        $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - ' . $uploadedFile->getClientOriginalName());
                        
                        $isian_field_informasi = $result->getRealPath();
                        $keterangan_isian = $uploadedFile->getClientOriginalName();
                    }
                }
                $aset_informasi = Aset_Informasi::create([
                    'id_aset' => (int)$grupSatuanField[0],
                    'kode_aset_informasi' => (int)$kode_aset_informasi,
                    'kode_grup_satuan' => (int)$grupSatuanField[1],
                    'id_field_info' => (int)$fieldList[$j],
                    'isian_field_informasi' => $isian_field_informasi,
                    'keterangan_isian' => $keterangan_isian,
                    'created_by' => $auth->id
                ]);

                $aset_informasi_insertedId = $aset_informasi->id;

                $aset_satuan_update = Aset_Satuan::where('kode_grup_satuan', $grupSatuanField[1])
                    ->update([
                        'kode_aset_informasi' => (int)$kode_aset_informasi,
                        'updated_at' => $current_datetime,
                        'updated_by' => $auth->id
                ]);
            }

            $kode_grup_satuan = $kode_grup_satuan . '#' . (int)$grupSatuanField[1];
            if($i < count($grupSatuanFieldList)-1)
            {
                $kode_grup_satuan  = $kode_grup_satuan . ', ';
            }

            $log = Ms_Log::getMsLog('ManajemenAsetController', 'simpanAsetInformasi');
            $aset_satuan_log = Aset_Satuan_Log::create([
                'kode_grup_satuan' => (int)$grupSatuanField[1],
                'id_log' => $log->id_log,
                'keterangan_log' => '-',
                'created_at' => $current_datetime,
                'created_by' => $auth->id
            ]);
        }

        return redirect('/manajemenAset')->with(['message_id' => 1, 'message_source' => 'Aset Informasi', 'message_crud' => 'ditambahkan', 'message_text' => 'Success', 'message_data' => $kode_grup_satuan]);
    }

    public function detailAsetInformasi($kodeGrupId)
    {
        $aset_satuan = Aset_Satuan::getSatuanAsetByKodeGrup($kodeGrupId);
        $gambar = Gambar::getGambarByKodeGrupSatuan($kodeGrupId);
        $field_isian_list = Aset_Satuan::getSatuanFieldIsianListByKodeGrup($kodeGrupId);
        $informasi_list = Aset_Informasi::getAsetInformasiListByKodeGrup($kodeGrupId);
        $aset_satuan_log = Aset_Satuan_Log::getLogByKodeGrupId($kodeGrupId);

        return view('/manajemen_aset/detailAsetInformasi', [
                    'kode_grup_satuan' => $kodeGrupId,
                    'aset_satuan' => $aset_satuan,
                    'gambar' => $gambar,
                    'field_isian_list' => $field_isian_list,
                    'informasi_list' => $informasi_list,
                    'aset_satuan_log' => $aset_satuan_log
                ]);
    }

    public function ubahFieldIsianAset(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $list_id_satuan = explode('-', $request->id_satuan);
        for ($i = 0 ; $i < count($list_id_satuan)-1 ; $i++)
        {
            $isian_field = $request->input('isian_field~' . $list_id_satuan[$i]);
            if($isian_field != null){
                $isian_field_update = Aset_Satuan::where('id_satuan', $list_id_satuan[$i])
                    ->update([
                        'isian_aset' => $isian_field,
                        'updated_at' => $current_datetime,
                        'updated_by' => $auth->id
                ]);
            }
        }
        return redirect('/manajemenAset/' . $request->kode_grup_satuan . '/informasi');
    }

    public function ubahAsetInformasi(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $list_id_aset_informasi = explode('-', $request->id_aset_informasi);

        for ($i = 0 ; $i < (count($list_id_aset_informasi)-1) ; $i++)
        {
            $id_field_info = Aset_Informasi::getIdFieldInfo($list_id_aset_informasi[$i])->id_field_info;
            $datatype = Ms_Field_Aset_Informasi::getDataTypeFieldAsetInformasi($id_field_info);
            $isian_field_informasi = '';
            $keterangan_isian = '';

            if ($datatype->nama_datatype != 'file') {
                $isian_field_informasi = $request->input('isian_field_informasi~' . $request->kode_grup_satuan . '_' . $id_field_info);
            } else {
                if($request->hasFile('isian_field_informasi~' . $request->kode_grup_satuan . '_' . $id_field_info)) { 
                    $uploadedFile = $request->file('isian_field_informasi~' . $request->kode_grup_satuan . '_' . $id_field_info);
                    
                    $rootPath = '/app/public/files/dokumentasi';
                    $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - ' . $uploadedFile->getClientOriginalName());
                    
                    $isian_field_informasi = $result->getRealPath();
                    $keterangan_isian = $uploadedFile->getClientOriginalName();
                }
            }

            $isian_field_info_update = Aset_Informasi::where('id_aset_informasi', $list_id_aset_informasi[$i])
                ->update([
                    'isian_field_informasi' => $isian_field_informasi,
                    'keterangan_isian' => $keterangan_isian,
                    'updated_at' => $current_datetime,
                    'updated_by' => $auth->id
            ]);
        }
        return redirect('/manajemenAset/' . $request->kode_grup_satuan . '/informasi');
    }

    //API

    public function getSumberPengadaanList(Request $request)
    {
        $list_sumber_aset = Aset::getSumberAset($request->id_aset);
        return response()->json(['list_sumber_aset' => $list_sumber_aset ]);
    }

    //Get Satuan Aset on the Manajemen Aset page
    public function getSatuanAsetPengadaanList(Request $request)
    {
        $list_satuan_aset_pengadaan = Aset::getSatuanAsetPengadaan($request->id_aset, $request->kode_pengadaan);
        return response()->json(['list_satuan_aset_pengadaan' => $list_satuan_aset_pengadaan]);
    }
}