<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Models\Pengadaan;
use App\Models\Pengadaan_Item;
use App\Models\Ms_Kategori_Pengadaan;
use App\Models\Ms_Penyedia;
use App\Models\Ms_Lokasi;
use App\Models\Dokumentasi;
use App\Models\Ms_Kategori_Aset;
use App\Models\Ms_Merek_Aset;
use App\Models\Aset_Satuan;
use App\Models\Aset_Masuk;
use App\Models\Aset_Stok;
use App\Models\Aset_Stok_Riwayat;
use App\Models\Ms_Log;
use App\Models\Aset_Satuan_Log;

class PengadaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter1 = 0;
        $filter1_name = null;
        $filter2 = 0;
        $filter2_name = null;

        if($request->is_filtered) {
            $is_filtered = $request->is_filtered;
            $filter1 = $request->filter1;
            if ($filter1 != 0) $filter1_name = Ms_Kategori_Pengadaan::getSingleKategoriPengadaan($filter1)->nama_kategori_pengadaan;
            $filter2 = $request->filter2;
            $filter2_name = $request->filter2;
        }

        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Pengadaan::getAllPengadaan($filter1,$filter2,$keyword,$skip,$take);
        $count = $result[0];
        $list_pengadaan = $result[1];

        $list_kategori_pengadaan = MS_Kategori_Pengadaan::get();
        $list_tahun_pengadaan = [];
        $idx = 0;
        $system_datetime = \Carbon\Carbon::now();
        $current_year = $system_datetime->year;
        for ($i = $current_year; $i >= $current_year-10; $i--)
        {
            $list_tahun_pengadaan[$idx] = $i;
            $idx++;
        }
        $list_penyedia = Ms_Penyedia::get();

        $system_datetime = \Carbon\Carbon::now();
        $current_year = $system_datetime->year;

        $idx = 0;
        for ($i = $current_year; $i >= $current_year-10; $i--)
        {
            $list_tahun_pengadaan[$idx] = $i;
            $idx++;
        }

        return view('/pengadaan/pengadaanIndex', [
            'list_pengadaan' => $list_pengadaan,
            'list_kategori_pengadaan' => $list_kategori_pengadaan,
            'list_tahun_pengadaan' => $list_tahun_pengadaan,
            'list_penyedia' => $list_penyedia,
            'count_list_pengadaan' => $count,
            'is_filtered' => $is_filtered,
            'filter1' => $filter1,
            'filter1_name' => $filter1_name,
            'filter2' => $filter2,
            'filter2_name' => $filter2_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function detail($pengadaanId)
    {
        $pengadaan = Pengadaan::getPengadaanByID($pengadaanId);
        $list_item_pengadaan = Pengadaan_Item::getItemPengadaan($pengadaanId);
        $list_dokumentasi_pengadaan = Dokumentasi::getDokumentasiList('pengadaan', 'kode_pengadaan', $pengadaanId);

        return view('/pengadaan/pengadaanDetail', [
            'pengadaan' => $pengadaan,
            'list_item_pengadaan' => $list_item_pengadaan,
            'list_dokumentasi_pengadaan' => $list_dokumentasi_pengadaan
        ]);
    }

    public function add()
    {
        $list_kategori_pengadaan = MS_Kategori_Pengadaan::get();
        $list_tahun_pengadaan = [];
        $list_penyedia = Ms_Penyedia::get();
        $list_lokasi = Ms_Lokasi::get();
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_merek_aset = Ms_Merek_Aset::get();

        $system_datetime = \Carbon\Carbon::now();
        $current_year = $system_datetime->year;

        $idx = 0;
        for ($i = $current_year; $i >= $current_year-10; $i--)
        {
            $list_tahun_pengadaan[$idx] = $i;
            $idx++;
        }

        return view('/pengadaan/pengadaanAdd', [
                    'list_kategori_pengadaan' => $list_kategori_pengadaan,
                    'list_tahun_pengadaan' => $list_tahun_pengadaan,
                    'list_penyedia' => $list_penyedia,
                    'list_lokasi' => $list_lokasi,
                    'list_kategori_aset' => $list_kategori_aset,
                    'list_merek_aset' => $list_merek_aset
                ]);
    }

    public function simpanPengadaan(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $pengadaan = Pengadaan::create([
            'nama_pengadaan' => $request->nama_pengadaan,
            'id_penyedia' => $request->id_penyedia,
            'id_kategori_pengadaan' => $request->id_kategori_pengadaan,
            'nomor_pengadaan' => $request->nomor_pengadaan,
            'tahun_pengadaan' => $request->tahun_pengadaan,
            'kode_anggaran' => $request->kode_anggaran,
            'nilai_anggaran' => $request->nilai_anggaran,
            'nama_ppk' => $request->nama_ppk,
            'tanggal_mulai_kontrak' => date('Y-m-d', strtotime($request->tanggal_mulai_kontrak)),
            'tanggal_selesai_kontrak' => date('Y-m-d', strtotime($request->tanggal_selesai_kontrak)),
            'keterangan_pengadaan' => $request->keterangan_pengadaan,
            'created_by' => $auth->id
        ]);
        $pengadaan_insertedId = $pengadaan->id;

        $fileCount = count($request->file());
        if($fileCount != 0) {
            $files = $request->file();
            $filesIndex = [];
            $idx = 0;
            foreach ($files as $file) {
                $input_name = array_search($file, $files);
                $input_index = str_replace('id_dokumentasi~', '', $input_name);
                $filesIndex[$idx] = $input_index;
                $idx++;
            }
            foreach($filesIndex as $idx) {
                if($request->hasFile('id_dokumentasi~'. $idx)){
                    $uploadedFile = $request->file('id_dokumentasi~'. $idx);

                    $rootPath = '\app\public\files\dokumentasi';
                    $nama_dokumentasi = $request->input('nama_dokumentasi~'.$idx);
                    $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - '. $uploadedFile->getClientOriginalName());

                    $id_lokasi = $request->input('dokumentasi~lokasi~'. $idx);
                    $id_kontainer = $request->input('dokumentasi~kontainer~'. $idx);
                    $id_posisi_kontainer = $request->input('dokumentasi~posisi_kontainer~'. $idx);

                    $keterangan_dokumentasi = $request->input('keterangan_dokumentasi~'.$idx);
                    $ukuran_dokumentasi = 0;
                    try {
                        $ukuran_dokumentasi = $request->file('id_dokumentasi~'.$idx)->getClientSize();
                    }
                    catch (\Exception $e) {
                        $ukuran_dokumentasi = 0;
                    }
                    
                    $tanggal_dokumentasi = $request->input('tanggal_dokumentasi~'.$idx);
                    $dokumentasi = Dokumentasi::create([
                        'src_tabel_dokumentasi' => 'pengadaan',
                        'src_kolom_dokumentasi' => 'kode_pengadaan',
                        'src_fk_dokumentasi' => $pengadaan_insertedId,
                        'id_lokasi' => $id_lokasi,
                        'id_kontainer' => $id_kontainer,
                        'id_posisi_kontainer' => $id_posisi_kontainer,
                        'nama_dokumentasi' => $nama_dokumentasi,
                        'keterangan_dokumentasi' => $keterangan_dokumentasi,
                        'tanggal_dokumentasi' => $tanggal_dokumentasi,
                        'ukuran_dokumentasi' => $ukuran_dokumentasi,
                        'link_dokumentasi' => $result->getRealPath(),
                        'created_by' => $auth->id
                    ]);
                    $dokumentasi_insertedId = $dokumentasi->id;
                }
            }            
        }

        $itemIndexArray = explode('~', $request->index_item);
        $itemCount = $request->count_item;

        $last_nomor_aset_masuk = 0;
        $no_aset_masuk = Aset_Masuk::getLastNomorAsetMasuk();
        if($no_aset_masuk) {
            $last_nomor_aset_masuk = $no_aset_masuk->nomor_aset_masuk;
        }
        $nomor_aset_masuk = ((int)$last_nomor_aset_masuk + 1);
        for($i = 0; $i < $itemCount; $i++)
        {
            $pengadaan_item = Pengadaan_Item::create([
                'kode_pengadaan' => $pengadaan_insertedId,
                'id_aset' => $request->input('tipe_aset~'.$itemIndexArray[$i]),
                'created_by' => $auth->id
            ]);
            $pengadaan_item_insertedId = $pengadaan_item->id;
            
            $cek_stok = Aset_Stok::getAsetStokByID($request->input('tipe_aset~'.$itemIndexArray[$i]));

            $id_stok = 0;
            $jumlah_stok = 0;
            if ($cek_stok) {
                $aset_stok = Aset_Stok::where('id_stok', $cek_stok->id_stok)
                    ->update([
                        'jumlah_stok' => ($cek_stok->jumlah_stok +  $request->input('jumlah_stok~' . $itemIndexArray[$i])),
                        'tanggal_stok' => $current_datetime,
                        'updated_at' => $current_datetime,
                        'updated_by' => $auth->id
                ]);
                $id_stok = $cek_stok->id_stok;
                $jumlah_stok = $cek_stok->jumlah_stok + $request->input('jumlah_stok~' . $itemIndexArray[$i]);
            } else {
                $aset_stok = Aset_Stok::create([
                    'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                    'jumlah_stok' => $request->input('jumlah_stok~' . $itemIndexArray[$i]),
                    'tanggal_stok' => $current_datetime,
                    'satuan_stok' => $request->input('satuan_stok~' . $itemIndexArray[$i]),
                    'created_by' => $auth->id

                ]);
                $id_stok = $aset_stok->id;
                $jumlah_stok = $request->input('jumlah_stok~' . $itemIndexArray[$i]);
            }

            $aset_stok_riwayat = Aset_Stok_Riwayat::create([
                'id_stok' => $id_stok,
                'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                'jumlah_stok' => $jumlah_stok,
                'tanggal_stok' => $current_datetime,
                'created_by' => $auth->id
            ]);

            $aset_masuk = Aset_Masuk::create([
                'nomor_aset_masuk' => $nomor_aset_masuk,
                'id_stok' => $id_stok,
                'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                'kode_pengadaan' => $pengadaan_insertedId,
                'jumlah_aset_masuk' => $request->input('jumlah_stok~' . $itemIndexArray[$i]),
                'tanggal_aset_masuk' => $current_datetime,
                'keterangan_aset_masuk' => 'Aset masuk berasal dari ' . $request->nama_pengadaan,
                'harga_satuan' => $request->input('harga_satuan~' . $itemIndexArray[$i]),
                'created_at' => $current_datetime,
                'created_by' => $auth->id
            ]);
            $aset_masuk_insertedId = $aset_masuk->id;

            $satuanIndexArray = explode('~', $request->input('index_satuan~'.$itemIndexArray[$i]));
            $satuanCount = $request->input('count_satuan~'.$itemIndexArray[$i]);
            for ($j = 0 ; $j < $satuanCount ; $j++)
            {
                $last_kode_grup_satuan = Aset_Satuan::getLastKodeGrupSatuan();
                $kode_grup_satuan = ((int)$last_kode_grup_satuan->kode_grup_satuan + 1);

                $satuanIndexFieldIsian = explode('~', $request->input('index_field_isian_satuan~'. $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]));
                $countSatuanIndexFieldIsian = count($satuanIndexFieldIsian) - 1;
                for($k = 0 ; $k < $countSatuanIndexFieldIsian ; $k++)
                {
                    $aset_satuan = Aset_Satuan::create([
                        'kode_pengadaan' => $pengadaan_insertedId,
                        'kode_grup_satuan' => $kode_grup_satuan,
                        'id_item' => $pengadaan_item_insertedId,
                        'id_aset' => $request->input('tipe_aset~' . $itemIndexArray[$i]),
                        'id_aset_masuk' => $aset_masuk_insertedId,
                        'id_lokasi' => $request->input('satuan~lokasi~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]),
                        'id_kontainer' => $request->input('satuan~kontainer~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]),
                        'id_posisi_kontainer' => $request->input('satuan~posisi_kontainer~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j]),
                        'field_aset' => $request->input('field_aset~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j] . '-' . $satuanIndexFieldIsian[$k]),
                        'isian_aset' => $request->input('isian_aset~' . $itemIndexArray[$i] . '-' . $satuanIndexArray[$j] . '-' . $satuanIndexFieldIsian[$k]),
                        'created_at' => $current_datetime,
                        'created_by' => $auth->id
                    ]);
                }

                $log = Ms_Log::getMsLog('PengadaanController', 'simpanPengadaan');
                $aset_satuan_log = Aset_Satuan_Log::create([
                    'kode_grup_satuan' => $kode_grup_satuan,
                    'id_log' => $log->id_log,
                    'keterangan_log' => $request->nama_pengadaan . ' Tahun Anggaran ' . $request->tahun_pengadaan . ' (' . $request->nomor_pengadaan . ')',
                    'created_at' => $current_datetime,
                    'created_by' => $auth->id
                ]);
            }
        }
        return redirect('/pengadaan');
    }

    public function edit($pengadaanId)
    {
        $pengadaan = Pengadaan::getPengadaanByID($pengadaanId);
        $list_dokumentasi_pengadaan = Dokumentasi::getDokumentasiList('pengadaan', 'kode_pengadaan', $pengadaanId);
        $list_item_pengadaan = Pengadaan_Item::getItemPengadaan($pengadaanId);
        $list_kategori_pengadaan = MS_Kategori_Pengadaan::get();
        $list_tahun_pengadaan = [];
        $list_penyedia = Ms_Penyedia::get();
        $list_lokasi = Ms_Lokasi::get();
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_merek_aset = Ms_Merek_Aset::get();

        $system_datetime = \Carbon\Carbon::now();
        $current_year = $system_datetime->year;

        $idx = 0;
        for ($i = $current_year; $i >= $current_year-10; $i--)
        {
            $list_tahun_pengadaan[$idx] = $i;
            $idx++;
        }

        return view('/pengadaan/pengadaanEdit', [
                    'pengadaan' => $pengadaan,
                    'list_dokumentasi_pengadaan' => $list_dokumentasi_pengadaan,
                    'list_item_pengadaan' => $list_item_pengadaan,
                    'list_kategori_pengadaan' => $list_kategori_pengadaan,
                    'list_tahun_pengadaan' => $list_tahun_pengadaan,
                    'list_penyedia' => $list_penyedia,
                    'list_lokasi' => $list_lokasi,
                    'list_kategori_aset' => $list_kategori_aset,
                    'list_merek_aset' => $list_merek_aset
                ]);
    }

    public function delete(Request $request)
    {
        Pengadaan::where('kode_pengadaan', $request->kode_pengadaan)->delete();
        return redirect()->route('pengadaan');        
    }


    public function getSatuanItemPengadaan(Request $request)
    {
        $list_satuan_item_pengadaan = Pengadaan_Item::getSatuanItemPengadaan($request->kode_pengadaan, $request->id_aset);
        return response()->json([ 'list_satuan_item_pengadaan' => $list_satuan_item_pengadaan ]);
    }

}