<?php

namespace App\Http\Controllers;
use DataTables;
//use Session;
//use Illuminate\Support\Facades\Session;
use App\User;
use App\Models\MS_Jenis_Kontainer;
use App\Models\MS_Kontainer;
use App\Models\MS_Lokasi;
use App\Models\MS_Posisi_Kontainer;
use App\Models\MS_Jenis_Aset;
use App\Models\MS_Merek_Aset;
use App\Models\MS_Jenis_Pengembangan;
use App\Models\MS_Kantor_Imigrasi;
use App\Models\MS_Penyedia;
use App\Models\MS_Menu;
use App\Models\Ms_Field_Spesifikasi_Aset;
use App\Models\Ms_Field_Aset_Informasi;
use App\Models\Dokumentasi;
use App\Models\Aplikasi_Sourcecode;
use App\Models\Aset_Informasi;
use App\Models\Ms_Signature;
use App\Models\Ms_Kategori_Aset;
use App\Models\Template;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;


class PengaturanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

   
/******************************************** DASHBOARD KONTAINER *********************************************/
    public function dashboard_kontainer()
    {
        
        $listlokasikontainer = MS_Lokasi::get();
        $listjeniskontainer = MS_Jenis_Kontainer::get();
        
        return view('/pengaturan/dashboard_kontainer', [
            'listlokasikontainer' => $listlokasikontainer,
            'listjeniskontainer' => $listjeniskontainer,
        ]);
    }

/******************************************** template *********************************************/
    public function templateBAST()
    {
        $summernote = Template::find(1);
        $ttd = Ms_Signature::getSignature(1);
        $listsign = Ms_Signature::getMsSignature();

        return view('pengaturan/templateBAST', [
            'summernote' => $summernote,
            'listsign' => $listsign,
            'ttd' => $ttd
        ]);
    }

    public function templateSPP()
    {
        
        $summernote = Template::find(1);
        $ttd = Ms_Signature::getSignature(1);
        $listsign = Ms_Signature::getMsSignature();

        return view('pengaturan/templateSPP', [
            'summernote' => $summernote,
            'listsign' => $listsign,
            'ttd' => $ttd
        ]);
    }

/******************************************** SIGNATURE *********************************************/
    public function signature(){
        $sign = null;
        $listsign = Ms_Signature::getListSignature();

        return view('pengaturan/signature',['sign' => $sign,'listsign' => $listsign]);
    }

    public function signature_save(Request $request)
    {

        $existsignature = Ms_Signature::getSignatureById($request->id_sign);

        if(!$existsignature){
            if($request->hasFile('gambar')){
                $uploadedFile = $request->file('gambar');  
                $path = $uploadedFile->store('public/files/signature');
                $filefoto_name = $uploadedFile->getClientOriginalName();
            }
            $master_signature = Ms_Signature::create([
                'jabatan' => $request->jabatan,
                'nama_pejabat' => $request->nama_pejabat,
                //'link_signature' => 'storage/app/signature/'.$path,
                'link_signature' => $path,
                'nip' => $request->nip,
            ]);
            //$permohonan_foto_insertedId = $permohonan_foto->id; 
        }
        else{
           if($request->hasFile('gambar')){
                $uploadedFile = $request->file('gambar');  
                $path = $uploadedFile->store('public/files/signature');
                $filefoto_name = $uploadedFile->getClientOriginalName();
                //$link_signature = 'storage/app/signature/'.$path;
                $link_signature = $path;
            }
            else { 
                $link_signature = $request->link_signature;
            }
            $master_signature = DB::table('ms_signature')->where('id', $request->id_sign)
            ->update([
                'jabatan' => $request->jabatan,
                'nama_pejabat' => $request->nama_pejabat,
                'link_signature' => $link_signature,
                'nip' => $request->nip,
            ]);
        }

           
       
        return redirect('/signature');
    }

    public function signature_edit($id)
    {
        
        $sign = Ms_Signature::getSignatureById($id);

        $listsign = Ms_Signature::getListSignature();
        return view('pengaturan/signature',['sign' => $sign,'listsign' => $listsign]);

    }

    public function signature_delete($id)
    {
        //delete
        $crud = Ms_Signature::getSignatureById($id);
        DB::table('ms_signature')
        ->where('id',$id)
        ->delete();


        $sign = null;
        $listsign = Ms_Signature::getListSignature();
        return redirect('signature');

    }

/******************************************** JENIS KONTAINER *********************************************/
    public function jenis_kontainer()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $mode = 'create';

        $listjeniskontainer = MS_Jenis_Kontainer::paginate(10);
        
        return view('/pengaturan/jenis_kontainer', [
            'result' => $result,
            'param' => $param,
            'listjeniskontainer' => $listjeniskontainer,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function jenis_kontainer_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $keterangan = $request->keterangan;
        $mode = $request->mode;

        
         $listjeniskontainer = MS_Jenis_Kontainer::paginate(10);

        return view('/pengaturan/jenis_kontainer', [
            'result' => $result,
            'param' => $param,
            'listjeniskontainer' => $listjeniskontainer,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function jenis_kontainer_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            //DB::table('ms_jenis_kontainer')->where('id_jeniskon',$id)
            MS_Jenis_Kontainer::where('id_jenis_kontainer', $request->id)->delete();
            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/jenis_kontainer')->with('noticemessage', 'Hapus jenis kontainer  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/jenis_kontainer')->with('noticemessage', 'Hapus jenis kontainer gagal.')->with('flag','0');
        
    }

    public function jenis_kontainer_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Jenis_Kontainer::where('id_jenis_kontainer', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Jenis_Kontainer::where('id_jenis_kontainer',$request->id)->update([
                        'nama_jenis_kontainer' => $request->name,
                        'keterangan_jenis_kontainer' => $request->keterangan,
                        'updated_by' => $auth->id,
                ]); 
                return redirect('/jenis_kontainer')->with('noticemessage', 'Update jenis kontainer  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/jenis_kontainer')->with('noticemessage', 'Update jenis kontainer gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Jenis_Kontainer::where('nama_jenis_kontainer', $request->name)->first();

            if(!$exist){
                $user = MS_Jenis_Kontainer::create([
                    'nama_jenis_kontainer' => $request->name,
                    'keterangan_jenis_kontainer' => $request->keterangan,
                    'created_by' => $auth->id,
                ]);
                return redirect('/jenis_kontainer')->with('noticemessage', 'Pendaftaran jenis kontainer  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/jenis_kontainer')->with('noticemessage', 'Pendaftaran jenis kontainer gagal.')->with('flag','0');   
            }
        }
    }

/******************************************** KONTAINER *********************************************/
    public function kontainer()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $jumlah_slot = 0;
        $id_jenis_kontainer = null;
        $id_lokasi = null;
        $mode = 'create';

        $listkontainer = MS_Kontainer::paginate(10);
        $listjeniskontainer = MS_Jenis_Kontainer::get();
        $listlokasi = MS_Lokasi::get();
        
        return view('/pengaturan/kontainer', [
            'result' => $result,
            'param' => $param,
            'listkontainer' => $listkontainer,
            'listjeniskontainer' => $listjeniskontainer,
            'listlokasi' => $listlokasi,
            'id' => $id,
            'name' => $name,
            'jumlah_slot' => $jumlah_slot, 
            'id_jenis_kontainer' => $id_jenis_kontainer,
            'id_lokasi' => $id_lokasi,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function kontainer_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $keterangan = $request->keterangan;
        $mode = $request->mode;
        $jumlah_slot = $request->jumlah_slot;
        $id_jenis_kontainer = $request->id_jenis_kontainer;
        $id_lokasi = $request->id_lokasi;

        $listkontainer = MS_Kontainer::paginate(10);
        $listjeniskontainer = MS_Jenis_Kontainer::get();
        $listlokasi = MS_Lokasi::get();

        return view('/pengaturan/kontainer', [
            'result' => $result,
            'param' => $param,
            'listkontainer' => $listkontainer,
            'listjeniskontainer' => $listjeniskontainer,
            'listlokasi' => $listlokasi,
            'id' => $id,
            'name' => $name,
            'jumlah_slot' => $jumlah_slot, 
            'id_jenis_kontainer' => $id_jenis_kontainer,
            'id_lokasi' => $id_lokasi,
            'keterangan' => $keterangan,
            'mode' => $mode  
        ]);
    }

    public function kontainer_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){            
            MS_Kontainer::where('id_kontainer', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect($request->route)->with('noticemessage', 'Hapus jenis kontainer  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect($request->route)->with('noticemessage', 'Hapus jenis kontainer gagal.')->with('flag','0');
        
    }

    public function kontainer_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Kontainer::where('id_kontainer', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Kontainer::where('id_kontainer',$request->id)->update([
                        'id_lokasi' => $request->id_lokasi,
                        'id_jenis_kontainer' => $request->id_jenis_kontainer,
                        'nama_kontainer' => $request->name,
                        'jumlah_slot' => $request->jumlah_slot,
                        'keterangan_kontainer' => $request->keterangan,
                        'updated_by' => $auth->id,
                ]); 
                return redirect($request->route)->with('noticemessage', 'Update jenis kontainer  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect($request->route)->with('noticemessage', 'Update jenis kontainer gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Kontainer::where('nama_kontainer', $request->name)->first();

            if(!$exist){
                $user = MS_Kontainer::create([
                        'id_lokasi' => $request->id_lokasi,
                        'id_jenis_kontainer' => $request->id_jenis_kontainer,
                        'nama_kontainer' => $request->name,
                        'jumlah_slot' => $request->jumlah_slot,
                        'keterangan_kontainer' => $request->keterangan,
                        'created_by' => $auth->id,
                ]);
                return redirect($request->route)->with('noticemessage', 'Pendaftaran jenis kontainer  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect($request->route)->with('noticemessage', 'Pendaftaran jenis kontainer gagal.')->with('flag','0');   
            }
        }
    }

    public function getKontainerList(Request $request)
    {
        $list_kontainer = MS_Kontainer::getKontainer($request->lokasi);
        return response()->json($list_kontainer);
    }


/******************************************** POSISI *********************************************/
    public function posisi_kontainer()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $id_kontainer = null;
        $mode = 'create';

        $listposisikontainer = MS_Posisi_Kontainer::paginate(10);
        $listkontainer = MS_Kontainer::get();
        
        return view('/pengaturan/posisi_kontainer', [
            'result' => $result,
            'param' => $param,
            'listposisikontainer' => $listposisikontainer,
            'listkontainer' => $listkontainer,
            'id' => $id,
            'name' => $name,
            'id_kontainer' => $id_kontainer,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function posisi_kontainer_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $keterangan = $request->keterangan;
        $mode = $request->mode;
        $id_kontainer = $request->id_kontainer;

        $listposisikontainer = MS_Posisi_Kontainer::paginate(10);
        $listkontainer = MS_Kontainer::get();

        return view('/pengaturan/posisi_kontainer', [
            'result' => $result,
            'param' => $param,
            'listposisikontainer' => $listposisikontainer,
            'listkontainer' => $listkontainer,
            'id' => $id,
            'name' => $name,            
            'id_kontainer' => $id_kontainer,
            'keterangan' => $keterangan,
            'mode' => $mode  
        ]);
    }

    public function posisi_kontainer_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){            
            MS_Posisi_Kontainer::where('id_posisi_kontainer', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect($request->route)->with('noticemessage', 'Hapus posisi kontainer  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect($request->route)->with('noticemessage', 'Hapus posisi kontainer gagal.')->with('flag','0');
        
    }

    public function posisi_kontainer_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Posisi_Kontainer::where('id_posisi_kontainer', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Posisi_Kontainer::where('id_posisi_kontainer',$request->id)->update([
                        'id_kontainer' => $request->id_kontainer,
                        'label_posisi_kontainer' => $request->name,
                        'keterangan_posisi_kontainer' => $request->keterangan,
                        'updated_by' => $auth->id,
                ]); 
                return redirect($request->route)->with('noticemessage', 'Update posisi kontainer  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect($request->route)->with('noticemessage', 'Update posisi kontainer gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Posisi_Kontainer::where('label_posisi_kontainer', $request->name)->first();

            if(!$exist){
                $user = MS_Posisi_Kontainer::create([
                        'id_kontainer' => $request->id_kontainer,
                        'label_posisi_kontainer' => $request->name,
                        'keterangan_posisi_kontainer' => $request->keterangan,
                        'created_by' => $auth->id,
                ]);
                return redirect($request->route)->with('noticemessage', 'Pendaftaran posisi kontainer  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect($request->route)->with('noticemessage', 'Pendaftaran posisi kontainer gagal.')->with('flag','0');   
            }
        }
    }


    public function getPosisiKontainerList(Request $request)
    {
        $list_posisi_kontainer = MS_Posisi_Kontainer::getPosisiKontainer($request->kontainer);
        return response()->json($list_posisi_kontainer);
    }


    /******************************************** LOKASI *********************************************/
    public function lokasi_kontainer()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $mode = 'create';

        $listlokasi = MS_Lokasi::paginate(10);
        
        return view('/pengaturan/lokasi_kontainer', [
            'result' => $result,
            'param' => $param,
            'listlokasi' => $listlokasi,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function lokasi_kontainer_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $keterangan = $request->keterangan;
        $mode = $request->mode;

        
        $listlokasi = MS_Lokasi::paginate(10);

        return view('/pengaturan/lokasi_kontainer', [
            'result' => $result,
            'param' => $param,
            'listlokasi' => $listlokasi,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function lokasi_kontainer_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            MS_Lokasi::where('id_lokasi', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect($request->route)->with('noticemessage', 'Hapus lokasi kontainer  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect($request->route)->with('noticemessage', 'Hapus lokasi kontainer gagal.')->with('flag','0');
        
    }

    public function lokasi_kontainer_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Lokasi::where('id_lokasi', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Lokasi::where('id_lokasi',$request->id)->update([
                        'nama_lokasi' => $request->name,
                        'keterangan_lokasi' => $request->keterangan,
                        'updated_by' => $auth->id,
                ]); 
                return redirect($request->route)->with('noticemessage', 'Update lokasi kontainer  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect($request->route)->with('noticemessage', 'Update lokasi kontainer gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Lokasi::where('nama_lokasi', $request->name)->first();

            if(!$exist){
                $user = MS_Lokasi::create([
                    'nama_lokasi' => $request->name,
                    'keterangan_lokasi' => $request->keterangan,
                    'created_by' => $auth->id,
                ]);
                return redirect($request->route)->with('noticemessage', 'Pendaftaran lokasi kontainer  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect($request->route)->with('noticemessage', 'Pendaftaran lokasi kontainer gagal.')->with('flag','0');   
            }
        }
    }

/******************************************** JENIS ASET *********************************************/
    public function jenis_aset()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $mode = 'create';
        $sign = null;
        $id_kategori = null;

        $listjenisaset = Ms_Jenis_Aset::getAllJenisAset();
        $listkategori = Ms_Kategori_Aset::get();
        
        return view('/pengaturan/jenis_aset', [
            'result' => $result,
            'param' => $param,
            'listjenisaset' => $listjenisaset,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode,
            'listkategori' => $listkategori,
            'sign' => $sign,
            'id_kategori_aset' => $id_kategori,
        ]);
    }

    public function jenis_aset_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $keterangan = $request->keterangan;
        $mode = $request->mode;
        $id_kategori_aset = $request->id_kategori;
        
         $listjenisaset = Ms_Jenis_Aset::getAllJenisAset();
         $sign = Ms_Jenis_Aset::getJenisAsetbyId($id);
         $listkategori = Ms_Kategori_Aset::get();
         /*$rs            = DB::table('ms_kategori_aset')
                            ->select('ms_kategori_aset.id_kategori_aset','ms_kategori_aset.nama_kategori_aset')
                            ->join('ms_jenis_aset', 'ms_kategori_aset.id_kategori_aset', 'ms_jenis_aset.id_kategori_aset') 
                            ->distinct()
                            ->get();*/

        return view('/pengaturan/jenis_aset', [
            'result' => $result,
            'param' => $param,
            'listjenisaset' => $listjenisaset,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode,
            'sign' => $sign, 
            'listkategori' => $listkategori,
            'id_kategori_aset' => $id_kategori_aset
        ]);
    }

    public function jenis_aset_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            Ms_Jenis_Aset::where('id_jenis_aset', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/jenis_aset')->with('noticemessage', 'Hapus jenis aset  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/jenis_aset')->with('noticemessage', 'Hapus jenis aset gagal.')->with('flag','0');
        
    }

    public function jenis_aset_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(Ms_Jenis_Aset::where('id_jenis_aset', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }

            if($exist){
                Ms_Jenis_Aset::where('id_jenis_aset',$request->id)->update([
                        'nama_jenis_aset' => $request->name,
                        'keterangan_jenis_aset' => $request->keterangan,
                        'id_kategori_aset' => $request->id_kategori_aset,
                        'updated_by' => $auth->id
                ]); 
                return redirect('/jenis_aset')->with('noticemessage', 'Update jenis aset  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/jenis_aset')->with('noticemessage', 'Update jenis aset gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = Ms_Jenis_Aset::where('nama_jenis_aset', $request->name)->first();

            if(!$exist){
                $user = MS_Jenis_Aset::create([
                    'nama_jenis_aset' => $request->name,
                    'keterangan_jenis_aset' => $request->keterangan,
                    'id_kategori_aset' => $request->id_kategori_aset,
                    'created_by' => $auth->id
                ]);
                return redirect('/jenis_aset')->with('noticemessage', 'Pendaftaran jenis aset  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/jenis_aset')->with('noticemessage', 'Pendaftaran jenis aset gagal.')->with('flag','0');   
            }
        }
    }

    public function getJenisAsetList(Request $request)
    {
        $list_jenis_aset = Ms_Jenis_Aset::getJenisAset($request->kategori_aset);
        return response()->json($list_jenis_aset);
    }

/******************************************** KATEGORI ASET *********************************************/
    public function kategori_aset()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $mode = 'create';

        $listkategoriaset = Ms_Kategori_Aset::paginate(10);
        
        return view('/pengaturan/kategori_aset', [
            'result' => $result,
            'param' => $param,
            'listkategoriaset' => $listkategoriaset,
            'id_kategori_aset' => $id,
            'nama_kategori_aset' => $name,
            'mode' => $mode
        ]);
    }

    public function kategori_aset_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $mode = $request->mode;

        
         $listkategoriaset = Ms_Kategori_Aset::paginate(10);

        return view('/pengaturan/kategori_aset', [
            'result' => $result,
            'param' => $param,
            'listkategoriaset' => $listkategoriaset,
            'id_kategori_aset' => $id,
            'nama_kategori_aset' => $name,
            'mode' => $mode
        ]);
    }

    public function kategori_aset_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            Ms_Kategori_Aset::where('id_kategori_aset', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/kategori_aset')->with('noticemessage', 'Hapus kategori aset  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/kategori_aset')->with('noticemessage', 'Hapus kategori aset gagal.')->with('flag','0');
        
    }

    public function kategori_aset_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(Ms_Kategori_Aset::where('id_kategori_aset', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                Ms_Kategori_Aset::where('id_kategori_aset',$request->id)->update([
                        'nama_kategori_aset' => $request->name,
                        'updated_by' => $auth->id,
                ]); 
                return redirect('/kategori_aset')->with('noticemessage', 'Update kategori aset  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/kategori_aset')->with('noticemessage', 'Update kategori aset gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = Ms_Kategori_Aset::where('nama_kategori_aset', $request->name)->first();

            if(!$exist){
                $user = Ms_Kategori_Aset::create([
                    'nama_kategori_aset' => $request->name,
                    'created_by' => $auth->id,
                ]);
                return redirect('/kategori_aset')->with('noticemessage', 'Pendaftaran kategori aset  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/kategori_aset')->with('noticemessage', 'Pendaftaran kategori aset gagal.')->with('flag','0');   
            }
        }
    }

/******************************************** MEREK ASET *********************************************/
    public function merek_aset()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $mode = 'create';
        $id_jenis_aset = null;
        $sign = null;

        $listmerekaset = MS_Merek_Aset::paginate(10);
        $list_jenis_aset = Ms_Jenis_Aset::get();
        
        return view('/pengaturan/merek_aset', [
            'result' => $result,
            'param' => $param,
            'listmerekaset' => $listmerekaset,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode,
            'id_jenis_aset' => $id_jenis_aset,
            'sign' => $sign,
            'list_jenis_aset' => $list_jenis_aset
        ]);
    }

    public function merek_aset_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $keterangan = $request->keterangan;
        $mode = $request->mode;
        $id_jenis_aset = $request->id_jenis_aset;

        
         $listmerekaset = MS_Merek_Aset::getAllJenisAset();
         $sign = Ms_Merek_Aset::getMerekAsetbyId($id);
         $listjenis = Ms_Jenis_Aset::get();

        return view('/pengaturan/merek_aset', [
            'result' => $result,
            'param' => $param,
            'listmerekaset' => $listmerekaset,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode,
            'sign' => $sign,
            'id_jenis_aset' => $id_jenis_aset,
            'listjenis' => $listjenis,
        ]);
    }

    public function merek_aset_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            MS_Merek_Aset::where('id_merek', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/merek_aset')->with('noticemessage', 'Hapus merek aset  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/merek_aset')->with('noticemessage', 'Hapus merek aset gagal.')->with('flag','0');
        
    }

    public function merek_aset_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Merek_Aset::where('id_merek', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Merek_Aset::where('id_merek',$request->id)->update([
                        'nama_merek' => $request->name,
                        'keterangan_merek' => $request->keterangan,
                        'id_jenis_aset' => $request->id_jenis_aset,
                        'updated_by' => $auth->id,
                ]); 
                return redirect('/merek_aset')->with('noticemessage', 'Update merek aset  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/merek_aset')->with('noticemessage', 'Update merek aset gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Merek_Aset::where('nama_merek', $request->name)->first();

            if(!$exist){
                $user = MS_Merek_Aset::create([
                    'nama_merek' => $request->name,
                    'keterangan_merek' => $request->keterangan,
                    'id_jenis_aset' => $request->id_jenis_aset,
                    'created_by' => $auth->id,
                ]);
                return redirect('/merek_aset')->with('noticemessage', 'Pendaftaran merek aset  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/merek_aset')->with('noticemessage', 'Pendaftaran merek aset gagal.')->with('flag','0');   
            }
        }
    }


/******************************************** JENIS PENGEMBANGAN *********************************************/
    public function jenis_pengembangan()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $mode = 'create';

        $listjenispengembangan = MS_Jenis_Pengembangan::paginate(10);
        
        return view('/pengaturan/jenis_pengembangan', [
            'result' => $result,
            'param' => $param,
            'listjenispengembangan' => $listjenispengembangan,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function jenis_pengembangan_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $keterangan = $request->keterangan;
        $mode = $request->mode;

        
         $listjenispengembangan = MS_Jenis_Pengembangan::paginate(10);

        return view('/pengaturan/jenis_pengembangan', [
            'result' => $result,
            'param' => $param,
            'listjenispengembangan' => $listjenispengembangan,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function jenis_pengembangan_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            MS_Jenis_Pengembangan::where('id_jenis_pengembangan', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/jenis_pengembangan')->with('noticemessage', 'Hapus jenis pengembangan  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/jenis_pengembangan')->with('noticemessage', 'Hapus jenis pengembangan gagal.')->with('flag','0');
        
    }

    public function jenis_pengembangan_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Jenis_Pengembangan::where('id_jenis_pengembangan', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Jenis_Pengembangan::where('id_jenis_pengembangan',$request->id)->update([
                        'nama_jenis_pengembangan' => $request->name,
                        'keterangan_jenis_pengembangan' => $request->keterangan,
                        'updated_by' => $auth->id,
                ]); 
                return redirect('/jenis_pengembangan')->with('noticemessage', 'Update jenis pengembangan  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/jenis_pengembangan')->with('noticemessage', 'Update jenis pengembangan gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Jenis_Pengembangan::where('jenis_pengembangan', $request->name)->first();

            if(!$exist){
                $user = MS_Jenis_Pengembangan::create([
                    'nama_jenis_pengembangan' => $request->name,
                    'keterangan_jenis_pengembangan' => $request->keterangan,
                    'created_by' => $auth->id,
                ]);
                return redirect('/jenis_pengembangan')->with('noticemessage', 'Pendaftaran jenis pengembangan  '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/jenis_pengembangan')->with('noticemessage', 'Pendaftaran jenis pengembangan gagal.')->with('flag','0');   
            }
        }
    }


/******************************************** UPT *********************************************/
    public function kantorjson(){
        $data = MS_Kantor_Imigrasi::get();

        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<small><div class="form-group row" >
                        <form action="/kantor_edit" method="post" enctype="multipart/form-data">
                            '.csrf_field().'
                            <input type="hidden" name="id" value="'.$data->kode_kanim.'">
                            <input type="hidden" name="name" value="'.$data->nama_kanim.'">
                            <input type="hidden" name="mode" value="edit">
                            <input type="hidden" name="route" value="kantor">
                            <input type="hidden" name="_method" value="POST">
                            <input type="submit" class="btn-xs" id="submitedit" name="submit" value="Ubah">
                        </form>
                        <form action="/kantor_delete" method="post" enctype="multipart/form-data">
                            '.csrf_field().'
                            <input type="hidden" name="id" value="'.$data->kode_kanim.'">
                            <input type="hidden" name="name" value="'.$data->nama_kanim.'">
                            <input type="hidden" name="mode" value="delete">
                            <input type="hidden" name="route" value="kantor">
                            <input type="hidden" name="_method" value="POST">
                            <input type="submit" class="btn-xs" id="submitdel" name="submit" value="Hapus">
                        </form>
                        </div></small>
                        ';
                //return '<a href="#edit-'.$data->kode_kanim.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
        ->make(true);
    }
   

    public function kantor()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $keterangan = null;
        $mode = 'create';

        $listkantor = MS_Kantor_Imigrasi::paginate(10);
        
        return view('/pengaturan/kantor', [
            'result' => $result,
            'param' => $param,
            'listkantor' => $listkantor,
            'id' => $id,
            'name' => $name,
            'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function kantor_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        //$keterangan = $request->keterangan;
        $mode = $request->mode;

        
         $listkantor = MS_Kantor_Imigrasi::paginate(10);

        return view('/pengaturan/kantor', [
            'result' => $result,
            'param' => $param,
            'listkantor' => $listkantor,
            'id' => $id,
            'name' => $name,
            //'keterangan' => $keterangan,
            'mode' => $mode
        ]);
    }

    public function kantor_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            MS_Kantor_Imigrasi::where('kode_kanim', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/kantor')->with('noticemessage', 'Hapus UPT  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/kantor')->with('noticemessage', 'Hapus UPT gagal.')->with('flag','0');
        
    }

    public function kantor_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Kantor_Imigrasi::where('kode_kanim', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Kantor_Imigrasi::where('kode_kanim',$request->id)->update([
                        'nama_kanim' => $request->name,
                        //'kode_kanwil' => $request->keterangan,
                        'updated_by' => $auth->id,
                ]); 
                return redirect('/kantor')->with('noticemessage', 'Update UPT  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/kantor')->with('noticemessage', 'Update UPT gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Kantor_Imigrasi::where('nama_kanim', $request->name)->first();

            if(!$exist){
                $user = MS_Kantor_Imigrasi::create([
                    'nama_kanim' => $request->name,
                    //'kode_kanwil' => $request->keterangan,
                    'created_by' => $auth->id,
                ]);
                return redirect('/kantor')->with('noticemessage', 'Pendaftaran UPT '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/kantor')->with('noticemessage', 'Pendaftaran UPT gagal.')->with('flag','0');   
            }
        }
    }


/******************************************** PENYEDIA *********************************************/
    public function penyedia()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $alamat = null;
        $notelp = null;
        $email = null;
        $mode = 'create';

        $listpenyedia = MS_Penyedia::paginate(10);
        
        return view('/pengaturan/penyedia', [
            'result' => $result,
            'param' => $param,
            'listpenyedia' => $listpenyedia,
            'id' => $id,
            'name' => $name,
            'alamat' => $alamat,
            'notelp' => $notelp,
            'email' => $email,
            'mode' => $mode
        ]);
    }

    public function penyedia_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $alamat = $request->alamat;
        $notelp = $request->notelp;
        $email = $request->email;
        $mode = $request->mode;

        
         $listpenyedia = MS_Penyedia::paginate(10);

        return view('/pengaturan/penyedia', [
            'result' => $result,
            'param' => $param,
            'listpenyedia' => $listpenyedia,
            'id' => $id,
            'name' => $name,
            'alamat' => $alamat,
            'notelp' => $notelp,
            'email' => $email,
            'mode' => $mode
        ]);
    }

    public function penyedia_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            MS_Penyedia::where('id_penyedia', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/penyedia')->with('noticemessage', 'Hapus Penyedia  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/penyedia')->with('noticemessage', 'Hapus Penyedia gagal.')->with('flag','0');
        
    }

    public function penyedia_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Penyedia::where('id_penyedia', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Penyedia::where('id_penyedia',$request->id)->update([
                        'nama_penyedia' => $request->name,
                        'alamat_penyedia' => $request->alamat,
                        'notelp_penyedia' => $request->notelp,
                        'email_penyedia' => $request->email,
                        'updated_by' => $auth->id,
                ]); 
                return redirect('/penyedia')->with('noticemessage', 'Update Penyedia  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/penyedia')->with('noticemessage', 'Update Penyedia gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Penyedia::where('nama_penyedia', $request->name)->first();

            if(!$exist){
                $user = MS_Penyedia::create([
                    'nama_penyedia' => $request->name,
                    'alamat_penyedia' => $request->alamat,
                    'notelp_penyedia' => $request->notelp,
                    'email_penyedia' => $request->email,
                    'created_by' => $auth->id,
                ]);
                return redirect('/penyedia')->with('noticemessage', 'Pendaftaran Penyedia '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/penyedia')->with('noticemessage', 'Pendaftaran Penyedia gagal.')->with('flag','0');   
            }
        }
    }

/******************************************** MENU *********************************************/
    public function menu()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $route = null;
        $deskripsi = null;
        $icon = null;
        $id_parent = null;
        $status = null;
        $urutan = null;
        $role = null;
        $mode = 'create';

        $listparent = MS_Menu::getMenu();//Menu::where('id_parent', '=', '0')->orderby("id_menu","asc")->orderby("urutan","asc")->get();
        
        return view('/pengaturan/menu', [
            'result' => $result,
            'param' => $param,
            'listparent' => $listparent,
            'id' => $id,
            'name' => $name,
            'route' => $route,
            'deskripsi' => $deskripsi,
            'icon' => $icon,
            'id_parent' => $id_parent,
            'status' => $status,
            'urutan' => $urutan,
            'role' => $role,
            'mode' => $mode
        ]);
    }

    public function menu_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;
        $name = $request->name;
        $route = $request->route;
        $deskripsi = $request->deskripsi;
        $icon = $request->icon;
        $id_parent = $request->id_parent;
        $status = $request->status;
        $urutan = $request->urutan;
        $role = $request->role;
        $mode = $request->mode;

        
        $listparent = MS_Menu::where('id_parent', '=', '0')->orderby("id_menu","asc")->orderby("urutan","asc")->get();

        return view('/pengaturan/menu', [
            'result' => $result,
            'param' => $param,
            'listparent' => $listparent,
            'id' => $id,
            'name' => $name,
            'route' => $route,
            'deskripsi' => $deskripsi,
            'icon' => $icon,
            'id_parent' => $id_parent,
            'status' => $status,
            'urutan' => $urutan,
            'role' => $role,
            'mode' => $mode
        ]);
    }

    public function menu_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            MS_Menu::where('id_menu', $request->id)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        Artisan::call('cache:clear');

        if($del)
            return redirect('/menu')->with('noticemessage', 'Hapus Menu  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/menu')->with('noticemessage', 'Hapus Menu gagal.')->with('flag','0');
        
    }

    public function menu_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(MS_Menu::where('id_menu', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                MS_Menu::where('id_menu',$request->id)->update([
                        'title' => $request->name,
                        'route' => $request->route,
                        'deskripsi' => $request->deskripsi,
                        'icon' => $request->icon,
                        'id_parent' => $request->id_parent,
                        'status' => $request->status,
                        'urutan' => $request->urutan,
                        'role' => $request->role,
                        'updated_by' => $auth->id,
                ]); 

                Artisan::call('cache:clear');

                return redirect('/menu')->with('noticemessage', 'Update Menu  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/menu')->with('noticemessage', 'Update Menu gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $exist = MS_Menu::where('title', $request->name)->first();

            if(!$exist){
                $user = MS_Menu::create([
                    'title' => $request->name,
                    'route' => $request->route,
                    'deskripsi' => $request->deskripsi,
                    'icon' => $request->icon,
                    'id_parent' => $request->id_parent,
                    'status' => $request->status,
                    'urutan' => $request->urutan,
                    'role' => $request->role,
                    'created_by' => $auth->id,
                ]);

                Artisan::call('cache:clear');

                return redirect('/menu')->with('noticemessage', 'Pendaftaran Menu '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/menu')->with('noticemessage', 'Pendaftaran Menu gagal.')->with('flag','0');   
            }
        }
    }
/******************************************** SPESIFIKASI FIELD *********************************************/
    public function getSpesifikasiFieldList (Request $request)
    {
        $list_field = Ms_Field_Spesifikasi_Aset::getFieldSpesifikasi($request->jenis_aset);
        return response()->json(['list_field' => $list_field]);
    }

    /******************************************* DOKUMENTASI DOWNLOAD ******************************************/

    public function downloadDokumentasi($id_dokumentasi)
    {
        $dokumentasi = Dokumentasi::getDokumentasi($id_dokumentasi);
        return response()->download($dokumentasi->link_dokumentasi);
    }

    public function downloadSourceCode($id_sourcecode)
    {
        $sourcecode = Aplikasi_Sourcecode::getSourceCode($id_sourcecode);
        return response()->download($sourcecode->link_sourcecode);
    }

    public function downloadFileInfo($id_aset_informasi)
    {
        $aset_info = Aset_Informasi::getFileInfo($id_aset_informasi);
        return response()->download($aset_info->isian_field_informasi);
    }

/******************************************** ASET INFORMASI FIELD *********************************************/
    public function getAsetInformasiFieldList (Request $request)
    {
        $list_field = Ms_Field_Aset_Informasi::getFieldAsetInformasi($request->jenis_aset, $request->kode_grup_satuan);
        return response()->json(['list_field' => $list_field]);
    }

    public function getAsetInformasiFieldListCatat (Request $request)
    {
        $list_field = Ms_Field_Aset_Informasi::getFieldAsetInformasiCatat($request->jenis_aset, $request->kode_grup_satuan);
        return response()->json(['list_field' => $list_field]);
    }



/******************************************** AKUN ******************************************/

    public function akun()
    {
        $result = null;
        $param = null;
        $id = null;         
        $name = null;
        $email = null;
        $nip = null;
        $password = null;
        $role = null;
        $mode = 'create';

        $listakun = User::getAllUser();

        return view('/pengaturan/akun', [
            'result' => $result,
            'param' => $param,
            'listakun' => $listakun,
            'id' => $id,
            'name' => $name,
            'email' => $email,
            'nip' => $nip,
            'password' => $password,
            'role' => $role,
            'mode' => $mode
        ]);
    }

    public function akunjson(){
        $data = User::getAllUser();
        // dd($data);  
        return Datatables::of($data)
            ->addColumn('action', function ($data) {
                return '<small><div class="form-group row" >
                        <form action="/akun_edit" method="post" enctype="multipart/form-data">
                            '.csrf_field().'
                            <input type="hidden" name="id" value="' . $data->id . '">
                            <input type="hidden" name="name" value="' . $data->name . '">
                            <input type="hidden" name="email" value="' . $data->email . '">
                            <input type="hidden" name="nip" value="' . $data->nip . '">
                            <input type="hidden" name="password" value="' . $data->password . '">
                            <input type="hidden" name="role" value="' . $data->role . '">
                            <input type="hidden" name="mode" value="edit">
                            <input type="hidden" name="route" value="akun">
                            <input type="hidden" name="_method" value="POST">
                            <input type="submit" class="btn-xs" id="submitedit" name="submit" value="Ubah">
                        </form>
                        <form action="/akun_delete" method="post" enctype="multipart/form-data">
                            '.csrf_field().'
                            <input type="hidden" name="id" value="'.$data->id.'">
                            <input type="hidden" name="name" value="'.$data->name.'">
                            <input type="hidden" name="email" value="' . $data->email . '">
                            <input type="hidden" name="nip" value="' . $data->nip . '">
                            <input type="hidden" name="password" value="' . $data->password . '">
                            <input type="hidden" name="role" value="' . $data->role . '">
                            <input type="hidden" name="mode" value="delete">
                            <input type="hidden" name="route" value="akun">
                            <input type="hidden" name="_method" value="POST">
                            <input type="submit" class="btn-xs" id="submitdel" name="submit" value="Hapus">
                        </form>
                        </div></small>
                        ';
                    })
        ->make(true);
    }

     public function akun_edit(Request $request)
    {
        $result = null;
        $param = null;

        $id = $request->id;         
        $name = $request->name;
        $email = $request->email;
        $nip = $request->nip;
        $password = $request->password;
        $role = $request->role;
        $mode = $request->mode;

        
        $listakun = User::getAllUser();

        return view('/pengaturan/akun', [
            'result' => $result,
            'param' => $param,
            'listakun' => $listakun,
            'id' => $id,
            'name' => $name,
            'email' => $email,
            'nip' => $nip,
            'password' => $password,
            'role' => $role,
            'mode' => $mode
        ]);
    }

    public function akun_delete(Request $request)
    {
        $id = $request->id;
        $mode = $request->mode;
        if($mode == 'delete'){
            User::where('id', $request->id)->delete();
            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect('/akun')->with('noticemessage', 'Hapus Akun  '. $request->name.'  berhasil.')->with('flag','1');
        else 
            return redirect('/akun')->with('noticemessage', 'Hapus Akun gagal.')->with('flag','0');
        
    }

    public function akun_save(Request $request)
    {
        $auth = User::getUser();
        $mode = $request->mode;

        if($mode == 'edit'){
            if(User::where('id', $request->id)->count() > 0){
                $exist = TRUE;
            }
            else{
                $exist = FALSE;
            }
            
            if($exist){
                User::where('id',$request->id)->update([
                        'name' => $request->name,
                        'email' => $request->email,
                        'nip' => $request->nip,
                        'password' => Hash::make($request->password),
                        'role' => $request->role
                ]); 
                return redirect('/akun')->with('noticemessage', 'Update Akun  '. $request->name.'  berhasil.')->with('flag','1');
            }else{
                return redirect('/akun')->with('noticemessage', 'Update Akun gagal.')->with('flag','0');
            }
                          
        }
        // create
        else{
            $existEmail = User::where('email', $request->email)->first();
            $existNip = User::where('nip', $request->nip)->first();
            if(!$existEmail || !$existNip){
                $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'nip' => $request->nip,
                    'password' => Hash::make($request->password),
                    'role' => $request->role,
                    'created_by' => $auth->id,
                ]);
                return redirect('/akun')->with('noticemessage', 'Pendaftaran Akun '. $request->name.'  berhasil.')->with('flag','1');   
            }
            else {
                return redirect('/akun')->with('noticemessage', 'Pendaftaran Akun gagal.')->with('flag','0');   
            }
        }
    }
}