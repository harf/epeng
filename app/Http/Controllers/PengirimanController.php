<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\Models\Ms_Merek_Aset;
use App\Models\Ms_Kategori_Aset;
use App\Models\MS_Lokasi;
use App\Models\Aset_Informasi;
use App\Models\Aset_Satuan_Log;
use App\Models\Aset_Satuan;
use App\Models\Pengiriman;
use App\Models\Alokasi;
use App\Models\Ms_Jenis_Alokasi;
use App\Models\Ms_Kantor_Imigrasi;


class PengirimanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        if($request->is_filtered) {
            $is_filtered = $request->is_filtered;
            $filter = $request->filter;
            $filter_name = Ms_Jenis_Aset::getSingleJenisAset($filter)->nama_jenis_aset;
        }
        
        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Pengiriman::getAllPengiriman($keyword,$skip,$take);
        $count = $result[0];
        $list_pengiriman = $result[1];
        // $list_jenis_aset = Ms_Jenis_Aset::getAllJenisAset();

        return view('/pengiriman/pengirimanIndex', [
            'list_pengiriman' => $list_pengiriman,
            // 'list_jenis_aset' => $list_jenis_aset,
            'count_list_pengiriman' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);

    }

    public function alokasi(Request $request)
    {

        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        if($request->is_filtered) {
            $is_filtered = $request->is_filtered;
            $filter = $request->filter;
            $filter_name = Ms_Jenis_Aset::getSingleJenisAset($filter)->nama_jenis_aset;
        }
        
        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Alokasi::getAllAlokasi($keyword,$skip,$take);
        $count = $result[0];
        $list_alokasi = $result[1];
        // $list_jenis_aset = Ms_Jenis_Aset::getAllJenisAset();

        return view('/pengiriman/pengirimanAlokasi', [
            'list_alokasi' => $list_alokasi,
            // 'list_jenis_aset' => $list_jenis_aset,
            'count_list_alokasi' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);

    }

    public function pusat(Request $request)
    {

        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        if($request->is_filtered) {
            $is_filtered = $request->is_filtered;
            $filter = $request->filter;
            $filter_name = Ms_Jenis_Aset::getSingleJenisAset($filter)->nama_jenis_aset;
        }
        
        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Pengiriman::getAllPengiriman($keyword,$skip,$take);
        $count = $result[0];
        $list_pengiriman = $result[1];
        // $list_jenis_aset = Ms_Jenis_Aset::getAllJenisAset();

        return view('/pengiriman/pengirimanPusat', [
            'list_pengiriman' => $list_pengiriman,
            // 'list_jenis_aset' => $list_jenis_aset,
            'count_list_pengiriman' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);

    }

    public function add()
    {
            $list_kategori_aset = Ms_Kategori_Aset::get();
            $list_merek_aset = Ms_Merek_Aset::get();
            $list_lokasi = Ms_Lokasi::get();
            $list_kantor_imigrasi = Ms_Kantor_Imigrasi::get();

            return view('/pengiriman/pengirimanAdd', [
                'list_kategori_aset' => $list_kategori_aset,
                'list_merek_aset' => $list_merek_aset,
                'list_lokasi' => $list_lokasi,
                'list_kantor_imigrasi' => $list_kantor_imigrasi,
            ]);
    }

    public function tambah()
    {
            $list_kategori_aset = Ms_Kategori_Aset::get();
            $list_merek_aset = Ms_Merek_Aset::get();
            $list_lokasi = Ms_Lokasi::get();
            $list_kantor_imigrasi = Ms_Kantor_Imigrasi::get();

            return view('/pengiriman/pengirimanTambah', [
                'list_kategori_aset' => $list_kategori_aset,
                'list_merek_aset' => $list_merek_aset,
                'list_lokasi' => $list_lokasi,
                'list_kantor_imigrasi' => $list_kantor_imigrasi,
            ]);
    }
    
    public function simpanPengiriman(Request $request)
    {

        $auth = User::getUser();
       
        $itemIndexArray = explode('~', $request->index_item);
        $itemCount = $request->count_item;

            $pengiriman = Pengiriman::create([
                'id_pengiriman' => $id_pengiriman,
                'id_permintaan' => $id_permintaan,
                'id_alokasi' => $id_alokasi,
                'nama_kurir' => $nama_kurir,
                'alamat_kurir' => $current_datetime,
                'nomor_resi' => $nomor_resi,
                'biaya_kirim' => $request->input('harga_satuan~'. $itemIndexArray[$i]),
                'created_at' => $current_datetime,
                'created_by' => $auth->id
            ]);
            $pengirimaninsertedId = $pengiriman->id;

            $fileCount = count($request->file());
            if($fileCount != 0) {
                $files = $request->file();
                $filesIndex = [];
                $idx = 0;
                foreach ($files as $file) {
                    $input_name = array_search($file, $files);
                    $input_index = str_replace('id_dokumentasi~', '', $input_name);
                    $filesIndex[$idx] = $input_index;
                    $idx++;
                }
                foreach($filesIndex as $idx) {
                    if($request->hasFile('id_dokumentasi~'. $idx)){
                        $uploadedFile = $request->file('id_dokumentasi~'. $idx);

                        $rootPath = '\app\public\files\dokumentasi';
                        $nama_dokumentasi = $request->input('nama_dokumentasi~'.$idx);
                        $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - '. $uploadedFile->getClientOriginalName());

                        $id_lokasi = $request->input('dokumentasi~lokasi~'. $idx);
                        $id_kontainer = $request->input('dokumentasi~kontainer~'. $idx);
                        $id_posisi_kontainer = $request->input('dokumentasi~posisi_kontainer~'. $idx);

                        $keterangan_dokumentasi = $request->input('keterangan_dokumentasi~'.$idx);
                        $ukuran_dokumentasi = 0;
                        try {
                            $ukuran_dokumentasi = $request->file('id_dokumentasi~'.$idx)->getClientSize();
                        }
                        catch (\Exception $e) {
                            $ukuran_dokumentasi = 0;
                        }
                        
                        $tanggal_dokumentasi = $request->input('tanggal_dokumentasi~'.$idx);
                        $dokumentasi = Dokumentasi::create([
                            'src_tabel_dokumentasi' => 'pengadaan',
                            'src_kolom_dokumentasi' => 'kode_pengadaan',
                            'src_fk_dokumentas
                            .i' => $pengadaan_insertedId,
                            'id_lokasi' => $id_lokasi,
                            'id_kontainer' => $id_kontainer,
                            'id_posisi_kontainer' => $id_posisi_kontainer,
                            'nama_dokumentasi' => $nama_dokumentasi,
                            'keterangan_dokumentasi' => $keterangan_dokumentasi,
                            'tanggal_dokumentasi' => $tanggal_dokumentasi,
                            'ukuran_dokumentasi' => $ukuran_dokumentasi,
                            'link_dokumentasi' => $result->getRealPath(),
                            'created_by' => $auth->id
                        ]);
                        $dokumentasi_insertedId = $dokumentasi->id;
                    }
                }    
            }

        return redirect('/manajemenAset');

    }
    

    public function rincian($id_pengiriman)
    {
        
        $aset_satuan_log = Aset_Satuan_Log::getLogByKodeGrupId($id_pengiriman);
        $informasi_list = Aset_Informasi::getAsetInformasiListByKodeGrup($id_pengiriman);
        $pengiriman = Pengiriman::getPengirimanByID($id_pengiriman);
        $field_isian_list = Aset_Satuan::getSatuanFieldIsianListByKodeGrup($id_pengiriman);

        return view('/pengiriman/pengirimanRincian', [
            'id_pengiriman' => $id_pengiriman,
            'pengiriman' => $pengiriman,
            'field_isian_list' => $field_isian_list,
            'informasi_list' => $informasi_list,
            'aset_satuan_log' => $aset_satuan_log
        ]);

    }
   

    public function alokasirincian($id_alokasi)
    {
        
        $aset_satuan_log = Aset_Satuan_Log::getLogByKodeGrupId($id_alokasi);
        $informasi_list = Aset_Informasi::getAsetInformasiListByKodeGrup($id_alokasi);
        $alokasi = Alokasi::getAlokasiByID($id_alokasi);
        $field_isian_list = Aset_Satuan::getSatuanFieldIsianListByKodeGrup($id_alokasi);

        return view('/pengiriman/pengirimanAlokasiRincian', [
            'id_alokasi' => $id_alokasi,
            'alokasi' => $alokasi,
            'field_isian_list' => $field_isian_list,
            'informasi_list' => $informasi_list,
            'aset_satuan_log' => $aset_satuan_log
        ]);

    }


    public function ubahAsetInformasi(Request $request)
    {

        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $list_id_aset_informasi = explode('-', $request->id_aset_informasi);

        for ($i = 0 ; $i < (count($list_id_aset_informasi)-1) ; $i++)
        {
            $id_field_info = Aset_Informasi::getIdFieldInfo($list_id_aset_informasi[$i])->id_field_info;
            $datatype = Ms_Field_Aset_Informasi::getDataTypeFieldAsetInformasi($id_field_info);
            $isian_field_informasi = '';
            $keterangan_isian = '';

            if ($datatype->nama_datatype != 'file') {
                $isian_field_informasi = $request->input('isian_field_informasi~' . $request->kode_grup_satuan . '_' . $id_field_info);
            } else {
                if($request->hasFile('isian_field_informasi~' . $request->kode_grup_satuan . '_' . $id_field_info)) { 
                    $uploadedFile = $request->file('isian_field_informasi~' . $request->kode_grup_satuan . '_' . $id_field_info);
                    
                    $rootPath = '/app/public/files/dokumentasi';
                    $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - ' . $uploadedFile->getClientOriginalName());
                    
                    $isian_field_informasi = $result->getRealPath();
                    $keterangan_isian = $uploadedFile->getClientOriginalName();
                }
            }

            $isian_field_info_update = Aset_Informasi::where('id_aset_informasi', $list_id_aset_informasi[$i])
                ->update([
                    'isian_field_informasi' => $isian_field_informasi,
                    'keterangan_isian' => $keterangan_isian,
                    'updated_at' => $current_datetime,
                    'updated_by' => $auth->id
            ]);
        }
        return redirect('/manajemenAset/' . $request->kode_grup_satuan . '/informasi');

    }

    public function tindakLanjutKirimanPusat()
    {
        $list_aset_informasi = Aset_Informasi::getAsetInformasiList();
        return view('/pengiriman/tindakLanjutKirimanPusat', [
            'list_aset_informasi' => $list_aset_informasi
        ]);
    }

    //API

    
    //Get Satuan Aset on the Manajemen Aset page
    public function getSatuanAsetPengadaanList(Request $request)
    {

        $list_satuan_aset_pengadaan = Aset::getSatuanAsetPengadaan($request->id_aset, $request->kode_pengadaan);
        return response()->json(['list_satuan_aset_pengadaan' => $list_satuan_aset_pengadaan]);

    }
}