<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Carbon\Carbon;
use App\Models\Ms_Lokasi;
use App\Models\Ms_Kategori_Aset;
use App\Models\Ms_Merek_Aset;
use DB;

class PersediaanController extends Controller
{
    public function page_add() {

        $rs 				= DB::table('aset')
	                    		->select('id_aset', 'id_jenis_aset', 'id_merek')
	                    		->get();

        $rskategori    		= DB::table('ms_kategori_aset')->select('id_kategori_aset', 'nama_kategori_aset', DB::raw("CONCAT(nama_kategori_aset) as display"))
                            	->get();

        $list_lokasi 		= Ms_Lokasi::get();
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_merek_aset 	= Ms_Merek_Aset::get();

        $rstahun 	 		= [];
        $system_datetime 	= \Carbon\Carbon::now();
        $current_year 		= $system_datetime->year;
        $idx 				= 0;
        for ($i = $current_year; $i >= $current_year-10; $i--)
        {
            $rstahun[$idx] = $i;
            $idx++;
        }
        
        return view('/persediaan/inputstokawalAdd', ['rs' => $rs, 'rskategori' => $rskategori, 'list_lokasi' => $list_lokasi, 'list_kategori_aset' => $list_kategori_aset, 'list_merek_aset' => $list_merek_aset, 'rstahun' => $rstahun]);

    }
}
