<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

use App\Models\Permintaan;
use App\Models\Permintaan_Item;
use App\Models\Ms_Lokasi;
use App\Models\Ms_Jenis_Aset;
use App\Models\Ms_Kantor_Imigrasi;
use App\Models\Aset;
use App\Models\Ms_Kategori_Aset;
use App\Models\Aset_Stok;
use App\Models\Dokumentasi;
use App\Models\Aset_Informasi;

class PermintaanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Permintaan::getAllPermintaan($skip,$take,$keyword);
        $count = $result[0];
        $list_permintaan = $result[1];

        return view('/permintaan/permintaanIndex', [
            'list_permintaan' => $list_permintaan,
            'count_list_permintaan' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function add()
    {
        $list_produk = Aset::get();
        $list_lokasi = Ms_Lokasi::get();
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_kanim = Ms_Kantor_Imigrasi::get();

        $system_datetime = \Carbon\Carbon::now();

        return view('/permintaan/permintaanAdd', [
            'list_produk' => $list_produk,
            'list_lokasi' => $list_lokasi,
            'list_kategori_aset' => $list_kategori_aset,
            'list_kanim' => $list_kanim,
        ]);
    }

    public function simpanPermintaan(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();
        $status_permintaan ="BELUM_DIKONFIRMASI";
        $id_jenis_alokasi = 1;

        $permintaan = Permintaan::create([
            'no_surat_permintaan' => $request->no_surat_permintaan,
            'tanggal_surat_permintaan' => date('Y-m-d', strtotime($request->tanggal_surat_permintaan)),
            'tanggal_permintaan' => $current_datetime,
            'kode_kanim' => $request->kode_kanim,
            'nama_pemohon' => $request->nama_pemohon,
            'nip_pemohon' => $request->nip_pemohon,
            'pangkat_golongan_pemohon' => $request->pangkat_golongan_pemohon,
            'jabatan_pemohon' => $request->jabatan_pemohon,
            'deskripsi_permintaan' => $request->deskripsi_permintaan,
            'status_permintaan' => $status_permintaan,
            'tanggal_status' => $current_datetime,
            'id_jenis_alokasi' => $id_jenis_alokasi,
            'created_at' => $current_datetime,
            'created_by' => $auth->id
        ]);
        $permintaan_insertedId = $permintaan->id;

        $fileCount = count($request->file());
        if($fileCount != 0) {
            $files = $request->file();
            $filesIndex = [];
            $idx = 0;
            foreach ($files as $file) {
                $input_name = array_search($file, $files);
                $input_index = str_replace('id_dokumentasi~', '', $input_name);
                $filesIndex[$idx] = $input_index;
                $idx++;
            }
            foreach($filesIndex as $idx) {
                if($request->hasFile('id_dokumentasi~'. $idx)){
                    $uploadedFile = $request->file('id_dokumentasi~'. $idx);

                    $rootPath = '\app\public\files\dokumentasi';
                    $nama_dokumentasi = $request->input('nama_dokumentasi~'.$idx);
                    $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - '. $uploadedFile->getClientOriginalName());

                    $id_lokasi = $request->input('dokumentasi~lokasi~'. $idx);
                    $id_kontainer = $request->input('dokumentasi~kontainer~'. $idx);
                    $id_posisi_kontainer = $request->input('dokumentasi~posisi_kontainer~'. $idx);

                    $keterangan_dokumentasi = $request->input('keterangan_dokumentasi~'.$idx);
                    $ukuran_dokumentasi = 0;
                    try {
                        $ukuran_dokumentasi = $request->file('id_dokumentasi~'.$idx)->getClientSize();
                    }
                    catch (\Exception $e) {
                        $ukuran_dokumentasi = 0;
                    }
                    
                    $tanggal_dokumentasi = $request->input('tanggal_dokumentasi~'.$idx);
                    $dokumentasi = Dokumentasi::create([
                        'src_tabel_dokumentasi' => 'pengadaan',
                        'src_kolom_dokumentasi' => 'kode_pengadaan',
                        // 'src_fk_dokumentasi' => $pengadaan_insertedId,
                        'id_lokasi' => $id_lokasi,
                        'id_kontainer' => $id_kontainer,
                        'id_posisi_kontainer' => $id_posisi_kontainer,
                        'nama_dokumentasi' => $nama_dokumentasi,
                        'keterangan_dokumentasi' => $keterangan_dokumentasi,
                        'tanggal_dokumentasi' => $tanggal_dokumentasi,
                        'ukuran_dokumentasi' => $ukuran_dokumentasi,
                        'link_dokumentasi' => $result->getRealPath(),
                        'created_by' => $auth->id
                    ]);
                    $dokumentasi_insertedId = $dokumentasi->id;
                }
            }            
        }

        $itemIndexArray = explode('~', $request->index_item);
        $itemCount = $request->count_item;

        for($i = 0; $i < $itemCount; $i++)
        {
            $permintaan_item = Permintaan_Item::create([
                'id_permintaan' => $permintaan_insertedId,
                'id_aset' => $request->input('id_aset~'.$itemIndexArray[$i]),
                'qty_prmnt_item' => $request->input('jumlah~'.$itemIndexArray[$i]),
                'keterangan_prmnt_item' => $request->input('keterangan~'.$itemIndexArray[$i]),
                'created_at' => $current_datetime,
                'created_by' => $auth->id
            ]);
            $permintaan_item_insertedId = $permintaan_item->id;
        }
        return redirect('/permintaan');
    }

    //API

    public function getSumberPermintaanList(Request $request)
    {
        $list_sumber_permintaan = Permintaan::getSumberPermintaan($request->id_permintaan);
        return response()->json(['list_sumber_permintaan' => $list_sumber_permintaan ]);
    }

    public function indexKonfirmasi(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Permintaan::getAllPermintaan($skip,$take,$keyword);
        $count = $result[0];
        $list_permintaan = $result[1];

        return view('/permintaan/konfirmasi', [
            'list_permintaan' => $list_permintaan,
            'count_list_permintaan' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function detailPermintaanInformasi($id_permintaan)
    {
        $list_aset_stok = Aset_Stok::getAsetStokByID2($id_permintaan);
        $list_permintaan = Permintaan::getSumberPermintaanbyID($id_permintaan);
        $list_permintaan_item = Permintaan::getSumberPermintaan($id_permintaan);

        return view('/permintaan/detailPermintaanInformasi', [
            'list_aset_stok' => $list_aset_stok,
            'id_permintaan' => $id_permintaan,
            'list_permintaan' => $list_permintaan,
            'list_permintaan_item' => $list_permintaan_item
        ]);
    }

    public function simpanKonfirmasi(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();
        $status_permintaan ="SUDAH_DIKONFIRMASI";

        $permintaan = Permintaan_Konfirmasi::create([
            'id_permintaan' => $request->id_permintaan,
            'status_konfirmasi' => $status_konfirmasi,
            'deskripsi_konfirmasi' => $request->deskripsi_konfirmasi,
            'created_at' => $current_datetime,
            'created_by' => $auth->id
        ]);
        $permintaan_insertedId = $permintaan->id;

        $permintaan = Permintaan::where('id_permintaan')
            ->update([
                'status_permintaan' => $status_permintaan,
                'tanggal_status' => $current_datetime
        ]);
    }

    public function alokasiIndex(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Permintaan::getAllPermintaan($skip,$take,$keyword);
        $count = $result[0];
        $list_permintaan = $result[1];

        return view('/permintaan/permintaanAlokasiIndex', [
            'list_permintaan' => $list_permintaan,
            'count_list_permintaan' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function alokasiAdd($id_permintaan)
    {   
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_permintaan = Permintaan::getSumberPermintaanbyID($id_permintaan);
        $list_permintaan_item = Permintaan::getSumberPermintaan($id_permintaan);

        return view('/permintaan/permintaanAlokasiAdd', [
            'list_kategori_aset' => $list_kategori_aset,
            'id_permintaan' => $id_permintaan,
            'list_permintaan' => $list_permintaan,
            'list_permintaan_item' => $list_permintaan_item
        ]);
    }

    public function getJenisAsetList(Request $request)
    {
        $list_jenis_aset = MS_Jenis_Aset::getJenisAsetPermintaan($request->kategori_aset, $request->id_permintaan);
        return response()->json($list_jenis_aset);
    }

    public function kirimIndex(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $count = 0;

        return view('/permintaan/permintaanKirimIndex', [
            'count' => $count,
            'is_filtered' => $is_filtered,
            'filter1' => $filter,
            'filter1_name' => $filter_name,
            'filter2' => $filter,
            'filter2_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function kirimAdd()
    {
        return view('/permintaan/permintaanKirimAdd', []);
    }

    public function riwayatPermintaan(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Permintaan::getAllPermintaan($skip,$take,$keyword);
        $count = $result[0];
        $list_permintaan = $result[1];

        return view('/permintaan/permintaanIndex', [
            'list_permintaan' => $list_permintaan,
            'count_list_permintaan' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function tindakLanjutPermintaan()
    {
        $list_aset_informasi = Aset_Informasi::getAsetInformasiList();
        return view('/permintaan/tindakLanjutPermintaan', [
            'list_aset_informasi' => $list_aset_informasi
        ]);
    }
}