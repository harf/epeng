<?php

namespace App\Http\Controllers;
use DataTables;
use App\User;
use App\Models\MS_Jenis_Kontainer;
use App\Models\MS_Kontainer;
use App\Models\MS_Lokasi;
use App\Models\MS_Posisi_Kontainer;
use App\Models\MS_Jenis_Aset;
use App\Models\MS_Merek_Aset;
use App\Models\MS_Jenis_Pengembangan;
use App\Models\MS_Kantor_Imigrasi;
use App\Models\MS_Penyedia;
use App\Models\MS_Menu;
use App\Models\Ms_Field_Spesifikasi_Aset;
use App\Models\Aset_Masuk;
use App\Models\Aset_Keluar;
use App\Models\Dokumentasi;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\LaporanAsetMasukExport;
use App\Exports\LaporanAsetKeluarExport;
use App\Exports\LaporanPersebaranAsetExport;

class LaporanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function laporanAsetMasuk(Request $request)
    {
        $page_number = 1;
        $page_size = 15;
        $periode_dari = null;
        $periode_sampai = null;

        if($request->periode_dari != "null" || $request->periode_dari != null) $periode_dari = $request->periode_dari;
        if($request->periode_sampai != "null" || $request->periode_sampai != null) $periode_sampai = $request->periode_sampai;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Aset_Masuk::getAsetMasuk($periode_dari, $periode_sampai, null, $skip, $take);
        $count = $result[0];
        $list_aset_masuk = $result[1];

        return view('/laporan/laporanAsetMasuk', [
            'list_aset_masuk' => $list_aset_masuk,
            'periode_dari' => $periode_dari,
            'periode_sampai' => $periode_sampai,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'count_list_aset_masuk' => $count
        ]);
    }

    public function exportLaporanAsetMasuk($nomor_aset_masuk)
    {
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        return Excel::download(new LaporanAsetMasukExport($nomor_aset_masuk), 'Laporan Aset Masuk Nomor #' . $nomor_aset_masuk . ' (' . $system_datetime->format('YmdHis') . ').xlsx');
    }

    public function detailLaporanAsetMasuk($nomor_aset_masuk)
    {
        $aset_masuk = Aset_Masuk::getAsetMasuk(null, null, $nomor_aset_masuk, null, null)[1];
        $list_item_aset_masuk = Aset_Masuk::getItemAsetMasuk($nomor_aset_masuk);
        $list_dokumentasi_aset_masuk = Dokumentasi::getDokumentasiList('aset_masuk', 'nomor_aset_masuk', $nomor_aset_masuk);

        return view('/laporan/laporanAsetMasukDetail', [
            'nomor_aset_masuk' => $nomor_aset_masuk,
            'aset_masuk' => $aset_masuk,
            'list_item_aset_masuk' => $list_item_aset_masuk,
            'list_dokumentasi_aset_masuk' => $list_dokumentasi_aset_masuk
        ]);
    }

    public function getSatuanAsetMasuk(Request $request)
    {
        $list_satuan_aset_masuk = Aset_Masuk::getSatuanAsetMasuk($request->id_aset_masuk, $request->id_aset);
        return response()->json($list_satuan_aset_masuk);
    }

    public function laporanAsetKeluar(Request $request)
    {
        $page_number = 1;
        $page_size = 15;
        $periode_dari = null;
        $periode_sampai = null;

        if($request->periode_dari != "null" || $request->periode_dari != null) $periode_dari = $request->periode_dari;
        if($request->periode_sampai != "null" || $request->periode_sampai != null) $periode_sampai = $request->periode_sampai;
            
        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Aset_Keluar::getAsetKeluar($periode_dari, $periode_sampai, null, $skip, $take);
        $count = $result[0];
        $list_aset_keluar = $result[1];

        return view('/laporan/laporanAsetKeluar', [
            'list_aset_keluar' => $list_aset_keluar,
            'periode_dari' => $periode_dari,
            'periode_sampai' => $periode_sampai,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'count_list_aset_keluar' => $count
        ]);
    }

    public function exportLaporanAsetKeluar($nomor_aset_keluar)
    {
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        return Excel::download(new LaporanAsetKeluarExport($nomor_aset_keluar), 'Laporan Aset Keluar Nomor #' . $nomor_aset_keluar . ' (' . $system_datetime->format('YmdHis') . ').xlsx');
    }

    public function detailLaporanAsetKeluar($nomor_aset_keluar)
    {
        $aset_keluar = Aset_Keluar::getAsetKeluar(null, null, $nomor_aset_keluar, null, null)[1];
        $list_item_aset_keluar = Aset_Keluar::getItemAsetKeluar($nomor_aset_keluar);
        $list_dokumentasi_aset_keluar = Dokumentasi::getDokumentasiList('aset_keluar', 'nomor_aset_keluar', $nomor_aset_keluar);

        return view('/laporan/laporanAsetKeluarDetail', [
            'nomor_aset_keluar' => $nomor_aset_keluar,
            'aset_keluar' => $aset_keluar,
            'list_item_aset_keluar' => $list_item_aset_keluar,
            'list_dokumentasi_aset_keluar' => $list_dokumentasi_aset_keluar
        ]);
    }

    public function getSatuanAsetKeluar(Request $request)
    {
        $list_satuan_aset_keluar = Aset_Keluar::getSatuanAsetKeluar($request->id_aset_keluar, $request->id_aset);
        return response()->json($list_satuan_aset_keluar);
    }

    public function laporanPersebaranAset(Request $request)
    {
        $page_number = 1;
        $page_size = 15;
        $kanim = null;

        if($request->kanim != "null" || $request->kanim != null) $kanim = $request->kanim;
        
        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Aset_Keluar::getPersebaranAset($kanim, $skip, $take);
        $count = $result[0];
        $list_persebaran_aset = $result[1];

        return view('/laporan/laporanPersebaranAset', [
            'list_persebaran_aset' => $list_persebaran_aset,
            'kanim' => $kanim,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'count_list_persebaran_aset' => $count
        ]);
    }

    public function exportLaporanPersebaranAset($kode_kanim)
    {
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $nama_lokasi = '';

        if ($kode_kanim != 0)
        {
            $kanim = MS_Kantor_Imigrasi::getKantorImigrasiByID($kode_kanim);
            $nama_lokasi = $kanim->nama_kanim;
        }
        else
        {
            $nama_lokasi = 'Lain-lain';
        }

        return Excel::download(new LaporanPersebaranAsetExport($kode_kanim), 'Laporan Persebaran Aset di ' . $nama_lokasi . ' (' . $system_datetime->format('YmdHis') . ').xlsx');
    }

    public function detailLaporanPersebaranAset($kode_kanim)
    {
        $list_persebaran_aset = Aset_Keluar::getPersebaranAsetDetail($kode_kanim);
        $list_item_persebaran_aset = Aset_Keluar::getItemPersebaranAsetDetail($kode_kanim);

        // dd($list_item_persebaran_aset);

        return view('/laporan/laporanPersebaranAsetDetail', [
            'list_persebaran_aset' => $list_persebaran_aset,
            'list_item_persebaran_aset' => $list_item_persebaran_aset,
            'kode_kanim' => $kode_kanim
        ]);
    }

    public function getSatuanAsetPersebaran(Request $request)
    {
        $list_satuan_aset_persebaran = Aset_Keluar::getSatuanAsetPersebaran($request->id_aset_keluar, $request->id_aset);
        return response()->json($list_satuan_aset_persebaran);
    }
}