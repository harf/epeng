<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Alamat;
use App\Models\Aplikasi;
use App\Models\Aplikasi_Sourcecode;
use App\Models\Ms_Jenis_Pengembangan;
use App\Models\Ms_Kategori_Pengadaan;
use App\Models\Pengadaan;
use App\Models\Tim_Pengembang;
use App\Models\Aplikasi_Versi;
use App\Models\Ms_Lokasi;
use App\Models\Dokumentasi;
use App\Models\Gambar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;



class AplikasiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $list_aplikasi = Aplikasi::get();
        $list_pengadaan = Pengadaan::get();
        $list_jenis_pengembangan = Ms_Jenis_Pengembangan::get();

        return view('/aplikasi/aplikasiIndex', [
            'list_aplikasi' => $list_aplikasi,
            'list_pengadaan' => $list_pengadaan,
            'list_jenis_pengembangan' => $list_jenis_pengembangan,
        ]);
    }

    public function detail($aplikasiId)
    {
        $aplikasi = Aplikasi::where('id_aplikasi',$aplikasiId)->first();
        $alamat = Alamat::where('src_tabel_alamat','aplikasi')
                ->where('src_kolom_alamat','id_aplikasi')
                ->where('src_fk_alamat',$aplikasiId)
                ->get();
        $pengembang = Tim_Pengembang::where('id_aplikasi',$aplikasiId)->get();
        $versi = Aplikasi_Versi::getAplikasiVersi($aplikasiId);
        $dokumentasi = Dokumentasi::getDokumentasiList('aplikasi', 'id_aplikasi', $aplikasiId);
        $screenshot = Gambar::getGambarAplikasiList('aplikasi', 'id_aplikasi', $aplikasiId);
        // dd($screenshot);
        $list_jenis_pengembangan = Ms_Jenis_Pengembangan::get();
        $list_lokasi = Ms_Lokasi::get();

        return view('/aplikasi/aplikasiDetail',[
            'aplikasi' => $aplikasi,
            'alamat' => $alamat,
            'pengembang' => $pengembang,
            'versi' => $versi,
            'dokumentasi' => $dokumentasi,
            'screenshot' => $screenshot,
            'list_jenis_pengembangan' => $list_jenis_pengembangan,
            'list_lokasi' => $list_lokasi
        ]);
    }

    public function aplikasiSave(Request $request)
    {
        //dd($request);
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $existapp = Aplikasi::where('id_aplikasi',$request->id_aplikasi)->first();

        if(!$existapp){
            $aplikasi = Aplikasi::create([
                'nama_aplikasi' => $request->name,
                'singkatan_aplikasi' => $request->alias,
                'id_jenis_pengembangan' => $request->id_jenisdev,
                'kode_pengadaan' => $request->kode_pengadaan,               
                'created_by' => $auth->id
            ]);
            $aplikasi_insertedId = $aplikasi->id;
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $aplikasi_insertedId]);
        }
        else{

            if($request->mode=="1"){                
                $aplikasi = Aplikasi::where('id_aplikasi',$request->id_aplikasi)
                ->update([
                    'nama_aplikasi' => $request->nama_aplikasi,
                    'singkatan_aplikasi' => $request->singkatan_aplikasi,
                    'kode_pengadaan' => $request->kode_pengadaan,    
                    'updated_by' => $auth->id
                ]);
                return redirect()->route('aplikasi');
            }
            elseif($request->mode=="2"){                
                $aplikasi = Aplikasi::where('id_aplikasi',$request->id_aplikasi)
                ->update([
                    'id_jenis_pengembangan' => $request->id_jenisdev,
                    'bahasa_pemrograman' => $request->bahasa_pemrograman,
                    'framework_pemrograman' => $request->framework_pemrograman,
                    'sistem_operasi' => $request->sistem_operasi,
                    'tanggal_dev' => date('Y-m-d', strtotime($request->tanggal_dev)),
                    'tanggal_live' => date('Y-m-d', strtotime($request->tanggal_live)),
                    'deskripsi_aplikasi' => $request->deskripsi_aplikasi,
                    'updated_by' => $auth->id
                ]);
                return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);
            }
        }
    }

    public function aplikasiDelete(Request $request)
    {
        Aplikasi::where('id_aplikasi', $request->id)->delete();
        return redirect()->route('aplikasi');        
    }


    public function administratorIndex(Request $request)
    {
        //dd($request->username);
        return view('/aplikasi/administratorIndex');
    }

    public function alamatAplikasiSave(Request $request)
    {
        //dd($request);
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $existalm = Alamat::where('id_alamat',$request->id_alamat)->first();

        if(!$existalm){
            $alamat = Alamat::create([
                'src_tabel_alamat' => 'aplikasi',
                'src_kolom_alamat' => 'id_aplikasi',
                'src_fk_alamat' => $request->id_aplikasi,
                'nama_alamat' => $request->name,           
                'subdomain' => $request->subdomain,        
                'ip_address' => $request->ipaddress,        
                'port' => $request->port,        
                'keterangan_alamat' => $request->keterangan_alamat,        
                'alamat_utama' => $request->alamat_utama,            
                'created_by' => $auth->id
            ]);
            $alamat_insertedId = $alamat->id;
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);
        }
        else{                        
            $alamat = Alamat::where('id_alamat',$request->id_alamat)
            ->update([
                'nama_alamat' => $request->name,           
                'subdomain' => $request->subdomain,        
                'ip_address' => $request->ipaddress,        
                'port' => $request->port,        
                'keterangan_alamat' => $request->keterangan_alamat,        
                'alamat_utama' => $request->alamat_utama,
                'updated_by' => $auth->id
            ]);
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);            
        }
    }

    public function alamatAplikasiDelete(Request $request)
    {
        $id_aplikasi = $request->id_aplikasi;
        $id_alamat = $request->id_alamat;
        $mode = $request->mode;
        if($mode == 'delete'){
            Alamat::where('src_tabel_alamat','aplikasi')
            ->where('src_kolom_alamat','id_aplikasi')
            ->where('src_fk_alamat',$id_aplikasi)
            ->where('id_alamat', $id_alamat)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi])->with('noticemessage', 'Hapus alamat berhasil.')->with('flag','1');
        else 
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi])->with('noticemessage', 'Hapus alamat gagal.')->with('flag','0');
        
    }


    public function pengembangAplikasiSave(Request $request)
    {
        //dd($request);
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $existprog = Tim_Pengembang::where('id_pengembang',$request->id_pengembang)->first();

        if(!$existprog){
            $prog = Tim_Pengembang::create([
                'id_aplikasi' => $request->id_aplikasi,
                'nama_pengembang' => $request->nama_pengembang,           
                'nik_pengembang' => $request->nik_pengembang,        
                'jabatan_pengembang' => $request->jabatan_pengembang,        
                'nohp_pengembang' => $request->nohp_pengembang,        
                'email_pengembang' => $request->email_pengembang,        
                'keterangan_pengembang' => $request->keterangan_pengembang,                 
                'created_by' => $auth->id
            ]);
            $prog_insertedId = $prog->id;
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);
        }
        else{                        
            $prog = Tim_Pengembang::where('id_pengembang',$request->id_pengembang)
            ->update([
                'nama_pengembang' => $request->nama_pengembang,           
                'nik_pengembang' => $request->nik_pengembang,        
                'jabatan_pengembang' => $request->jabatan_pengembang,        
                'nohp_pengembang' => $request->nohp_pengembang,        
                'email_pengembang' => $request->email_pengembang,        
                'keterangan_pengembang' => $request->keterangan_pengembang,                 
                'updated_by' => $auth->id
            ]);
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);            
        }
    }

    public function pengembangAplikasiDelete(Request $request)
    {
        $id_aplikasi = $request->id_aplikasi;
        $id_pengembang = $request->id_pengembang;
        $mode = $request->mode;
        if($mode == 'delete'){
            Tim_Pengembang::where('id_pengembang', $id_pengembang)->delete();

            $del = TRUE;
        }else{
            $del = FALSE;
        }

        if($del)
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi])->with('noticemessage', 'Hapus pengembang berhasil.')->with('flag','1');
        else 
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi])->with('noticemessage', 'Hapus pengembang gagal.')->with('flag','0');
        
    }

    public function versiAplikasiSave(Request $request)
    {
        // dd($request);
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $existversi = Aplikasi_Versi::where('id_versi',$request->id_versi)->first();
        if(!$existversi){
            $versi = Aplikasi_Versi::create([
                'id_aplikasi' => $request->id_aplikasi,
                'nomor_versi' => $request->nomor_versi,           
                'tanggal_versi' => date('Y-m-d', strtotime($request->tanggal_versi)),
                'rincian_versi' => $request->rincian_versi,             
                'created_by' => $auth->id
            ]);
            $versi_insertedId = $versi->id;
            
            $fileCount = count($request->file());

            if($fileCount != 0) {
                if($request->hasFile('id_sourcecode')) {
                    $uploadedFile = $request->file('id_sourcecode');
                    $rootPath = '\app\public\files\sourcecode';
                    $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - '. $uploadedFile->getClientOriginalName());
                    // dd($result);

                    $id_lokasi = $request->input('sourcecode~lokasi');
                    $id_kontainer = $request->input('sourcecode~kontainer');
                    $id_posisi_kontainer = $request->input('sourcecode~posisi_kontainer');

                    $keterangan_sourcecode = $request->input('keterangan_sourcecode');
                    $ukuran_sourcecode = 0;
                    try {
                        $ukuran_sourcecode = $request->file('id_sourcecode')->getClientSize();
                    }
                    catch (\Exception $e) {
                        $ukuran_sourcecode = 0;
                    }
                    $sourcecode = Aplikasi_Sourcecode::create([
                        'id_aplikasi' => $request->id_aplikasi,
                        'id_versi' => $versi_insertedId,
                        'id_lokasi' => $id_lokasi,
                        'id_kontainer' => $id_kontainer,
                        'id_posisi_kontainer' => $id_posisi_kontainer,
                        'nama_sourcecode' => $uploadedFile->getClientOriginalName(),
                        'ukuran_sourcecode' => $ukuran_sourcecode,
                        'link_sourcecode' => $result->getRealPath(),
                        'keterangan_sourcecode' => $keterangan_sourcecode,
                        'created_by' => $auth->id
                    ]);
                    $sourcecode_insertedId = $sourcecode->id;
                }
                if ($request->hasFile('id_dokumentasi')) {
                    $uploadedFile = $request->file('id_dokumentasi');

                    $rootPath = '\app\public\files\dokumentasi';
                    $nama_dokumentasi = $request->input('nama_dokumentasi');
                    $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - '. $uploadedFile->getClientOriginalName());

                    $id_lokasi = $request->input('dokumentasi~lokasi');
                    $id_kontainer = $request->input('dokumentasi~kontainer');
                    $id_posisi_kontainer = $request->input('dokumentasi~posisi_kontainer');

                    $keterangan_dokumentasi = $request->input('keterangan_dokumentasi');
                    $ukuran_dokumentasi = 0;
                    try {
                        $ukuran_dokumentasi = $request->file('id_dokumentasi')->getClientSize();
                    }
                    catch (\Exception $e) {
                        $ukuran_dokumentasi = 0;
                    }
                    
                    $tanggal_dokumentasi = $request->input('tanggal_dokumentasi');
                    $dokumentasi = Dokumentasi::create([
                        'src_tabel_dokumentasi' => 'aplikasi_versi',
                        'src_kolom_dokumentasi' => 'id_versi',
                        'src_fk_dokumentasi' => $versi_insertedId,
                        'id_lokasi' => $id_lokasi,
                        'id_kontainer' => $id_kontainer,
                        'id_posisi_kontainer' => $id_posisi_kontainer,
                        'nama_dokumentasi' => $nama_dokumentasi,
                        'keterangan_dokumentasi' => $keterangan_dokumentasi,
                        'tanggal_dokumentasi' => $tanggal_dokumentasi,
                        'ukuran_dokumentasi' => $ukuran_dokumentasi,
                        'link_dokumentasi' => $result->getRealPath(),
                        'created_by' => $auth->id
                    ]);
                    $dokumentasi_insertedId = $dokumentasi->id;
                }            
            }
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);
        }
        else{                        
            $versi = Aplikasi_Versi::where('id_versi',$request->id_versi)
            ->update([
                'nomor_versi' => $request->nomor_versi,           
                'tanggal_versi' => $request->tanggal_versi,        
                'rincian_versi' => $request->rincian_versi,                
                'updated_by' => $auth->id
            ]);
            return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);            
        }
    }

    public function dokumentasiAplikasiSave(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $fileCount = count($request->file());

        if($fileCount != 0) {
            if ($request->hasFile('id_dokumentasi')) {
                $uploadedFile = $request->file('id_dokumentasi');

                $rootPath = '\app\public\files\dokumentasi';
                $nama_dokumentasi = $request->input('nama_dokumentasi');
                $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - '. $uploadedFile->getClientOriginalName());

                $id_lokasi = $request->input('id_lokasi');
                $id_kontainer = $request->input('id_kontainer');
                $id_posisi_kontainer = $request->input('id_posisi_kontainer');

                $keterangan_dokumentasi = $request->input('keterangan_dokumentasi');
                $ukuran_dokumentasi = 0;
                try {
                    $ukuran_dokumentasi = $request->file('id_dokumentasi')->getClientSize();
                }
                catch (\Exception $e) {
                    $ukuran_dokumentasi = 0;
                }
                
                $tanggal_dokumentasi = $request->input('tanggal_dokumentasi');
                $dokumentasi = Dokumentasi::create([
                    'src_tabel_dokumentasi' => 'aplikasi',
                    'src_kolom_dokumentasi' => 'id_aplikasi',
                    'src_fk_dokumentasi' => $request->id_aplikasi,
                    'id_lokasi' => $id_lokasi,
                    'id_kontainer' => $id_kontainer,
                    'id_posisi_kontainer' => $id_posisi_kontainer,
                    'nama_dokumentasi' => $nama_dokumentasi,
                    'keterangan_dokumentasi' => $keterangan_dokumentasi,
                    'tanggal_dokumentasi' => $tanggal_dokumentasi,
                    'ukuran_dokumentasi' => $ukuran_dokumentasi,
                    'link_dokumentasi' => $result->getRealPath(),
                    'created_by' => $auth->id
                ]);
                $dokumentasi_insertedId = $dokumentasi->id;
            }            
        }
        return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);
    }

    public function gambarAplikasiSave(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $fileCount = count($request->file());

        if($fileCount != 0) {
            if ($request->hasFile('id_gambar')) {
                $uploadedFile = $request->file('id_gambar');

                $rootPath = '\app\public\files\gambar';
                $nama_gambar = $request->input('nama_gambar');
                $result = $uploadedFile->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - '. $uploadedFile->getClientOriginalName());

                $id_lokasi = $request->input('id_lokasi');
                $id_kontainer = $request->input('id_kontainer');
                $id_posisi_kontainer = $request->input('id_posisi_kontainer');

                $keterangan_gambar = $request->input('keterangan_gambar');
                $ukuran_gambar = 0;
                try {
                    $ukuran_gambar = $request->file('id_gambar')->getClientSize();
                }
                catch (\Exception $e) {
                    $ukuran_gambar = 0;
                }
                
                $path_gambar = 'public/files/gambar/' . $system_datetime->format('YmdHis') . ' - ' . $uploadedFile->getClientOriginalName();
                $gambar = Gambar::create([
                    'src_tabel_gambar' => 'aplikasi',
                    'src_kolom_gambar' => 'id_aplikasi',
                    'src_fk_gambar' => $request->id_aplikasi,
                    'nama_gambar' => $nama_gambar,
                    'ukuran_gambar' => $ukuran_gambar,
                    'link_gambar' => $path_gambar,
                    'keterangan_gambar' => $keterangan_gambar,
                    'gambar_utama' => 'N',
                    'created_by' => $auth->id
                ]);
            }            
        }
        return redirect()->route('aplikasiDetail', ['aplikasiId' => $request->id_aplikasi]);
    }
}