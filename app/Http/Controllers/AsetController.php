<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Auth;
use App\Models\Aset;
use App\Models\Aset_Spesifikasi;
use App\Models\Ms_Field_Spesifikasi_Aset;
use App\Models\Ms_Kategori_Aset;
use App\Models\Ms_Merek_Aset;
use App\Models\Ms_Jenis_Aset;
use App\Models\Gambar;
use App\Models\MS_Lokasi;
use App\Models\Dokumentasi;
use App\Models\Aset_Satuan;

class AsetController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $message_id = session()->get('message_id');
        $message_source = session()->get('message_source');
        $message_crud = session()->get('message_crud');
        $message_text = session()->get('message_text');
        $message_data = session()->get('message_data');

        $is_filtered = 0;
        $filter = 0;
        $filter_name = null;

        if($request->is_filtered) {
            $is_filtered = $request->is_filtered;
            $filter = $request->filter;
            $filter_name = Ms_Jenis_Aset::getSingleJenisAset($filter)->nama_jenis_aset;
        }
        
        $is_searched = 0;
        $keyword = null;
        if($request->is_searched) {
            $is_searched = $request->is_searched;
            $keyword = $request->keyword;
        }

        $page_number = 1;
        $page_size = 15;

        if($request->page_number != 0) $page_number = $request->page_number;
        if($request->page_size != 0) $page_size = $request->page_size;

        $skip = ($page_number - 1) * $page_size;
        $take = $page_size;

        $result = Aset::getAsetListIndex($filter,$keyword,$skip,$take);
        $count = $result[0];
        $list_aset_index = $result[1];
        $list_jenis_aset = Ms_Jenis_Aset::getAllJenisAset();

        return view('/aset/asetIndex', [
            'list_aset_index' => $list_aset_index,
            'list_jenis_aset' => $list_jenis_aset,
            'count_list_aset_index' => $count,
            'is_filtered' => $is_filtered,
            'filter' => $filter,
            'filter_name' => $filter_name,
            'is_searched' => $is_searched,
            'keyword' => $keyword,
            'page_size' => $page_size,
            'page_number' => $page_number,
            'message_id' => $message_id,
            'message_source' => $message_source,
            'message_crud' => $message_crud,
            'message_text' => $message_text,
            'message_data' => $message_data
        ]);
    }

    public function add()
    {
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_merek_aset = Ms_Merek_Aset::get();
        $list_lokasi = Ms_Lokasi::get();

        return view('/aset/asetAdd', [
            'list_kategori_aset' => $list_kategori_aset,
            'list_merek_aset' => $list_merek_aset,
            'list_lokasi' => $list_lokasi
        ]);
    }

    public function simpanAset(Request $request)
    {
        $auth = User::getUser();
        $system_datetime = \Carbon\Carbon::now();
        $current_datetime  = $system_datetime->toDateTimeString();

        $aset = Aset::create([
            'id_jenis_aset' => $request->id_jenis_aset,
            'id_merek' => $request->id_merek,
            'tipe_aset' => $request->tipe_aset,
            'keterangan_aset' => $request->keterangan_aset,
            'created_by' => $auth->id
        ]);
        $aset_insertedId = $aset->id;

        $spesifikasiCount = $request->count_spesifikasi;
        for($i = 0; $i < $spesifikasiCount; $i++)
        {
            $idx = $i + 1;
            $aset_spesifikasi = Aset_Spesifikasi::create([
                'id_aset' => $aset_insertedId,
                'id_field' => $request->input('id_field~'. $idx),
                'isian_field' => $request->input('isian_field~'. $idx),
                'created_by' => $auth->id
            ]);

            $aset_spesifikasi_insertedId = $aset_spesifikasi->id;
        }

        $gambarCount = $request->count_gambar;
        $gambarArray = explode('~', $request->index_gambar);
        for($j = 0; $j < $gambarCount; $j++)
        {
            if($request->hasFile('id_gambar~' . $gambarArray[$j])) {
                $uploadedGambar = $request->file('id_gambar~' . $gambarArray[$j]);

                $rootPath = '\app\public\files\gambar';
                $nama_gambar = $request->input('nama_gambar~' . $gambarArray[$j]);
                $store_gambar = $uploadedGambar->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - ' . $uploadedGambar->getClientOriginalName());
                $ukuran_gambar = 0;
                $gambar_utama = '';
                if ($request->gambar_utama == $gambarArray[$j]) {
                    $gambar_utama = 'Y';
                } else {
                    $gambar_utama = 'N';
                }

                try {
                    $ukuran_gambar = $request->file('id_gambar~' . $gambarArray[$j])->getClientSize();
                } catch (\Exception $e) {
                    $ukuran_gambar = 0;
                }

                $path_gambar = 'public/files/gambar/' . $system_datetime->format('YmdHis') . ' - ' . $uploadedGambar->getClientOriginalName();
                $gambar = Gambar::create([
                    'src_tabel_gambar' => 'aset',
                    'src_kolom_gambar' => 'id_aset',
                    'src_fk_gambar' => $aset_insertedId,
                    'nama_gambar' => $request->input('nama_gambar~'. $gambarArray[$j]),
                    'ukuran_gambar' => $ukuran_gambar,
                    'link_gambar' => $path_gambar,
                    'keterangan_gambar' => $request->input('keterangan_gambar~'. $gambarArray[$j]),
                    'gambar_utama' => $gambar_utama,
                    'created_by' => $auth->id
                ]);
            }
        }

        $dokumenCount = $request->count_dokumen;
        $dokumenArray = explode('~', $request->index_dokumen);
        for($k = 0; $k < $dokumenCount; $k++)
        {
            if($request->hasFile('id_dokumentasi~' . $dokumenArray[$k])) {
                $uploadedDokumen = $request->file('id_dokumentasi~' . $dokumenArray[$k]);

                $rootPath = '\app\public\files\dokumentasi';
                $nama_dokumentasi = $request->input('nama_dokumentasi~' . $dokumenArray[$k]);
                $store_dokumen = $uploadedDokumen->move(storage_path() . $rootPath, $system_datetime->format('YmdHis') . ' - ' . $uploadedDokumen->getClientOriginalName());
                $ukuran_dokumen = 0;

                try {
                    $ukuran_dokumen = $request->file('id_dokumentasi~' . $dokumenArray[$k])->getClientSize();
                } catch (\Exception $e) {
                    $ukuran_dokumen = 0;
                }
                $dokumen = Dokumentasi::create([
                    'src_tabel_dokumentasi' => 'aset',
                    'src_kolom_dokumentasi' => 'id_aset',
                    'src_fk_dokumentasi' => $aset_insertedId,
                    'id_lokasi' => $request->input('dokumentasi~lokasi~'. $dokumenArray[$k]),
                    'id_kontainer' => $request->input('dokumentasi~kontainer~'. $dokumenArray[$k]),
                    'id_posisi_kontainer' => $request->input('dokumentasi~posisi_kontainer~'. $dokumenArray[$k]),
                    'nama_dokumentasi' => $request->input('nama_dokumentasi~'. $dokumenArray[$k]),
                    'tanggal_dokumentasi' => $request->input('tanggal_dokumentasi~'. $dokumenArray[$k]),
                    'keterangan_dokumentasi' => $request->input('keterangan_dokumentasi~'. $dokumenArray[$k]),
                    'ukuran_dokumentasi' => $ukuran_dokumen,
                    'link_dokumentasi' => $store_dokumen->getRealPath(),
                    'created_by' => $auth->id
                ]);
            }   
        }
        return redirect('/aset')->with(['message_id' => 1, 'message_source' => 'Aset', 'message_crud' => 'ditambahkan', 'message_text' => 'Success', 'message_data' => $aset_insertedId]);
    }

    public function detail($asetId)
    {
        $aset = Aset::getAset($asetId);
        $list_gambar = Gambar::getGambarAsetList('aset', 'id_aset', $asetId);
        $list_spesifikasi_aset = Aset_Spesifikasi::getAsetSpesifikasi($asetId);
        $list_dokumentasi_aset = Dokumentasi::getDokumentasiList('aset', 'id_aset', $asetId);

        // dd($list_dokumentasi_aset);

        return view('/aset/asetDetail', [
            'aset' => $aset,
            'list_gambar' => $list_gambar,
            'list_spesifikasi_aset' => $list_spesifikasi_aset,
            'list_dokumentasi_aset' => $list_dokumentasi_aset
        ]);
    }

    public function edit($asetId)
    {
        // dd($asetId);
        $aset = Aset::getAset($asetId);
        $list_spesifikasi_aset = Aset_Spesifikasi::getAsetSpesifikasi($asetId);
        $list_gambar_aset = Gambar::getGambarAsetList('aset', 'id_aset', $asetId);
        $list_dokumentasi_aset = Dokumentasi::getDokumentasiList('aset', 'id_aset', $asetId);
        // dd($list_dokumentasi_aset);
        $list_kategori_aset = Ms_Kategori_Aset::get();
        $list_merek_aset = Ms_Merek_Aset::get();
        $list_lokasi = Ms_Lokasi::get();

        return view('/aset/asetEdit', [
            'aset' => $aset,
            'list_spesifikasi_aset' => $list_spesifikasi_aset,
            'list_gambar_aset' => $list_gambar_aset,
            'list_dokumentasi_aset' => $list_dokumentasi_aset,
            'list_kategori_aset' => $list_kategori_aset,
            'list_merek_aset' => $list_merek_aset,
            'list_lokasi' => $list_lokasi
        ]);
    }

    public function delete(Request $request)
    {
        Aset::where('id_aset', $request->id_aset)->delete();
        return redirect()->route('aset');        
    }

    public function getTipeAsetList(Request $request)
    {
        $list_tipe_aset = Aset::getTipeAset($request->jenis_aset, $request->merek_aset);
        return response()->json($list_tipe_aset);
    }

    public function getSpesifikasiAsetList(Request $request)
    {
        $list_field = Ms_Field_Spesifikasi_Aset::getFieldSpesifikasi($request->jenis_aset);
        $list_spesifikasi = Aset_Spesifikasi::getAsetSpesifikasi($request->tipe_aset);
        $list_gambar = Gambar::getGambarAset('aset', 'id_aset', $request->tipe_aset);
        return response()->json(['list_field' => $list_field,
                                 'list_spesifikasi' => $list_spesifikasi,
                                 'list_gambar' => $list_gambar ]);
    }

    //catatAsetKeluar
    public function getAsetList(Request $request)
    {
        $list_aset = Aset::getAsetByJenisAset($request->jenis_aset);
        // dd($list_aset);
        return response()->json($list_aset);
    }

    public function getSatuanAsetList(Request $request)
    {
        $list_satuan_aset = Aset_Satuan::getSatuanAset($request->aset, $request->jenis_aset);
        return response()->json([ 'list_satuan_aset' => $list_satuan_aset ]);
    }
}