<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'nip', 'password' , 'status', 'activation_code',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


   public static function getUser()
   {
      $id_user = Auth::id();
       $data = DB::table('users')->where('id','=',$id_user)
        ->first();
        return $data;
   }

   public static function getAllUser()
   {
      $data = DB::table('users')
              ->get();

      return $data;

   }

}
