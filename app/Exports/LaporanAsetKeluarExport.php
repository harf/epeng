<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use App\User;
use App\Models\MS_Jenis_Kontainer;
use App\Models\MS_Kontainer;
use App\Models\MS_Lokasi;
use App\Models\MS_Posisi_Kontainer;
use App\Models\MS_Jenis_Aset;
use App\Models\MS_Merek_Aset;
use App\Models\MS_Jenis_Pengembangan;
use App\Models\MS_Kantor_Imigrasi;
use App\Models\MS_Penyedia;
use App\Models\MS_Menu;
use App\Models\Ms_Field_Spesifikasi_Aset;
use App\Models\Aset_Masuk;
use App\Models\Aset_Keluar;

class LaporanAsetKeluarExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nomor_aset_keluar;

    public function __construct($nomor_aset_keluar = "")
    {
    	$this->nomor_aset_keluar = $nomor_aset_keluar;
    }

    public function view(): View
    {
        $aset_keluar = Aset_Keluar::getAsetKeluar(null, null, $this->nomor_aset_keluar);
        $list_download_item_aset_keluar = Aset_Keluar::getDownloadItemAsetKeluar($this->nomor_aset_keluar);
        // dd($list_download_item_aset_keluar);
    	return view('laporan.downloadLaporanAsetKeluar', [
    		'nomor_aset_keluar' => $this->nomor_aset_keluar,
    		'aset_keluar' => $aset_keluar,
            'list_download_item_aset_keluar' => $list_download_item_aset_keluar
    	]);
    }
}
