<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use App\User;
use App\Models\MS_Jenis_Kontainer;
use App\Models\MS_Kontainer;
use App\Models\MS_Lokasi;
use App\Models\MS_Posisi_Kontainer;
use App\Models\MS_Jenis_Aset;
use App\Models\MS_Merek_Aset;
use App\Models\MS_Jenis_Pengembangan;
use App\Models\MS_Kantor_Imigrasi;
use App\Models\MS_Penyedia;
use App\Models\MS_Menu;
use App\Models\Ms_Field_Spesifikasi_Aset;
use App\Models\Aset_Masuk;
use App\Models\Aset_Keluar;

class LaporanAsetMasukExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $nomor_aset_masuk;

    public function __construct($nomor_aset_masuk = "")
    {
    	$this->nomor_aset_masuk = $nomor_aset_masuk;
    }

    public function view(): View
    {
        $aset_masuk = Aset_Masuk::getAsetMasuk(null, null, $this->nomor_aset_masuk);
        $list_download_item_aset_masuk = Aset_Masuk::getDownloadItemAsetMasuk($this->nomor_aset_masuk);
        // dd($list_download_item_aset_masuk);
    	return view('laporan.downloadLaporanAsetMasuk', [
    		'nomor_aset_masuk' => $this->nomor_aset_masuk,
    		'aset_masuk' => $aset_masuk,
            'list_download_item_aset_masuk' => $list_download_item_aset_masuk
    	]);
    }
}
