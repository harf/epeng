<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use App\User;
use App\Models\MS_Jenis_Kontainer;
use App\Models\MS_Kontainer;
use App\Models\MS_Lokasi;
use App\Models\MS_Posisi_Kontainer;
use App\Models\MS_Jenis_Aset;
use App\Models\MS_Merek_Aset;
use App\Models\MS_Jenis_Pengembangan;
use App\Models\MS_Kantor_Imigrasi;
use App\Models\MS_Penyedia;
use App\Models\MS_Menu;
use App\Models\Ms_Field_Spesifikasi_Aset;
use App\Models\Aset_Masuk;
use App\Models\Aset_Keluar;

class LaporanPersebaranAsetExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $kode_kanim;

    public function __construct($kode_kanim = "")
    {
    	$this->kode_kanim = $kode_kanim;
    }

    public function view(): View
    {
        $nama_lokasi = '';

        if ($this->kode_kanim != 0)
        {
            $kanim = MS_Kantor_Imigrasi::getKantorImigrasiByID($this->kode_kanim);
            $nama_lokasi = $kanim->nama_kanim;
        }
        else
        {
            $nama_lokasi = 'Lain-lain';
        }

        $list_download_item_persebaran_aset = Aset_Keluar::getDownloadItemPersebaranAset($this->kode_kanim);
        // dd($list_download_item_persebaran_aset);
    	return view('laporan.downloadLaporanPersebaranAset', [
    		'kode_kanim' => $this->kode_kanim,
            'nama_lokasi' => $nama_lokasi,
            'list_download_item_persebaran_aset' => $list_download_item_persebaran_aset
    	]);
    }
}
