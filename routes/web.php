<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	if(Auth::check()){
		Artisan::call('cache:clear');
		return view('home');
	} else {
    	return view('auth/login');
	}
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/verify-user/{code}', 'Auth\RegisterController@activateUser')->name('activate.user');
Route::get('/mail-config',  function() {
    return config('mail');
});
Route::get('error',['as'=>'error','uses'=>'ErrorHandlerController@defaultError']);
Route::get('404',['as'=>'404','uses'=>'ErrorHandlerController@errorCode404']);
Route::get('405',['as'=>'405','uses'=>'ErrorHandlerController@errorCode405']);

Route::get('/clearcache', function() {
    Artisan::call('cache:clear');
    return view('/home');
})->name('clearcache');


/////////////////////////////// APLIKASI ///////////////////////////////
	Route::get('/aplikasi', 'AplikasiController@index')->name('aplikasi');
	Route::get('/aplikasi/{aplikasiId?}', 'AplikasiController@detail')->name('aplikasiDetail');
	Route::post('/aplikasi/{aplikasiId?}/administrator', 'AplikasiController@administratorIndex')->name('administratorIndex');
	Route::post('/aplikasi', 'AplikasiController@aplikasiSave')->name('aplikasiSave');
	Route::post('/aplikasiDelete', 'AplikasiController@aplikasiDelete')->name('aplikasiDelete');
	Route::post('/alamatAplikasiSave', 'AplikasiController@alamatAplikasiSave')->name('alamatAplikasiSave');
	Route::post('/alamatAplikasiDelete', 'AplikasiController@alamatAplikasiDelete')->name('alamatAplikasiDelete');
	Route::post('/pengembangAplikasiSave', 'AplikasiController@pengembangAplikasiSave')->name('pengembangAplikasiSave');
	Route::post('/pengembangAplikasiDelete', 'AplikasiController@pengembangAplikasiDelete')->name('pengembangAplikasiDelete');
	Route::post('/versiAplikasiSave', 'AplikasiController@versiAplikasiSave')->name('versiAplikasiSave');
	Route::post('/dokumentasiAplikasiSave', 'AplikasiController@dokumentasiAplikasiSave')->name('dokumentasiAplikasiSave');
	Route::post('/gambarAplikasiSave', 'AplikasiController@gambarAplikasiSave')->name('gambarAplikasiSave');


////////////////////////////////// ASET ////////////////////////////////////////
	Route::get('/aset', 'AsetController@index')->name('aset');
	Route::get('/aset/tambah', 'AsetController@add')->name('asetAdd');
	Route::get('/aset/{asetId?}/detail', 'AsetController@detail')->name('asetDetail');
	Route::get('api/get-tipe-aset-list','AsetController@getTipeAsetList');
	Route::get('api/get-spesifikasi-aset-list','AsetController@getSpesifikasiAsetList');
	Route::post('/simpanAset', 'AsetController@simpanAset')->name('simpanAset');
	Route::get('api/get-aset-list','AsetController@getAsetList');
	Route::get('api/get-satuan-aset-list','AsetController@getSatuanAsetList');
	Route::get('/aset/{asetId?}/ubah', 'AsetController@edit')->name('asetEdit');
	Route::post('/asetDelete', 'AsetController@delete')->name('asetDelete');

////////////////////////////////// PENGADAAN /////////////////////////////////////
	Route::post('/simpanPengadaan',  'PengadaanController@simpanPengadaan')->name('simpanPengadaan');
	Route::get('/pengadaan', 'PengadaanController@index')->name('pengadaan');
	Route::get('/pengadaan/tambah', 'PengadaanController@add')->name('pengadaanAdd');
	Route::get('/pengadaan/{pengadaanId?}', 'PengadaanController@detail')->name('pengadaanDetail');
	Route::get('api/get-satuan-item-pengadaan','PengadaanController@getSatuanItemPengadaan');
	Route::get('/pengadaan/{pengadaanId?}/ubah', 'PengadaanController@edit')->name('pengadaanEdit');
	Route::post('/pengadaanDelete', 'PengadaanController@delete')->name('pengadaanDelete');

////////////////////////////////// PERMINTAAN /////////////////////////////////////
	Route::post('/simpanPermintaan',  'PermintaanController@simpanPermintaan')->name('simpanPermintaan');
	Route::get('/permintaan', 'PermintaanController@index')->name('permintaan');
	
	Route::get('/inputPermintaan', 'PermintaanController@add')->name('inputPermintaan');
	Route::get('/permintaan/{permintaanId?}', 'PermintaanController@detail')->name('permintaanDetail');
	Route::get('/permintaan/{permintaanId?}/ubah', 'PermintaanController@edit')->name('permintaanEdit');
	Route::post('/PermintaanDelete', 'PermintaanController@delete')->name('permintaanDelete');
	Route::get('/permintaanKirimIndex', 'PermintaanController@kirimIndex')->name('permintaanKirimIndex');
	Route::get('/permintaanKirimAdd', 'PermintaanController@kirimAdd')->name('permintaanKirimAdd');
	Route::get('/riwayatPermintaan', 'PermintaanController@riwayatPermintaan')->name('riwayatPermintaan');
	Route::get('/tindakLanjutPermintaan', 'PermintaanController@tindakLanjutPermintaan')->name('tindakLanjutPermintaan');

////////////////////////////////// KONFIRMASI PERMINTAAN /////////////////////////////////////
// Route::post('/simpanPermintaan',  'PermintaanController@simpanPermintaan')->name('simpanPermintaan'); get-sumber-permintaan-list
	Route::get('/konfirmasi', 'PermintaanController@indexKonfirmasi')->name('konfirmasi');
	Route::get('/detailPermintaanInformasi/{id_permintaan?}/informasi', 'PermintaanController@detailPermintaanInformasi')->name('detailPermintaanInformasi');
	Route::get('api/get-sumber-permintaan-list','PermintaanController@getSumberPermintaanList');
	Route::post('/simpanKonfirmasi',  'PermintaanController@simpanKonfirmasi')->name('simpanKonfirmasi');

////////////////////////////////// ALOKASI PERMINTAAN /////////////////////////////////////
	Route::get('/alokasiIndex', 'PermintaanController@alokasiIndex')->name('alokasiIndex');
	Route::get('/permintaanAlokasiAdd/{id_permintaan?}/add', 'PermintaanController@alokasiAdd')->name('detailPermintaanInformasi');
	Route::get('api/get-jenis-aset-list-permintaan','PermintaanController@getJenisAsetListPermintaan');

////////////////////////////////// PERSEDIAAN /////////////////////////////////////
	Route::post('/simpanPersediaan',  'PersediaanController@simpanPersediaan')->name('simpanPersediaan');
	Route::get('/persediaan', 'PersediaanController@index')->name('persediaan');
	Route::get('/inputStokAwal', 'PersediaanController@page_add')->name('inputstokawalAdd');
	Route::get('/persediaan/{persediaanId?}', 'PersediaanController@detail')->name('persediaanDetail');
	Route::get('api/get-satuan-item-persediaan','PersediaanController@getSatuanItempersediaan');
	Route::get('/persediaan/{persediaanId?}/ubah', 'PersediaanController@edit')->name('persediaanEdit');
	Route::post('/persediaanDelete', 'PersediaanController@delete')->name('persediaanDelete');

////////////////////////////////// MANAJEMEN ASET /////////////////////////////////////
	Route::get('/manajemenAset', 'ManajemenAsetController@index')->name('manajemenAset');
	Route::get('/manajemenAset/rincian/{asetId?}', 'ManajemenAsetController@detail')->name('manajemenAsetDetail');
	Route::get('/manajemenAset/aktivitas/{asetId?}', 'ManajemenAsetController@activity')->name('manajemenAsetActivity');
	Route::get('/manajemenAset/catatAsetMasuk', 'ManajemenAsetController@catatAsetMasuk')->name('catatAsetMasuk');
	Route::get('/manajemenAset/catatAsetKeluar', 'ManajemenAsetController@catatAsetKeluar')->name('catatAsetKeluar');
	Route::get('/manajemenAset/catatAsetInformasi', 'ManajemenAsetController@catatAsetInformasi')->name('catatAsetInformasi');
	Route::get('/manajemenAset/{kodeGrupId?}/informasi', 'ManajemenAsetController@detailAsetInformasi')->name('detailAsetInformasi');
	Route::post('/simpanAsetMasuk', 'ManajemenAsetController@simpanAsetMasuk')->name('simpanAsetMasuk');
	Route::post('/simpanAsetKeluar', 'ManajemenAsetController@simpanAsetKeluar')->name('simpanAsetKeluar');
	Route::post('/simpanAsetInformasi', 'ManajemenAsetController@simpanAsetInformasi')->name('simpanAsetInformasi');	
	Route::post('/ubahFieldIsianAset', 'ManajemenAsetController@ubahFieldIsianAset')->name('ubahFieldIsianAset');	
	Route::post('/ubahAsetInformasi', 'ManajemenAsetController@ubahAsetInformasi')->name('ubahAsetInformasi');
	Route::get('api/get-sumber-pengadaan-list','ManajemenAsetController@getSumberPengadaanList');
	Route::get('api/get-satuan-aset-pengadaan-list','ManajemenAsetController@getSatuanAsetPengadaanList');

///////////////////////////////////PENGATURAN//////////////////////////////////////
	//*** TEMPLATE ***//
	Route::get('/templateBAST', 'PengaturanController@templateBAST')->name('templateBAST');
	Route::get('/templateSPP', 'PengaturanController@templateSPP')->name('templateSPP');

	//*** signature ***//
	Route::get('/signature',  'PengaturanController@signature')->name('signature');
	Route::get('/signature_edit/{id}',  'PengaturanController@signature_edit')->name('signature_edit');
	Route::get('/signature_delete/{id}',  'PengaturanController@signature_delete')->name('signature_delete');
	Route::post('/signature_save',  'PengaturanController@signature_save')->name('signature_save');

	//*** JENIS KONTAINER ***//
	Route::get('/jenis_kontainer',  'PengaturanController@jenis_kontainer')->name('jenis_kontainer');
	Route::post('/jenis_kontainer_edit',  'PengaturanController@jenis_kontainer_edit')->name('jenis_kontainer_edit');
	Route::post('/jenis_kontainer_delete',  'PengaturanController@jenis_kontainer_delete')->name('jenis_kontainer_delete');
	Route::post('/jenis_kontainer_save',  'PengaturanController@jenis_kontainer_save')->name('jenis_kontainer_save');

	//*** KONTAINER ***//
	Route::get('/kontainer',  'PengaturanController@kontainer')->name('kontainer');
	Route::post('/kontainer_edit',  'PengaturanController@kontainer_edit')->name('kontainer_edit');
	Route::post('/kontainer_delete',  'PengaturanController@kontainer_delete')->name('kontainer_delete');
	Route::post('/kontainer_save',  'PengaturanController@kontainer_save')->name('kontainer_save');
	Route::get('api/get-kontainer-list','PengaturanController@getKontainerList');

	//*** POSISI KONTAINER ***//
	Route::get('/posisi_kontainer',  'PengaturanController@posisi_kontainer')->name('posisi_kontainer');
	Route::post('/posisi_kontainer_edit',  'PengaturanController@posisi_kontainer_edit')->name('posisi_kontainer_edit');
	Route::post('/posisi_kontainer_delete',  'PengaturanController@posisi_kontainer_delete')->name('posisi_kontainer_delete');
	Route::post('/posisi_kontainer_save',  'PengaturanController@posisi_kontainer_save')->name('posisi_kontainer_save');
	Route::get('api/get-posisi-kontainer-list','PengaturanController@getPosisiKontainerList');

	//*** LOKASI KONTAINER ***//
	Route::get('/lokasi_kontainer',  'PengaturanController@lokasi_kontainer')->name('lokasi_kontainer');
	Route::post('/lokasi_kontainer_edit',  'PengaturanController@lokasi_kontainer_edit')->name('lokasi_kontainer_edit');
	Route::post('/lokasi_kontainer_delete',  'PengaturanController@lokasi_kontainer_delete')->name('lokasi_kontainer_delete');
	Route::post('/lokasi_kontainer_save',  'PengaturanController@lokasi_kontainer_save')->name('lokasi_kontainer_save');

	//*** DASHBOARD KONTAINER ***//
	Route::get('/dashboard_kontainer',  'PengaturanController@dashboard_kontainer')->name('dashboard_kontainer');

	//*** JENIS ASET ***//
	Route::get('/jenis_aset',  'PengaturanController@jenis_aset')->name('jenis_aset');
	Route::post('/jenis_aset_edit',  'PengaturanController@jenis_aset_edit')->name('jenis_aset_edit');
	Route::post('/jenis_aset_delete',  'PengaturanController@jenis_aset_delete')->name('jenis_aset_delete');
	Route::post('/jenis_aset_save',  'PengaturanController@jenis_aset_save')->name('jenis_aset_save');
	Route::get('api/get-jenis-aset-list','PengaturanController@getJenisAsetList');

	//*** MEREK ASET ***//
	Route::get('/merek_aset',  'PengaturanController@merek_aset')->name('merek_aset');
	Route::post('/merek_aset_edit',  'PengaturanController@merek_aset_edit')->name('merek_aset_edit');
	Route::post('/merek_aset_delete',  'PengaturanController@merek_aset_delete')->name('merek_aset_delete');
	Route::post('/merek_aset
	_save',  'PengaturanController@merek_aset_save')->name('merek_aset_save');

	//*** KATEGORI ASET ***//
	Route::get('/kategori_aset',  'PengaturanController@kategori_aset')->name('kategori_aset');
	Route::post('/kategori_aset_edit',  'PengaturanController@kategori_aset_edit')->name('kategori_aset_edit');
	Route::post('/kategori_aset_delete',  'PengaturanController@kategori_aset_delete')->name('kategori_aset_delete');
	Route::post('/kategori_aset_save',  'PengaturanController@kategori_aset_save')->name('kategori_aset_save');

	//*** JENIS PENGEMBANGAN ***//
	Route::get('/jenis_pengembangan',  'PengaturanController@jenis_pengembangan')->name('jenis_pengembangan');
	Route::post('/jenis_pengembangan_edit',  'PengaturanController@jenis_pengembangan_edit')->name('jenis_pengembangan_edit');
	Route::post('/jenis_pengembangan_delete',  'PengaturanController@jenis_pengembangan_delete')->name('jenis_pengembangan_delete');
	Route::post('/jenis_pengembangan_save',  'PengaturanController@jenis_pengembangan_save')->name('jenis_pengembangan_save');

	//*** UPT ***//
	Route::get('/kantor',  'PengaturanController@kantor')->name('kantor');
	Route::post('/kantor_edit',  'PengaturanController@kantor_edit')->name('kantor_edit');
	Route::post('/kantor_delete',  'PengaturanController@kantor_delete')->name('kantor_delete');
	Route::post('/kantor_save',  'PengaturanController@kantor_save')->name('kantor_save');
	Route::get('json/kantorjson','PengaturanController@kantorjson');
	
	//*** Penyedia ***//
	Route::get('/penyedia',  'PengaturanController@penyedia')->name('penyedia');
	Route::post('/penyedia_edit',  'PengaturanController@penyedia_edit')->name('penyedia_edit');
	Route::post('/penyedia_delete',  'PengaturanController@penyedia_delete')->name('penyedia_delete');
	Route::post('/penyedia_save',  'PengaturanController@penyedia_save')->name('penyedia_save');
	
	//*** Menu ***//
	Route::get('/menu',  'PengaturanController@menu')->name('menu');
	Route::post('/menu_edit',  'PengaturanController@menu_edit')->name('menu_edit');
	Route::post('/menu_delete',  'PengaturanController@menu_delete')->name('menu_delete');
	Route::post('/menu_save',  'PengaturanController@menu_save')->name('menu_save');

	//*** Field Spesifikasi Aset ***//
	Route::get('api/get-spesifikasi-field-list','PengaturanController@getSpesifikasiFieldList');

	//*** AKUN ***//
	Route::get('/akun',  'PengaturanController@akun')->name('akun');
	Route::post('/akun_edit',  'PengaturanController@akun_edit')->name('akun_edit');
	Route::post('/akun_delete',  'PengaturanController@akun_delete')->name('akun_edit');
	Route::post('/akun_save',  'PengaturanController@akun_save')->name('akun_save');
	Route::get('json/akunjson','PengaturanController@akunjson');

	//*** Field Aset Informasi ***//
	Route::get('api/get-field-aset-informasi-list', 'PengaturanController@getAsetInformasiFieldListCatat');

/////////////////////////////////// LAPORAN //////////////////////////////////////
	Route::get('/laporanAsetMasuk',  'LaporanController@laporanAsetMasuk')->name('laporanAsetMasuk');
	Route::get('/laporanAsetMasuk/{nomor_aset_masuk}',  'LaporanController@detailLaporanAsetMasuk')->name('detailLaporanAsetMasuk');
	Route::get('api/get-satuan-aset-masuk','LaporanController@getSatuanAsetMasuk');
	Route::get('/downloadLaporanAsetMasuk/{nomor_aset_masuk}',  'LaporanController@exportLaporanAsetMasuk')->name('downloadLaporanAsetMasuk');
	// Route::post('/cetakLaporanAsetMasuk',  'LaporanController@cetakLaporanAsetMasuk')->name('cetakLaporanAsetMasuk');
	// Route::get('json/laporanasetmasukjson','LaporanController@getLaporanAsetMasukJson');
	// Route::post('/periodeLaporanAsetMasuk',  'LaporanController@filterPeriodeAsetMasuk')->name('periodeLaporanAsetMasuk');

	Route::get('/laporanAsetKeluar',  'LaporanController@laporanAsetKeluar')->name('laporanAsetKeluar');
	Route::get('/laporanAsetKeluar/{nomor_aset_keluar}',  'LaporanController@detailLaporanAsetKeluar')->name('detailLaporanAsetKeluar');
	Route::get('api/get-satuan-aset-keluar','LaporanController@getSatuanAsetKeluar');
	Route::get('/downloadLaporanAsetKeluar/{nomor_aset_keluar}',  'LaporanController@exportLaporanAsetKeluar')->name('downloadLaporanAsetKeluar');

	Route::get('/laporanPersebaranAset',  'LaporanController@laporanPersebaranAset')->name('laporanPersebaranAset');
	Route::get('/laporanPersebaranAset/{kode_kanim}',  'LaporanController@detailLaporanPersebaranAset')->name('detailLaporanPersebaranAset');
	Route::get('api/get-satuan-aset-persebaran','LaporanController@getSatuanAsetPersebaran');
	Route::get('/downloadLaporanPersebaranAset/{kode_kanim}',  'LaporanController@exportLaporanPersebaranAset')->name('downloadLaporanPersebaranAset');


////////////////////////////////// PENGIRIMAN //////////////////////////////////////
	Route::get('/pengiriman',  'PengirimanController@index')->name('pengiriman');
	Route::get('/pengirimanAdd', 'PengirimanController@add')->name('pengirimanAdd');
	Route::get('/pengirimanTambah', 'PengirimanController@tambah')->name('pengirimanTambah');
	Route::get('/pengirimanAlokasi', 'PengirimanController@alokasi')->name('pengirimanAlokasi');
	Route::get('/pengirimanPusat', 'PengirimanController@pusat')->name('pengirimanPusat');
	Route::get('/pengirimanRincian/{id_pengiriman?}/rincian', 'PengirimanController@rincian')->name('pengirimanRincian');
	Route::get('/pengirimanAlokasiRincian/{id_alokasi?}/rincian', 'PengirimanController@alokasirincian')->name('pengirimanAlokasiRincian');
	Route::get('/tindakLanjutKirimanPusat', 'PengirimanController@tindakLanjutKirimanPusat')->name('tindakLanjut');
	

////////////////////////////////// DOKUMENTASI /////////////////////////////////////
	Route::get('/downloadDokumentasi/{id_dokumentasi?}', 'PengaturanController@downloadDokumentasi')->name('downloadDokumentasi');
	Route::get('/downloadSourceCode/{id_sourcecode?}', 'PengaturanController@downloadSourceCode')->name('downloadSourceCode');
	Route::get('/downloadFileInfo/{id_aset_info?}', 'PengaturanController@downloadFileInfo')->name('downloadFileInfo');


// ///////////////////////////////// KREDENSIAL ///////////////////////////////////
// 	Route::get('/kredensial', 'PengaturanController@kredensial')->name('kredensial');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
