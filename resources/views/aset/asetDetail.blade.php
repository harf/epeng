<script src="/js/core/jquery.min.js" type="text/javascript"></script>

<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>

@extends('layouts.master')
@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/aset">Aset SIMKIM</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Detail</u></li>
  </ol>
</nav>
<div class="row">
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
    <div class="card card-profile">
      <div class="card-header card-header-primary">
        <h3 class="card-title"><strong>{{ ($aset->nama_merek)?$aset->nama_merek:'' }} {{ ($aset->tipe_aset)?$aset->tipe_aset:'' }}</strong></h3>
      </div>
      <div class="card-body" align="left">
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
          <div class="carousel-inner">
            @foreach($list_gambar as $key => $value)
              @if($value->gambar_utama == "Y")
              <div class="carousel-item active">
                <center>
                  <img class="d-block w-100" src="{{ str_replace('public', '/storage', $value->link_gambar) }}" alt="{{ $value->nama_gambar }}" style="height: 650px;">
                  <div class="carousel-caption d-none d-md-block">
                    <h2 class="otto"><strong>{{ $value->nama_gambar }}</strong></h2>
                    <h3 class="smash">{{ $value->keterangan_gambar }}</h3>
                  </div>
                </center>
              </div> 
              @else
              <div class="carousel-item">
                <center>
                  <img class="d-block w-100" src="{{ str_replace('public', '/storage', $value->link_gambar) }}" alt="{{ $value->nama_gambar }}" style="height: 750px;">
                  <div class="carousel-caption d-none d-md-block">
                    <h2 class="otto"><strong>{{ $value->nama_gambar }}</strong></h2> 
                    <h3 class="smash">{{ $value->keterangan_gambar }}</h3>
                  </div>
                </center>
              </div> 
              @endif  
            @endforeach
          </div>
          @if(count($list_gambar) > 1)
          <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          @endif
        </div>
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <strong class="text-primary"><u>RINCIAN ASET</u></strong>
        </div>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                Merek
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <strong class="text-primary">{{ ($aset->nama_merek)?$aset->nama_merek:'-' }}</strong>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                Tipe
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <strong class="text-primary">{{ ($aset->tipe_aset)?$aset->tipe_aset:'-' }}</strong>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                Kategori Aset
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <strong class="text-primary">{{ ($aset->nama_kategori_aset)?$aset->nama_kategori_aset:'-' }}</strong>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                Jenis Aset
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <strong class="text-primary">{{ ($aset->nama_jenis_aset)?$aset->nama_jenis_aset:'-' }}</strong>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <strong class="text-primary"><u>SPESIFIKASI ASET</u></strong>
        </div>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?php
              $length_list_field = count($list_spesifikasi_aset);
              $length_list_kiri = round($length_list_field/2);
              $length_list_kanan = $length_list_field - $length_list_kiri;
              for ($i = 0; $i < $length_list_kiri; $i++)
              {
            ?>
                <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    {{ $list_spesifikasi_aset[$i]->nama_field_spek }}
                  </div>
            <?php
                if ($list_spesifikasi_aset[$i]->isian_field)
                {
            ?>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <strong class="text-primary">{{ $list_spesifikasi_aset[$i]->isian_field }}</strong>
                  </div>
            <?php
                } else {
            ?>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <strong class="text-primary">-</strong>
                  </div>
            <?php
                }
            ?>
                </div>
            <?php
              }
            ?>
          </div>
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <?php
              for ($j = $length_list_kiri; $j < $length_list_field; $j++)
              {
            ?>
              <div class="row">
                  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                    {{ $list_spesifikasi_aset[$j]->nama_field_spek }}
                  </div>
            <?php
                if ($list_spesifikasi_aset[$j]->isian_field)
                {
            ?>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <strong class="text-primary">{{ $list_spesifikasi_aset[$j]->isian_field }}</strong>
                  </div>
            <?php
                } else {
            ?>
                  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                    <strong class="text-primary">-</strong>
                  </div>
            <?php
                }
            ?>
                </div>
            <?php
              }
            ?>
          </div>
        </div>
        <hr>
        <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <strong class="text-primary"><u>DOKUMENTASI ASET</u></strong>
        </div>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <table class="table table-border table-hover">
            <colgroup>
              <col style="width:5%;">
              <col style="width:30%;">
              <col style="width:15%;">
              <col style="width:20%;">
              <col style="width:30%;">
            </colgroup>
            <thead class="thead-light">
              <tr>
                <th>NO.</th>
                <th>DOKUMENTASI ASET</th>
                <th>TANGGAL DOKUMENTASI</th>
                <th>KETERANGAN DOKUMENTASI</th>
                <th>DOKUMENTASI FISIK DISIMPAN DI</th>
              </tr>
            </thead>
            <tbody>
              @if(count($list_dokumentasi_aset) != 0)
                @foreach($list_dokumentasi_aset as $key => $value)
                  <tr>
                    <td>{{ ($key + 1) }}.</td>
                    <td><u><a href="/downloadDokumentasi/{{ $value->id_dokumentasi }}">{{ $value->nama_dokumentasi }} <i class="fa fa-download"></i></a></u></td>
                    <td>{{ strftime( "%d %B %Y", strtotime($value->tanggal_dokumentasi)) }}</td>
                    <td>{{ $value->keterangan_dokumentasi }}</td>
                    <td>
                      {{ $value->id_lokasi ? $value->nama_lokasi : ''  }} {{ $value->id_kontainer ? ' >> ' . $value->nama_kontainer : '' }} {{ $value->id_posisi_kontainer ?' >> ' . $value->label_posisi_kontainer : '' }}
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5"><center><i>Tidak ada data.</i></center></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>      
      </div>
    </div>
  </div>
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
</div>

<script type="text/javascript">
</script>

@endsection



