<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/aset">Aset SIMKIM</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Tambah</u></li>
  </ol>
</nav>
<div class="row">
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <form method="POST" action="{{ route('simpanAset') }}" enctype="multipart/form-data">
      <div class="card card-profile">
        <div class="card-header card-header-primary">
          <h4 class="card-title"><strong>Tambah Aset SIMKIM</strong></h4>
        </div>
        <div class="card-body" align="left">
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row">
            <div class="col-md-2 col-sm-2 col-xs-2">
              Kategori Aset
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <select class="form-control input-sm" name="id_kategori_aset" id="kategori-aset">
                <option value=""  selected="true">Pilih Kategori Aset</option>
                @foreach ($list_kategori_aset as $kategori)
                  <option value="{{ $kategori->id_kategori_aset }}">{{ $kategori->nama_kategori_aset }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
              Jenis Aset
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <select class="form-control input-sm" name="id_jenis_aset" id="jenis-aset">
                <option value=""  selected="true">Pilih Jenis Aset</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-2 col-sm-2 col-xs-2">
              Merek Aset
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <select class="form-control input-sm" name="id_merek" id="merek-aset">
                <option value=""  selected="true">Pilih Merek Aset</option>
                @foreach ($list_merek_aset as $merek)
                  <option value="{{ $merek->id_merek }}">{{ $merek->nama_merek }}</option>
                @endforeach
              </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2">
              Tipe Aset
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <input type="text" class="form-control" name="tipe_aset" id="tipe-aset">
            </div>
          </div>
          <hr>
          <div class="form-group row">
            <div class="col-md-2 col-sm-2 col-xs-2">
              Spesifikasi Aset
            </div>
          </div>
          <div class="row">
            <input type="hidden" name="count_spesifikasi" id="count-spesifikasi" value="0" />
            <input type="hidden" name="index_spesifikasi" id="index-spesifikasi" value="" />
            <div class="row col-md-6 col-sm-6 col-xs-6" id="div-spesifikasi-field-kiri">
            </div>
            <div class="row col-md-6 col-sm-6 col-xs-6" id="div-spesifikasi-field-kanan">
            </div>
          </div>
          <hr>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Keterangan Aset
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <textarea class="form-control" name="keterangan_aset" rows="3"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                &emsp;Gambar Aset &emsp;
                <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-gambar" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
              </div>
              <input type="hidden" id="count-gambar" name="count_gambar" value="0" />
              <input type="hidden" id="last-gambar-id" value="0" />
              <input type="hidden" id="index-gambar" name="index_gambar" value="" />
              <input type="hidden" id="gambar-utama" name="gambar_utama" value="" />
            </div>
            <div id="gambar-aset">
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                &emsp;Dokumen Aset &emsp;
                <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-dokumen" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
              </div>
              <input type="hidden" id="count-dokumen" name="count_dokumen" value="0" />
              <input type="hidden" id="last-dokumen-id" value="0" />
              <input type="hidden" id="index-dokumen" name="index_dokumen" value="" />
            </div>
            <div id="dokumen-aset">

            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row pull-right">
            <a href="/aset" class="btn btn-secondary"><i class="fa fa-caret-left"></i>&emsp;Batal</a>
            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Simpan Aset SIMKIM</button>
          </div>
        </div>
      </div>
      {{ csrf_field() }}
    </form>
  </div>
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
</div>

<script type="text/javascript"> 
$(document).ready(function(){
  window.onbeforeunload = function () {
    var msg = "Apakah Anda akan meninggalkan halaman ini? Data akan hilang sebelum Anda menyimpannya.";
    return msg;
  };

  $(document).on("change", "#kategori-aset", function (e) {
    e.preventDefault();
    var kategori_aset = e.target.value;
    // console.log(kategori_aset);
    if(kategori_aset){
      $.ajax({
         type:"GET",
         url:"{{url('api/get-jenis-aset-list')}}?kategori_aset="+kategori_aset,
         success:function(res){
          if(res){
            console.log(res);            
            $('#jenis-aset').empty();
            $('#tipe-aset').val('');
            $('#jenis-aset').append('<option value="" disable="true" selected="true">Pilih Jenis Aset</option>');
            $.each(res,function(index,Obj){
                $("#jenis-aset").append('<option value="'+Obj.id_jenis_aset+'">'+Obj.nama_jenis_aset+'</option>');
              });

          }else{
            $('#jenis-aset').empty();
            $('#tipe-aset').val('');
            $('#jenis-aset').append('<option value="" disable="true" selected="true">Pilih Jenis Aset</option>');
          }
        },
        error:function(x) {
          console.log(x);
        }
      });
    }
    else{
      $('#jenis-aset').empty();
      $('#tipe-aset').val('');
      $('#jenis-aset').append('<option value="" disable="true" selected="true">Pilih Jenis Aset</option>');
    }
  });

  $(document).on("change", "#jenis-aset", function (e) {
    e.preventDefault();
    var jenis_aset = e.target.value;
    $('#div-spesifikasi-field-kiri').empty();
    $('#div-spesifikasi-field-kanan').empty();

    if(jenis_aset) {
      $.ajax({
         type:"GET",
         url:"{{url('api/get-spesifikasi-field-list')}}?jenis_aset="+jenis_aset,
         success:function(res){
          if(res){
            var list_field = res.list_field;
            var length_list_field = list_field.length;
            $('#count-spesifikasi').val(length_list_field);

            var index_spesifikasi = $('#index-spesifikasi').val();

            var length_list_field = res.list_field.length;
            var length_list_kiri = Math.round(length_list_field/2);
            for (var i = 0; i < length_list_kiri; i++)
            {
              var idx = i + 1;
              $('#div-spesifikasi-field-kiri').append('<div class="form-group row col-md-12 col-sm-12 col-xs-12"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[i].nama_field_spek +'</div><div class="col-md-7 col-sm-7 col-xs-7"><input type="hidden" name="id_field~' + idx + '" value="' + list_field[i].id_field + '" /><input type="text" name="isian_field~' + idx + '" class="form-control" placeholder="Masukkan '+ list_field[i].nama_field_spek +'" value=""></div></div>');
              index_spesifikasi = index_spesifikasi + idx + '~';
            }

            var length_list_kanan = length_list_field - length_list_kiri;
            for (var j = length_list_kiri; j < length_list_field; j++)
            {
              var idx = j + 1;
              $('#div-spesifikasi-field-kanan').append('<div class="form-group row col-md-12 col-sm-12 col-xs-12"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[j].nama_field_spek +'</div><div class="col-md-7 col-sm-7 col-xs-7"><input type="hidden" name="id_field~' + idx + '" value="' + list_field[j].id_field + '" /><input type="text" name="isian_field~' + idx + '" class="form-control" placeholder="Masukkan '+ list_field[j].nama_field_spek +'"></div></div>');
              index_spesifikasi = index_spesifikasi + idx + '~';
            }
            $('#index-spesifikasi').val(index_spesifikasi);
          }
          else {
             $('#div-spesifikasi-field-kiri').empty();
             $('#div-spesifikasi-field-kanan').empty();
          }
         }
      });
    } else {
       $('#div-spesifikasi-field-kiri').empty();
       $('#div-spesifikasi-field-kanan').empty();
    }
  });

  $(document).on("click", ".btn-tambah-dokumen", function (e) {
    e.preventDefault();
    var current = $("#last-dokumen-id").val();
    var next = parseInt(current) + 1;
    $('#dokumen-aset').append('<div id="dokumen-' + (next) + '" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="form-group row"><div class="col-md-3 col-sm-3 col-xs-3"><div class="row"><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-sm btn-link btn-danger" id="btn-hapus-dokumen-' + next + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div><div class="col-md-10 col-sm-10 col-xs-10"><input type="text" class="form-control" placeholder="Nama dokumen" name="nama_dokumentasi~' + (next) + '"> </div></div></div><div class="col-md-5 col-sm-5 col-xs-5"><div class="input-group"><input type="text" class="form-control" id="url-dokumen-aset-tambah-' + (next) + '" readonly="" style="background-color: white;" placeholder="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB"><div class="input-group-append"><div class="btn btn-primary btn-sm"><span><i class="fa fa-upload"></i></span><input type="file" id="file-dokumen-aset-tambah-' + (next) + '" rel="tooltip" data-original-title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." class="upload" name="id_dokumentasi~' + (next) + '" /></div></div></div></div><div class="col-md-4 col-sm-4 col-xs-4"><div class="input-group"><input type="date" name="tanggal_dokumentasi~' + (next) + '"class="form-control" placeholder="Tanggal Dokumentasi" rel="tooltip" title="Tanggal Dokumentasi" data-original-title="Tanggal Dokumentasi"><div class="input-group-append"><div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div></div></div></div></div><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><input type="text" class="form-control" placeholder="Keterangan dokumen" name="keterangan_dokumentasi~' + (next) + '"></div></div><br><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><div class="radio"><label class="radio-inline">Apakah ada dokumen secara fisik?</label>&emsp;<br><label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="Y"> Ya </label>&emsp;<label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="N"> Tidak </label></div></div><div id="form-penyimpanan-dokumentasi-' + (next) + '" class="col-md-8 col-sm-8 col-xs-8">Disimpan di<div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~lokasi~' + (next) + '" id="tambah-lokasi-dropdown-' + (next) + '"><option value=""  selected="true">Pilih Lokasi</option>@foreach ($list_lokasi as $lokasi)<option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>@endforeach</select></div><div class="col-md-4 col-sm-4  col-xs-4"><select class="form-control" name="dokumentasi~kontainer~' + (next) + '" id="tambah-kontainer-dropdown-' + (next) + '"><option value="" selected="true">Pilih kontainer...</option></select></div><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~posisi_kontainer~' + (next) + '" id="tambah-posisi-kontainer-dropdown-' + (next) + '"><option value="" selected>Pilih posisi kontainer...</option></select></div></div></div></div></div></div></div>');
    var count_dokumen = $("#count-dokumen").val();
    $("#count-dokumen").val(parseInt(count_dokumen)+1);
    $("#last-dokumen-id").val(parseInt(next));
    $("#form-penyimpanan-dokumentasi-" + next).hide();

    var current_index_dokumen = $("#index-dokumen").val();
    $("#index-dokumen").val(current_index_dokumen + next + "~");                  
  });

  $(document).on("click", "[id^=btn-hapus-dokumen-]", function (e) {
    var targetId = e.currentTarget.id.split('-').pop();
    $('#dokumen-' + targetId).remove();
    var current = $("#count-dokumen").val();
    var next = parseInt(current) - 1;
    $("#count-dokumen").val(next);

    var current_index_dokumen = $("#index-dokumen").val();
    var new_index_dokumen = current_index_dokumen.replace(targetId + '~', '');
    $("#index-dokumen").val(new_index_dokumen);
  });

  $('[id^=form-penyimpanan-dokumentasi-]').hide();
  $(document).on('change', '[class^="dokumentasi-fisik-"]', function (e) {
      var targetClass = $(e.currentTarget).attr('class').split(' ').pop();
      var targetClassId = targetClass.split('-').pop();
      e.preventDefault();
      if (this.value == "Y") { //check value if it is domicilio
          $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).show(750); //than show
      } else {
          $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).hide(750); //else hide
          $('#tambah-lokasi-dropdown-' + targetClassId).val('');
          $('#tambah-kontainer-dropdown-' + targetClassId).val('');
          $('#tambah-posisi-kontainer-dropdown-' + targetClassId).val('');
      }
  });

  $(document).on("change", "[id^=tambah-lokasi-dropdown-]", function (e) {
    e.preventDefault();
    var targetId = e.currentTarget.id.split('-').pop();
    var lokasi = e.target.value;

    if(lokasi){
      $.ajax({
         type:"GET",
         url:"{{url('api/get-kontainer-list')}}?lokasi="+lokasi,
         success:function(res){
          if(res){

              $('#tambah-kontainer-dropdown-' + targetId).empty();
              $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
              $('#tambah-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
              $('#tambah-posisi-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

              $.each(res,function(index,Obj){
                  $("#tambah-kontainer-dropdown-" + targetId).append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
              });

          }else{
             $("#tambah-kontainer-dropdown-" + targetId).empty();
              $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
          }
         }
      });
    }
    else{
      $("#tambah-kontainer-dropdown-" + targetId).empty();
      $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
    }

  });

  $(document).on("change", "[id^=tambah-kontainer-dropdown-]", function (e) {
    e.preventDefault();
    var targetId = e.currentTarget.id.split('-').pop();
    var kontainer = e.target.value;

    if(kontainer){
      $.ajax({
         type:"GET",
         url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
         success:function(res){
          if(res){

              $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
              $('#tambah-posisi-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

              $.each(res,function(index,Obj){
                  $("#tambah-posisi-kontainer-dropdown-" + targetId).append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
              });

          }else{
             $("#tambah-posisi-kontainer-dropdown-" + targetId).empty();
          }
         }
      });
    }
    else{
      $("#tambah-posisi-kontainer-dropdown-" + targetId).empty();
    }
  });

  $(document).on("change", "[id^=file-dokumen-aset-tambah-]", function (e) {
    var targetId = e.currentTarget.id.split('-').pop();
    $("#url-dokumen-aset-tambah-" + targetId).val(this.value.replace(/C:\\fakepath\\/i, ''));
  });

  $(document).on("change", "[id^=file-gambar-aset-tambah-]", function (e) {
    var gambar = this;
    var targetId = e.currentTarget.id.split('-').pop();
    $("#url-gambar-aset-tambah-" + targetId).val(this.value.replace(/C:\\fakepath\\/i, ''));
    if (gambar.files && gambar.files[0]) {
      var reader = new FileReader();
      reader.onload = function (event) {
        $('#preview-' + targetId).attr('src', event.target.result);
      };
      reader.readAsDataURL(gambar.files[0]);
    }
  });

  $(document).on("click", ".btn-tambah-gambar", function (e) {
    e.preventDefault();
    var current = $("#last-gambar-id").val();
    var next = parseInt(current) + 1;

    $('#gambar-aset').append('<div id="gambar-' + (next) +'" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-7 col-sm-7 col-xs-7"><div class="form-group row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"><button type="button" class="btn btn-sm btn-link btn-danger" id="btn-hapus-gambar-' + (next) + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div><div class="col-md-11 col-sm-11 col-xs-11"><input type="text" class="form-control" name="nama_gambar~' + (next) +'" placeholder="Nama Gambar"></div></div><div class="form-group row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><div class="input-group"><input type="text" class="form-control" id="url-gambar-aset-tambah-' + (next) + '" readonly="" style="background-color: white;" placeholder="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB"><div class="input-group-append"><div class="btn btn-primary btn-sm"><span><i class="fa fa-upload"></i></span><input type="file" id="file-gambar-aset-tambah-' + (next) + '" rel="tooltip" data-original-title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." class="upload" name="id_gambar~' + (next) + '" /></div></div></div></div></div><div class="form-group row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><textarea class="form-control" name="keterangan_gambar~' + (next) + '" rows="2" placeholder="Keterangan Gambar"></textarea></div></div><div class="form-group row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><div class="radio"><label class="radio-inline"><input type="radio" id="gambar-default-' + (next) + '" value="' + (next) + '"> Jadikan gambar utama</label></div></div></div></div><div class="row col-md-5 col-sm-5 col-xs-5"><div class="preview-aset"><img id="preview-' + (next) + '" src="#" alt="Gambar ' + (next) + '" /></div></div></div></div>');

    var current_index_gambar = $("#index-gambar").val();
    $("#index-gambar").val(current_index_gambar + next + "~");

    var index_gambar = $('#index-gambar').val().split('~');
    if (index_gambar.length != 0 && index_gambar.length <= 2)
    {
      $('#gambar-utama').val(index_gambar[0]);
      $('#gambar-default-' + index_gambar[0]).prop('checked', true);
    }

    var count_gambar = $("#count-gambar").val();
    $("#count-gambar").val(parseInt(count_gambar)+1);
    $("#last-gambar-id").val(parseInt(next));                 
  });

  $(document).on("click", "[id^=btn-hapus-gambar-]", function (e) {
    var targetId = e.currentTarget.id.split('-').pop();
    $('#gambar-' + targetId).remove();
    var current = $("#count-gambar").val();
    var next = parseInt(current) - 1;
    $("#count-gambar").val(next);

    var current_index_gambar = $("#index-gambar").val();
    var new_index_gambar = current_index_gambar.replace(targetId + '~', '');
    $("#index-gambar").val(new_index_gambar);

    var gambar_utama = $('#gambar-utama').val();
    var index_gambar = $('#index-gambar').val().split('~');
    
    if (targetId == gambar_utama)
    {
      $('#gambar-utama').val(index_gambar[0]);
      $('#gambar-default-' + index_gambar[0]).prop('checked', true);
    }
    if (index_gambar.length != 0 && index_gambar.length <= 2)
    {
      $('#gambar-utama').val(index_gambar[0]);
      $('#gambar-default-' + index_gambar[0]).prop('checked', true);
    }
  });

  $(document).on('change', '[id^=gambar-default-]', function (e) {
      e.preventDefault();
      var targetId = e.currentTarget.id.split('-').pop();

      $('#gambar-utama').val(targetId);
      
      var index_gambar = $('#index-gambar').val().split('~');
      for (var i = 0; i < index_gambar.length; i++)
      {
        $('#gambar-default-' + index_gambar[i]).prop('checked', false);
      }
      $('#gambar-default-' + targetId).prop('checked', true);
  });
});
</script>
@endsection