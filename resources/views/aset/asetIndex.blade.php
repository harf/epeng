<script src="/js/core/jquery.min.js" type="text/javascript"></script>

<script type="text/javascript">

  $(document).ready(function(){
    $("select").select2();
  });
  $(document).ready(function(){
    $('.form-penyimpanan').hide();
    $(".dokumentasi-fisik").change(function () { //use change event
        if (this.value == "yadisimpan") { //check value if it is domicilio
            $('.form-penyimpanan').stop(true,true).show(750); //than show
        } else {
            $('.form-penyimpanan').stop(true,true).hide(750); //else hide
        }
    });

    $('.form-penyimpanan-sourcecode').hide();
    $(".sourcecode-fisik").change(function () { //use change event
        if (this.value == "yadisimpan") { //check value if it is domicilio
            $('.form-penyimpanan-sourcecode').stop(true,true).show(750); //than show
        } else {
            $('.form-penyimpanan-sourcecode').stop(true,true).hide(750); //else hide
        }
    });

    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
  });

  $(document).ready(function(){
    var showChar = 105;
    var ellipsestext = "...";
    var moretext = "lebih rinci";
    var lesstext = "lebih ringkas";
    $('.more').each(function() {
      var content = $(this).html();
      var textcontent = $(this).text();

      if (textcontent.length > showChar) {

        var c = textcontent.substr(0, showChar);

        var html = '<span class="container" style="padding-left: 0px;"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';
        $(this).html(html);
        $(this).after('<a href="" class="morelink badge badge-dark" align="right">' + moretext + '</a>');
      }

    });

    $(".morelink").click(function() {
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
        $(this).prev().children('.morecontent').fadeToggle(500, function(){
          $(this).prev().fadeToggle(500);
        });
         
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
        $(this).prev().children('.container').fadeToggle(500, function(){
          $(this).next().fadeToggle(500);
        });
      }
      return false;
    });

    $("a.showmore").click(function(e) {
      e.preventDefault();
      $(this).parent().next().slideToggle("fast");
      $(this).css("display", "none");
    });
  });

  $(document).ready(function(){
    $(document).on("click", ".open-modalHapusAset", function () {
      var hapus_aset_id_aset = $(this).data('aset_id_aset');
      var hapus_aset_nama_merek = $(this).data('aset_nama_merek');
      var hapus_aset_tipe_aset = $(this).data('aset_tipe_aset');
      
      $(".modal-footer #hapus_aset_id_aset").val( hapus_aset_id_aset );
      $(".modal-body #hapus_aset").html(hapus_aset_nama_merek + ' ' + hapus_aset_tipe_aset);
    });
  }); 

  $(document).ready(function() {
    $(document).on("change", "#id-jenis-aset", function(e) {
      e.preventDefault();
      var targetId = e.target.value;

      var keyword = $("#keyword-search").val();
      var is_searched = 0;
      if (keyword == '') {
        keyword = "null";
        is_searched = 0;
      } else {
        is_searched = 1;
      }

      var page_size = $("#page-size").val();
      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      if (targetId != 0) window.location.href = baseUrl + "?is_filtered=1&is_searched=" + is_searched + "&filter=" + targetId + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=1";
      else window.location.href = baseUrl;
    });

    $(document).on("keypress", "#keyword-search", function(e) {
      if(e.which === 13){
        e.preventDefault();
        var keyword = $("#keyword-search").val();
        var filter = $("#id-jenis-aset").val();

        var is_filtered = 0;
        if (filter == 0) {
          is_filtered = 0;
          jenis_aset = null;
        } else {
          is_filtered = 1;
          jenis_aset = filter;
        }

        var page_size = $("#page-size").val();
        var page_number = $("#page-number").val();

        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        
        if (keyword.replace(" ", "") != '') window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=1&filter=" + filter + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=" + page_number;
        else if (is_filtered != 0) window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=0&filter=" + filter + "&keyword=null" + "&page_size=" + page_size + "&page_number=1";
        else window.location.href = baseUrl;
      }
    });

    $(document).on("change", "#page-size", function(e) {
      e.preventDefault();
      var targetId = e.target.value;
      var filter = $("#id-jenis-aset").val();
      var keyword = $("#keyword-search").val();

      var is_filtered = 0;
      if (filter == 0) is_filtered = 0;
      else is_filtered = 1;

      var is_searched = 0;
      if (keyword == '') {
        keyword = "null";
        is_searched = 0;
      } else {
        is_searched = 1;
      }

      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=" + is_searched + "&filter=" + filter + "&keyword=" + keyword + "&page_size=" + targetId + "&page_number=1";
    });
  });
</script>

@extends('layouts.master')
@section('content')
  @if($message_text)
    <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" @if($message_id == 1) style="background: rgba(76, 175, 80, 0.6);" @else style="background: rgba(255, 119, 119, 0.6);" @endif>
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><small>PESAN SISTEM</small></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-2 col-sm-2 col-xs-2" style="text-align: center;">
                <center>
                  @if($message_id == 1)
                    <h1 class="text-success"><i class="fa fa-check-circle"></i></h1>
                  @else
                    <h1 class="text-danger"><i class="fa fa-times-circle"></i></h1>
                  @endif
                </center>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <blockquote>
                  @if($message_id == 1)
                    <h4 class="text-success"><strong>Data {{ $message_source }} berhasil {{ $message_crud }}.</strong></h4>
                  @else
                    <h4 class="text-danger"><strong>Data {{ $message_source }} gagal {{ $message_crud }}.</strong></h4>
                    <footer><small>Error: {{ $message_text }}</small></footer>
                  @endif
                </blockquote>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">Tutup</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(window).on('load',function(){
        $('#modalMessage').modal('show');
      });
    </script>
  @endif
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6">
              <div class="row">
                <div class="col-md-4">
                  <h3 class="card-title text-primary">
                    <small>
                      <strong>
                        <i class="fa fa-laptop"></i>&emsp;Aset SIMKIM
                      </strong>
                    </small>
                  </h3>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <a href="{{ route('asetAdd') }}" class="card-title btn btn-primary pull-right">Tambah Aset</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-10">
              <i><small>Halaman {{ $page_number }}/{{ceil($count_list_aset_index/$page_size)}} - menampilkan @if($is_filtered == 0 && $is_searched == 0) seluruh Aset @else Aset @if($is_filtered != 0) <u>{{ $filter_name }}</u> @endif @if($is_searched != 0) @if($is_filtered != 0) > @endif "<u>{{ $keyword }}</u>" @endif @endif sebanyak @if($list_aset_index) <strong>{{ count($list_aset_index) }}/{{ ($count_list_aset_index)?$count_list_aset_index:0 }} data</strong> @else <strong>0 data</strong>@endif.</small></i>
            </div>
          </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-filter"></i>&emsp;</span>
                </div>
                <select class="form-control input-sm" name="id_jenis_aset" id="id-jenis-aset" rel="tooltip" title="Filter data berdasarkan Jenis Aset SIMKIM" @if($is_filtered != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" @endif>
                  <option value="0"  selected="true">Semua Jenis Aset</option>
                  @if($list_jenis_aset)
                    @foreach ($list_jenis_aset as $jenis_aset)
                      @if($jenis_aset->id_jenis_aset == $filter)
                        <option value="{{ $jenis_aset->id_jenis_aset }}" selected="true">
                          {{ $jenis_aset->nama_jenis_aset }}
                        </option>
                      @else
                        <option value="{{ $jenis_aset->id_jenis_aset }}">
                          {{ $jenis_aset->nama_jenis_aset }}
                        </option>
                      @endif
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-search"></i>&emsp;</span>
                </div>
                <input type="text" class="form-control" id="keyword-search" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter untuk mencari data" rel="tooltip" title="Cari data Aset SIMKIM" @if($is_searched != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" value="{{ $keyword }}" @endif>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-eye"></i>&emsp;</span>
                </div>
                <input type="hidden" id="page-number" value="{{ $page_number }}">
                <select class="form-control input-sm" name="page_size" id="page-size" rel="tooltip" title="Tampilkan banyaknya jumlah data dalam satu halaman">
                  <option value="15"  selected="true">15</option>
                  <option value="30" @if($page_size == 30) selected="true" @endif>30</option>
                  <option value="60" @if($page_size == 60) selected="true" @endif>60</option>
                  <option value="120" @if($page_size == 120) selected="true" @endif>120</option>
                  <option value="240" @if($page_size == 240) selected="true" @endif>240</option>
                  <option value="480" @if($page_size == 480) selected="true" @endif>240</option>
                  <option value="960" @if($page_size == 960) selected="true" @endif>960</option>
                </select>
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm">&emsp;data</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <br>
  <div class="row">
    @if($list_aset_index)
      @foreach($list_aset_index as $aset => $value)
      <div class="col-lg-4 col-md-4 col-sm-4" style="margin-bottom: 40px;">
        <div class="card card-stats" @if($message_data == $value->id_aset) @if($message_crud == 'ditambahkan') style="background-color: #fff7a0;" @endif @endif>
          <div class="card-header card-header-icon">
            <div class="card-icon-3">
              @if($message_data == $value->id_aset)
                @if($message_crud == 'ditambahkan')
                  <span class="notify-badge">BARU</span>
                @endif
              @endif
              <img src="{{ str_replace('public', '/storage', $value->link_gambar) }}" alt="{{ $value->nama_gambar }}">
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center" style="text-align: center; padding-top: 10px;">
              <h4 class="card-title text-primary"><span class="badge badge-primary" style="background-color: #2349a0; color: #fff763; text-transform: uppercase;">{{ $value->nama_jenis_aset }}</span> - {{ $value->nama_merek }} {{ $value->tipe_aset }}</h4>
            </div>
          </div>
          <div class="card-footer">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="stats">
                <div class="stats">
                  <i>terakhir diubah pada <br> {{ strftime( "%d %B %Y  %H:%M:%S", strtotime(($value->updated_at)?$value->updated_at:$value->created_at)) }}</i>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">           
              <button type="button" rel="tooltip" title="Hapus Aset" class="open-modalHapusAset btn btn-danger btn-link btn-sm pull-right" data-toggle="modal" data-target=".modalHapusAset" data-backdrop="static" data-keyboard="false" data-aset_id_aset="{{ $value->id_aset}}" data-aset_nama_merek="{{ $value->nama_merek}}" data-aset_tipe_aset="{{ $value->tipe_aset}}">
                <i class="fa fa-trash"></i>
              </button>
              <button type="button" rel="tooltip" title="Ubah Aset" class="btn btn-info btn-link btn-sm pull-right" onclick="window.open('aset/{{ $value->id_aset }}/ubah','_blank');">
                <i class="fa fa-gear"></i>
              </button>            
              <button type="button" rel="tooltip" title="Lihat Rincian Aset" class="btn btn-success btn-link btn-sm pull-right" onclick="window.open('aset/{{ $value->id_aset }}/detail','_blank');">
                <i class="fa fa-eye"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
      @endforeach
    @else
      <div class="col-lg-12 col-md-12 col-sm-12"<center><i>Tidak ada data.</i></center></div>
    @endif
  </div>
  <br>
  <hr>
  <div class="row">
    <div class="pull-right">
      <nav aria-label="...">
        <ul class="pagination">
          @if($page_number == 1)
            <li class="page-item disabled">
              <span class="page-link">Sebelumnya</span>
            </li>
          @else
            <li class="page-item">
              <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter={{ $filter }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number-1 }}">Sebelumnya</a>
            </li>  
          @endif
          @for($i = 0; $i < $count_list_aset_index/$page_size; $i++)
            <li class="page-item @if($page_number == $i+1) active @endif">
              @if($page_number == $i+1)
                <span class="page-link">
                  {{ $i+1 }}
                  <span class="sr-only">(current)</span>
                </span>
              @else
                <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter={{ $filter }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $i+1 }}">{{ $i+1 }}</a>
              @endif
            </li>
          @endfor
          @if($page_number == ceil($count_list_aset_index/$page_size))
            <li class="page-item disabled">
              <span class="page-link">Selanjutnya</span>
            </li>
          @else
            <li class="page-item">
              <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter={{ $filter }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number+1 }}">Selanjutnya</a>
            </li> 
          @endif
        </ul>
      </nav>
    </div>
  </div>

<!-- POPUP HAPUS ASET -->
<div class="modal fade modalHapusAset" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS ASET</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus aset <i class="text-primary"><span id="hapus_aset"></span></i>?
      </div>
      <div class="modal-footer">
        <form action="/asetDelete" method="post" enctype="multipart/form-data">                                          
            {{ csrf_field() }}  
            <input type="hidden" name="id_aset" id="hapus_aset_id_aset"/>
            <input type="hidden" name="_method" value="POST">
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
            <button type="submit" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection