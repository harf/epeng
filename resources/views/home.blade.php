@extends('layouts.master')

@section('content')
<?php
    $auth2 = App\User::getUser();
?>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <h3 class="text-primary">Hai, {{ $auth2->name }}!</h3>
                    <h5><small>Login sebagai <i>{{ $auth2->role }}</i></small></h5>
                    <h4>Selamat Datang di Aplikasi e-Pengelolaan BMN (<strong class="text-primary">e-Peng</strong>).</h4>
                </div>
            </div>
            <!-- 
            <div class="card">
                <div class="card-body">
                    <div class="row col-lg-12 col-md-12 col-sm-12">
                        <?php 
                            if ($auth2->role == 'Timtik' || $auth2->role == 'Admin') {
                        ?>
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('aplikasi') }}">
                                <center>
                                    <h2><i class="fa fa-globe"></i></h2>
                                    <h5>Dokumentasi Aplikasi SIMKIM</h5>
                                </center>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                        </div>
                        <?php
                        }
                        if($auth2->role == 'PPK' || $auth2->role == 'Admin') {
                        ?>
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('aplikasi') }}">
                                <center>
                                    <h2><i class="fa fa-laptop"></i></h2>
                                    <h5>Dokumentasi Aset SIMKIM</h5>
                                </center>
                            </a>
                        </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row col-lg-12 col-md-12 col-sm-12">
                        <?php
                        if($auth2->role == 'PPK' || $auth2->role == 'Admin') {
                        ?>
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('pengadaan') }}">
                                <center>
                                    <h2><i class="fa fa-clipboard"></i></h2>
                                    <h5>Paket Pengadaan</h5>
                                </center>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                        </div>
                        <?php
                        }
                        if($auth2->role == 'PPK' || $auth2->role == 'Timtik' || $auth2->role == 'Admin') {
                        ?>
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('manajemenAset') }}">
                                <center>
                                    <h2><i class="fa fa-stack-overflow"></i></h2>
                                    <h5>Manajemen Aset</h5>
                                </center>
                            </a>
                        </div>
                        <?php
                        }
                        ?>
                        
                    </div>
                </div>
            </div>
            
            <!-- <div class="card">
                <div class="card-body">
                    <div class="row col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('permintaan') }}">
                                <center>
                                    <h2><i class="fa fa-clipboard"></i></h2>
                                    <h5> Permintaan</h5>
                                </center>
                            </a>
                        </div>
                    </div>
                </div>
            </div> -->

            <?php
            if($auth2->role == 'Timtik' || $auth2->role == 'Admin') {
            ?>
            <!-- <div class="card">
                <div class="card-body">
                    <div class="row col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('laporanAsetMasuk') }}">
                                <center>
                                    <h2><i class="fa fa-archive"></i></h2>
                                    <h5>Laporan Aset Masuk</h5>
                                </center>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2">
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('laporanAsetKeluar') }}">
                                <center>
                                    <h2><i class="fa fa-paper-plane"></i></h2>
                                    <h5>Laporan Aset Keluar</h5>
                                </center>
                            </a>
                        </div>
                    </div>
                    <br>
                    <div class="row col-lg-12 col-md-12 col-sm-12">
                        <div class="col-lg-5 col-md-5 col-sm-5" style="border: 1px solid #9f34af; border-radius: 10px; height: 125px; padding: 10px;">
                            <a href="{{ route('laporanPersebaranAset') }}">
                                <center>
                                    <h2><i class="fa fa-building"></i></h2>
                                    <h5>Laporan Persebaran Aset</h5>
                                </center>
                            </a>
                        </div>
                    </div>
                </div>
            </div> -->
            <?php
            }
            ?>
        </div>
    </div>
</div>
@endsection
