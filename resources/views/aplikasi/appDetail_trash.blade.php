<div class="form-group row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                <i>SubDomain</i> Aplikasi
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9 editData">
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="form-group row col-md-7 col-sm-7 col-xs-7">
                <div class="col-md-5 col-sm-5 col-xs-5">
                  Alamat IP Aplikasi
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7 editData">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group row col-md-5 col-sm-5 col-xs-5">
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <i>Port</i> Aplikasi
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7 editData">
                  <input type="text" class="form-control">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                <i>Database</i> Aplikasi
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9 editData">
                <input type="text" class="form-control">
              </div>
            </div>
            <div class="row">
              <div class="form-group row col-md-7 col-sm-7 col-xs-7">
                <div class="col-md-5 col-sm-5 col-xs-5">
                  Alamat IP <i>Database</i>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7 editData">
                  <input type="text" class="form-control">
                </div>
              </div>
              <div class="form-group row col-md-5 col-sm-5 col-xs-5">
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <i>Port Database</i>
                </div>
                <div class="col-md-7 col-sm-7 col-xs-7 editData">
                  <input type="text" class="form-control">
                </div>
              </div>
            </div>  