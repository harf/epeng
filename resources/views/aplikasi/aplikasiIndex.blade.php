<script src="/js/core/jquery.min.js" type="text/javascript"></script>
<meta name="csrf-token" content="<?php echo csrf_token() ?>">
<script type="text/javascript">
  $(document).on("click", ".open-modalUbahAplikasi", function () {
     var editid = $(this).data('id');
     var editname = $(this).data('name');
     var editsingkatan = $(this).data('singkatan');
     var editkodepengadaan = $(this).data('kodepengadaan');

     $(".modal-body #id_aplikasi").val( editid );
     $(".modal-body #nama_aplikasi").val( editname );
     $(".modal-body #singkatan_aplikasi").val( editsingkatan );
     $(".modal-body #kode_pengadaan").val( editkodepengadaan );

    document.getElementById("id_aplikasi").innerHTML = editid;
    document.getElementById("nama_aplikasi").innerHTML = editname;
    document.getElementById("singkatan_aplikasi").innerHTML = editsingkatan;
    document.getElementById("kode_pengadaan").innerHTML = editkodepengadaan;

});

$(document).on("click", ".open-modalHapusAplikasi", function () {
     var hapusid = $(this).data('id');
     var hapusname = $(this).data('name');
    
     $(".modal-footer #id").val( hapusid );
     $(".modal-footer #name").val( hapusname );

    document.getElementById("hapusname").innerHTML = hapusname;

});  

</script>
@extends('layouts.master')
@section('content')
  
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6">
              <h3 class="card-title text-primary">
                <small>
                  <strong>
                    <i class="fa fa-globe"></i>&emsp;Aplikasi SIMKIM
                  </strong>
                </small>
              </h3>
            </div>
            <div class="col-md-6">
              <button type="submit" class="card-title btn btn-primary pull-right" data-toggle="modal" data-target=".modalTambahAplikasi" data-backdrop="static" data-keyboard="false">Tambah Aplikasi</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  @if(session('noticemessage'))
      @if(session('flag') == '1')
          <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
      @else
          <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
      @endif
      <div class="panel-heading" align="center" style="{{ $style }}">
          <div class="form-group row" align="center">
              <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
          </div>
      </div> 
      <br />
  @endif 
  <br>
  <br>
  <div class="row">
@foreach($list_aplikasi as $key => $value)
    <div class="col-lg-4 col-md-6 col-sm-6">
      <div class="card card-stats" style="margin-bottom: 45px;">
        <div class="card-header card-header-icon">
          <div class="card-icon">
            <strong class="text">{{ strtoupper($value->singkatan_aplikasi) }}</strong>
          </div>
          <h4 class="card-title text-primary"><strong>{{ $value->nama_aplikasi}}</strong></h4>
          <a href="#" class="card-category badge badge-light"><u>link</u></a>
        </div>
        <div class="card-footer">
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="stats">
              <i>terakhir diubah pada <br> {{ strftime( "%d %B %Y  %H:%M:%S", strtotime(($value->updated_at)?$value->updated_at:$value->created_at)) }}</i>
            </div>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">           
            <button type="button" rel="tooltip" title="Hapus" class="open-modalHapusAplikasi btn btn-danger btn-link btn-sm pull-right" data-toggle="modal" data-target="#modalHapusAplikasi" data-backdrop="static" data-keyboard="false" data-id="{{$value->id_aplikasi}}" data-name="{{$value->nama_aplikasi}}" >
              <i class="fa fa-trash"></i>
            </button>

            <button type="button" rel="tooltip" title="Ubah" class="open-modalUbahAplikasi btn btn-info btn-link btn-sm pull-right" data-toggle="modal" data-target="#modalUbahAplikasi" data-backdrop="static" data-keyboard="false" data-id="{{$value->id_aplikasi}}" data-name="{{$value->nama_aplikasi}}"  data-singkatan="{{$value->singkatan_aplikasi}}"  data-kodepengadaan="{{$value->kode_pengadaan}}" >
              <i class="fa fa-gear"></i>
            </button>    

            
            <button type="button" rel="tooltip" title="Lihat" class="btn btn-success btn-link btn-sm pull-right" onclick="window.open('aplikasi/{{ $value->id_aplikasi }}','_blank');" data-backdrop="static" data-keyboard="false">
              <i class="fa fa-eye"></i>
            </button>
          </div>
        </div>
      </div>
    </div>
    @endforeach
  </div>
 

<!-- POPUP TAMBAH APLIKASI -->
<div class="modal fade modalTambahAplikasi" id="modalTambahAplikasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">TAMBAH APLIKASI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('aplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}
      <div class="modal-body">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="name" name="name" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Singkatan Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="alias" name="alias">
            </div>
          </div>

          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Jenis Pengembangan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <select class="form-control input-sm" name="id_jenisdev" id="id_jenisdev" required="required">
                  <option value=""  selected="true">Pilih Jenis Pengembangan</option>
                  @foreach ($list_jenis_pengembangan as $jenisdev)
                    <option value="{{ $jenisdev->id_jenis_pengembangan }}">{{ $jenisdev->nama_jenis_pengembangan }}</option>
                  @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Paket Pengadaan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <select class="form-control input-sm" name="kode_pengadaan" id="kode_pengadaan">
                  <option value=""  selected="true">Pilih Paket Pengadaan</option>
                  @foreach ($list_pengadaan as $pengadaan)
                    <option value="{{ $pengadaan->kode_pengadaan }}">{{ $pengadaan->nama_pengadaan }}</option>
                  @endforeach
              </select>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Tambah Aplikasi Baru</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- POPUP UBAH APLIKASI -->
<div class="modal fade modalUbahAplikasi" id="modalUbahAplikasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">UBAH APLIKASI</strong></small><br><i class="text-primary">SPRI</i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('aplikasiSave') }}" enctype="multipart/form-data">   
        {{ csrf_field() }}
      <div class="modal-body">     
        <input type="hidden" name="mode" value="1">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="id_aplikasi" name="id_aplikasi" readonly="readonly"  >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="nama_aplikasi" name="nama_aplikasi" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Singkatan Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="singkatan_aplikasi" name="singkatan_aplikasi" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Paket Pengadaan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              
              <select class="form-control input-sm" name="kode_pengadaan" id="kode_pengadaan">
                  <option value=""  selected="true">Pilih Paket Pengadaan</option>
                  @foreach ($list_pengadaan as $pengadaan)
                    <option value="{{ $pengadaan->kode_pengadaan }}">{{ $pengadaan->nama_pengadaan }}</option>
                  @endforeach
              </select>

            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Simpan Perubahan Aplikasi SIMKIM</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- POPUP HAPUS APLIKASI -->
<div class="modal fade modalHapusAplikasi" id="modalHapusAplikasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS APLIKASI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus aplikasi <span id="hapusname"></span> ?       
      </div>
      <div class="modal-footer">
         <form action="/aplikasiDelete" method="post" enctype="multipart/form-data">                                          
            {{ csrf_field() }}  
            <input type="hidden" name="id" id="id" />
            <input type="hidden" name="name" id="name" />
            <input type="hidden" name="_method" value="POST">
            
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
            <button type="submit" class="btn btn-success" id="submit">
                <i class="fa fa-trash"></i>&emsp;Ya, Hapus
            </button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection