<script src="/js/core/jquery.min.js" type="text/javascript"></script>

<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>

@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/aplikasi">Aplikasi</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Detail</u></li>
  </ol>
</nav>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card card-profile">
        <div class="card-header card-header-primary">
            <h3 class="card-title"><strong>{{ $aplikasi->nama_aplikasi}}</strong></h3>
            <?php 
                $paket = App\Models\Pengadaan::where('kode_pengadaan',$aplikasi->kode_pengadaan)->first();     
                $jenis_dev = App\Models\Ms_Jenis_Pengembangan::where('id_jenis_pengembangan',$aplikasi->id_jenis_pengembangan)->first();          
            ?>
            <p class="card-category card-title"><i>{{ $paket?$paket->nama_pengadaan:'' }}</i></p>
        </div>
        @if(session('noticemessage'))
            @if(session('flag') == '1')
                <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
            @else
                <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
            @endif
            <div class="panel-heading" align="center" style="{{ $style }}">
                <div class="form-group row" align="center">
                    <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                </div>
            </div> 
            <br />
        @endif 

      <div class="card-body">
        <div class="row">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="text-align: left;">
            <div class="row col-md-12">
              <h5><strong class="text-align"><u>DESKRIPSI APLIKASI {{ $aplikasi->singkatan_aplikasi }}</u></strong>
                <button type="button" rel="tooltip" title="Ubah Deskripsi Aplikasi" class="btn btn-primary btn-link btn-sm" data-toggle="modal" data-target=".modalUbahDeskripsiAplikasi" data-backdrop="static" data-keyboard="false"><i class="fa fa-gear"></i></button></h5>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4 text-gray">Jenis Pengembangan</div>
              <div class="col-md-8 col-sm-8 col-xs-8 text-primary"><strong>{{ $jenis_dev->nama_jenis_pengembangan}}</strong></div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4 text-gray">Bahasa Pemrograman</div>
              <div class="col-md-8 col-sm-8 col-xs-8 text-primary"><strong>{{ $aplikasi->bahasa_pemrograman}}</strong></div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4 text-gray">Framework Pemrograman</div>
              <div class="col-md-8 col-sm-8 col-xs-8 text-primary"><strong>{{ $aplikasi->framework_pemrograman}}</strong></div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4 text-gray">Sistem Operasi</div>
              <div class="col-md-8 col-sm-8 col-xs-8 text-primary"><strong>{{ $aplikasi->sistem_operasi}}</strong></div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4 text-gray">Tanggal <i>Development</i></div>
              <div class="col-md-8 col-sm-8 col-xs-8 text-primary"><strong>{{ strftime( "%d %B %Y", strtotime($aplikasi->tanggal_dev)) }}</strong></div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4 text-gray">Tanggal <i>Production</i></div>
              <div class="col-md-8 col-sm-8 col-xs-8 text-primary"><strong>{{ strftime( "%d %B %Y", strtotime($aplikasi->tanggal_live)) }}</strong></div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4 text-gray">Deskripsi</div>
              <div class="col-md-8 col-sm-8 col-xs-8 text-primary"><strong>{{ $aplikasi->deskripsi_aplikasi}}</strong></div>
            </div>            
          </div>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row col-md-12">
              <h5><strong class="text-align"><u>ALAMAT APLIKASI {{ $aplikasi->singkatan_aplikasi }}</u></strong>
                <button type="button" rel="tooltip" title="Tambah alamat Aplikasi" class="open-modalTambahAlamat btn btn-primary btn-link btn-sm" data-toggle="modal" data-target=".modalTambahAlamat" 
                data-backdrop="static" data-keyboard="false" data-alamat_id_aplikasi="{{$aplikasi->id_aplikasi}}"  ><i class="fa fa-plus-circle "></i></button></h5>
            </div>
            <br>
            <div style="height: 400px; overflow-x: auto; overflow-y: auto;">
              <table class="table table-bordered table-responsive table-hover table-condensed">
                <colgroup>
                  <col style="width:5%;">
                  <col style="width:25%;">
                  <col style="width:25%;">
                  <col style="width:20%;">
                  <col style="width:10%;">
                  <col style="width:20%;">
                </colgroup>
                <thead class="text-primary">
                  <th>No</th>
                  <th>Nama Koneksi</th>
                  <th>Subdomain</th>
                  <th><i>IP Address</i></th>
                  <th><i>Port</i></th>
                  <th>Aksi</th>
                </thead>
                <tbody>
                    @if(count($alamat) != 0)
                      @foreach($alamat as $key => $value)
                      <tr>
                        <td>{{ $value->id_alamat }}</td>
                        <td>{{ $value->nama_alamat }}</td>
                        <td><a href="{{ $value->subdomain.':'.$value->port }}" target="_blank" ><u>{{ $value->subdomain }}</u></a></td>
                        <td><a href="{{ $value->ip_address.':'.$value->port }}" target="_blank" >{{ $value->ip_address }}</a></td>
                        <td>{{ $value->port }}</td>
                        <td>
                          <h4>
                            <small>
                              <a href="#" rel="tooltip" title="Keterangan Koneksi" class="open-modalKeteranganKoneksi text-info" data-toggle="modal" data-target=".modalKeteranganKoneksi" data-backdrop="static" data-keyboard="false" data-alamat_keterangan_alamat="{{$value->keterangan_alamat}}" data-alamat_nama_aplikasi="{{ $aplikasi->nama_aplikasi}}"  ><i class="fa fa-info-circle"></i></a>&emsp;
                              <a href="#" rel="tooltip" title="Ubah Koneksi" class="open-modalUbahKoneksi text-success" data-toggle="modal" data-target=".modalUbahKoneksi" data-backdrop="static" data-keyboard="false" data-alamat_nama_aplikasi="{{ $aplikasi->nama_aplikasi}}" 
                                data-alamat_id_aplikasi="{{ $aplikasi->id_aplikasi}}"
                                data-alamat_id_alamat="{{ $value->id_alamat}}"
                                data-alamat_nama_alamat="{{ $value->nama_alamat}}"
                                data-alamat_subdomain="{{ $value->subdomain}}"
                                data-alamat_ip_address="{{ $value->ip_address}}"
                                data-alamat_port="{{ $value->port}}"
                                data-alamat_utama="{{ $value->alamat_utama}}"
                                data-alamat_keterangan_alamat="{{$value->keterangan_alamat}}" 
                                ><i class="fa fa-pencil"></i></a>&emsp;
                              <a href="#" rel="tooltip" title="Hapus Koneksi" class="open-modalHapusKoneksi text-danger" data-toggle="modal" data-target=".modalHapusKoneksi" data-backdrop="static" data-keyboard="false" data-alamat_id_aplikasi="{{ $aplikasi->id_aplikasi}}"
                                data-alamat_id_alamat="{{ $value->id_alamat}}"
                                data-alamat_nama_alamat="{{ $value->nama_alamat}}"><i class="fa fa-trash"></i></a>
                            </small>
                          </h4>
                        </td>
                      </tr>
                      @endforeach
                    @else
                        <tr>
                            <td colspan="6"><i>Tidak ada data.</i></td>
                        </tr>
                    @endif
                </tbody>
              </table>
            </div>
            <!-- <hr>
                <div class="row col-md-12 primary" style="">
                  <div class="col-md-3">
                    <h5><strong class="text-align"><u>ADMINISTRATOR APLIKASI</u></strong></h5>
                  </div>
                  <div class="col-md-9">
                    <button type="button" rel="tooltip" title="Dibutuhkan verifikasi password akun untuk melihat daftar administrator aplikasi SPRI" class="btn btn-primary btn-md" data-toggle="modal" data-target=".modalVerifikasiLihatAdmin" data-backdrop="static" data-keyboard="false">
                      Lihat Daftar Administrator Aplikasi SPRI
                    </button>
                  </div>
                </div>
             -->    
          </div>
        </div>
        <hr>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="row col-md-12">
                <center>
                  <h5><strong class="text-align"><u>TIM PENGEMBANG APLIKASI {{ $aplikasi->singkatan_aplikasi }}</u></strong>
                    <button type="button" rel="tooltip" title="Tambah Pengembang" class="open-modalTambahPengembang btn btn-primary btn-link btn-sm" data-toggle="modal" data-target=".modalTambahPengembang" data-backdrop="static" data-keyboard="false" data-prog_id_aplikasi="{{$aplikasi->id_aplikasi}}"  ><i class="fa fa-plus-circle"></i></button></h5>
                </center>
              </div>
              <br>
              <div style="height: 500px; overflow-x: auto; overflow-y: auto;">
                <table class="table table-bordered table-responsive table-hover">
                  <colgroup>
                    <col style="width:5%;">
                    <col style="width:45%;">
                    <col style="width:35%;">
                    <col style="width:15%;">
                  </colgroup>
                  <thead class="text-primary">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Jabatan</th>
                    <th>Aksi</th>
                  </thead>
                  <tbody>
                    @if(count($pengembang) != 0)
                        @foreach($pengembang as $key => $value)
                        <tr>

                          <td>{{ $value->id_pengembang }}</td>
                          <td>{{ $value->nama_pengembang }}</td>
                          <td>{{ $value->jabatan_pengembang }}</td>
                          
                          <td>
                            <h4>
                              <small>
                                <a href="#" rel="tooltip" title="Rincian Pengembang" class="open-modalRincianPengembang text-info" data-toggle="modal" data-target=".modalRincianPengembang" data-backdrop="static" data-keyboard="false" data-prog_id_pengembang="{{ $value->id_pengembang}}"
                                  data-prog_nama_pengembang="{{ $value->nama_pengembang}}"
                                  data-prog_jabatan_pengembang="{{ $value->jabatan_pengembang}}"
                                  data-prog_nik_pengembang="{{ $value->nik_pengembang}}"
                                  data-prog_nohp_pengembang="{{ $value->nohp_pengembang}}"
                                  data-prog_email_pengembang="{{ $value->email_pengembang}}"
                                  data-prog_keterangan_pengembang="{{ $value->keterangan_pengembang}}"
                                  ><i class="fa fa-info-circle"></i></a>&emsp;
                                <a href="#" rel="tooltip" title="Ubah Pengembang" class="open-modalUbahPengembang text-success" data-toggle="modal" data-target=".modalUbahPengembang" data-backdrop="static" data-keyboard="false" data-prog_id_aplikasi="{{ $aplikasi->id_aplikasi}}" 
                                  data-prog_id_pengembang="{{ $value->id_pengembang}}"
                                  data-prog_nama_pengembang="{{ $value->nama_pengembang}}"
                                  data-prog_jabatan_pengembang="{{ $value->jabatan_pengembang}}"
                                  data-prog_nik_pengembang="{{ $value->nik_pengembang}}"
                                  data-prog_nohp_pengembang="{{ $value->nohp_pengembang}}"
                                  data-prog_email_pengembang="{{ $value->email_pengembang}}"
                                  data-prog_keterangan_pengembang="{{ $value->keterangan_pengembang}}"><i class="fa fa-pencil"></i></a>&emsp;
                                <a href="#" rel="tooltip" title="Hapus Pengembang" class="open-modalHapusPengembang text-danger" data-toggle="modal" data-target=".modalHapusPengembang" data-backdrop="static" data-keyboard="false" data-prog_id_aplikasi="{{ $aplikasi->id_aplikasi}}"
                                data-prog_id_pengembang="{{ $value->id_pengembang}}"
                                data-prog_nama_pengembang="{{ $value->nama_pengembang}}"><i class="fa fa-trash"></i></a>
                              </small>
                            </h4>
                          </td>
                        </tr>  
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4"><i>Tidak ada data.</i></td>
                        </tr>
                    @endif                  
                  </tbody>
                </table>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="row col-md-12">
                <center>
                  <h5><strong class="text-align"><u>VERSI APLIKASI {{ $aplikasi->singkatan_aplikasi }}</u></strong>
                    <button type="button" rel="tooltip" title="Tambah Versi Aplikasi" class="open-modalTambahVersi btn btn-primary btn-link btn-sm" data-toggle="modal" data-target=".modalTambahVersi" data-backdrop="static" data-keyboard="false" data-versi_id_aplikasi="{{$aplikasi->id_aplikasi}}"><i class="fa fa-plus-circle"></i></button></h5>
                </center>
              </div>
              <br>
              <div style="height: 500px; overflow-x: auto; overflow-y: auto;">
                <table class="table table-bordered table-responsive table-hover">
                  <colgroup>
                    <col style="width:10%;">
                    <col style="width:25%;">
                    <col style="width:40%;">
                    <col style="width:10%;">
                    <col style="width:15%;">
                  </colgroup>
                  <thead class="text-primary">
                    <th>Nomor Versi</th>
                    <th>Tanggal Versi</th>
                    <th>Rincian Versi</th>
                    <th>Source Code</th>
                    <th>Aksi</th>
                  </thead>
                  <tbody>
                    @if(count($versi) != 0)
                        @foreach($versi as $key => $value)
                        <tr>

                          <td>{{ $value->nomor_versi }}</td>                      
                          <td>{{ strftime( "%d %B %Y", strtotime($value->tanggal_versi)) }}</td>
                          <td>{{ $value->rincian_versi }}</td>
                          <td>
                            @if($value->id_sourcecode)
                            <a href="/downloadSourceCode/{{ $value->id_sourcecode }}" rel="tooltip" title="Unduh Source Code" class="text-danger"><u>Unduh</u></a></td>
                            @else
                            -
                            @endif
                          <td>
                            <h4>
                              <small>
                                <a href="#" rel="tooltip" title="Rincian Versi" class="open-modalRincianVersi text-info" data-toggle="modal" data-target=".modalRincianVersi" data-backdrop="static" data-keyboard="false" data-versi_nomor_versi="{{ $value->nomor_versi}}" 
                                  data-versi_tanggal_versi="{{ ($value->tanggal_versi)?$value->tanggal_versi:'-' }}"
                                  data-versi_rincian_versi="{{ ($value->rincian_versi)?$value->rincian_versi:'-' }}"
                                  data-versi_id_sourcecode="{{ ($value->id_sourcecode)?$value->id_sourcecode:'-' }}"
                                  data-versi_nama_sourcecode="{{ ($value->nama_sourcecode)?$value->nama_sourcecode:'-' }}"
                                  data-versi_link_sourcecode="{{ ($value->link_sourcecode)?$value->link_sourcecode:'-' }}"
                                  data-versi_keterangan_sourcecode="{{ ($value->keterangan_sourcecode)?$value->keterangan_sourcecode:'-' }}"
                                  data-versi_as_id_lokasi="{{ ($value->as_id_lokasi)?$value->as_id_lokasi:'-' }}"
                                  data-versi_as_nama_lokasi="{{ ($value->as_nama_lokasi)?$value->as_nama_lokasi:'-' }}"
                                  data-versi_as_id_kontainer="{{ ($value->as_id_kontainer)?$value->as_id_kontainer:'-' }}"
                                  data-versi_as_nama_kontainer="{{ ($value->as_nama_kontainer)?$value->as_nama_kontainer:'-' }}"
                                  data-versi_as_id_posisi_kontainer="{{ ($value->as_id_posisi_kontainer)?$value->as_id_posisi_kontainer:'-' }}"
                                  data-versi_as_label_posisi_kontainer="{{ ($value->as_label_posisi_kontainer)?$value->as_label_posisi_kontainer:'-' }}"
                                  data-versi_id_dokumentasi="{{ ($value->id_dokumentasi)?$value->id_dokumentasi:'-' }}"
                                  data-versi_nama_dokumentasi="{{ ($value->nama_dokumentasi)?$value->nama_dokumentasi:'-' }}"
                                  data-versi_tanggal_dokumentasi="{{ ($value->tanggal_dokumentasi)?$value->tanggal_dokumentasi:'-' }}"
                                  data-versi_keterangan_dokumentasi="{{ ($value->keterangan_dokumentasi)?$value->keterangan_dokumentasi:'-' }}"
                                  data-versi_link_dokumentasi="{{ ($value->link_dokumentasi)?$value->link_dokumentasi:'-' }}"
                                  data-versi_d_id_lokasi="{{ ($value->d_id_lokasi)?$value->d_id_lokasi:'-' }}"
                                  data-versi_d_nama_lokasi="{{ ($value->d_nama_lokasi)?$value->d_nama_lokasi:'-' }}"
                                  data-versi_d_id_kontainer="{{ ($value->d_id_kontainer)?$value->d_id_kontainer:'-' }}"
                                  data-versi_d_nama_kontainer="{{ ($value->d_nama_kontainer)?$value->d_nama_kontainer:'-' }}"
                                  data-versi_d_id_posisi_kontainer="{{ ($value->d_id_posisi_kontainer)?$value->d_id_posisi_kontainer:'-' }}"
                                  data-versi_d_label_posisi_kontainer="{{ ($value->d_label_posisi_kontainer)?$value->d_label_posisi_kontainer:'-' }}"><i class="fa fa-info-circle" ></i></a>&emsp;

                                <a href="#" rel="tooltip" title="Ubah Versi" class="open-modalUbahVersi text-success" data-toggle="modal" data-target=".modalUbahVersi" data-backdrop="static" data-keyboard="false"
                                  data-versi_id_aplikasi="{{ $aplikasi->id_aplikasi}}"
                                  data-versi_id_versi="{{ $value->id_versi}}" 
                                  data-versi_nomor_versi="{{ $value->nomor_versi}}"
                                  data-versi_tanggal_versi="{{ ($value->tanggal_versi)?$value->tanggal_versi:'-' }}"
                                  data-versi_rincian_versi="{{ ($value->rincian_versi)?$value->rincian_versi:'-' }}"
                                  data-versi_id_sourcecode="{{ ($value->id_sourcecode)?$value->id_sourcecode:'-' }}"
                                  data-versi_nama_sourcecode="{{ ($value->nama_sourcecode)?$value->nama_sourcecode:'-' }}"
                                  data-versi_link_sourcecode="{{ ($value->link_sourcecode)?$value->link_sourcecode:'-' }}"
                                  data-versi_keterangan_sourcecode="{{ ($value->keterangan_sourcecode)?$value->keterangan_sourcecode:'-' }}"
                                  data-versi_as_id_lokasi="{{ ($value->as_id_lokasi)?$value->as_id_lokasi:'-' }}"
                                  data-versi_as_nama_lokasi="{{ ($value->as_nama_lokasi)?$value->as_nama_lokasi:'-' }}"
                                  data-versi_as_id_kontainer="{{ ($value->as_id_kontainer)?$value->as_id_kontainer:'-' }}"
                                  data-versi_as_nama_kontainer="{{ ($value->as_nama_kontainer)?$value->as_nama_kontainer:'-' }}"
                                  data-versi_as_id_posisi_kontainer="{{ ($value->as_id_posisi_kontainer)?$value->as_id_posisi_kontainer:'-' }}"
                                  data-versi_as_label_posisi_kontainer="{{ ($value->as_label_posisi_kontainer)?$value->as_label_posisi_kontainer:'-' }}"
                                  data-versi_id_dokumentasi="{{ ($value->id_dokumentasi)?$value->id_dokumentasi:'-' }}"
                                  data-versi_nama_dokumentasi="{{ ($value->nama_dokumentasi)?$value->nama_dokumentasi:'-' }}"
                                  data-versi_tanggal_dokumentasi="{{ ($value->tanggal_dokumentasi)?$value->tanggal_dokumentasi:'-' }}"
                                  data-versi_keterangan_dokumentasi="{{ ($value->keterangan_dokumentasi)?$value->keterangan_dokumentasi:'-' }}"
                                  data-versi_link_dokumentasi="{{ ($value->link_dokumentasi)?$value->link_dokumentasi:'-' }}"
                                  data-versi_d_id_lokasi="{{ ($value->d_id_lokasi)?$value->d_id_lokasi:'-' }}"
                                  data-versi_d_nama_lokasi="{{ ($value->d_nama_lokasi)?$value->d_nama_lokasi:'-' }}"
                                  data-versi_d_id_kontainer="{{ ($value->d_id_kontainer)?$value->d_id_kontainer:'-' }}"
                                  data-versi_d_nama_kontainer="{{ ($value->d_nama_kontainer)?$value->d_nama_kontainer:'-' }}"
                                  data-versi_d_id_posisi_kontainer="{{ ($value->d_id_posisi_kontainer)?$value->d_id_posisi_kontainer:'-' }}"
                                  data-versi_d_label_posisi_kontainer="{{ ($value->d_label_posisi_kontainer)?$value->d_label_posisi_kontainer:'-' }}"
                                  ><i class="fa fa-pencil"></i></a>&emsp;
                                <a href="#" rel="tooltip" title="Hapus Versi" class="text-danger" data-toggle="modal" data-target=".modalHapusVersi" data-backdrop="static" data-keyboard="false"><i class="fa fa-trash"></i></a>
                              </small>
                            </h4>
                          </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5"><i>Tidak ada data.</i></td>
                        </tr>
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row col-md-12">
                <center>
                  <h5><strong class="text-align"><u><i>FILE </i>DOKUMENTASI APLIKASI {{ $aplikasi->singkatan_aplikasi }}</u></strong>
                    <button type="button" rel="tooltip" title="Tambah Dokumentasi Aplikasi" class="open-modalTambahDokumentasi btn btn-primary btn-link btn-sm" data-toggle="modal" data-target=".modalTambahDokumentasi" data-backdrop="static" data-keyboard="false" data-dokumentasi_id_aplikasi="{{$aplikasi->id_aplikasi}}"><i class="fa fa-plus-circle"></i></button></h5>
                </center>
              </div>
              <br>
              <div style="height: 500px; overflow-x: auto; overflow-y: auto;">
                <table class="table table-bordered table-responsive table-hover">
                  <colgroup>
                    <col style="width:3%;">
                    <col style="width:20%;">
                    <col style="width:7%;">
                    <col style="width:10%;">
                    <col style="width:25%;">
                    <col style="width:5%;">
                    <col style="width:23%;">
                    <col style="width:15%;">
                  </colgroup>
                  <thead class="text-primary">
                    <th>No</th>
                    <th>Nama Dokumentasi</th>
                    <th>Ukuran</th>
                    <th>Tanggal</th>
                    <th>Dokumentasi Fisik Disimpan Di</th>
                    <th><i>File</i></th>
                    <th>Keterangan Dokumentasi</th>
                    <th>Aksi</th>
                  </thead>
                  <tbody>
                    @if(count($dokumentasi) != 0)
                      @foreach($dokumentasi as $key => $value)
                        <tr>
                          <td>{{ ($key + 1) }}</td>
                          <td>{{ $value->nama_dokumentasi }}</td>
                          <td>{{ $value->ukuran_dokumentasi }}</td>
                          <td>{{ strftime( "%d %B %Y", strtotime($value->tanggal_dokumentasi)) }}</td>
                          <td>Disimpan di Dit. Sistik Lantai 2 >> Lemari Pak Cipto >> Sekat A4</td>
                          <td>
                            @if($value->id_dokumentasi)
                            <a href="/downloadDokumentasi/{{ $value->id_dokumentasi }}" rel="tooltip" title="Unduh Dokumentasi" class="text-danger"><u>Unduh</u></a></td>
                            @else
                            -
                            @endif
                          </td>
                          <td>{{ $value->keterangan_dokumentasi }}</td>
                          <td>
                            <h4>
                              <small>
                                <a href="#" rel="tooltip" title="Ubah Dokumentasi" class="text-success" data-toggle="modal" data-target=".modalUbahDokumentasi" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil"></i></a>&emsp;
                                <a href="#" rel="tooltip" title="Hapus Dokumentasi" class="text-danger" data-toggle="modal" data-target=".modalHapusDokumentasi" data-backdrop="static" data-keyboard="false"><i class="fa fa-trash"></i></a>
                              </small>
                            </h4>
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <tr>
                        <td colspan="8"><i>Tidak ada data.</i></td>
                      </tr>
                    @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="row col-md-12">
                <center>
                  <h5><strong class="text-align"><u><i>SCREENSHOT</i> APLIKASI {{ $aplikasi->singkatan_aplikasi }}</u></strong>
                    <button type="button" rel="tooltip" title="Tambah Screenshot Aplikasi" class="open-modalTambahScreenshot btn btn-primary btn-link btn-sm" data-toggle="modal" data-target=".modalTambahScreenshot" data-backdrop="static" data-keyboard="false" data-screenshot_id_aplikasi="{{ $aplikasi->id_aplikasi }}"><i class="fa fa-plus-circle"></i></button></h5>
                </center>
              </div>
              <br>
              <div class="row">
                @if(count($screenshot) != 0)
                  @foreach($screenshot as $key => $value)
                    <div class="col-md-3 screenshot-aplikasi-thumbnails-div" style="margin: 0px 0px 20px 0px;">
                      <a href="#" class="open-modalLihatScreenshot" rel="tooltip" title="Lihat Screenshot" data-toggle="modal" data-target=".modalLihatScreenshot" data-backdrop="static" data-keyboard="false" data-screenshot_id_aplikasi="{{ $aplikasi->id_aplikasi}}"
                      data-screenshot_id_gambar="{{ $value->id_gambar }}" 
                      data-screenshot_nama_gambar="{{ ($value->nama_gambar)?$value->nama_gambar:'-' }}"
                      data-screenshot_link_gambar="{{ ($value->link_gambar)?$value->link_gambar:'-' }}"
                      data-screenshot_keterangan_gambar="{{ ($value->keterangan_gambar)?$value->keterangan_gambar:'-' }}"
                      ><img src="{{ str_replace('public', '/storage', $value->link_gambar) }}"
                                  alt="Second slide" class="screenshot-aplikasi-thumbnails" style="border: 1px solid #e28eff; border-radius: 10px;"></a>
                    </div>
                  @endforeach
                @else
                  <i>Tidak ada data.</i>
                @endif
              </div>
              <br>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- POPUP UBAH DESKRIPSI APLIKASI -->
<div class="modal fade modalUbahDeskripsiAplikasi" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><strong class="text-align">UBAH DESKRIPSI APLIKASI {{ $aplikasi->id_aplikasi}}</strong></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('aplikasiSave') }}" enctype="multipart/form-data">        
        <input type="hidden" name="id_aplikasi" value="{{ $aplikasi->id_aplikasi }}">  
        <input type="hidden" name="mode" value="2">
        {{ csrf_field() }}
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Jenis Pengembangan
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            
            <select class="form-control input-sm" name="id_jenisdev" id="id_jenisdev" required="required">
              <option value=""  selected="true">Pilih Jenis Pengembangan</option>
              @foreach ($list_jenis_pengembangan as $jenisdev)
                <option value="{{ $jenisdev->id_jenis_pengembangan }}">{{ $jenisdev->nama_jenis_pengembangan }}</option>
              @endforeach
            </select>
              
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Bahasa Pemrograman
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <input type="text" class="form-control" id="bahasa_pemrograman" name="bahasa_pemrograman" value="{{ ($aplikasi->bahasa_pemrograman)?$aplikasi->bahasa_pemrograman:'' }}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <i>Framework</i> Pemrograman
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <input type="text" class="form-control" id="framework_pemrograman" name="framework_pemrograman" value="{{ ($aplikasi->framework_pemrograman)?$aplikasi->framework_pemrograman:'' }}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Sistem Operasi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <input type="text" class="form-control" id="sistem_operasi" name="sistem_operasi" value="{{ ($aplikasi->sistem_operasi)?$aplikasi->sistem_operasi:'' }}">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Tanggal <i>Development</i>
          </div>
          <?php 
            $tanggal_dev_yyyyMMdd = '';
            if($aplikasi->tanggal_dev){
                $tanggal_dev_yyyyMMdd=date('Y-m-d', strtotime($aplikasi->tanggal_dev));
            } 
          ?>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="input-group">
              <input type="date" class="form-control" name="tanggal_dev" id="tanggal_dev" value="{{ $tanggal_dev_yyyyMMdd }}" >
              <div class="input-group-append">
                <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Tanggal <i>Production</i>
          </div>
           <?php 
            $tanggal_live_yyyyMMdd = '';
            if($aplikasi->tanggal_live){
                $tanggal_live_yyyyMMdd=date('Y-m-d', strtotime($aplikasi->tanggal_live));
            } 
          ?>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="input-group">
              <input type="date" class="form-control" name="tanggal_live" id="tanggal_live" value="{{ $tanggal_live_yyyyMMdd }}" >
              <div class="input-group-append">
                <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Deskripsi Aplikasi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <textarea class="form-control" id="deskripsi_aplikasi" name="deskripsi_aplikasi" rows="3">{{ ($aplikasi->deskripsi_aplikasi)?$aplikasi->deskripsi_aplikasi:'' }}</textarea>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- POPUP TAMBAH KONEKSI -->
<div class="modal fade modalTambahAlamat" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">TAMBAH ALAMAT APLIKASI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('alamatAplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}
      <div class="modal-body">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="id_aplikasi" name="id_aplikasi" readonly="readonly"  >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama Koneksi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="name" name="name" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Subdomain</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="subdomain" name="subdomain">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>IP Address</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="ipaddress" name="ipaddress" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Port</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="port" name="port" >
            </div>
          </div>
          <!--div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Dipasang di
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <select class="form-control" id="exampleFormControlSelect1">
                <option selected>Pilih...</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
          </div-->
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Alamat Utama?
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <label class="radio-inline">
                  <input type="radio" id="alamat_utama_y" name="alamat_utama" value="Y" >Ya
                </label>
                <label class="radio-inline">
                  <input type="radio" id="alamat_utama_t" name="alamat_utama" value="N" >Tidak
                </label>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" id="keterangan_alamat" name="keterangan_alamat" rows="3"></textarea>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Tambah Alamat Baru</button>
      </div>
      </form>

    </div>
  </div>
</div>

<!-- POPUP UBAH KONEKSI -->
<div class="modal fade modalUbahKoneksi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">UBAH ALAMAT</strong></small><br><i class="text-primary"><span id="alamat_nama_aplikasi" name="alamat_nama_aplikasi"></span></i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form method="POST" action="{{ route('alamatAplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}
        <input type="hidden" name="mode" value="1">
      <div class="modal-body">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="edit_alamat_id_aplikasi" name="id_aplikasi" readonly="readonly"  >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Alamat
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="edit_alamat_id_alamat" name="id_alamat" readonly="readonly"  >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama Koneksi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="edit_alamat_nama_alamat" name="name" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Subdomain</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="edit_alamat_subdomain" name="subdomain">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>IP Address</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="edit_alamat_ip_address" name="ipaddress" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Port</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="edit_alamat_port" name="port" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Alamat Utama?
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
            <label class="radio-inline">
                  <input type="radio" id="edit_alamat_utama_y" name="alamat_utama" value="Y" >Ya
                </label>
                <label class="radio-inline">
                  <input type="radio" id="edit_alamat_utama_n" name="alamat_utama" value="N" >Tidak
                </label>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" id="edit_alamat_keterangan_alamat" name="keterangan_alamat" rows="3"></textarea>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- POPUP KETERANGAN KONEKSI -->
<div class="modal fade modalKeteranganKoneksi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">KETERANGAN ALAMAT</strong></small><br>
          <i class="text-primary"><span id="alamat_nama_aplikasi" name="alamat_nama_aplikasi"></span></i>
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <span id="alamat_keterangan_alamat" name="alamat_keterangan_alamat"></span>
        <br>
        <br>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP HAPUS KONEKSI -->
<div class="modal fade modalHapusKoneksi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS ALAMAT</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus alamat <i class="text-primary"><span id="hapus_alamat_nama_alamat"></span></i>?
      </div>
      <div class="modal-footer">
        <form action="/alamatAplikasiDelete" method="post" enctype="multipart/form-data">                                          
            {{ csrf_field() }}  
            <input type="hidden" name="id_aplikasi" id="hapus_alamat_id_aplikasi"  />
            <input type="hidden" name="id_alamat" id="hapus_alamat_id_alamat"  />
            <input type="hidden" name="mode" value="delete">
            <input type="hidden" name="_method" value="POST">

            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
            <button type="submit" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- POPUP VERIFIKASI PASSWORD LIHAT ADMIN -->
<div class="modal fade modalVerifikasiLihatAdmin" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <form action="/aplikasi/{{ 1 }}/administrator" method="post" target="_blank">
        <div class="modal-header">
          <h5 class="modal-title"><small><strong class="text-align">LIHAT DAFTAR ADMINISTRATOR APLIKASI SPRI</strong></small><br><i class="text-primary">VERIFIKASI AKUN</i></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                <i>Username</i>
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="text" class="form-control" name="username">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                <i>Password</i>
              </div>
              <div class="col-md-9 col-sm-9 col-xs-9">
                <input type="password" class="form-control" name="password">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                <i>Captcha</i>
              </div>
              <div class="col-md-4 col-sm-4 col-xs-4">
                Captcha goes here
              </div>
              <div class="col-md-5 col-sm-5 col-xs-5">
                <input type="text" class="form-control" name="captcha">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="POST">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;VERIFIKASI</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->

<!-- POPUP TAMBAH PENGEMBANG -->
<div class="modal fade modalTambahPengembang" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">TAMBAH PENGEMBANG APLIKASI </strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     <form method="POST" action="{{ route('pengembangAplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}
      <div class="modal-body">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="id_aplikasi" name="id_aplikasi" readonly="readonly"  >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama Pengembang
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="nama_pengembang"  name="nama_pengembang" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>NIK Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-">
              <input type="number" class="form-control" id="nik_pengembang" name="nik_pengembang" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Jabatan Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="jabatan_pengembang" name="jabatan_pengembang" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>No. HP Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="nohp_pengembang" name="nohp_pengembang" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Email Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="email" class="form-control" id="email_pengembang" name="email_pengembang" >
            </div>
          </div>
<!--           <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>SK Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_sk-pengembang-ubah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="skpengembang" id="dynamic-file_sk-pengembang-ubah" class="upload" />
                  </div>
                </div>
              </div>
              <input type="text" class="form-control" placeholder="Keterangan SK Pengembang">
            </div>
          </div>  -->
          <!-- <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Foto Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_foto-pengembang-ubah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="fotopengembang" id="dynamic-file_foto-pengembang-ubah" class="upload" />
                  </div>
                </div>
              </div>
              <input type="text" class="form-control" placeholder="Keterangan Foto Pengembang">
            </div>
          </div> -->
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" id="keterangan_pengembang" name="keterangan_pengembang" rows="3"></textarea>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Tambah Pengembang Baru</button>
      </div>
        </form>
    </div>
  </div>
</div>

<!-- POPUP UBAH PENGEMBANG -->
<div class="modal fade modalUbahPengembang" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">UBAH PENGEMBANG</strong></small><br></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('pengembangAplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}
       
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editprog_id_aplikasi" name="id_aplikasi" readonly="readonly"  >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Pengembang
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editprog_id_pengembang" name="id_pengembang" readonly="readonly"  >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama Pengembang
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editprog_nama_pengembang"  name="nama_pengembang" required="required">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>NIK Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-">
              <input type="number" class="form-control" id="editprog_nik_pengembang" name="nik_pengembang" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Jabatan Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editprog_jabatan_pengembang" name="jabatan_pengembang" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>No. HP Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editprog_nohp_pengembang" name="nohp_pengembang" >
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Email Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="email" class="form-control" id="editprog_email_pengembang" name="email_pengembang" >
            </div>
          </div>
          <!-- <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>SK Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_sk-pengembang-ubah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="skpengembang" id="dynamic-file_sk-pengembang-ubah" class="upload" />
                  </div>
                </div>
              </div>
              <input type="text" class="form-control" placeholder="Keterangan SK Pengembang">
            </div>
          </div>  -->
          <!-- <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Foto Pengembang</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_foto-pengembang-ubah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="fotopengembang" id="dynamic-file_foto-pengembang-ubah" class="upload" />
                  </div>
                </div>
              </div>
              <input type="text" class="form-control" placeholder="Keterangan Foto Pengembang">
            </div>
          </div> -->
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" id="editprog_keterangan_pengembang" name="keterangan_pengembang" rows="3"></textarea>
            </div>
          </div>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- POPUP KETERANGAN PENGEMBANG -->
<div class="modal fade modalRincianPengembang" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">RINCIAN PENGEMBANG</strong></small><br></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    
      <div class="modal-body">
        <!-- <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4"></div>
          <div class="col-md-4 col-sm-4 col-xs-4"><center>
            <div class="foto-pengembang-div">
              <img class="img foto-pengembang" src="/img/faces/marc.jpg" alt="foto-hendy-dwi-harfianto" />
            </div>
          </center></div>
          <div class="col-md-4 col-sm-4 col-xs-4"></div>
        </div>
        <hr> -->
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            ID Pengembang
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary"><span id="detprog_id_pengembang" name="detprog_id_pengembang"></span></strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Nama Pengembang
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary"><span id="detprog_nama_pengembang" name="detprog_nama_pengembang"></span></strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <i>NIK Pengembang</i>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-">
            <strong class="text-primary"><span id="detprog_nik_pengembang" name="detprog_nik_pengembang"></span></strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <i>Jabatan Pengembang</i>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-">
            <strong class="text-primary"><span id="detprog_jabatan_pengembang" name="detprog_jabatan_pengembang"></span></strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <i>No. HP Pengembang</i>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-">
            <strong class="text-primary"><span id="detprog_nohp_pengembang" name="detprog_nohp_pengembang"></span></strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <i>Email Pengembang</i>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-">
            <strong class="text-primary"><span id="detprog_email_pengembang" name="detprog_email_pengembang"></span></strong>
          </div>
        </div>
        
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Keterangan
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong><span id="detprog_keterangan_pengembang" name="detprog_keterangan_pengembang"></span></strong>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP HAPUS PENGEMBANG -->
<div class="modal fade modalHapusPengembang" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS PENGEMBANG</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-body">
        Apakah Anda yakin akan menghapus pengembang <i class="text-primary"><span id="hapusprog_nama_pengembang"></span></i>?
      </div>
      <div class="modal-footer">
        <form action="/pengembangAplikasiDelete" method="post" enctype="multipart/form-data">                                          
            {{ csrf_field() }}  
            <input type="hidden" name="id_aplikasi" id="hapusprog_id_aplikasi"  />
            <input type="hidden" name="id_pengembang" id="hapusprog_id_pengembang"  />
            <input type="hidden" name="mode" value="delete">
            <input type="hidden" name="_method" value="POST">

            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
            <button type="submit" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
        </form>
      </div>

    </div>
  </div>
</div>


<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->


<!-- POPUP TAMBAH VERSI -->
<div class="modal fade modalTambahVersi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><strong class="text-align">TAMBAH VERSI APLIKASI </strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="POST" action="{{ route('versiAplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}       
        <div class="modal-body">
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    ID Aplikasi
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input type="text" class="form-control" id="id_aplikasi" name="id_aplikasi" readonly="readonly"  >
                </div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group row col-lg-6 col-md-6 col-sm-6 col-xs-6"> 
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        Nomor Versi
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" id="nomor_versi" name="nomor_versi" required="required"  >
                    </div>
                </div>
                <div class="form-group row col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <i>Tanggal Versi</i>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="input-group">
                            <input type="date" name="tanggal_versi" id="tanggal_versi" class="form-control">
                            <div class="input-group-append">
                                <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    Rincian Versi
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <textarea class="form-control" id="rincian_versi" name="rincian_versi" rows="3"></textarea>
                </div>
            </div> 
            <hr>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <i>Source Code</i>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="input-group">
                        <input type="text" class="form-control" id="dynamic-url_sourcecode-versi-tambah" readonly="" placeholder="Ukuran maksimum source code 1 GB">
                        <div class="input-group-append">
                            <div class="btn btn-primary btn-sm">
                                <span><i class="fa fa-upload"></i></span>
                            <input type="file" name="id_sourcecode" id="dynamic-file_sourcecode-versi-tambah" class="upload" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <input type="text" class="form-control" placeholder="Keterangan source code" name="keterangan_sourcecode">
                </div>
            </div>  
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">

                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <div class="radio">
                    <label class="radio-inline">Ada source code secara fisik?</label>&emsp;           
                    <label class="radio-inline"><input type="radio" class="sourcecode-fisik" name="isSourceCodeKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                    <label class="radio-inline"><input type="radio" class="sourcecode-fisik" name="isSourceCodeKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
                    </div>
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12 form-penyimpanan-sourcecode">
                <div class="col-md-2 col-sm-2 col-xs-2"></div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <div class="row ">
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            Disimpan di
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="sourcecode~lokasi" id="tambah-sourcecode-lokasi-dropdown">
                                <option value=""  selected="true">Pilih Lokasi</option>
                                @foreach ($list_lokasi as $lokasi)
                                    <option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="sourcecode~kontainer" id="tambah-sourcecode-kontainer-dropdown">
                                <option value="" selected="true">Pilih kontainer...</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="sourcecode~posisi_kontainer" id="tambah-sourcecode-posisi-kontainer-dropdown">
                                <option value="" selected="true">Pilih posisi kontainer...</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>   
            <hr>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <i>Dokumentasi Versi</i>
                </div>
                <div class="form-group col-md-3 col-sm-3 col-xs-3">
                    <input type="text" class="form-control" placeholder="Nama dokumen" name="nama_dokumentasi">
                </div>
                <div class="form-group col-md-4 col-sm-4 col-xs-4">
                    <div class="input-group">
                        <input type="text" class="form-control" id="dynamic-url_dokumentasi-versi-tambah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                        <div class="input-group-append">
                            <div class="btn btn-primary btn-sm">
                                <span><i class="fa fa-upload"></i></span>
                                <input type="file" name="id_dokumentasi" id="dynamic-file_dokumentasi-versi-tambah" class="upload" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group">
                        <input type="date" name="tanggal_dokumentasi" id="tanggal_dokumentasi" class="form-control">
                        <div class="input-group-append">
                            <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>  
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input type="text" class="form-control" placeholder="Keterangan dokumentasi" name="keterangan_dokumentasi">
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                  
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                  <div class="radio">
                    <label class="radio-inline">Ada dokumentasi fisik?</label>&emsp;           
                    <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isDocKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                    <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isDocKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
                  </div>
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12 form-penyimpanan">
                <div class="col-md-2 col-sm-2 col-xs-2">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <div class="row ">
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            Disimpan di
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="dokumentasi~lokasi" id="tambah-dokumentasi-versi-lokasi-dropdown">
                                <option value=""  selected="true">Pilih Lokasi</option>
                                @foreach ($list_lokasi as $lokasi)
                                    <option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="dokumentasi~kontainer" id="tambah-dokumentasi-versi-kontainer-dropdown">
                                <option value="" selected="true">Pilih kontainer...</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="dokumentasi~posisi_kontainer" id="tambah-dokumentasi-versi-posisi-kontainer-dropdown">
                                <option value="" selected="true">Pilih posisi kontainer...</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Tambah Versi Baru</button>
      </div>
        </form>
    </div>
  </div>
</div>

<!-- POPUP UBAH VERSI -->
<div class="modal fade modalUbahVersi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title"><strong class="text-align">UBAH VERSI APLIKASI </strong></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form method="POST" action="{{ route('versiAplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}       
        <div class="modal-body">
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    ID Aplikasi
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input type="text" class="form-control" id="editversi_id_aplikasi" name="id_aplikasi" readonly="readonly">
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    ID Versi
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input type="text" class="form-control" id="editversi_id_versi" name="id_versi" readonly="readonly">
                </div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="form-group row col-lg-6 col-md-6 col-sm-6 col-xs-6"> 
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        Nomor Versi
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" id="editversi_nomor_versi" name="nomor_versi" required="required">
                    </div>
                </div>
                <div class="form-group row col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <i>Tanggal Versi</i>
                    </div>
                    <div class="col-md-8 col-sm-8 col-xs-8">
                        <div class="input-group">
                            <input type="date" name="tanggal_versi" id="editversi_tanggal_versi" class="form-control">
                            <div class="input-group-append">
                                <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    Rincian Versi
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <textarea class="form-control" id="editversi_rincian_versi" name="rincian_versi" rows="3"></textarea>
                </div>
            </div> 
            <hr>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <i>Source Code</i>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="input-group">
                        <input type="text" class="form-control" id="dynamic-url_sourcecode-versi-tambah" readonly="" placeholder="Ukuran maksimum source code 1 GB">
                        <div class="input-group-append">
                            <div class="btn btn-primary btn-sm">
                                <span><i class="fa fa-upload"></i></span>
                            <input type="file" name="id_sourcecode" id="dynamic-file_sourcecode-versi-tambah" class="upload" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <input type="text" class="form-control" placeholder="Keterangan source code" name="keterangan_sourcecode">
                </div>
            </div>  
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">

                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <div class="radio">
                    <label class="radio-inline">Ada source code secara fisik?</label>&emsp;           
                    <label class="radio-inline"><input type="radio" class="sourcecode-fisik" name="isSourceCodeKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                    <label class="radio-inline"><input type="radio" class="sourcecode-fisik" name="isSourceCodeKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
                    </div>
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12 form-penyimpanan-sourcecode">
                <div class="col-md-2 col-sm-2 col-xs-2"></div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <div class="row ">
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            Disimpan di
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="sourcecode~lokasi" id="tambah-sourcecode-lokasi-dropdown">
                                <option value=""  selected="true">Pilih Lokasi</option>
                                @foreach ($list_lokasi as $lokasi)
                                    <option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="sourcecode~kontainer" id="tambah-sourcecode-kontainer-dropdown">
                                <option value="" selected="true">Pilih kontainer...</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="sourcecode~posisi_kontainer" id="tambah-sourcecode-posisi-kontainer-dropdown">
                                <option value="" selected="true">Pilih posisi kontainer...</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>   
            <hr>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                    <i>Dokumentasi Versi</i>
                </div>
                <div class="form-group col-md-3 col-sm-3 col-xs-3">
                    <input type="text" class="form-control" placeholder="Nama dokumen" name="nama_dokumentasi">
                </div>
                <div class="form-group col-md-4 col-sm-4 col-xs-4">
                    <div class="input-group">
                        <input type="text" class="form-control" id="dynamic-url_dokumentasi-versi-tambah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                        <div class="input-group-append">
                            <div class="btn btn-primary btn-sm">
                                <span><i class="fa fa-upload"></i></span>
                                <input type="file" name="id_dokumentasi" id="dynamic-file_dokumentasi-versi-tambah" class="upload" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-3 col-sm-3 col-xs-3">
                    <div class="input-group">
                        <input type="date" name="tanggal_dokumentasi" id="tanggal_dokumentasi" class="form-control">
                        <div class="input-group-append">
                            <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>  
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <input type="text" class="form-control" placeholder="Keterangan dokumentasi" name="keterangan_dokumentasi">
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-2 col-sm-2 col-xs-2">
                  
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                  <div class="radio">
                    <label class="radio-inline">Ada dokumentasi fisik?</label>&emsp;           
                    <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isDocKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                    <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isDocKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
                  </div>
                </div>
            </div>
            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12 form-penyimpanan">
                <div class="col-md-2 col-sm-2 col-xs-2">
                </div>
                <div class="col-md-10 col-sm-10 col-xs-10">
                    <div class="row ">
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            Disimpan di
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="dokumentasi~lokasi" id="tambah-dokumentasi-versi-lokasi-dropdown">
                                <option value=""  selected="true">Pilih Lokasi</option>
                                @foreach ($list_lokasi as $lokasi)
                                    <option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="dokumentasi~kontainer" id="tambah-dokumentasi-versi-kontainer-dropdown">
                                <option value="" selected="true">Pilih kontainer...</option>
                            </select>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <select class="form-control" name="dokumentasi~posisi_kontainer" id="tambah-dokumentasi-versi-posisi-kontainer-dropdown">
                                <option value="" selected="true">Pilih posisi kontainer...</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>    
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
        </form>
    </div>
  </div>
</div>
<!-- 
<div class="modal fade modalUbahVersi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">UBAH VERSI</strong></small><br><i class="text-primary">SPRI</i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('versiAplikasiSave') }}" enctype="multipart/form-data">        
        {{ csrf_field() }}
       
        <div class="modal-body">
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Aplikasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editversi_id_aplikasi" name="id_aplikasi" readonly="readonly"  >
            </div>
          </div>
          
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              ID Versi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editversi_id_versi" name="id_versi">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nomor Versi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" id="editversi_nomor_versi" name="nomor_versi">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Tanggal Versi</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="date" id="editversi_tanggal_versi" name="tanggal_versi" class="form-control">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Source Code</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_dokumentasi-versi-tambah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="dokumentasiversi" id="dynamic-file_dokumentasi-versi-tambah" class="upload" />
                  </div>
                </div>
              </div>
              <input type="text" class="form-control" placeholder="Keterangan dokumentasi">
            </div>
          </div>  
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="radio">
                <label class="radio-inline">Ada source code secara fisik?</label>&emsp;           
                <label class="radio-inline"><input type="radio" class="sourcecode-fisik" name="isSourceCodeKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                <label class="radio-inline"><input type="radio" class="sourcecode-fisik" name="isSourceCodeKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
              </div>
            </div>
          </div>
          <div class="form-group row form-penyimpanan-sourcecode">
            <div class="col-md-4 col-sm-4 col-xs-4">
              
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              Disimpan di
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih lokasi...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih kontainer...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih posisi kontainer...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
            </div>
          </div>  
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Dokumentasi Versi</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_dokumentasi-versi-ubah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="dokumentasiversi" id="dynamic-file_dokumentasi-versi-ubah" class="upload" />
                  </div>
                </div>
              </div>
              <input type="text" class="form-control" placeholder="Keterangan dokumentasi">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="radio">
                <label class="radio-inline">Ada dokumentasi fisik?</label>&emsp;           
                <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
              </div>
            </div>
          </div>
          <div class="form-group row form-penyimpanan">
            <div class="col-md-4 col-sm-4 col-xs-4">
              
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              Disimpan di
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih lokasi...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih kontainer...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih posisi kontainer...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
            </div>
          </div>   
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Rincian Versi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" id="editversi_rincian_versi" name="rincian_versi" rows="3"></textarea>
            </div>
          </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
        </form>
    </div>
  </div>
</div> -->

<!-- POPUP RINCIAN VERSI -->
<div class="modal fade modalRincianVersi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">RINCIAN VERSI</strong></small><br></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Nomor Versi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary"><span id="detversi_nomor_versi" name="detversi_nomor_versi"></span></strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Tanggal Versi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary"><span id="detversi_tanggal_versi" name="detversi_tanggal_versi"></span></strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Rincian Versi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary"><span id="detversi_rincian_versi" name="detversi_rincian_versi"></span></strong>
          </div>
        </div> 
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <i>Source Code</i>
          </div>
          <div class="col-md-2 col-sm-2 col-xs-2">
             <a href="#" rel="tooltip" title="Unduh Source Code" class="text-danger" id="detversi_link_sourcecode" name="detversi_link_sourcecode"><u>Unduh</u></a>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <small><i class="text-primary">Keterangan</i>: <span id="detversi_keterangan_sourcecode" name="detversi_keterangan_sourcecode"></span></small><br>
            <small><i class="text-primary">Sourcecode fisik disimpan di</i>: <span id="detversi_lokasi_sourceode" name="detversi_lokasi_sourceode"></span></small>
          </div>
        </div> 
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Dokumentasi Versi
          </div>
          <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="#" rel="tooltip" title="Unduh Dokumentasi Versi" class="text-danger" id="detversi_link_dokumentasi" name="detversi_link_dokumentasi"><u>Unduh</u></a>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <small><i>Keterangan</i>: <span id="detversi_keterangan_dokumentasi" name="detversi_keterangan_dokumentasi"></span></small><br>
            <small><i class="text-primary">Dokumentasi fisik disimpan di</i>: <span id="detversi_lokasi_dokumentasi" name="detversi_lokasi_dokumentasi"></span></small>
          </div>
        </div>      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP HAPUS VERSI -->
<div class="modal fade modalHapusVersi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS VERSI APLIKASI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus versi <i class="text-primary">2.1566</i>?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
        <button type="button" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
      </div>
    </div>
  </div>
</div>


<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->


<!-- POPUP TAMBAH DOKUMENTASI -->
<div class="modal fade modalTambahDokumentasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">TAMBAH DOKUMENTASI APLIKASI <i class="text-primary">SPRI</i></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('dokumentasiAplikasiSave') }}" enctype="multipart/form-data">        
      {{ csrf_field() }}
        <div class="modal-body">
            <div class="form-group row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    ID Aplikasi
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <input type="text" class="form-control" id="id_aplikasi" name="id_aplikasi" readonly="readonly"  >
                </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4">
                Nama Dokumentasi
              </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                <input type="text" class="form-control" name="nama_dokumentasi">
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4">
                Tanggal Dokumentasi
              </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="input-group">
                  <input type="date" name="tanggal_dokumentasi" id="tanggal_dokumentasi" class="form-control">
                  <div class="input-group-append">
                    <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4">
                <i>File</i> Dokumentasi
              </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="input-group">
                  <input type="text" class="form-control" id="dynamic-url_dokumentasi-tambah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                  <div class="input-group-append">
                    <div class="btn btn-primary btn-sm">
                      <span><i class="fa fa-upload"></i></span>
                      <input type="file" name="id_dokumentasi" id="dynamic-file_dokumentasi-tambah" class="upload" />
                    </div>
                  </div>
                </div>
              </div>
            </div>      
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4">
                Keterangan Dokumentasi
              </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                <textarea class="form-control" rows="3" name="keterangan_dokumentasi"></textarea>
              </div>
            </div>
            <hr>
            <div class="form-group row">
              <div class="col-md-4 col-sm-4 col-xs-4">
                Ada dokumentasi fisik?
              </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="radio">
                  <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                  <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
                </div>
              </div>
            </div>
            <div class="form-group row form-penyimpanan">
              <div class="col-md-4 col-sm-4 col-xs-4">
                Disimpan di
              </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                <div class="row">
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <select class="form-control" name="id_lokasi" id="tambah-dokumentasi-lokasi-dropdown">
                      <option value=""  selected="true">Pilih Lokasi</option>
                      @foreach ($list_lokasi as $lokasi)
                          <option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <select class="form-control" name="id_kontainer" id="tambah-dokumentasi-kontainer-dropdown">
                      <option value="" selected="true">Pilih kontainer...</option>
                    </select>
                  </div>
                  <div class="col-md-4 col-sm-4 col-xs-4">
                    <select class="form-control" name="id_posisi_kontainer" id="tambah-dokumentasi-posisi-kontainer-dropdown">
                      <option value="" selected="true">Pilih posisi kontainer...</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Tambah Dokumentasi Baru</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- POPUP UBAH DOKUMENTASI -->
<div class="modal fade modalUbahDokumentasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">UBAH DOKUMENTASI</strong></small><br><i class="text-primary">SPRI Mobile Manual</i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama Dokumentasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Tanggal Dokumentasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="date" name="tanggal_dokumentasi" id="tanggal_dokumentasi" class="form-control">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>File</i> Dokumentasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_dokumentasi-ubah" readonly="" placeholder="Ukuran maksimum dokumentasi 1 GB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="dokumentasiaplikasi" id="dynamic-file_dokumentasi-ubah" class="upload" />
                  </div>
                </div>
              </div>
              <input type="text" class="form-control" placeholder="Keterangan dokumentasi">
            </div>
          </div>
          <hr>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan Dokumentasi
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
          <hr>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Ada dokumentasi fisik?
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="radio">
                <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isKept" id="inlineRadio1" value="yadisimpan"> Ya, Ada </label>&emsp;
                <label class="radio-inline"><input type="radio" class="dokumentasi-fisik" name="isKept" id="inlineRadio2" value="tidakdisimpan"> Tidak </label>
              </div>
            </div>
          </div>
          <div class="form-group row form-penyimpanan">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Disimpan di
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih lokasi...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih kontainer...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-4">
                  <select class="form-control">
                    <option selected>Pilih posisi kontainer...</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP RINCIAN DOKUMENTASI -->
<div class="modal fade modalRincianDokumentasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">RINCIAN DOKUMENTASI</strong></small><br><i class="text-primary">SPRI Mobile Buku Panduan</i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Nama Dokumentasi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary">SPRI Mobile Buku Panduan</strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Ukuran <i>File</i>
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary">1 GB</strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Tanggal Dokumentasi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary">21 Agustus 2018</strong>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            <i>File</i> Dokumentasi
          </div>
          <div class="col-md-2 col-sm-2 col-xs-2">
            <button type="button" rel="tooltip" title="" class="btn btn-info btn-sm" data-original-title="Lihat File Dokumentasi">Lihat<div class="ripple-container"></div></button>
          </div>
          <div class="col-md-6 col-sm-6 col-xs-6">
            <small><i>Keterangan</i>: kjnasac lahdssoc iajdscjasd cp;aijdasdasd adf fq3f qewfq efqef qew fq efqwe</small><br>
            <small><i class="text-primary">Disimpan di</i>: kjnasac lahdssoc iajdscjasd cp;aijdasdasd adf fq3f qewfq efqef qew fq efqwe</small>
          </div>
        </div>      
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Keterangan Dokumentasi
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            <strong class="text-primary">adslcha dchieh cqehrco qhciqh q39eh f971y c 82g4rci uw4 8 7t4gfo83g4o5fcg32o48f g4rfh3oi4rg3c9 y45 8gf5tuvhrkfghwe rgwyr9g p 9 gali hvgaigfvao8 egf v</strong>
          </div>
        </div>
        <hr>
        <div class="form-group row">
          <div class="col-md-4 col-sm-4 col-xs-4">
            Dokumentasi fisik disimpan di
          </div>
          <div class="col-md-8 col-sm-8 col-xs-8">
            Lokasi: <strong class="text-primary">DC Lantai 2</strong><br>
            Kontainer: <strong class="text-primary">RACK_BEDUGUL</strong><br>
            Posisi Kontainer: <strong class="text-primary">Sekat A-B</strong><br>
          </div>
        </div> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP HAPUS DOKUMENTASI -->
<div class="modal fade modalHapusDokumentasi" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS VERSI APLIKASI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus dokumentasi <i class="text-primary">SPRI Mobile Buku Panduan</i>?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
        <button type="button" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
      </div>
    </div>
  </div>
</div>


<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->
<!------------------------------------------------------------------------------------------------------------------------------------------------------->


<!-- POPUP TAMBAH SCREENSHOT -->
<div class="modal fade modalTambahScreenshot" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">TAMBAH SCREENSHOT APLIKASI </strong> <i class="text-primary">{{ $aplikasi->singkatan_aplikasi }}</i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" action="{{ route('gambarAplikasiSave') }}" enctype="multipart/form-data">        
      {{ csrf_field() }}
      <div class="modal-body">
          <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="col-md-4 col-sm-4 col-xs-4">
                  ID Aplikasi
              </div>
              <div class="col-md-8 col-sm-8 col-xs-8">
                  <input type="text" class="form-control" id="id_aplikasi" name="id_aplikasi" readonly="readonly"  >
              </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama <i>Screenshot</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control" name="nama_gambar">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>File Screenshot</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_screenshot-tambah" readonly="" placeholder="Ukuran maksimum screenshot 2 MB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="id_gambar" id="dynamic-file_screenshot-tambah" class="upload"/>
                  </div>
                </div>
              </div>
              <div class="preview-screenshot">
                <img id="ss-tambah-preview" src="#" alt="Screenshot Aplikasi" />
              </div>
            </div>
          </div>      
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan <i>Screenshot</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" rows="3" name="keterangan_gambar"></textarea>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
          <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Tambah Screenshot Baru</button>
        </div>
      </form>
    </div>
  </div>
</div>

<!-- POPUP LIHAT SCREENSHOT -->
<div class="modal fade modalLihatScreenshot" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
          <h5 class="modal-title"><small><strong class="text-align">SCREENSHOT APLIKASI </strong> <strong>SPRI</strong></small><br><i id="detscreenshot_nama_gambar" class="text-primary"></i></h5>
        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
          <div class="form-group row pull-right">
            <h4>
              <a href="#" rel="tooltip" title="Ubah Screenshot" class="text-success" data-toggle="modal" data-target=".modalUbahScreenshot" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil"></i></a>&emsp;
              <a href="#" rel="tooltip" title="Hapus Screenshot" class="text-danger" data-toggle="modal" data-target=".modalHapusScreenshot" data-backdrop="static" data-keyboard="false"><i class="fa fa-trash"></i></a>
            </h4>
          </div>
        </div>
      </div>
      <div class="modal-body">
        <div class="row">
          <img src="" id="detscreenshot_link_gambar" class="screenshot-aplikasi-popup">
        </div>
        <div class="form-group row">
            <p class="text-primary" id="detscreenshot_keterangan_gambar"></p>
        </div>
        <hr style="margin: 0em; border-width: 1px;">
        <div class="form-group row pull-right">
          <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- POPUP UBAH SCREENSHOT -->
<div class="modal fade modalUbahScreenshot" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">UBAH SCREENSHOT</strong></small><br><i class="text-primary">Fitur Wawancara dan Pengambilan Biometrik</i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Nama <i>Screenshot</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>File Screenshot</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <div class="input-group">
                <input type="text" class="form-control" id="dynamic-url_screenshot-ubah" readonly="" placeholder="Ukuran maksimum screenshot 2 MB">
                <div class="input-group-append">
                  <div class="btn btn-primary btn-sm">
                    <span><i class="fa fa-upload"></i></span>
                    <input type="file" name="screenshotaplikasi" id="dynamic-file_screenshot-ubah" class="upload" onchange="readImageSSubah(this);"/>
                  </div>
                </div>
              </div>
              <div class="preview-screenshot">
                <img id="ss-ubah-preview" src="#" alt="your image" />
              </div>
            </div>
          </div>      
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan <i>Screenshot</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP HAPUS SCREENSHOT -->
<div class="modal fade modalHapusScreenshot" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS SCREENSHOT</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus screenshot <i class="text-primary">Fitur Wawancara dan Pengambilan Biometrik</i>?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
        <button type="button" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    moment.locale('id');

    window.onbeforeunload = function () {
      var msg = "Apakah Anda akan meninggalkan halaman ini? Data akan hilang sebelum Anda menyimpannya.";
      return msg;
    };


    <?php if( $aplikasi->id_jenis_pengembangan ){ ?>
      $('[name=id_jenisdev]').val('{{ $aplikasi->id_jenis_pengembangan }}');
    <?php } ?>

    $('.form-penyimpanan').hide();

    $(document).on("change", "[id^=dynamic-file_]", function (e) {
        var target = e.currentTarget.id.split('_').pop();
        $("#dynamic-url_" + target).val(this.value.replace(/C:\\fakepath\\/i, ''));
    });

    $(".dokumentasi-fisik").change(function () { //use change event
        if (this.value == "yadisimpan") { //check value if it is domicilio
            $('.form-penyimpanan').stop(true,true).show(750); //than show
        } else {
            $('.form-penyimpanan').stop(true,true).hide(750); //else hide
        }
    });

    $('.form-penyimpanan-sourcecode').hide();
    $(".sourcecode-fisik").change(function () { //use change event
        if (this.value == "yadisimpan") { //check value if it is domicilio
            $('.form-penyimpanan-sourcecode').stop(true,true).show(750); //than show
        } else {
            $('.form-penyimpanan-sourcecode').stop(true,true).hide(750); //else hide
        }
    });

    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });


    $("#tambah-sourcecode-lokasi-dropdown").change(function (e) {
        e.preventDefault();
        var lokasi = e.target.value;
        
        if(lokasi){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kontainer-list')}}?lokasi=" + lokasi,
             success:function(res){
              if(res){
                  $('#tambah-sourcecode-kontainer-dropdown').empty();
                  $('#tambah-sourcecode-posisi-kontainer-dropdown').empty();
                  $('#tambah-sourcecode-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
                  $('#tambah-sourcecode-posisi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-sourcecode-kontainer-dropdown').append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-sourcecode-kontainer-dropdown').empty();
                  $('#tambah-sourcecode-posisi-kontainer-dropdown').empty();
              }
             }, error:function(xhr) {
                console.log(xhr);
             }
          });
        }
        else{
          $('#tambah-sourcecode-kontainer-dropdown').empty();
          $('#tambah-sourcecode-posisi-kontainer-dropdown').empty();
        }
    });

    $("#tambah-sourcecode-kontainer-dropdown").change(function (e) {
        e.preventDefault();
        var kontainer = e.target.value;

        if(kontainer){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
             success:function(res){
              if(res){
                  $('#tambah-sourcecode-posisi-kontainer-dropdown').empty();
                  $('#tambah-sourcecode-posisi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-sourcecode-posisi-kontainer-dropdown').append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-sourcecode-posisi-kontainer-dropdown').empty();
              }
             }, error:function(xhr) {
                console.log(xhr);
             }
          });
        }
        else{
          $('#tambah-sourcecode-posisi-kontainer-dropdown').empty();
        }
    });

    $("#tambah-dokumentasi-versi-lokasi-dropdown").change(function (e) {
        e.preventDefault();
        var lokasi = e.target.value;
        
        if(lokasi){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kontainer-list')}}?lokasi=" + lokasi,
             success:function(res){
              if(res){
                  $('#tambah-dokumentasi-versi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-versi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
                  $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-dokumentasi-versi-kontainer-dropdown').append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-dokumentasi-versi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').empty();
              }
             }, error:function(xhr) {
                console.log(xhr);
             }
          });
        }
        else{
          $('#tambah-dokumentasi-versi-kontainer-dropdown').empty();
          $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').empty();
        }
    });

    $("#tambah-dokumentasi-versi-kontainer-dropdown").change(function (e) {
        e.preventDefault();
        var kontainer = e.target.value;

        if(kontainer){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
             success:function(res){
              if(res){
                  $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').empty();
              }
             }, error:function(xhr) {
                console.log(xhr);
             }
          });
        }
        else{
          $('#tambah-dokumentasi-versi-posisi-kontainer-dropdown').empty();
        }
    });

    $("#tambah-dokumentasi-lokasi-dropdown").change(function (e) {
        e.preventDefault();
        var lokasi = e.target.value;
        
        if(lokasi){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kontainer-list')}}?lokasi=" + lokasi,
             success:function(res){
              if(res){
                  $('#tambah-dokumentasi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-posisi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
                  $('#tambah-dokumentasi-posisi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-dokumentasi-kontainer-dropdown').append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-dokumentasi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-posisi-kontainer-dropdown').empty();
              }
             }, error:function(xhr) {
                console.log(xhr);
             }
          });
        }
        else{
          $('#tambah-dokumentasi-kontainer-dropdown').empty();
          $('#tambah-dokumentasi-posisi-kontainer-dropdown').empty();
        }
    });

    $("#tambah-dokumentasi-kontainer-dropdown").change(function (e) {
        e.preventDefault();
        var kontainer = e.target.value;

        if(kontainer){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
             success:function(res){
              if(res){
                  $('#tambah-dokumentasi-posisi-kontainer-dropdown').empty();
                  $('#tambah-dokumentasi-posisi-kontainer-dropdown').append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-dokumentasi-posisi-kontainer-dropdown').append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-dokumentasi-posisi-kontainer-dropdown').empty();
              }
             }, error:function(xhr) {
                console.log(xhr);
             }
          });
        }
        else{
          $('#tambah-dokumentasi-posisi-kontainer-dropdown').empty();
        }
    });

    $(document).on("click", ".open-modalTambahAlamat", function () {
        var editid_aplikasi = $(this).data('alamat_id_aplikasi');
        $(".modal-body #id_aplikasi").val( editid_aplikasi );
        document.getElementById("id_aplikasi").innerHTML = editid_aplikasi;
    });

    $(document).on("click", ".open-modalKeteranganKoneksi", function () {
        var alamat_keterangan_alamat = $(this).data('alamat_keterangan_alamat');
        var alamat_nama_aplikasi = $(this).data('alamat_nama_aplikasi');
       
        $(".modal-body #alamat_keterangan_alamat").val( alamat_keterangan_alamat );
        $(".modal-title #alamat_nama_aplikasi").val( alamat_nama_aplikasi );
        document.getElementById("alamat_keterangan_alamat").innerHTML = alamat_keterangan_alamat;
        document.getElementById("alamat_nama_aplikasi").innerHTML = alamat_nama_aplikasi;
    });

    $(document).on("click", ".open-modalTambahDokumentasi", function () {
        var id_aplikasi = $(this).data('dokumentasi_id_aplikasi');
        $(".modal-body #id_aplikasi").val( id_aplikasi );
        document.getElementById("id_aplikasi").innerHTML = id_aplikasi;
    });

    $(document).on("click", ".open-modalTambahScreenshot", function () {
        var id_aplikasi = $(this).data('screenshot_id_aplikasi');
        $(".modal-body #id_aplikasi").val( id_aplikasi );
        document.getElementById("id_aplikasi").innerHTML = id_aplikasi;
    });

    $(document).on("click", ".open-modalUbahKoneksi", function () {

        var edit_alamat_id_aplikasi = $(this).data('alamat_id_aplikasi');
        var edit_alamat_nama_aplikasi = $(this).data('alamat_nama_aplikasi');
        var edit_alamat_id_alamat = $(this).data('alamat_id_alamat');
        var edit_alamat_nama_alamat = $(this).data('alamat_nama_alamat');
        var edit_alamat_subdomain = $(this).data('alamat_subdomain');
        var edit_alamat_ip_address = $(this).data('alamat_ip_address');
        var edit_alamat_port = $(this).data('alamat_port');
        var edit_alamat_utama = $(this).data('alamat_utama');
        var edit_alamat_keterangan_alamat = $(this).data('alamat_keterangan_alamat');
        

        $(".modal-title #edit_alamat_nama_aplikasi").val( edit_alamat_nama_aplikasi );
        $(".modal-body #edit_alamat_id_aplikasi").val( edit_alamat_id_aplikasi );
        $(".modal-body #edit_alamat_id_alamat").val( edit_alamat_id_alamat );
        $(".modal-body #edit_alamat_nama_alamat").val( edit_alamat_nama_alamat );
        $(".modal-body #edit_alamat_subdomain").val( edit_alamat_subdomain );
        $(".modal-body #edit_alamat_ip_address").val( edit_alamat_ip_address );
        $(".modal-body #edit_alamat_port").val( edit_alamat_port );
        $(".modal-body #edit_alamat_keterangan_alamat").val( edit_alamat_keterangan_alamat );


        var radiobtny = document.getElementById("edit_alamat_utama_y");
        var radiobtnn = document.getElementById("edit_alamat_utama_n");

        if(edit_alamat_utama == 'Y'){      
          radiobtny.checked = true;      
        }else if(edit_alamat_utama == 'N'){      
          radiobtnn.checked = true;
        }
        document.getElementById("edit_alamat_nama_aplikasi").innerHTML = edit_alamat_nama_aplikasi;
        document.getElementById("edit_alamat_id_aplikasi").innerHTML = edit_alamat_id_aplikasi;
        document.getElementById("edit_alamat_id_alamat").innerHTML = edit_alamat_id_alamat;
        document.getElementById("edit_alamat_nama_alamat").innerHTML = edit_alamat_nama_alamat;
        document.getElementById("edit_alamat_subdomain").innerHTML = edit_alamat_subdomain;
        document.getElementById("edit_alamat_ip_address").innerHTML = edit_alamat_ip_address;
        document.getElementById("edit_alamat_port").innerHTML = edit_alamat_port;
        document.getElementById("edit_alamat_keterangan_alamat").innerHTML = edit_alamat_keterangan_alamat;

    });

    $(document).on("click", ".open-modalHapusKoneksi", function () {
        var hapus_alamat_id_aplikasi = $(this).data('alamat_id_aplikasi');
        var hapus_alamat_id_alamat = $(this).data('alamat_id_alamat');
        var hapus_alamat_nama_alamat = $(this).data('alamat_nama_alamat');

        
         $(".modal-footer #hapus_alamat_id_aplikasi").val( hapus_alamat_id_aplikasi );
         $(".modal-footer #hapus_alamat_id_alamat").val( hapus_alamat_id_alamat );
         $(".modal-body #hapus_alamat_nama_alamat").val( hapus_alamat_nama_alamat );

        document.getElementById("hapus_alamat_id_aplikasi").innerHTML = hapus_alamat_id_aplikasi;
        document.getElementById("hapus_alamat_id_alamat").innerHTML = hapus_alamat_id_alamat;
        document.getElementById("hapus_alamat_nama_alamat").innerHTML = hapus_alamat_nama_alamat;

    }); 

    $(document).on("click", ".open-modalTambahPengembang", function () {
        var editid_aplikasi = $(this).data('prog_id_aplikasi');
        $(".modal-body #id_aplikasi").val( editid_aplikasi );
        document.getElementById("id_aplikasi").innerHTML = editid_aplikasi;

    });


    $(document).on("click", ".open-modalRincianPengembang", function () {
        var detprog_id_pengembang = $(this).data('prog_id_pengembang');
        var detprog_nama_pengembang = $(this).data('prog_nama_pengembang');
        var detprog_jabatan_pengembang = $(this).data('prog_jabatan_pengembang');
        var detprog_nik_pengembang = $(this).data('prog_nik_pengembang');
        var detprog_nohp_pengembang = $(this).data('prog_nohp_pengembang');
        var detprog_email_pengembang = $(this).data('prog_email_pengembang');
        var detprog_keterangan_pengembang = $(this).data('prog_keterangan_pengembang');

        $(".modal-body #detprog_id_pengembang").val( detprog_id_pengembang );
        $(".modal-body #detprog_nama_pengembang").val( detprog_nama_pengembang );
        $(".modal-body #detprog_jabatan_pengembang").val( detprog_jabatan_pengembang );
        $(".modal-body #detprog_nik_pengembang").val( detprog_nik_pengembang );
        $(".modal-body #detprog_nohp_pengembang").val( detprog_nohp_pengembang );
        $(".modal-body #detprog_email_pengembang").val( detprog_email_pengembang );
        $(".modal-body #detprog_keterangan_pengembang").val( detprog_keterangan_pengembang );

        document.getElementById("detprog_id_pengembang").innerHTML = detprog_id_pengembang;
        document.getElementById("detprog_nama_pengembang").innerHTML = detprog_nama_pengembang;
        document.getElementById("detprog_jabatan_pengembang").innerHTML = detprog_jabatan_pengembang;
        document.getElementById("detprog_nik_pengembang").innerHTML = detprog_nik_pengembang;
        document.getElementById("detprog_nohp_pengembang").innerHTML = detprog_nohp_pengembang;
        document.getElementById("detprog_email_pengembang").innerHTML = detprog_email_pengembang;
        document.getElementById("detprog_keterangan_pengembang").innerHTML = detprog_keterangan_pengembang;

    });

    $(document).on("click", ".open-modalUbahPengembang", function () {
        var editprog_id_aplikasi = $(this).data('prog_id_aplikasi');
        var editprog_id_pengembang = $(this).data('prog_id_pengembang');
        var editprog_nama_pengembang = $(this).data('prog_nama_pengembang');
        var editprog_jabatan_pengembang = $(this).data('prog_jabatan_pengembang');
        var editprog_nik_pengembang = $(this).data('prog_nik_pengembang');
        var editprog_nohp_pengembang = $(this).data('prog_nohp_pengembang');
        var editprog_email_pengembang = $(this).data('prog_email_pengembang');
        var editprog_keterangan_pengembang = $(this).data('prog_keterangan_pengembang');

        $(".modal-body #editprog_id_aplikasi").val( editprog_id_aplikasi );
        $(".modal-body #editprog_id_pengembang").val( editprog_id_pengembang );
        $(".modal-body #editprog_nama_pengembang").val( editprog_nama_pengembang );
        $(".modal-body #editprog_jabatan_pengembang").val( editprog_jabatan_pengembang );
        $(".modal-body #editprog_nik_pengembang").val( editprog_nik_pengembang );
        $(".modal-body #editprog_nohp_pengembang").val( editprog_nohp_pengembang );
        $(".modal-body #editprog_email_pengembang").val( editprog_email_pengembang );
        $(".modal-body #editprog_keterangan_pengembang").val( editprog_keterangan_pengembang );

        document.getElementById("editprog_id_aplikasi").innerHTML = editprog_id_aplikasi;
        document.getElementById("editprog_id_pengembang").innerHTML = editprog_id_pengembang;
        document.getElementById("editprog_nama_pengembang").innerHTML = editprog_nama_pengembang;
        document.getElementById("editprog_jabatan_pengembang").innerHTML = editprog_jabatan_pengembang;
        document.getElementById("editprog_nik_pengembang").innerHTML = editprog_nik_pengembang;
        document.getElementById("editprog_nohp_pengembang").innerHTML = editprog_nohp_pengembang;
        document.getElementById("editprog_email_pengembang").innerHTML = editprog_email_pengembang;
        document.getElementById("editprog_keterangan_pengembang").innerHTML = editprog_keterangan_pengembang;

    });


    $(document).on("click", ".open-modalHapusPengembang", function () {
        var hapusprog_id_aplikasi = $(this).data('prog_id_aplikasi');
        var hapusprog_id_pengembang = $(this).data('prog_id_pengembang');
        var hapusprog_nama_pengembang = $(this).data('prog_nama_pengembang');

        
         $(".modal-footer #hapusprog_id_aplikasi").val( hapusprog_id_aplikasi );
         $(".modal-footer #hapusprog_id_pengembang").val( hapusprog_id_pengembang );
         $(".modal-body #hapusprog_nama_pengembang").val( hapusprog_nama_pengembang );

        document.getElementById("hapusprog_id_aplikasi").innerHTML = hapusprog_id_aplikasi;
        document.getElementById("hapusprog_id_pengembang").innerHTML = hapusprog_id_pengembang;
        document.getElementById("hapusprog_nama_pengembang").innerHTML = hapusprog_nama_pengembang;

    }); 


    $(document).on("click", ".open-modalTambahVersi", function () {
        var addid_aplikasi = $(this).data('versi_id_aplikasi');
        $(".modal-body #id_aplikasi").val( addid_aplikasi );
        document.getElementById("id_aplikasi").innerHTML = addid_aplikasi;

    });

    $(document).on("click", ".open-modalRincianVersi", function () {
        var detversi_nomor_versi = $(this).data('versi_nomor_versi');
        var detversi_tanggal_versi = moment($(this).data('versi_tanggal_versi')).format('Do MMMM YYYY');
        var detversi_rincian_versi = $(this).data('versi_rincian_versi');
        var detversi_id_sourcecode = $(this).data('versi_id_sourcecode');
        var detversi_nama_sourcecode = $(this).data('versi_nama_sourcecode');
        var detversi_link_sourcecode = $(this).data('versi_link_sourcecode');
        var detversi_keterangan_sourcecode = $(this).data('versi_keterangan_sourcecode');
        var detversi_as_id_lokasi = $(this).data('versi_as_id_lokasi');
        var detversi_as_nama_lokasi = $(this).data('versi_as_nama_lokasi');
        var detversi_as_id_kontainer = $(this).data('versi_as_id_kontainer');
        var detversi_as_nama_kontainer = $(this).data('versi_as_nama_kontainer');
        var detversi_as_id_posisi_kontainer = $(this).data('versi_as_id_posisi_kontainer');
        var detversi_as_label_posisi_kontainer = $(this).data('versi_as_label_posisi_kontainer');
        var detversi_id_dokumentasi = $(this).data('versi_id_dokumentasi');
        var detversi_nama_dokumentasi = $(this).data('versi_nama_dokumentasi');
        var detversi_tanggal_dokumentasi = moment($(this).data('versi_tanggal_dokumentasi')).format('Do MMMM YYYY');
        var detversi_keterangan_dokumentasi = $(this).data('versi_keterangan_dokumentasi');
        var detversi_link_dokumentasi = $(this).data('versi_link_dokumentasi');
        var detversi_d_id_lokasi = $(this).data('versi_d_id_lokasi');
        var detversi_d_nama_lokasi = $(this).data('versi_d_nama_lokasi');
        var detversi_d_id_kontainer = $(this).data('versi_d_id_kontainer');
        var detversi_d_nama_kontainer = $(this).data('versi_d_nama_kontainer');
        var detversi_d_id_posisi_kontainer = $(this).data('versi_d_id_posisi_kontainer');
        var detversi_d_label_posisi_kontainer = $(this).data('versi_d_label_posisi_kontainer');

        $(".modal-body #detversi_nomor_versi").html( detversi_nomor_versi );
        $(".modal-body #detversi_tanggal_versi").html( detversi_tanggal_versi );
        $(".modal-body #detversi_rincian_versi").html( detversi_rincian_versi );
        if(detversi_id_sourcecode != '-') {
          $(".modal-body #detversi_link_sourcecode").attr("href", "/downloadSourceCode/" + detversi_id_sourcecode);
        }
        $(".modal-body #detversi_keterangan_sourcecode").html( detversi_keterangan_sourcecode );
        var lokasi_sourcecode = '';
        if(detversi_as_id_lokasi != '-') {
            lokasi_sourcecode = lokasi_sourcecode + detversi_as_nama_lokasi;
            if(detversi_as_id_kontainer != '-') {
                lokasi_sourcecode = lokasi_sourcecode + ' >> ' + detversi_as_nama_kontainer;
                if(detversi_as_id_posisi_kontainer != '-') {
                    lokasi_sourcecode = lokasi_sourcecode + ' >> ' + detversi_as_label_posisi_kontainer;
                }
            }
        } else {
            lokasi_sourcecode = '-';
        }
        $(".modal-body #detversi_lokasi_sourceode").html(lokasi_sourcecode);
        if(detversi_id_dokumentasi != '-') {
          $(".modal-body #detversi_link_dokumentasi").attr("href", "/downloadDokumentasi/" + detversi_id_dokumentasi);
        }
        $(".modal-body #detversi_keterangan_dokumentasi").html( detversi_keterangan_dokumentasi );
        var lokasi_dokumentasi = '';
        if(detversi_d_id_lokasi != '-') {
            lokasi_dokumentasi = lokasi_dokumentasi + detversi_d_nama_lokasi;
            if(detversi_d_id_kontainer != '-') {
                lokasi_dokumentasi = lokasi_dokumentasi + ' >> ' + detversi_d_nama_kontainer;
                if(detversi_d_id_posisi_kontainer != '-') {
                    lokasi_dokumentasi = lokasi_dokumentasi + ' >> ' + detversi_d_label_posisi_kontainer;
                }
            }
        } else {
            lokasi_dokumentasi = '-';
        }
        $(".modal-body #detversi_lokasi_dokumentasi").html(lokasi_dokumentasi);
    });

    $(document).on("click", ".open-modalUbahVersi", function () {
        console.log($(this));
        var editversi_id_aplikasi = $(this).data('versi_id_aplikasi');
        var editversi_id_versi = $(this).data('versi_id_versi');
        var editversi_nomor_versi = $(this).data('versi_nomor_versi');
        var editversi_tanggal_versi = $(this).data('versi_tanggal_versi');
        var editversi_rincian_versi = $(this).data('versi_rincian_versi');
        var editversi_id_sourcecode = $(this).data('versi_id_sourcecode');
        var editversi_nama_sourcecode = $(this).data('versi_nama_sourcecode');
        var editversi_link_sourcecode = $(this).data('versi_link_sourcecode');
        var editversi_keterangan_sourcecode = $(this).data('versi_keterangan_sourcecode');
        var editversi_as_id_lokasi = $(this).data('versi_as_id_lokasi');
        var editversi_as_nama_lokasi = $(this).data('versi_as_nama_lokasi');
        var editversi_as_id_kontainer = $(this).data('versi_as_id_kontainer');
        var editversi_as_nama_kontainer = $(this).data('versi_as_nama_kontainer');
        var editversi_as_id_posisi_kontainer = $(this).data('versi_as_id_posisi_kontainer');
        var editversi_as_label_posisi_kontainer = $(this).data('versi_as_label_posisi_kontainer');
        var editversi_id_dokumentasi = $(this).data('versi_id_dokumentasi');
        var editversi_nama_dokumentasi = $(this).data('versi_nama_dokumentasi');
        var editversi_tanggal_dokumentasi = moment($(this).data('versi_tanggal_dokumentasi')).format('Do MMMM YYYY');
        var editversi_keterangan_dokumentasi = $(this).data('versi_keterangan_dokumentasi');
        var editversi_link_dokumentasi = $(this).data('versi_link_dokumentasi');
        var editversi_d_id_lokasi = $(this).data('versi_d_id_lokasi');
        var editversi_d_nama_lokasi = $(this).data('versi_d_nama_lokasi');
        var editversi_d_id_kontainer = $(this).data('versi_d_id_kontainer');
        var editversi_d_nama_kontainer = $(this).data('versi_d_nama_kontainer');
        var editversi_d_id_posisi_kontainer = $(this).data('versi_d_id_posisi_kontainer');
        var editversi_d_label_posisi_kontainer = $(this).data('versi_d_label_posisi_kontainer');

        var tgltemp = editversi_tanggal_versi.split(" ");
        var tgl2temp = tgltemp[0].split("-");
        // alert(tgl2temp);
        var d = (tgl2temp[0]) + "-" + (tgl2temp[1]) + "-" + (tgl2temp[2]);
        // alert(d);
        $(".modal-body #editversi_id_aplikasi").val( editversi_id_aplikasi );
        $(".modal-body #editversi_id_versi").val( editversi_id_versi );
        $(".modal-body #editversi_nomor_versi").val( editversi_nomor_versi );
        $(".modal-body #editversi_tanggal_versi").val( d );
        $(".modal-body #editversi_rincian_versi").val( editversi_rincian_versi );

        document.getElementById("editversi_id_aplikasi").innerHTML = editversi_id_aplikasi;
        document.getElementById("editversi_id_versi").innerHTML = editversi_id_versi;
        document.getElementById("editversi_nomor_versi").innerHTML = editversi_nomor_versi;
        document.getElementById("editversi_tanggal_versi").innerHTML = d;
        document.getElementById("editversi_rincian_versi").innerHTML = editversi_rincian_versi;
    });


    $(document).on("click", ".open-modalLihatScreenshot", function () {
        console.log($(this));
        var detscreenshot_id_aplikasi = $(this).data('screenshot_id_aplikasi');
        // alert(detscreenshot_id_aplikasi);
        var detscreenshot_id_gambar = $(this).data('screenshot_id_gambar'); 
        var detscreenshot_nama_gambar = $(this).data('screenshot_nama_gambar');
        var detscreenshot_link_gambar = $(this).data('screenshot_link_gambar');
        var detscreenshot_keterangan_gambar = $(this).data('screenshot_keterangan_gambar');
        // alert(detscreenshot_nama_gambar);

        $(".modal-body #detscreenshot_id_aplikasi").html( detscreenshot_id_aplikasi );
        $(".modal-body #detscreenshot_id_gambar").html( detscreenshot_id_gambar );
        $(".modal #detscreenshot_nama_gambar").html( detscreenshot_nama_gambar );

        if(detscreenshot_link_gambar != '-') {
          $(".modal-body #detscreenshot_link_gambar").attr("src", detscreenshot_link_gambar.replace('public', '/storage'));
        }
        $(".modal-body #detscreenshot_keterangan_gambar").html( detscreenshot_keterangan_gambar );
    });

    $(document).on("change", "#dynamic-file_screenshot-tambah", function (e) {
      var gambar = this;
      if (gambar.files && gambar.files[0]) {
        var reader = new FileReader();
        reader.onload = function (event) {
          $('#ss-tambah-preview').attr('src', event.target.result);
        };
        reader.readAsDataURL(gambar.files[0]);
      }
    });

});  
</script>

@endsection



