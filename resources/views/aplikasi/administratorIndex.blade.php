@extends('layouts.master')
@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card card-profile">
      <!-- <img class="card-img-top" src="/img/cover.jpg" alt="Card image cap"> -->
      <div class="row">
        <div class="card-avatar">
          <a href="#pablo">
            <img class="img" src="/img/faces/marc.jpg" />
          </a>
        </div>
      </div>
      <div class="card-body">
        <div data-image="/img/sidebar-1.jpg">
          <h4 class="card-category text-gray">SPRI</h4>
          <h3 class="card-title text-primary"><strong>Surat Perjalanan Republik Indonesia</strong></h3>
          <h5 class="card-title text-gray"><i>Paket Pengadaan Infrastruktur Sistem Paspor Republik Indonesia</i></h5>
        </div>
        <hr>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row col-md-12">
            <h5><strong class="text-align"><u>ADMINISTRATOR APLIKASI SPRI</u></strong>
              <button type="button" rel="tooltip" title="Tambah Administrator" class="btn btn-primary btn-link btn-sm" data-toggle="modal" data-target=".modalTambahAdministrator" data-backdrop="static" data-keyboard="false"><i class="fa fa-plus-circle"></i></button></h5>
          </div>
          <br>
          <div style="height: 500px; overflow-x: auto; overflow-y: auto;">
            <table class="table table-bordered table-responsive table-hover">
              <colgroup>
                <col style="width:5%;">
                <col style="width:20%;">
                <col style="width:15%;">
                <col style="width:15%;">
                <col style="width:20%;">
                <col style="width:10%;">
                <col style="width:10%;">
                <col style="width:5%;">
              </colgroup>
              <thead class="text-primary" align="center">
                <th>No</th>
                <th>Username</th>
                <th>Password</th>
                <th>Koneksi</th>
                <th>Keterangan</th>
                <th>Created</th>
                <th>Updated</th>
                <th>Aksi</th>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>rinrina.rahma@gmail.com</td>
                  <td>151131813168461</td>
                  <td>SPRI Non-Mobile</td>
                  <td> dfihe uhqiuehf9p e[qhfqheflqehf lqehf </td>
                  <td> oiqhehiqehlqkewh oiq3bckl</td>
                  <td>asdasd dbfkj asdkfagsdkf gasdkf gaksdfkasdfhg kadh</td>
                  <td align="center">
                    <h4>
                      <small>
                        <a href="#" rel="tooltip" title="Ubah Administrator" class="text-success" data-toggle="modal" data-target=".modalUbahAdministrator" data-backdrop="static" data-keyboard="false"><i class="fa fa-pencil"></i></a>&emsp;
                        <a href="#" rel="tooltip" title="Hapus Administrator" class="text-danger" data-toggle="modal" data-target=".modalHapusAdministrator" data-backdrop="static" data-keyboard="false"><i class="fa fa-trash"></i></a>
                      </small>
                    </h4>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

<!-- POPUP TAMBAH ADMINISTRATOR -->
<div class="modal fade modalTambahAdministrator" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong><small>Aplikasi SPRI</small><br><i class="text-primary">TAMBAH ADMINISTRATOR BARU</i></strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Username
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <input type="text" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Password</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <input type="password" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Konfirmasi Password</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <input type="password" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Koneksi</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <select class="form-control" id="exampleFormControlSelect1">
                <option selected>Pilih...</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Tambah Administrator Baru</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP UBAH KONEKSI -->
<div class="modal fade modalUbahAdministrator" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><small><strong class="text-align">UBAH ADMINISTRATOR</strong></small><br><i class="text-primary">rinrina.rahma@gmail.com</i></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Username
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <input type="text" class="form-control" disabled="disabled" readonly="readonly">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Password Lama</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <input type="password" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Password Baru</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <input type="password" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Konfirmasi Password Baru</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <input type="password" class="form-control">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              <i>Koneksi</i>
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <select class="form-control" id="exampleFormControlSelect1">
                <option selected>Pilih...</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-4 col-sm-4 col-xs-4">
              Keterangan
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 editData">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tutup</button>
        <button type="button" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan</button>
      </div>
    </div>
  </div>
</div>

<!-- POPUP HAPUS KONEKSI -->
<div class="modal fade modalHapusAdministrator" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS KONEKSI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus administrator <i class="text-primary">rinrina.rahma@gmail.com</i>?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
        <button type="button" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
      </div>
    </div>
  </div>
</div>