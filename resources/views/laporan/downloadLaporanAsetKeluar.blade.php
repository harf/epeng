<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>

<p>Laporan Aset Keluar Nomor #{{ $nomor_aset_keluar }}</p>
<br>
@foreach($aset_keluar as $key => $value)
    <p>Tanggal Aset Keluar: {{ strftime( "%d %B %Y", strtotime($value->tanggal_aset_keluar)) }}</p>
    <p>Jenis Aset Keluar: {{ $value->jenis_aset_keluar }}</p>
    <p>Tujuan Aset Keluar: {{ ($value->nama_kanim)?$value->nama_kanim:$value->lokasi_keluar_lain }}</p>
    <p>Total Jumlah Aset Keluar: {{ $value->jumlah_aset_keluar }}</p>
    <p>Keterangan Aset Keluar: {{ ($value->keterangan_aset_keluar)?$value->keterangan_aset_keluar:'-' }}</p>
@endforeach
<br>
<table>
    <thead>
    <tr>
        <th>No.</th>
        <th>Merek</th>
        <th>Tipe</th>
        <th>Jenis</th>
        <th>Kategori</th>
        <th>Field Aset</th>
        <th>Isian Field Aset</th>
    </tr>
    </thead>
    <tbody>

    @foreach($list_download_item_aset_keluar as $key => $value)
        <tr>
            <td>{{ ($key + 1) }}</td>
            <td>{{ $value->nama_merek }}</td>
            <td>{{ $value->tipe_aset }}</td>
            <td>{{ $value->nama_jenis_aset }}</td>
            <td>{{ $value->nama_kategori_aset }}</td>
            <td>{{ $value->asa_field_aset }}</td>
            <td>{{ $value->asa_isian_aset }}</td>
        </tr>
    @endforeach
    </tbody>
</table>