<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>
<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/laporanAsetMasuk">Laporan Aset Masuk</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Detail #{{ $nomor_aset_masuk }}</u></li>
  </ol>
</nav>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    @foreach ($aset_masuk as $key => $value)
      <div class="card card-profile">
        <div class="card-header card-header-primary">
          <h4 class="card-title"><strong>Laporan Aset Masuk Nomor #{{ $value->nomor_aset_masuk }}</strong></h4>
        </div>
        <div class="card-body" align="left">
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <div class="form-group row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  Tanggal Aset Masuk
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                  <strong class="text-primary">{{ strftime( "%d %B %Y", strtotime($value->tanggal_aset_masuk)) }}</strong>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  Total Jumlah Aset Masuk
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                  <strong class="text-primary">{{ $value->jumlah_aset_masuk }} aset</strong>
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <div class="form-group row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  Sumber Aset Masuk
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                  <strong class="text-primary">{{ ($value->nama_pengadaan)?$value->nama_pengadaan:'Non pengadaan' }}</strong>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  Keterangan Aset Masuk
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                  <strong class="text-primary">{{ ($value->keterangan_aset_masuk)?$value->keterangan_aset_masuk:'-' }}</strong>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    @endforeach
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h4 class="card-title"><strong>Dokumen Aset Masuk</strong></h4>
        </div>
        <br>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <table class="table table-border table-hover">
            <colgroup>
              <col style="width:5%;">
              <col style="width:30%;">
              <col style="width:15%;">
              <col style="width:20%;">
              <col style="width:30%;">
            </colgroup>
            <thead class="thead-light">
              <tr>
                <th>NO.</th>
                <th>DOKUMEN</th>
                <th>TANGGAL DOKUMEN</th>
                <th>KETERANGAN DOKUMEN</th>
                <th>DOKUMEN FISIK DISIMPAN DI</th>
              </tr>
            </thead>
            <tbody>
              @if(count($list_dokumentasi_aset_masuk) != 0)
                @foreach($list_dokumentasi_aset_masuk as $key => $value)
                  <tr>
                    <td>{{ ($key + 1) }}.</td>
                    <td><u><a href="/downloadDokumentasi/{{ $value->id_dokumentasi }}">{{ $value->nama_dokumentasi }} <i class="fa fa-download"></i></a></u></td>
                    <td>{{ strftime( "%d %B %Y", strtotime($value->tanggal_dokumentasi)) }}</td>
                    <td>{{ $value->keterangan_dokumentasi }}</td>
                    <td>
                      {{ $value->id_lokasi ? $value->nama_lokasi : ''  }} {{ $value->id_kontainer ? ' >> ' . $value->nama_kontainer : '' }} {{ $value->id_posisi_kontainer ?' >> ' . $value->label_posisi_kontainer : '' }}
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5"><center><i>Tidak ada data.</i></center></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h4 class="card-title"><strong>Rincian Item Aset Masuk</strong></h4>
        </div>
        <br>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <table class="table table-bordered table-sm" id="table-laporan-aset-masuk">
            <colgroup>
              <col style="width:3%;">
              <col style="width:5%;">
              <col style="width:20%;">
              <col style="width:30%;">
              <col style="width:20%;">
              <col style="width:15%;">
              <col style="width:7%;">
            </colgroup>
            <thead class="thead-light">
              <tr>
                <th></th>
                <th><strong>NO.</strong></th>
                <th colspan="2"><strong>ITEM ASET MASUK</strong></th>
                <th><strong>JENIS ASET</strong></th>
                <th><strong>KATEGORI ASET </strong></th>
                <th><strong>JUMLAH</strong></th>
              </tr>
            </thead>
            <tbody>
              @if($list_item_aset_masuk)
                @foreach ($list_item_aset_masuk as $key => $value)
                  <tr>
                    <td id="aset-expandable-td_{{ $value->id_aset_masuk }}-{{ $value->id_aset }}" style="cursor: pointer;">
                      <button type="button" id="btn-aset-exp-{{ $value->id_aset_masuk }}-{{ $value->id_aset }}" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
                    </td>
                    <td>{{ ($key + 1) }}.<input type="hidden" id="check-aset-{{ $value->id_aset_masuk }}-{{ $value->id_aset }}" value="0"></td>
                    <td><img src="{{ str_replace('public', '/storage', $value->link_gambar) }}" alt="{{ $value->nama_gambar }}" class="gambar-aset-thumbnails"></td>
                    <td>{{ $value->nama_merek }} {{ $value->tipe_aset }}</td>
                    <td>{{ $value->nama_jenis_aset }}</td>
                    <td>{{ $value->nama_kategori_aset }}</td>
                    <td>{{ $value->jumlah_aset_masuk }} {{ $value->satuan_stok }}</td>
                  </tr>
                  <tr id="tr-satuan-aset-{{ $value->id_aset_masuk }}-{{ $value->id_aset }}">
                    <td colspan="7">
                      <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="hidden-satuan-aset-{{ $value->id_aset_masuk }}-{{ $value->id_aset }}">
                        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          Satuan Item yang Masuk:
                        </div>
                        <hr>
                        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <table class="table table-bordered table-hover table-sm">
                            <colgroup>
                              <col style="width:10%;">
                              <col style="width:20%;">
                              <col style="width:30%;">
                              <col style="width:20%;">
                              <col style="width:20%;">
                            </colgroup>
                            <thead class="thead-light" style="vertical-align: middle; text-align: center;">
                              <tr>
                                <th>Satuan #</th>
                                <th>Field Aset</th>
                                <th>Isian Field Aset</th>
                                <th>Harga</th>
                                <th>Status Saat Ini</th>
                                <th>Keterangan Aset</th>
                              </tr>
                            </thead>
                            <tbody id="tbody-satuan-aset-{{ $value->id_aset_masuk }}-{{ $value->id_aset }}">
                             
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="7"><center><i>Tidak ada data.</i></center></td>
                </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
    </div>  
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $("[id^=tr-satuan-aset-]").hide();

    $(document).on("click", "[id^=aset-expandable-td_]", function (e) {
      e.preventDefault();
      var target = e.currentTarget.id.split('_').pop();
      var targetParent = target.split('-')[0];
      var targetChild = target.split('-')[1];

      if($('#btn-aset-exp-' + targetParent + '-' + targetChild).length) {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-col-' + targetParent +'-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-minus-circle"></i></button>');
        if($('#check-aset-' + targetParent + '-' + targetChild).val() == 0) {
          var last_kode_grup = 0;
          var count_grup_satuan = 0;
          var index_col = 0;
          $.ajax({
            type:"GET",
            url:"{{url('api/get-satuan-aset-masuk')}}?id_aset_masuk=" + targetParent + "&id_aset=" + targetChild,
            success:function(res){
              if(res.length){
                var last_index_list = parseInt(res.length) - 1;
                $.each(res,function(index,Obj){
                  console.log(res);
                  var status_satuan_aset = '-';
                  var keterangan_satuan_aset = '<a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-link btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a>';
                  
                  if (Obj.asa_id_aset_keluar) {
                    if (Obj.ake_jenis_aset_keluar == 'Alokasi')
                    {
                      status_satuan_aset = '<i class="text-success">Sudah dialokasikan ke ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' pada tanggal ' + moment(Obj.ake_tanggal_aset_keluar).format('Do MMMM YYYY') + '.</i>';
                    }
                    else
                    {
                      status_satuan_aset = '<i class="text-success">Aset dipinjamkan kepada ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                        // keterangan_satuan_aset = 'Penerima: ' + Obj.penerima_aset;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' dan harus dikembalikan pada tanggal ' + moment(Obj.ake_tanggal_aset_kembali).format('Do MMMM YYYY') + '.</i>'
                    }
                  } else {
                    if (Obj.asa_id_lokasi) {
                      status_satuan_aset = 'Belum dialokasikan dan masih tersimpan di ' + Obj.asa_nama_lokasi;
                      if (Obj.asa_id_kontainer) {
                        status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_nama_kontainer;
                        if(Obj.asa_id_posisi_kontainer) {
                          status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_label_posisi_kontainer;
                        }
                      } 
                    }
                  }

                  if(Obj.asa_kode_aset_informasi) {
                    var keterangan_satuan_aset = '<a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a>';
                  }

                  if (last_kode_grup == 0) {
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + keterangan_satuan_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                    index_col = index_col + 1;
                  } else if (Obj.asa_kode_grup_satuan == last_kode_grup) {
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));

                    if(index == last_index_list)
                    {
                      $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      last_kode_grup = '';                    
                      count_grup_satuan = 0;
                      index = 0;
                    }
                  } else {
                    $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                    last_kode_grup = '';                    
                    count_grup_satuan = 0;
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + keterangan_satuan_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                    index_col = index_col + 1;
                  }
                  $('#check-satuan-aset-' + targetParent + '-' + targetChild).val(1);
                  last_kode_grup = Obj.asa_kode_grup_satuan;
                });
                $('#check-aset-' + targetParent + '-' + targetChild).val(1);
              } else {
                var emptyRowSatuan = '<tr><td colspan="6"><i>Tidak ada data.</i></td></tr>';
                $(emptyRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                $('#check-aset-' + targetParent + '-' + targetChild).val(0);
              }
           }, error:function(x) {
            alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
            console.log(x);
           }
          });
        } else {
          $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "");
        }
      } else {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-exp-' + targetParent + '-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>');
        $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "display: none");
      }
      $('#tr-satuan-aset-' + targetParent + '-' + targetChild).slideToggle(); 
    }); 
  });
</script>

@endsection