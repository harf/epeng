<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>
<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/laporanPersebaranAset">Laporan Persebaran Aset</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Detail</u></li>
  </ol>
</nav>
<div class="row">
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <input type="hidden" id="kode-kanim" value="{{ $kode_kanim }}">
    @foreach ($list_persebaran_aset as $key => $value)
      <div class="card card-profile">
        <div class="card-header card-header-primary">
          <h4 class="card-title"><strong>Laporan Persebaran Aset di {{ ($value->nama_kanim)?$value->nama_kanim:$value->lokasi_keluar_lain }}</strong></h4>
        </div>
        <div class="card-body" align="left">
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="card-title"><strong>Rincian Item Persebaran</strong></h4>
          </div>
          <br>
          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <table class="table table-bordered table-hover table-sm" id="table-laporan-aset-masuk">
              <colgroup>
                <col style="width:3%;">
                <col style="width:5%;">
                <col style="width:20%;">
                <col style="width:30%;">
                <col style="width:20%;">
                <col style="width:15%;">
                <col style="width:7%;">
              </colgroup>
              <thead class="thead-light">
                <tr>
                  <th></th>
                  <th><strong>NO.</strong></th>
                  <th colspan="2"><strong>ITEM PERSEBARAN</strong></th>
                  <th><strong>JENIS ASET</strong></th>
                  <th><strong>KATEGORI ASET </strong></th>
                  <th><strong>JUMLAH</strong></th>
                </tr>
              </thead>
              <tbody>
                <?php $index_item = 0; ?>
                @if(count($list_item_persebaran_aset) != 0)
                  @foreach ($list_item_persebaran_aset as $key2 => $value2)
                    @if($kode_kanim != 0)
                      @if($value->kode_kanim == $value2->kode_kanim)
                        <tr>
                          <td id="aset-expandable-td_{{ $value2->id_aset_keluar }}-{{ $value2->id_aset }}" style="cursor: pointer;">
                            <button type="button" id="btn-aset-exp-{{ $value2->id_aset_keluar }}-{{ $value2->id_aset }}" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
                          </td>
                          <td>{{ ++$index_item }}.<input type="hidden" id="check-aset-{{ $value2->id_aset_keluar }}-{{ $value2->id_aset }}" value="0"></td>
                          <td><img src="{{ str_replace('public', '/storage', $value2->link_gambar) }}" alt="{{ $value2->nama_gambar }}" class="gambar-aset-thumbnails"></td>
                          <td>{{ $value2->nama_merek }} {{ $value2->tipe_aset }}</td>
                          <td>{{ $value2->nama_jenis_aset }}</td>
                          <td>{{ $value2->nama_kategori_aset }}</td>
                          <td>{{ $value2->jumlah_aset_keluar }} {{ $value2->satuan_stok }}</td>
                        </tr>
                        <tr id="tr-satuan-aset-{{ $value2->id_aset_keluar }}-{{ $value2->id_aset }}">
                          <td colspan="7">
                            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="hidden-satuan-aset-{{ $value2->id_aset_keluar }}-{{ $value2->id_aset }}">
                              <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                Satuan Item:
                              </div>
                              <hr>
                              <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table class="table table-bordered table-hover table-sm">
                                  <colgroup>
                                    <col style="width:10%;">
                                    <col style="width:20%;">
                                    <col style="width:30%;">
                                    <col style="width:20%;">
                                    <col style="width:20%;">
                                  </colgroup>
                                  <thead class="thead-light" style="vertical-align: middle; text-align: center;">
                                    <tr>
                                      <th>Satuan #</th>
                                      <th>Field Aset</th>
                                      <th>Isian Field Aset</th>
                                      <th>Harga</th>
                                      <th>Status Saat Ini</th>
                                      <th>Keterangan Aset</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody-satuan-aset-{{ $value2->id_aset_keluar }}-{{ $value2->id_aset }}">
                                   
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </td>
                        </tr>
                      @endif
                    @else
                      @if($value->nomor_aset_keluar == $value2->nomor_aset_keluar)
                        <tr>
                          <td id="aset-expandable-td_{{ $value2->id_aset_keluar}}-{{ $value2->id_aset }}" style="cursor: pointer;">
                            <button type="button" id="btn-aset-exp-{{ $value2->id_aset_keluar}}-{{ $value2->id_aset }}" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
                          </td>
                          <td>{{ ($index_item + 1) }}.<input type="hidden" id="check-aset-{{ $value2->id_aset_keluar}}-{{ $value2->id_aset }}" value="0"></td>
                          <td><img src="{{ str_replace('public', '/storage', $value2->link_gambar) }}" alt="{{ $value2->nama_gambar }}" class="gambar-aset-thumbnails"></td>
                          <td>{{ $value2->nama_merek }} {{ $value2->tipe_aset }}</td>
                          <td>{{ $value2->nama_jenis_aset }}</td>
                          <td>{{ $value2->nama_kategori_aset }}</td>
                          <td>{{ $value2->jumlah_aset_keluar }} {{ $value2->satuan_stok }}</td>
                        </tr>
                        <tr id="tr-satuan-aset-{{ $value2->id_aset_keluar}}-{{ $value2->id_aset }}">
                          <td colspan="7">
                            <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="hidden-satuan-aset-{{ $value2->id_aset_keluar}}-{{ $value2->id_aset }}">
                              <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                Satuan Item:
                              </div>
                              <hr>
                              <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table class="table table-bordered table-hover table-sm">
                                  <colgroup>
                                    <col style="width:10%;">
                                    <col style="width:20%;">
                                    <col style="width:30%;">
                                    <col style="width:20%;">
                                    <col style="width:20%;">
                                  </colgroup>
                                  <thead class="thead-light" style="vertical-align: middle; text-align: center;">
                                    <tr>
                                      <th>Satuan #</th>
                                      <th>Field Aset</th>
                                      <th>Isian Field Aset</th>
                                      <th>Harga</th>
                                      <th>Status Saat Ini</th>
                                      <th>Keterangan Aset</th>
                                    </tr>
                                  </thead>
                                  <tbody id="tbody-satuan-aset-{{ $value2->id_aset_keluar}}-{{ $value2->id_aset }}">
                                   
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </td>
                        </tr>
                      @endif
                    @endif
                  @endforeach
                @else
                  <tr>
                    <td colspan="6"><i>Tidak ada data.</i></td>
                  </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <br>
      <hr>
      <br>
    @endforeach
  </div>
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $("[id^=tr-satuan-aset-]").hide();

    $(document).on("click", "[id^=aset-expandable-td_]", function (e) {
      e.preventDefault();
      var target = e.currentTarget.id.split('_').pop();
      var targetParent = target.split('-')[0];
      var targetChild = target.split('-')[1];

      if($('#btn-aset-exp-' + targetParent + '-' + targetChild).length) {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-col-' + targetParent +'-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-minus-circle"></i></button>');
        if($('#check-aset-' + targetParent + '-' + targetChild).val() == 0) {
          var last_kode_grup = 0;
          var count_grup_satuan = 0;
          var index_col = 0;
          $.ajax({
            type:"GET",
            url:"{{url('api/get-satuan-aset-persebaran')}}?id_aset_keluar=" + targetParent + "&id_aset=" + targetChild,
            success:function(res){
              if(res.length){
                console.log(res);
                var last_index_list = parseInt(res.length) - 1;
                $.each(res,function(index,Obj){
                  var status_satuan_aset = '-';
                  var keterangan_satuan_aset = '<a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-link btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a>';
                  
                  if (Obj.asa_id_aset_keluar) {
                    if (Obj.ake_jenis_aset_keluar == 'Alokasi')
                    {
                      status_satuan_aset = '<i class="text-success">Sudah dialokasikan ke ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' pada tanggal ' + moment(Obj.ake_tanggal_aset_keluar).format('Do MMMM YYYY') + '.</i>';
                    }
                    else
                    {
                      status_satuan_aset = '<i class="text-success">Aset dipinjamkan kepada ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                        // keterangan_satuan_aset = 'Penerima: ' + Obj.penerima_aset;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' dan harus dikembalikan pada tanggal ' + moment(Obj.ake_tanggal_aset_kembali).format('Do MMMM YYYY') + '.</i>'
                    }
                  } else {
                    if (Obj.asa_id_lokasi) {
                      status_satuan_aset = 'Belum dialokasikan dan masih tersimpan di ' + Obj.asa_nama_lokasi;
                      if (Obj.asa_id_kontainer) {
                        status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_nama_kontainer;
                        if(Obj.asa_id_posisi_kontainer) {
                          status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_label_posisi_kontainer;
                        }
                      } 
                    }
                  }

                  if(Obj.asa_kode_aset_informasi) {
                    var keterangan_satuan_aset = '<a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a>';
                  }

                  if (last_kode_grup == 0) {
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + keterangan_satuan_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                    index_col = index_col + 1;
                  } else if (Obj.asa_kode_grup_satuan == last_kode_grup) {
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));

                    if(index == last_index_list)
                    {
                      $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      last_kode_grup = '';                    
                      count_grup_satuan = 0;
                      index = 0;
                    }
                  } else {
                    $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                    last_kode_grup = '';                    
                    count_grup_satuan = 0;
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + keterangan_satuan_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                    index_col = index_col + 1;
                  }
                  $('#check-satuan-aset-' + targetParent + '-' + targetChild).val(1);
                  last_kode_grup = Obj.asa_kode_grup_satuan;
                });
                $('#check-aset-' + targetParent + '-' + targetChild).val(1);
              } else {
                var emptyRowSatuan = '<tr><td colspan="6"><i>Tidak ada data.</i></td></tr>';
                $(emptyRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                $('#check-aset-' + targetParent + '-' + targetChild).val(0);
              }
           }, error:function(x) {
            alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
            console.log(x);
           }
          });
        } else {
          $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "");
        }
      } else {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-exp-' + targetParent + '-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>');
        $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "display: none");
      }
      $('#tr-satuan-aset-' + targetParent + '-' + targetChild).slideToggle(); 
    }); 
  });
</script>

@endsection