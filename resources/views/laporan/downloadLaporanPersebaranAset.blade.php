<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>

<p>Laporan Persebaran Aset di {{ $nama_lokasi }}</p>
<br>
<table>
    <thead>
    <tr>
        <th>No.</th>
        <th>Merek</th>
        <th>Tipe</th>
        <th>Jenis</th>
        <th>Kategori</th>
        <th>Field Aset</th>
        <th>Isian Field Aset</th>
        <th>Lokasi Persebaran</th>
    </tr>
    </thead>
    <tbody>

    @foreach($list_download_item_persebaran_aset as $key => $value)
        <tr>
            <td>{{ ($key + 1) }}</td>
            <td>{{ $value->nama_merek }}</td>
            <td>{{ $value->tipe_aset }}</td>
            <td>{{ $value->nama_jenis_aset }}</td>
            <td>{{ $value->nama_kategori_aset }}</td>
            <td>{{ $value->field_aset }}</td>
            <td>{{ $value->isian_aset }}</td>
            <td>{{ ($value->kode_kanim)?$value->nama_kanim:$value->lokasi_keluar_lain }}</td>
        </tr>
    @endforeach
    </tbody>
</table>