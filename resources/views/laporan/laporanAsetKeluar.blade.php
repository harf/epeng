<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>
<meta name="csrf-token" content="{{ csrf_token() }}" />

@section('content')
<div class="row">
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
  <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
            <div class="row">
              <div class="col-md-12">
                <h3 class="card-title text-primary">
                  <small>
                    <strong>
                      <i class="fa fa-paper-plane"></i>&emsp;&emsp;Laporan Aset Keluar
                    </strong>
                  </small>
                </h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <form method="GET" action="/laporanAsetKeluar?periode_dari={{ $periode_dari }}&periode_sampai={{ $periode_sampai }}&page_size={{ $page_size }}&page_number={{ $page_number }}">
          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
              <i>Periode Laporan</i>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <div class="input-group">
                <input type="date" name="periode_dari" id="periode-dari" class="form-control" value="{{ $periode_dari }}">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
              s.d.
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <div class="input-group">
                <input type="date" name="periode_sampai" id="periode-sampai" class="form-control" value="{{ $periode_sampai }}">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
            <input type="hidden" name="page_size" value="{{ $page_size }}">
            <input type="hidden" name="page_number" value="{{ $page_number }}">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
              <button type="submit" class="btn btn-primary btn-sm">Terapkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
    <hr>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row">
          <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
            <h5>
              @if(!$periode_dari)
                Menampilkan seluruh laporan aset keluar.
              @else
                Menampilkan laporan aset keluar dari <strong class='text-primary'>{{ strftime( "%d %B %Y", strtotime($periode_dari)) }} - {{ strftime( '%d %B %Y', strtotime($periode_sampai)) }}.</strong>
              @endif
            </h5>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="input-group input-group-sm mb-3">
              <div class="input-group-prepend">
                <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-eye"></i>&emsp;</span>
              </div>
              <input type="hidden" id="page-number" value="{{ $page_number }}">
              <select class="form-control input-sm" name="page_size" id="page-size" rel="tooltip" title="Tampilkan banyaknya jumlah data dalam satu halaman">
                <option value="15"  selected="true">15</option>
                <option value="30" @if($page_size == 30) selected="true" @endif>30</option>
                <option value="60" @if($page_size == 60) selected="true" @endif>60</option>
                <option value="120" @if($page_size == 120) selected="true" @endif>120</option>
                <option value="240" @if($page_size == 240) selected="true" @endif>240</option>
                <option value="480" @if($page_size == 480) selected="true" @endif>480</option>
                <option value="960" @if($page_size == 960) selected="true" @endif>960</option>
              </select>
              <div class="input-group-prepend">
                <span class="input-group-text text-primary" id="inputGroup-sizing-sm">&emsp;data</span>
              </div>
            </div>
          </div>
        </div>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <table class="table table-bordered table-hover table-sm" id="table-laporan-aset-keluar">
            <colgroup>
              <col style="width:15%;">
              <col style="width:20%;">
              <col style="width:10%;">
              <col style="width:20%;">
              <col style="width:10%;">
              <col style="width:10%;">
            </colgroup>
            <thead class="thead-primary" style="vertical-align: middle; text-align: center;">
              <tr>
                <th><strong>NOMOR ASET KELUAR</strong></th>
                <th><strong>TANGGAL ASET KELUAR</strong></th>
                <th><strong>JENIS ASET KELUAR</strong></th>
                <th><strong>TUJUAN ASET KELUAR</strong></th>
                <th><strong>JUMLAH ASET KELUAR</strong></th>
                <th><strong>AKSI</strong></th>
              </tr>
            </thead>
            <tbody>
              @if($list_aset_keluar)
                @foreach($list_aset_keluar as $key => $value)
                  <tr>
                    <td style="vertical-align: middle; text-align: center;">#{{ $value->nomor_aset_keluar }}</td>
                    <td>{{ strftime( "%d %B %Y", strtotime($value->tanggal_aset_keluar)) }}</td>
                    <td>{{ $value->jenis_aset_keluar }}</td>
                    <td>{{ ($value->nama_kanim)?$value->nama_kanim:$value->lokasi_keluar_lain }}</td>
                    <td>{{ $value->jumlah_aset_keluar }} aset</td>
                    <td style="vertical-align: middle; text-align: center;">
                      <a href="/laporanAsetKeluar/{{ $value->nomor_aset_keluar }}" rel="tooltip" title="Lihat Laporan Aset Keluar Nomor #{{ $value->nomor_aset_keluar }}" class="text-info"><i class="fa fa-eye"></i></a>&emsp;
                      <a href="/downloadLaporanAsetKeluar/{{ $value->nomor_aset_keluar }}" rel="tooltip" title="Unduh Laporan Aset Keluar Nomor #{{ $value->nomor_aset_keluar }}" class="text-danger"><i class="fa fa-download"></i></a>
                    </td>
                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="5"><center><i>Tidak ada data.</i></center></td>
                </tr>
              @endif
            </tbody>
          </table>  
        </div>
      </div>
    </div>
    <br>
    <hr>
    <div class="row">
      <div class="pull-right">
        <nav aria-label="...">
          <ul class="pagination">
            @if($page_number == 1)
              <li class="page-item disabled">
                <span class="page-link">Sebelumnya</span>
              </li>
            @else
              <li class="page-item">
                <a class="page-link" href="?periode_dari={{ $periode_dari }}&periode_sampai={{ $periode_sampai }}&page_size={{ $page_size }}&page_number={{ $page_number-1 }}">Sebelumnya</a>
              </li>  
            @endif
            @for($i = 0; $i < $count_list_aset_keluar/$page_size; $i++)
              <li class="page-item @if($page_number == $i+1) active @endif">
                @if($page_number == $i+1)
                  <span class="page-link">
                    {{ $i+1 }}
                    <span class="sr-only">(current)</span>
                  </span>
                @else
                  <a class="page-link" href="?periode_dari={{ $periode_dari }}&periode_sampai={{ $periode_sampai }}&page_size={{ $page_size }}&page_number={{ $i+1 }}">{{ $i+1 }}</a>
                @endif
              </li>
            @endfor
            @if($page_number == ceil($count_list_aset_keluar/$page_size))
              <li class="page-item disabled">
                <span class="page-link">Selanjutnya</span>
              </li>
            @else
              <li class="page-item">
                <a class="page-link" href="?periode_dari={{ $periode_dari }}&periode_sampai={{ $periode_sampai }}&page_size={{ $page_size }}&page_number={{ $page_number+1 }}">Selanjutnya</a>
              </li> 
            @endif
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("select").select2();
  });
  $(document).ready(function(){
    $(document).on("change", "#page-size", function(e) {
      e.preventDefault();
      var targetId = e.target.value;
      var periode_dari = $("#periode-dari").val();
      var periode_sampai = $("#periode-sampai").val();

      if(!periode_dari || periode_dari == '') periode_dari = '';
      if(!periode_sampai || periode_sampai == '') periode_sampai = '';

      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      window.location.href = baseUrl + "?periode_dari=" + periode_dari + "&periode_sampai=" + periode_sampai + "&page_size=" + targetId + "&page_number=1";
    });
  });
</script>

@endsection