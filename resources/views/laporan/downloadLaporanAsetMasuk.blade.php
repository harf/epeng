<?php
  use Carbon\Carbon;
  setlocale (LC_TIME, 'id_ID');
?>

<p>Laporan Aset Masuk Nomor #{{ $nomor_aset_masuk }}</p>
<br>
@foreach($aset_masuk as $key => $value)
    <p>Tanggal Aset Masuk: {{ strftime( "%d %B %Y", strtotime($value->tanggal_aset_masuk)) }}</p>
    <p>Tanggal Jumlah Masuk: {{ $value->jumlah_aset_masuk }}</p>
    <p>Sumber Aset Masuk: {{ ($value->nama_pengadaan)?$value->nama_pengadaan:'Non pengadaan' }}</p>
    <p>Keterangan Aset Masuk: {{ ($value->keterangan_aset_masuk)?$value->keterangan_aset_masuk:'-' }}</p>
@endforeach
<br>
<table>
    <thead>
    <tr>
        <th>No.</th>
        <th>Merek</th>
        <th>Tipe</th>
        <th>Jenis</th>
        <th>Kategori</th>
        <th>Field Aset</th>
        <th>Isian Field Aset</th>
        <th>Harga</th>
        <th>Status Saat Ini</th>
        <th>Keterangan Aset</th>
    </tr>
    </thead>
    <tbody>

    @foreach($list_download_item_aset_masuk as $key => $value)
        <?php
            $status_satuan_aset = '';
        
        if($value->asa_id_aset_keluar != null || $value->asa_id_aset_keluar != 0)
        {
            if($value->ake_jenis_aset_keluar == 'Alokasi')
            {
                $status_satuan_aset = 'Sudah dialokasikan ke ';
                if((int)$value->ake_kode_kanim != null || (int)$value->ake_kode_kanim != 0) $status_satuan_aset = $status_satuan_aset . $value->ake_nama_kanim;
                else $status_satuan_aset = $status_satuan_aset . $value->ake_lokasi_keluar_lain;
                $status_satuan_aset = $status_satuan_aset . ' pada tanggal ' . $value->ake_tanggal_aset_keluar;
            }
            else
            {
                $status_satuan_aset = 'Aset dipinjamkan kepada ';
                if((int)$value->ake_kode_kanim != null || (int)$value->ake_kode_kanim != 0) $status_Satuan_aset = $status_satuan_aset . $value->ake_nama_kanim;
                else $status_satuan_aset = $status_satuan_aset + $value->ake_lokasi_keluar_lain;
                $status_satuan_aset = $status_satuan_aset . ' dan harus dikembalikan pada tanggal ' . $value->ake_tanggal_aset_kembali;
            }
        } else {
            $status_satuan_aset = 'Belum dialokasikan dan masih tersimpan di ' . $value->asa_nama_lokasi;
            if((int)$value->asa_id_kontainer != null || (int)$value->asa_id_kontainer != 0)
            {
                $status_satuan_aset = $status_satuan_aset . ' >> ' . $value->asa_nama_kontainer;
                if($value->asa_id_posisi_kontainer) $status_satuan_aset = $status_satuan_aset . ' >> ' . $value->asa_label_posisi_kontainer;
            }
        }
        ?>
        <tr>
            <td>{{ ($key + 1) }}</td>
            <td>{{ $value->nama_merek }}</td>
            <td>{{ $value->tipe_aset }}</td>
            <td>{{ $value->nama_jenis_aset }}</td>
            <td>{{ $value->nama_kategori_aset }}</td>
            <td>{{ $value->asa_field_aset }}</td>
            <td>{{ $value->asa_isian_aset }}</td>
            <td>{{ $value->asm_harga_satuan }}</td>
            <td>{{ $status_satuan_aset }}</td>
            <td>-</td>
        </tr>
    @endforeach
    </tbody>
</table>