
@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>

      
    <div class="col-sm-12" align="left">          
        <ul class="nav nav-tabs bg-purple">
            <li class="nav-item"><a class="nav-link" href="{{ route('kategori_aset') }}">Kategori Aset</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('jenis_aset') }}">Jenis Aset</a></li>
            <li class="nav-item"><a class="nav-link active" href="#">Merek Aset</a></li>
        </ul>
        <div class="panel panel-default">

            <div class="row">    
            
            <div class="col-md-4 col-sm-6 col-xs-12">           
                <div class="card card-stats">
                <form method="POST" action="{{ route('merek_aset_save') }}">
                    <input type="hidden" name="route" value="merek_aset">
                    {{ csrf_field() }} 
                    @csrf

                    <div class="form-group row"  align="center">
                        @if($mode == 'edit')
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp;&nbsp; EDIT MEREK ASET</h5>
                                <hr>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;&nbsp; TAMBAH MEREK ASET</h5>
                                <hr>
                            </div>
                        @endif

                        <div class="col-md-4" align="left">
                                <small>Jenis Aset</small>
                        </div>

                        <div class="col-md-8">
                            <select class="form-control input-sm" name="id_jenis_aset" id="id_jenis_aset">
                                <option value=""  selected="true">Pilih Jenis Aset</option>
                                @if($sign)
                                    @foreach ($listjenis as $jenis)
                                        <option value="{{ $jenis->id_jenis_aset }}"
                                        @if ( old('id_jenis_aset', $id_jenis_aset) == $jenis->id_jenis_aset )
                                            selected="selected"
                                        @endif
                                        >{{ str_replace('_',' ',$jenis->nama_jenis_aset) }}
                                        </option>
                                    @endforeach
                                @else
                                    @foreach ($list_jenis_aset as $jenis)
                                        <option value="{{ $jenis->id_jenis_aset }}">{{ $jenis->nama_jenis_aset }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>   

                        <div class="col-md-4" align="left">
                                <small>Merek Aset</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ($name)?$name:old('name') }}" placeholder="Jenis Aset" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Keterangan</small>
                        </div>
                        <div class="col-md-8">
                            <input id="keterangan" type="text" class="form-control{{ $errors->has('keterangan') ? ' is-invalid' : '' }}" name="keterangan" value="{{ ($keterangan)?$keterangan:old('keterangan') }}" placeholder="Keterangan" required autofocus>

                            @if ($errors->has('keterangan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('keterangan') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                 

                    <?php   if($mode == 'edit') { 
                                $button = 'Simpan';
                    ?>
                                <input type="hidden" name="mode" value="edit">                            
                                <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                            }
                            else {
                                $button = 'Tambah';
                    ?>
                                <input type="hidden" name="mode" value="create">
                    <?php    
                            }
                    ?>
                    <div class="form-group row" align="center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-login">
                                {{ $button }}
                            </button>                        
                        </div>
                    </div>
                </form>

                </div>
            </div>
                       
            <div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR MEREK ASET</h5>
                            <hr>
                        </div> 
                    
                   
                    @if(session('noticemessage'))
                        @if(session('flag') == '1')
                            <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                        @else
                            <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                        @endif
                        <div class="panel-heading" align="center" style="{{ $style }}">
                            <div class="form-group row" align="center">
                                <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                            </div>
                        </div> 
                        <br />
                    @endif     
                        <div class="col-sm-12">
                          <table class="table table-hover" align="center" width="70%">
                            <colgroup>
                              <col style="width:40%;">
                              <col style="width:40%;">
                              <col style="width:20%;">
                            </colgroup>
                            <thead>
                                <tr align="center">
                                    <th ><small> Jenis Aset </small></th>
                                    <th ><small> Merek </small></th>
                                    <th ><small> Keterangan </small></th>
                                    <th ><small> <span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp; Kelola </small></th>
                                </tr>
                            </thead>
                            <tbody> <?php $no=1; ?>
                                @foreach($listmerekaset as $key => $value)

                                <?php
                                    $jenis = App\Models\Ms_Jenis_Aset::getbyID($value->id_jenis_aset);
                                ?>

                                <tr align="center" style="height: 15">
                                    <td> <small>{{ $jenis->nama_jenis_aset }} </small></td>
                                    <td> <small>{{ $value->nama_merek }} </small></td>
                                    <td> <small>{{ $value->keterangan_merek }} </small></td>

                                    <td >                             
                                        <div class="row" >    
                                        <form action="/merek_aset_edit" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_merek }}">
                                            <input type="hidden" name="id_jenis_aset" value="{{ $jenis->id_jenis_aset }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_merek }}">
                                            <input type="hidden" name="keterangan" value="{{ $value->keterangan_merek }}">
                                            <input type="hidden" name="mode" value="edit">
                                            <input type="hidden" name="route" value="merek_aset">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitedit" name="submit" value="EDIT">
                                        </form>
                                        <form action="/merek_aset_delete" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_merek }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_merek }}">
                                            <input type="hidden" name="keterangan" value="{{ $value->keterangan_merek }}">
                                            <input type="hidden" name="mode" value="delete">
                                            <input type="hidden" name="route" value="merek_aset">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitdel" name="submit" value="DELETE">
                                        </form></small>
                                        </div>
                                    </td>  
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                          
                        </div> 
                         <div class="col-sm-12" align="center" >
                            <div style="text-align: center;">{{  $listmerekaset->links() }}</div>
                           
                        </div>                          

                    </div> 
    
                </div>
            </div>
        </div>

        </div>  
    </div>
@endsection