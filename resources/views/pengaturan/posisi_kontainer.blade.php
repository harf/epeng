
@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>

      
    <div class="col-sm-12" align="left">          
        <ul class="nav nav-tabs bg-purple">
          <li class="nav-item"><a class="nav-link" href="{{ route('dashboard_kontainer') }}">Dashboard Kontainer</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('jenis_kontainer') }}">Jenis Kontainer</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('kontainer') }}">Kontainer</a></li>
          <li class="nav-item"><a class="nav-link active" href="#">Posisi</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('lokasi_kontainer') }}">Lokasi</a></li>
        </ul>
        <div class="panel panel-default">

            <div class="row">    
            
            <div class="col-md-4 col-sm-6 col-xs-12">           
                <div class="card card-stats">
                <form method="POST" action="{{ route('posisi_kontainer_save') }}">
                    <input type="hidden" name="route" value="posisi_kontainer">
                    {{ csrf_field() }} 
                    @csrf

                    <div class="form-group row"  align="center">
                        @if($mode == 'edit')
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp;&nbsp; EDIT POSISI CONTAINER</h5>
                                <hr>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;&nbsp; TAMBAH POSISI CONTAINER</h5>
                                <hr>
                            </div>
                        @endif
                        
                        <div class="col-md-4" align="left">
                                <small>Label Kontainer</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ($name)?$name:old('name') }}" placeholder="Label Kontainer" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4" align="left">
                                <small>Kontainer</small>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control input-sm" name="id_kontainer" id="id_kontainer">
                                <option value="0"  selected="true">Pilih Kontainer</option>
                            @foreach ($listkontainer as $kontainer)
                                <option value="{{ $kontainer->id_kontainer }}"
                                @if ( old('id_kontainer', $id_kontainer) == $kontainer->id_kontainer )
                                    selected="selected"
                                @endif
                                >{{ str_replace('_',' ',$kontainer->nama_kontainer) }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>      

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Keterangan</small>
                        </div>
                        <div class="col-md-8">
                            <input id="keterangan" type="text" class="form-control{{ $errors->has('keterangan') ? ' is-invalid' : '' }}" name="keterangan" value="{{ ($keterangan)?$keterangan:old('keterangan') }}" placeholder="Keterangan" required autofocus>

                            @if ($errors->has('keterangan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('keterangan') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                 

                    <?php   if($mode == 'edit') { 
                                $button = 'Simpan';
                    ?>
                                <input type="hidden" name="mode" value="edit">                            
                                <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                            }
                            else {
                                $button = 'Tambah';
                    ?>
                                <input type="hidden" name="mode" value="create">
                    <?php    
                            }
                    ?>
                    <div class="form-group row" align="center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-login">
                                {{ $button }}
                            </button>                        
                        </div>
                    </div>
                </form>

                </div>
            </div>
                       
            <div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR POSISI CONTAINER</h5>
                            <hr>
                        </div> 
                    
                   
                    @if(session('noticemessage'))
                        @if(session('flag') == '1')
                            <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                        @else
                            <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                        @endif
                        <div class="panel-heading" align="center" style="{{ $style }}">
                            <div class="form-group row" align="center">
                                <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                            </div>
                        </div> 
                        <br />
                    @endif     
                        <div class="col-sm-12">
                          <table class="table table-hover" align="center" width="70%">
                            <colgroup>
                              <col style="width:20%;">
                              <col style="width:20%;">
                              <col style="width:20%;">
                              <col style="width:20%;">
                            </colgroup>
                            <thead>
                                <tr align="center">
                                    <th ><small> Label Posisi Kontainer </small></th>
                                    <th ><small> Kontainer </small></th>
                                    <th ><small> Keterangan </small></th>
                                    <th ><small> <span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp; Kelola </small></th>
                                </tr>
                            </thead>
                            <tbody> <?php $no=1; ?>
                                @foreach($listposisikontainer as $key => $value)

                                <?php
                                    $kontainer = App\Models\MS_Kontainer::getbyID($value->id_kontainer);
                                ?>
                                <tr align="center" style="height: 15">
                                    <td> <small>{{ $value->label_posisi_kontainer }} </small></td>
                                    <td> <small>{{ $kontainer->nama_kontainer }} </small></td>
                                    <td> <small>{{ $value->keterangan_posisi_kontainer }} </small></td>


                                    <td >                             
                                        <div class="row" >    
                                        <form action="/posisi_kontainer_edit" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_posisi_kontainer }}">
                                            <input type="hidden" name="name" value="{{ $value->label_posisi_kontainer }}">
                                            <input type="hidden" name="keterangan" value="{{ $value->keterangan_posisi_kontainer }}">
                                            <input type="hidden" name="id_kontainer" value="{{ $value->id_kontainer }}">
                                            <input type="hidden" name="mode" value="edit">
                                            <input type="hidden" name="route" value="posisi_kontainer">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitedit" name="submit" value="EDIT">
                                        </form>
                                        <form action="/posisi_kontainer_delete" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_posisi_kontainer }}">
                                            <input type="hidden" name="name" value="{{ $value->label_posisi_kontainer }}">
                                            <input type="hidden" name="keterangan" value="{{ $value->keterangan_posisi_kontainer }}">
                                            <input type="hidden" name="id_kontainer" value="{{ $value->id_kontainer }}">
                                            <input type="hidden" name="mode" value="delete">
                                            <input type="hidden" name="route" value="posisi_kontainer">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitdel" name="submit" value="DELETE">
                                        </form></small>
                                        </div>
                                    </td>  
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                          
                        </div> 
                         <div class="col-sm-12" align="center" >
                            <div style="text-align: center;">{{  $listposisikontainer->links() }}</div>
                           
                        </div>                          

                    </div> 
    
                </div>
            </div>
        </div>

        </div>  
    </div>
@endsection