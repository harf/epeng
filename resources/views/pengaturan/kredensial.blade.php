@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>

      
    <div class="col-sm-12" align="left">          
        <ul class="nav nav-tabs bg-purple">
          <li class="nav-item"><a class="nav-link active" href="#">Menu</a></li>
          <!--li class="nav-item"><a class="nav-link" href="{{ route('penyedia') }}">Penyedia</a></li-->
        </ul>
        <div class="panel panel-default">

            <div class="row">    
            
            <div class="col-md-4 col-sm-6 col-xs-12">           
                <div class="card card-stats">
                <form method="POST" action="{{ route('menu_save') }}">
                    <input type="hidden" name="route" value="kantor">
                    {{ csrf_field() }} 
                    @csrf

                    <div class="form-group row"  align="center">
                        @if($mode == 'edit')
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp;&nbsp; EDIT MENU</h5>
                                <hr>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;&nbsp; TAMBAH MENU</h5>
                                <hr>
                            </div>
                        @endif
                        
                        <div class="col-md-4" align="left">
                                <small>Nama Menu</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ($name)?$name:old('name') }}" placeholder="Nama Menu" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                               
                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Route</small>
                        </div>
                        <div class="col-md-8">
                            <input id="route" type="text" class="form-control{{ $errors->has('route') ? ' is-invalid' : '' }}" name="route" value="{{ ($route)?$route:old('route') }}" placeholder="Route" required autofocus>

                            @if ($errors->has('route'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('route') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Deskripsi</small>
                        </div>
                        <div class="col-md-8">
                            <textarea id="deskripsi" type="textarea" class="form-control{{ $errors->has('deskripsi') ? ' is-invalid' : '' }}" name="deskripsi" placeholder="Deskripsi" required autofocus>{{ ($deskripsi)?$deskripsi:old('deskripsi') }} </textarea>

                            @if ($errors->has('deskripsi'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('deskripsi') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  

                     <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Icon</small>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control input-sm" name="icon" id="icon">
                                <option value="0"  selected="true">Pilih Icon</option>
                                <option value="fa fa-address-book" >fa fa-address-book</option>
                                <option value="fa fa-allergies" >fa fa-allergies</option>
                            
                            </select>
                        </div> 
                    </div>  

                    <div class="form-group row">
                        <div class="col-md-4" align="left">
                                <small>Menu Parent</small>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control input-sm" name="id_parent" id="id_parent">
                                <option value="0"  selected="true">Set as Parent</option>
                            @foreach ($listparent as $parent)
                                <option value="{{ $parent->id_menu }}"
                                @if ( old('id_parent', $id_parent) == $parent->id_menu )
                                    selected="selected"
                                @endif
                                >{{ str_replace('_',' ',$parent->title) }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>     


                    <div class="form-group row">
                        <div class="col-md-4" align="left">
                                <small>Status</small>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control input-sm" name="status" id="status">
                                <option value="1"  
                                @if ( old('status', $status) == '1' ) 
                                    selected="selected"
                                @endif >Aktif</option>
                                <option value="0" 
                                @if ( old('status', $status) == '0' ) 
                                    selected="selected"
                                @endif >Non Aktif</option>                            
                            </select>
                        </div>
                    </div>     

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Urutan</small>
                        </div>
                        <div class="col-md-8">
                            <input id="urutan" type="text" class="form-control{{ $errors->has('urutan') ? ' is-invalid' : '' }}" name="urutan" value="{{ ($urutan)?$urutan:old('urutan') }}" placeholder="Urutan" required autofocus>

                            @if ($errors->has('urutan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('urutan') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  

                    <div class="form-group row">
                        <div class="col-md-4" align="left">
                                <small>Role</small>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control input-sm" name="role" id="role">
                                <option value="All"  
                                @if ( old('role', $role) == 'All' ) 
                                    selected="selected"
                                @endif >All</option>
                                <option value="Admin" 
                                @if ( old('role', $role) == 'Admin' ) 
                                    selected="selected"
                                @endif >Admin</option>         
                                <option value="PPK" 
                                @if ( old('role', $role) == 'PPK' ) 
                                    selected="selected"
                                @endif >PPK</option>       
                                <option value="Timtik" 
                                @if ( old('role', $role) == 'Timtik' ) 
                                    selected="selected"
                                @endif >Timtik</option>                          
                            </select>
                        </div>
                    </div>     

                    <?php   if($mode == 'edit') { 
                                $button = 'Simpan';
                    ?>
                                <input type="hidden" name="mode" value="edit">                            
                                <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                            }
                            else {
                                $button = 'Tambah';
                    ?>
                                <input type="hidden" name="mode" value="create">
                    <?php    
                            }
                    ?>
                    <div class="form-group row" align="center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-login">
                                {{ $button }}
                            </button>                        
                        </div>
                    </div>
                </form>

                </div>
            </div>
                       
            <div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR MENU</h5>
                            <hr>
                        </div> 
                    
                   
                    @if(session('noticemessage'))
                        @if(session('flag') == '1')
                            <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                        @else
                            <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                        @endif
                        <div class="panel-heading" align="center" style="{{ $style }}">
                            <div class="form-group row" align="center">
                                <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                            </div>
                        </div> 
                        <br />
                    @endif     
                        <div class="col-sm-12">
                          <table class="table table-hover" width="70%">
                            <colgroup>
                              <col style="width:2%;">
                              <col style="width:10%;">
                              <col style="width:2%;">
                              <col style="width:15%;">
                              <col style="width:10%;">
                              <col style="width:20%;">
                              <col style="width:5%;">
                              <col style="width:5%;">
                              <col style="width:20%;">
                            </colgroup>
                            <thead>
                                <tr align="center">
                                    <th ><small> No </small></th>
                                    <th ><small> Parent </small></th>
                                    <th ><small>  </small></th>
                                    <th ><small> Menu </small></th>
                                    <th ><small> Route </small></th>
                                    <th ><small> Deskripsi </small></th>
                                    <th ><small> Status </small></th>
                                    <th ><small> Role </small></th>
                                    <th ><small> <span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp; Kelola </small></th>
                                </tr>
                            </thead>
                            <tbody> <?php $no=1; 

                                $listparent = App\Models\Ms_Menu::getMenu();
                            ?>
                                @foreach($listparent as $key => $value)
                                
                                <?php 
                                        $root_ = App\Models\Ms_Menu::getbyID($value->id_parent);
                                        $root = ($root_)?$root_->title:'#';
                                    ?>
                                <tr align="left" style="height: 15">
                                    <td> <small>{{ $value->urutan }} </small></td>
                                    <td> <small>{{ $root }} </small></td>
                                    <td> <small> <i class="{{ $value->icon }}"></i>  </small></td>
                                    <td> <small>{{ $value->title }} </small></td>
                                    <td> <small>{{ $value->route }} </small></td>
                                    <td> <small>{{ $value->deskripsi }} </small></td>
                                    <td> <small>{{ ($value->status=='1')?"Aktif":"Non-aktif" }} </small></td>
                                    <td> <small>{{ $value->role }} </small></td>

                                    <td >                             
                                        <div class="row" >    
                                        <form action="/menu_edit" method="post" enctype="multipart/form-data">
                                     
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_menu }}">
                                            <input type="hidden" name="name" value="{{ $value->title }}">
                                            <input type="hidden" name="route" value="{{ $value->route }}">
                                            <input type="hidden" name="deskripsi" value="{{ $value->deskripsi }}">
                                            <input type="hidden" name="icon" value="{{ $value->icon }}">
                                            <input type="hidden" name="id_parent" value="{{ $value->id_parent }}">
                                            <input type="hidden" name="status" value="{{ $value->status }}">
                                            <input type="hidden" name="urutan" value="{{ $value->urutan }}">
                                            <input type="hidden" name="role" value="{{ $value->role }}">
                                            <input type="hidden" name="mode" value="edit">
                                            <input type="hidden" name="route" value="kantor">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitedit" name="submit" value="EDIT">
                                        </form>
                                        <form action="/menu_delete" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_menu }}">
                                            <input type="hidden" name="name" value="{{ $value->title }}">
                                            <input type="hidden" name="route" value="{{ $value->route }}">
                                            <input type="hidden" name="deskripsi" value="{{ $value->deskripsi }}">
                                            <input type="hidden" name="icon" value="{{ $value->icon }}">
                                            <input type="hidden" name="id_parent" value="{{ $value->id_parent }}">
                                            <input type="hidden" name="status" value="{{ $value->status }}">
                                            <input type="hidden" name="urutan" value="{{ $value->urutan }}">
                                            <input type="hidden" name="role" value="{{ $value->role }}">
                                            <input type="hidden" name="mode" value="delete">
                                            <input type="hidden" name="route" value="kantor">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitdel" name="submit" value="DELETE">
                                        </form></small>
                                        </div>
                                    </td>  
                                </tr>

                                <?php 
                                $listchild = App\Models\Ms_Menu::getChild($value->id_menu);
                                if(count($listchild) > 0 ) {
                                    ?>

                                    @foreach ($listchild as $child)
                                    
                                    <?php 
                                        $parent_ = App\Models\Ms_Menu::getbyID($child->id_parent);
                                        $parent = ($parent_)?$parent_->title:'-';
                                    ?>

                                    <tr align="left" style="height: 15">
                                        <td> <small>{{ $value->urutan }}.{{ $child->urutan }} </small></td>
                                        <td> <small>{{ $parent }} </small></td>
                                        <td> <small> <i class="{{ $child->icon }}"></i>  </small></td>
                                        <td> <small>{{ $child->title }} </small></td>
                                        <td> <small>{{ $child->route }} </small></td>
                                        <td> <small>{{ $child->deskripsi }} </small></td>
                                        <td> <small>{{ ($child->status=='1')?"Aktif":"Non-aktif" }} </small></td>
                                        <td> <small>{{ $child->role }} </small></td>

                                        <td >                             
                                            <div class="row" >    
                                            <form action="/menu_edit" method="post" enctype="multipart/form-data">
                                         
                                                {{ csrf_field() }}  
                                                <input type="hidden" name="id" value="{{ $child->id_menu }}">
                                                <input type="hidden" name="name" value="{{ $child->title }}">
                                                <input type="hidden" name="route" value="{{ $child->route }}">
                                                <input type="hidden" name="deskripsi" value="{{ $child->deskripsi }}">
                                                <input type="hidden" name="icon" value="{{ $child->icon }}">
                                                <input type="hidden" name="id_parent" value="{{ $child->id_parent }}">
                                                <input type="hidden" name="status" value="{{ $child->status }}">
                                                <input type="hidden" name="urutan" value="{{ $child->urutan }}">
                                                <input type="hidden" name="role" value="{{ $child->role }}">
                                                <input type="hidden" name="mode" value="edit">
                                                <input type="hidden" name="route" value="kantor">
                                                <input type="hidden" name="_method" value="POST">
                                                <input type="submit" class="btn btn-link btn-xs" id="submitedit" name="submit" value="EDIT">
                                            </form>
                                            <form action="/menu_delete" method="post" enctype="multipart/form-data">                                          
                                                {{ csrf_field() }}  
                                                <input type="hidden" name="id" value="{{ $child->id_menu }}">
                                                <input type="hidden" name="name" value="{{ $child->title }}">
                                                <input type="hidden" name="route" value="{{ $child->route }}">
                                                <input type="hidden" name="deskripsi" value="{{ $child->deskripsi }}">
                                                <input type="hidden" name="icon" value="{{ $child->icon }}">
                                                <input type="hidden" name="id_parent" value="{{ $child->id_parent }}">
                                                <input type="hidden" name="status" value="{{ $child->status }}">
                                                <input type="hidden" name="urutan" value="{{ $child->urutan }}">
                                                <input type="hidden" name="role" value="{{ $child->role }}">
                                                <input type="hidden" name="mode" value="delete">
                                                <input type="hidden" name="route" value="kantor">
                                                <input type="hidden" name="_method" value="POST">
                                                <input type="submit" class="btn btn-link btn-xs" id="submitdel" name="submit" value="DELETE">
                                            </form></small>
                                            </div>
                                        </td>  
                                    </tr>


                                    @endforeach

                                    <?php
                                }
                                ?>
                                @endforeach
                            </tbody>
                          </table>
                          
                        </div> 
                                              

                    </div> 
    
                </div>
            </div>
        </div>

        </div>  
    </div>
@endsection