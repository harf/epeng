
@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>

      
    <div class="col-sm-12" align="left">          
        <ul class="nav nav-tabs bg-purple">
          <li class="nav-item"><a class="nav-link" href="{{ route('kantor') }}">UPT</a></li>
          <li class="nav-item"><a class="nav-link active" href="#">Penyedia</a></li>
        </ul>
        <div class="panel panel-default">

            <div class="row">    
            
            <div class="col-md-4 col-sm-6 col-xs-12">           
                <div class="card card-stats">
                <form method="POST" action="{{ route('penyedia_save') }}">
                    <input type="hidden" name="route" value="penyedia">
                    {{ csrf_field() }} 
                    @csrf

                    <div class="form-group row"  align="center">
                        @if($mode == 'edit')
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp;&nbsp; EDIT PENYEDIA</h5>
                                <hr>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;&nbsp; TAMBAH PENYEDIA</h5>
                                <hr>
                            </div>
                        @endif
                        
                        <div class="col-md-4" align="left">
                                <small>Penyedia</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ($name)?$name:old('name') }}" placeholder="Penyedia" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                          
                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Alamat</small>
                        </div>
                        <div class="col-md-8">
                            <textarea id="alamat" type="textarea" class="form-control{{ $errors->has('alamat') ? ' is-invalid' : '' }}" name="alamat" placeholder="Alamat" required autofocus>{{ ($alamat)?$alamat:old('alamat') }} </textarea>

                            @if ($errors->has('alamat'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('alamat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  


                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>No Telp</small>
                        </div>
                        <div class="col-md-8">
                            <input id="notelp" type="text" class="form-control{{ $errors->has('notelp') ? ' is-invalid' : '' }}" name="notelp" value="{{ ($notelp)?$notelp:old('notelp') }}" placeholder="No Telp" required autofocus>

                            @if ($errors->has('notelp'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('notelp') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Email</small>
                        </div>
                        <div class="col-md-8">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ ($email)?$notelp:old('email') }}" placeholder="Email" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>  

                    <?php   if($mode == 'edit') { 
                                $button = 'Simpan';
                    ?>
                                <input type="hidden" name="mode" value="edit">                            
                                <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                            }
                            else {
                                $button = 'Tambah';
                    ?>
                                <input type="hidden" name="mode" value="create">
                    <?php    
                            }
                    ?>
                    <div class="form-group row" align="center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-login">
                                {{ $button }}
                            </button>                        
                        </div>
                    </div>
                </form>

                </div>
            </div>
                       
            <div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR PENYEDIA</h5>
                            <hr>
                        </div> 
                    
                   
                    @if(session('noticemessage'))
                        @if(session('flag') == '1')
                            <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                        @else
                            <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                        @endif
                        <div class="panel-heading" align="center" style="{{ $style }}">
                            <div class="form-group row" align="center">
                                <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                            </div>
                        </div> 
                        <br />
                    @endif     
                        <div class="col-sm-12">
                          <table class="table table-hover" align="center" width="70%">
                            <colgroup>
                              <col style="width:30%;">
                              <col style="width:30%;">
                              <col style="width:10%;">
                              <col style="width:10%;">
                              <col style="width:20%;">
                            </colgroup>
                            <thead>
                                <tr align="center">
                                    <th ><small> Penyedia </small></th>
                                    <th ><small> Alamat </small></th>
                                    <th ><small> No Telp </small></th>
                                    <th ><small> Email </small></th>
                                    <th ><small> <span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp; Kelola </small></th>
                                </tr>
                            </thead>
                            <tbody> <?php $no=1; ?>
                                @foreach($listpenyedia as $key => $value)
                                
                                <tr align="center" style="height: 15">
                                    <td> <small>{{ $value->nama_penyedia }} </small></td>
                                    <td> <small>{{ $value->alamat_penyedia }} </small></td>
                                    <td> <small>{{ $value->notelp_penyedia }} </small></td>
                                    <td> <small>{{ $value->email_penyedia }} </small></td>

                                    <td >                             
                                        <div class="row" >    
                                        <form action="/penyedia_edit" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_penyedia }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_penyedia }}">
                                            <input type="hidden" name="alamat" value="{{ $value->alamat_penyedia }}">
                                            <input type="hidden" name="notelp" value="{{ $value->notelp_penyedia }}">
                                            <input type="hidden" name="email" value="{{ $value->email_penyedia }}">
                                            <input type="hidden" name="mode" value="edit">
                                            <input type="hidden" name="route" value="penyedia">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitedit" name="submit" value="EDIT">
                                        </form>
                                        <form action="/penyedia_delete" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_penyedia }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_penyedia }}">
                                            <input type="hidden" name="alamat" value="{{ $value->alamat_penyedia }}">
                                            <input type="hidden" name="notelp" value="{{ $value->notelp_penyedia }}">
                                            <input type="hidden" name="email" value="{{ $value->email_penyedia }}">
                                            <input type="hidden" name="mode" value="delete">
                                            <input type="hidden" name="route" value="penyedia">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitdel" name="submit" value="DELETE">
                                        </form></small>
                                        </div>
                                    </td>  
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                          
                        </div> 
                         <div class="col-sm-12" align="center" >
                            <div style="text-align: center;">{{ $listpenyedia->links() }}</div>
                           
                        </div>                          

                    </div> 
    
                </div>
            </div>
        </div>

        </div>  
    </div>
@endsection