<script src="/js/core/jquery.min.js" integrity=""></script>
@extends('layouts.master')
  <meta name="csrf-token" content="<?php echo csrf_token() ?>">
@section('content')

<!-- include summernote css/js-->
<link href="css/summernote.css" rel="stylesheet">
<script src="js/summernote.js"></script>    
 
    <div class="col-sm-12">   
    <ul class="nav nav-tabs bg-purple">
      <li class="nav-item"><a class="nav-link active" href="#">BERITA ACARA TRANSFER BMN</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ route('templateSPP') }}">SURAT PENGANTAR PENGIRIMAN</a></li>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading" style="background-color: #c1fff3;"><strong>SETTING TEMPLATE SURAT BERITA ACARA TRANSFER BMN(BAST)</strong></div>
        <div class="panel-body">
        
        <form action="/saveTemplate" method="post" enctype="multipart/form-data">    
        
        <div class="form-group row">
            <div class="col-sm-2">Header</div>
              <div class="col-sm-10">   
                  <?php $kumhamimistat = "checked";
                      $kumhamstat = "";
                      $kumhamtextstat = "";
                      if($summernote) { 
                        if($summernote->header == 'img/kumham.png') {
                          $kumhamimistat = "";
                          $kumhamtextstat = "";
                          $kumhamstat = "checked";
                        }  
                        elseif($summernote->header == 'img/kumhamimi.png') {
                          $kumhamimistat = "checked";
                          $kumhamtextstat = "";
                          $kumhamstat = "";
                        }    
                        elseif($summernote->header == 'img/kumhamimitext.png') {
                          $kumhamimistat = "";
                          $kumhamtextstat = "checked";
                          $kumhamstat = "";
                        }                      
                      }
                  ?>   
                  <input type="radio" id="header" name="header" value="img/kumhamimi.png" {{ $kumhamimistat }} > &emsp;<img src="{{ URL::to('img/kumhamimi.png') }}" height="100"></input> &emsp;
                  <input type="radio" id="header" name="header" value="img/kumham.png"  {{ $kumhamstat }}> &emsp;<img src="{{ URL::to('img/kumham.png') }}" height="100"></input>&emsp;
                  <input type="radio" id="header" name="header" value="img/kumhamimitext.png"  {{ $kumhamtextstat }}> &emsp;<img src="{{ URL::to('img/kumhamimitext.png') }}" height="100"></input>
              </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-2">Header</div>
            <div class="col-sm-10"></div>
        </div>


        <div class="form-group row">
            <div class="col-sm-12">
              <textarea name="template" id="template" class="summernote"></textarea>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-2">TTD</div>
              <select  class="form-control input-sm" class="col-sm-10" name="signature" id="signature">  
                  <option value="0"  selected="true">Pilih Pejabat Penandatangan</option>  
                    @foreach ($listsign as $sign)
                      <option value="{{ $sign->id }}">{{ $sign->jabatan }} | {{ $sign->nama_pejabat }}</option>
                    @endforeach
              </select>
        </div>

        <div class="form-group row">
            <div class="col-sm-12">
              <textarea name="template_footer" id="template_footer" class="summernote_footer"></textarea>
            </div>
        </div>

        
        
        <div class="form-group row">
<b>PANDUAN : </b>
<br><i>Gunakan tag berikut untuk parameter template surat 
<br>Nama Pendaftar :<span style="color: red">&lt;nama&gt;</span> 
<br>Nomor Permohonan  :  <span style="color: red">&lt;no_tiket&gt;</span> 
<br>Jenis Kelamin : <span style="color: red">&lt;jns_kel&gt;</span> 
<br>Tempat / Tanggal Lahir : <span style="color: red">&lt;tmp_lahir&gt;</span> , <span style="color: red">&lt;tgl_lahir&gt;</span> 
<br>NIK : <span style="color: red">&lt;nik&gt;</span> 
<br>Nomor Paspor : <span style="color: red">&lt;no_paspor&gt;</span> 
<br>Alamat : <span style="color: red">&lt;alamat&gt;</span> , <span style="color: red">&lt;kelurahan&gt;</span> , <span style="color: red">&lt;kecamatan&gt;</span> , <span style="color: red">&lt;kabupaten&gt;</span> , <span style="color: red">&lt;provinsi&gt;</span> , <span style="color: red">&lt;kodepos&gt;</span> 
<br>Pendidikan : <span style="color: red">&lt;desc_jenjang&gt;</span> 
<br>Nama Universitas : <span style="color: red">&lt;nm_universitas&gt;</span> 
<br>Alasan Penolakan : <span style="color: red">&lt;keterangan&gt;</span> 
</i>
            </div>
        </div>
                    
        <br>

        <div class="form-group row">
            <div class="col-sm-12" align="center">
                <button type="submit" class="btn btn-lg btn-success">Submit</button>
                <a class="btn btn-lg btn-info" >Contoh PDF</a>
            </div>
        </div>


        {{ csrf_field() }}


        </form>
        </div>
    </div>
</div>          
@endsection

<script src="tinymce/tinymce.min.js"></script>
<script>

    tinymce.init({
        selector: '#template',
        height: 200,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help wordcount'
        ],
        toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });
    $('#template').summernote('code', {!! json_encode($summernote->template) !!});
</script>

<script>
    tinymce.init({
        selector: '#template_footer',
        height: 200,
        menubar: false,
        plugins: [
            'advlist autolink lists link image charmap print preview anchor textcolor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code help wordcount'
        ],
        toolbar: ' fontname | insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
    });

    $('#template_footer').summernote('code', {!! json_encode($summernote->template_footer) !!});
</script>

