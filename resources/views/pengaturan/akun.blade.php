
<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    }); 

}); 

$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'json/akunjson',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'nip', name: 'nip' },
            { data: 'password', name: 'password' },
            { data: 'role', name: 'role' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});

</script>



      
    <div class="col-sm-12" align="left">          
        <ul class="nav nav-tabs bg-purple">
          <li class="nav-item"><a class="nav-link active" href="#">Akun</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('penyedia') }}">Hak Akses</a></li>
        </ul>
        <div class="panel panel-default">
            <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">           
                <div class="card card-stats">
                <form method="POST" action="{{ route('akun_save') }}">
                    <input type="hidden" name="route" value="akun">
                    {{ csrf_field() }} 
                    @csrf
                    <div class="form-group row"  align="center">
                        @if($mode == 'edit')
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp;&nbsp; EDIT AKUN</h5>
                                <hr>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;&nbsp; TAMBAH AKUN</h5>
                                <hr>
                            </div>
                        @endif
                    </div>
                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Nama</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ($name)?$name:old('name') }}" placeholder="Nama" autocomplete="off" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Email</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ ($email)?$email:old('email') }}" placeholder="Email" autocomplete="off" required autofocus>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>NIP</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="nip" type="text" class="form-control{{ $errors->has('nip') ? ' is-invalid' : '' }}" name="nip" value="{{ ($nip)?$email:old('nip') }}" placeholder="NIP" autocomplete="off" required autofocus>

                            @if ($errors->has('nip'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('nip') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Password</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" autocomplete="off" required autofocus>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row" align="center">
                        <div class="col-md-4" align="left">
                                <small>Role</small>
                        </div>
                        <div class="col-md-8">
                            <select class="form-control input-sm" name="role" id="role">
                                <option value="All"  
                                @if ( old('role', $role) == 'All' ) 
                                    selected="selected"
                                @endif >All</option>
                                <option value="Admin" 
                                @if ( old('role', $role) == 'Admin' ) 
                                    selected="selected"
                                @endif >Admin</option>         
                                <option value="PPK" 
                                @if ( old('role', $role) == 'PPK' ) 
                                    selected="selected"
                                @endif >PPK</option>       
                                <option value="Timtik" 
                                @if ( old('role', $role) == 'Timtik' ) 
                                    selected="selected"
                                @endif >Timtik</option>                          
                            </select>
                        </div>
                    </div> 
                               

                    <?php   if($mode == 'edit') { 
                                $button = 'Simpan';
                    ?>
                                <input type="hidden" name="mode" value="edit">                            
                                <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                            }
                            else {
                                $button = 'Tambah';
                    ?>
                                <input type="hidden" name="mode" value="create">
                    <?php    
                            }
                    ?>
                    <div class="form-group row" align="center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-login">
                                {{ $button }}
                            </button>                        
                        </div>
                    </div>
                </form>

                </div>
            </div>
                       
            <div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR AKUN </h5>
                            <hr>
                        </div>                  
                           @if(session('noticemessage'))
                                @if(session('flag') == '1')
                                    <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                                @else
                                    <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                                @endif
                                <div class="panel-heading" align="center" style="{{ $style }}">
                                    <div class="form-group row" align="center">
                                        <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                                    </div>
                                </div> 
                                <br />
                            @endif   
                    
                        <div class="col-sm-12">
                          <table class="table table-striped table-bordered table-responsive" id="users-table">
                                <thead>
                                       <th>ID Akun</th>
                                       <th>Nama</th>
                                       <th>Email</th>
                                       <th>NIP</th>
                                       <th>Password</th>
                                       <th>Role</th>
                                       <th>Action</th>
                                </thead>                
                           </table>                          
                        </div>         
                    </div> 
                </div>
            </div>
        </div>

        </div>  
    </div>
@endsection