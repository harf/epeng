<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

@section('content')
<div class="row">
	@if($listsign)
  	<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  	</div>

  	<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    	<form method="POST" action="{{ route('signature_save') }}" enctype="multipart/form-data">
      		<div class="card card-profile">
        		<div class="card-header card-header-primary">
          			<h4 class="card-title"><strong>Setting Master Tanda Tangan</strong></h4>
        		</div>
		        <div class="card-body" align="left">
		        </div>
      		</div>

      		<div class="card card-profile">
		        <div class="card-body" align="left">
		          	<div class="form-group row">
		            	<div class="col-md-3 col-sm-3 col-xs-3">
		              		Jabatan
		            	</div>

			            <div class="col-md-9 col-sm-9 col-xs-9">
			              	<input type="text" class="form-control" name="jabatan" value="{{ ($sign)?$sign->jabatan:'' }}">
			              	<input type="hidden" name="id_sign" value={{ ($sign)?$sign->id:'' }}>
			            </div>
		          	</div>

		          	<div class="form-group row">
		              	<div class="col-md-3 col-sm-3 col-xs-3">
		                	Nama Pejabat
		              	</div>
		             	<div class="col-md-9 col-sm-9 col-xs-9">
		                	<input type="text" class="form-control" name="nama_pejabat"  value="{{ ($sign)?$sign->nama_pejabat:'' }}">
		              	</div>
		            </div>

		            <div class="form-group row">
		              	<div class="col-md-3 col-sm-3 col-xs-3">
		                	Tanda Tangan
		              	</div>
		              	
		             	<div class="col-md-6 col-sm-6 col-xs-6">
		                    <div class="input-group">
		                        <input type="text" class="form-control" id="signature_text" readonly="" style="background-color: white;" placeholder="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB">
		                        <div>
		                        	<div class="btn btn-primary btn-sm">
		                        		<span>
		                        			<i class="fa fa-upload"></i>
		                        		</span>
		                        		<input type="file" id="id_gambar" class="upload" rel="tooltip" data-original-title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." class="upload" name="gambar"/>
		                            </div>
		                        </div>
		                    </div>
	                	</div>
                        @if($sign)
		                	<div class="col-md-2 col-sm-2 col-xs-2">
		                 		<img id="img" style="max-height: 200px" src=" {{ str_replace('public','/storage',$sign->link_signature) }}" ald="Signature">
		                	</div>
		                	<div class="col-sm-2"></div>
		                @else
		                	<div class="col-md-2 col-sm-2 col-xs-2">
	                            <div>
	                        		<img id="img" style="max-height: 200px;" src="#" alt="Gambar">
	                        	</div>
                        	</div>
                        	<div class="col-sm-2"></div>
		                @endif	
		            </div>
		            <div class="form-group row">
		              <div class="col-md-3 col-sm-3 col-xs-3">
		                NIP
		              </div>
		              <div class="col-md-9 col-sm-9 col-xs-9">
		                <input type="text" class="form-control" name="nip" value="{{ ($sign)?$sign->nip:'' }}">
		              </div>
		            </div>
		      	</div>
      
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row pull-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Simpan Paket Pengadaan</button>
          </div>
        </div>
      </div>
      {{ csrf_field() }}
    </form>
  </div>
  <div class="col-sm-12">
                    <table class="table table-hover" >
                    <thead>
                        <tr  style="text-align-last: center; ">
                            <th > Jabatan  </th>
                            <th > Nama Pejabat </th>
                            <th > Tanda Tangan</th>
                            <th > NIP </th>
                            <th > Aksi</th>
                        </tr>
                    </thead>
                    <tbody> <?php $no=1; ?>
                         @foreach($listsign as $key => $value)
                          <tr style="text-align-last: center; ">
                              <td> {{ $value->jabatan }} </td>
                              <td> {{ $value->nama_pejabat }} </td>
                              <td><img style="width: 200px;" src="{{ str_replace('public','/storage',$value->link_signature) }}" alt="Signature"> </td>

                              <td> {{ $value->nip }} </td>
                              <td><a href="{{action('PengaturanController@signature_edit', $value->id)}}" class="btn btn-warning">Ubah</a>
                            	    <a href="{{action('PengaturanController@signature_delete', $value->id)}}" class="btn btn-danger">Hapus</a>
                              </td>
                          </tr>
                         @endforeach
                         
                    </tbody>
                    </table>
                    <div style="text-align: center;">{{  $listsign->links() }}</div>
                </div>
  <!-- <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div> -->
</div>
	@else
        <p>Data tidak ditemukan.</p>
	@endif

<script type="text/javascript">
	$('#id_gambar').on('change', function(e){
		var gambar = this;
		$("#signature_text").val(this.value.replace(/C:\\fakepath\\/i, ''));
		if (gambar.files && gambar.files[0]) {
		      var reader = new FileReader();
		      reader.onload = function (event) {
		        $('#img').attr('src', event.target.result);
		      };
		      reader.readAsDataURL(gambar.files[0]);
		    }
	});
</script>
@endsection
