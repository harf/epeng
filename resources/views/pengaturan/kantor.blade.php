
<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    }); 

}); 

$(function() {
    $('#kantor-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'json/kantorjson',
        columns: [
            { data: 'kode_kanim', name: 'kode_kanim' },
            { data: 'nama_kanim', name: 'nama_kanim' },
            { data: 'kode_kanwil', name: 'kode_kanwil' },
            { data: 'action', name: 'action', orderable: false, searchable: false}
        ]
    });
});

</script>



      
    <div class="col-sm-12" align="left">          
        <ul class="nav nav-tabs bg-purple">
          <li class="nav-item"><a class="nav-link active" href="#">UPT</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('penyedia') }}">Penyedia</a></li>
        </ul>
        <div class="panel panel-default">

            <div class="row">    
            
            <div class="col-md-4 col-sm-6 col-xs-12">           
                <div class="card card-stats">
                <form method="POST" action="{{ route('kantor_save') }}">
                    <input type="hidden" name="route" value="kantor">
                    {{ csrf_field() }} 
                    @csrf

                    <div class="form-group row"  align="center">
                        @if($mode == 'edit')
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp;&nbsp; EDIT UPT</h5>
                                <hr>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;&nbsp; TAMBAH UPT</h5>
                                <hr>
                            </div>
                        @endif
                        
                        <div class="col-md-4" align="left">
                                <small>UPT</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ($name)?$name:old('name') }}" placeholder="UPT" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                               

                    <?php   if($mode == 'edit') { 
                                $button = 'Simpan';
                    ?>
                                <input type="hidden" name="mode" value="edit">                            
                                <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                            }
                            else {
                                $button = 'Tambah';
                    ?>
                                <input type="hidden" name="mode" value="create">
                    <?php    
                            }
                    ?>
                    <div class="form-group row" align="center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-login">
                                {{ $button }}
                            </button>                        
                        </div>
                    </div>
                </form>

                </div>
            </div>
                       
            <div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR UPT </h5>
                            <hr>
                        </div>                  
                           @if(session('noticemessage'))
                                @if(session('flag') == '1')
                                    <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                                @else
                                    <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                                @endif
                                <div class="panel-heading" align="center" style="{{ $style }}">
                                    <div class="form-group row" align="center">
                                        <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                                    </div>
                                </div> 
                                <br />
                            @endif   
                    
                        <div class="col-sm-12">
                          <table class="table table-striped table-bordered" id="kantor-table">
                                <thead>
                                       <th>Kode Kanim</th>
                                       <th>Nama Kanim</th>
                                       <th>Kode Kanwil</th>
                                       <th>Action</th>
                                </thead>                
                           </table>                          
                        </div>         
                    </div> 
                </div>
            </div>

            <!--div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR UPT</h5>
                            <hr>
                        </div> 
                    
                   
                    @if(session('noticemessage'))
                        @if(session('flag') == '1')
                            <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                        @else
                            <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                        @endif
                        <div class="panel-heading" align="center" style="{{ $style }}">
                            <div class="form-group row" align="center">
                                <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                            </div>
                        </div> 
                        <br />
                    @endif     
                        <div class="col-sm-12">
                          <table class="table table-hover" align="center" width="70%">
                            <colgroup>
                              <col style="width:80%;">
                              <col style="width:20%;">
                            </colgroup>
                            <thead>
                                <tr align="center">
                                    <th ><small> Nama UPT </small></th>
                                    <th ><small> <span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp; Kelola </small></th>
                                </tr>
                            </thead>
                            <tbody> <?php $no=1; ?>
                                @foreach($listkantor as $key => $value)
                                
                                <tr align="center" style="height: 15">
                                    <td> <small>{{ $value->nama_kanim }} </small></td>

                                    <td >                             
                                        <small>    <div class="row" >
                                        <form action="/kantor_edit" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->kode_kanim }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_kanim }}">
                                            <input type="hidden" name="mode" value="edit">
                                            <input type="hidden" name="route" value="kantor">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitedit" name="submit" value="EDIT">
                                        </form>
                                        <form action="/kantor_delete" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->kode_kanim }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_kanim }}">
                                            <input type="hidden" name="mode" value="delete">
                                            <input type="hidden" name="route" value="kantor">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitdel" name="submit" value="DELETE">
                                        </form></div>
                                        </small>
                                    </td>  
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                          
                        </div> 
                         <div class="col-sm-12" align="center" >
                            <div style="text-align: center;">{{ $listkantor->links() }}</div>
                           
                        </div>                          

                    </div> 
    
                </div>
            </div-->

        </div>

        </div>  
    </div>
@endsection