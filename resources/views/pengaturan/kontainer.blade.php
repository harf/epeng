
@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });
}); 
</script>

      
    <div class="col-sm-12" align="left">          
        <ul class="nav nav-tabs bg-purple">
          <li class="nav-item"><a class="nav-link" href="{{ route('dashboard_kontainer') }}">Dashboard Kontainer</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('jenis_kontainer') }}">Jenis Kontainer</a></li>
          <li class="nav-item"><a class="nav-link active" href="#">Kontainer</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('posisi_kontainer') }}">Posisi</a></li>
          <li class="nav-item"><a class="nav-link" href="{{ route('lokasi_kontainer') }}">Lokasi</a></li>
        </ul>
        <div class="panel panel-default">

            <div class="row">    
            
            <div class="col-md-4 col-sm-6 col-xs-12">           
                <div class="card card-stats">
                <form method="POST" action="{{ route('kontainer_save') }}">
                    <input type="hidden" name="route" value="kontainer">
                    {{ csrf_field() }} 
                    @csrf

                    <div class="form-group row"  align="center">
                        @if($mode == 'edit')
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp;&nbsp; EDIT KONTAINER</h5>
                                <hr>
                            </div>
                        @else
                            <div class="col-md-12">
                                <h5><span class="glyphicon glyphicon-plus"></span> &nbsp;&nbsp;&nbsp; TAMBAH KONTAINER</h5>
                                <hr>
                            </div>
                        @endif
                        
                        <div class="col-md-4" align="left">
                                <small>Nama Kontainer</small>
                        </div>
                        
                        <div class="col-md-8">
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ($name)?$name:old('name') }}" placeholder="Nama Jenis Kontainer" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-4" align="left">
                                <small>Lokasi</small>
                        </div>
                        <div class="col-md-8">

                            <select class="form-control input-sm" name="id_lokasi" id="id_lokasi">
                                <option value="0"  selected="true">Pilih Lokasi</option>
                            @foreach ($listlokasi as $lokasi)
                                <option value="{{ $lokasi->id_lokasi }}"
                                @if ( old('id_lokasi', $id_lokasi) == $lokasi->id_lokasi )
                                    selected="selected"
                                @endif
                                >{{ str_replace('_',' ',$lokasi->nama_lokasi) }}</option>
                            @endforeach
                            </select>


                        </div>
                    </div>      

                    <div class="form-group row">
                        <div class="col-md-4" align="left">
                                <small>Jenis Kontainer</small>
                        </div>
                        <div class="col-md-8">

                            <select class="form-control input-sm" name="id_jenis_kontainer" id="id_jenis_kontainer">
                                <option value="0"  selected="true">Pilih Jenis</option>
                            @foreach ($listjeniskontainer as $jenis_kontainer)
                                <option value="{{ $jenis_kontainer->id_jenis_kontainer }}"
                                @if ( old('id_jenis_kontainer', $id_jenis_kontainer) == $jenis_kontainer->id_jenis_kontainer )
                                    selected="selected"
                                @endif
                                >{{ str_replace('_',' ',$jenis_kontainer->nama_jenis_kontainer) }}</option>
                            @endforeach
                            </select>


                        </div>
                    </div>  

                    <!--div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Jumlah Slot</small>
                        </div>
                        <div class="col-md-8">
                            <input id="jumlah_slot" type="number" class="form-control{{ $errors->has('jumlah_slot') ? ' is-invalid' : '' }}" name="jumlah_slot" value="{{ ($jumlah_slot)?$jumlah_slot:old('jumlah_slot') }}" placeholder="Jumlah Slot" required autofocus>

                            @if ($errors->has('jumlah_slot'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('jumlah_slot') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div-->

                    <div class="form-group row"  align="center">
                        <div class="col-md-4" align="left">
                                <small>Keterangan</small>
                        </div>
                        <div class="col-md-8">
                            <input id="keterangan" type="text" class="form-control{{ $errors->has('keterangan') ? ' is-invalid' : '' }}" name="keterangan" value="{{ ($keterangan)?$keterangan:old('keterangan') }}" placeholder="Keterangan" required autofocus>

                            @if ($errors->has('keterangan'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('keterangan') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                 

                    <?php   if($mode == 'edit') { 
                                $button = 'Simpan';
                    ?>
                                <input type="hidden" name="mode" value="edit">                            
                                <input type="hidden" name="id" value="{{ $id }}">
                    <?php
                            }
                            else {
                                $button = 'Tambah';
                    ?>
                                <input type="hidden" name="mode" value="create">
                    <?php    
                            }
                    ?>
                    <div class="form-group row" align="center">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary btn-login">
                                {{ $button }}
                            </button>                        
                        </div>
                    </div>
                </form>

                </div>
            </div>
                       
            <div class="col-md-8 col-sm-6 col-xs-12">            
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-12">
                            <h5><span class="glyphicon glyphicon-th-list"></span> &nbsp;&nbsp;&nbsp; DAFTAR KONTAINER</h5>
                            <hr>
                        </div> 
                    
                   
                    @if(session('noticemessage'))
                        @if(session('flag') == '1')
                            <?php $icon="glyphicon-ok"; $style = "background-color: #ECFFFB; color: #195800;"; ?>
                        @else
                            <?php $icon="glyphicon-exclamation-sign"; $style = "background-color: #FFA8A8; color: #5A0000;"; ?>
                        @endif
                        <div class="panel-heading" align="center" style="{{ $style }}">
                            <div class="form-group row" align="center">
                                <span class="glyphicon {{ $icon }}"> </span> &nbsp;&nbsp;&nbsp; {!! session('noticemessage') !!}
                            </div>
                        </div> 
                        <br />
                    @endif     
                        <div class="col-sm-12">
                          <table class="table table-hover" align="center" width="70%">
                            <colgroup>
                              <col style="width:20%;">
                              <col style="width:20%;">
                              <col style="width:15%;">
                              <col style="width:5%;">
                              <col style="width:20%;">
                              <col style="width:20%;">
                            </colgroup>
                            <thead>
                                <tr align="center">
                                    <th ><small> Nama Kontainer </small></th>
                                    <th ><small> Lokasi </small></th>
                                    <th ><small> Jenis </small></th>
                                    <th ><small> Slot </small></th>
                                    <th ><small> Keterangan </small></th>
                                    <th ><small> <span class="glyphicon glyphicon-edit"></span> &nbsp;&nbsp; Kelola </small></th>
                                </tr>
                            </thead>
                            <tbody> <?php $no=1; ?>
                                @foreach($listkontainer as $key => $value)

                                <?php
                                    $jenis_kontainer = App\Models\MS_Jenis_Kontainer::getbyID($value->id_jenis_kontainer);
                                    $lokasi = App\Models\MS_Lokasi::getbyID($value->id_lokasi);
                                ?>
                                <tr align="center" style="height: 15">
                                    <td> <small>{{ $value->nama_kontainer }} </small></td>
                                    <td> <small>{{ $lokasi->nama_lokasi }} </small></td>
                                    <td> <small>{{ $jenis_kontainer->nama_jenis_kontainer }} </small></td>
                                    <td> <small>{{ $value->jumlah_slot }} </small></td>
                                    <td> <small>{{ $value->keterangan_kontainer }} </small></td>


                                    <td >                             
                                        <div class="row" >    
                                        <form action="/kontainer_edit" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_kontainer }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_kontainer }}">
                                            <input type="hidden" name="keterangan" value="{{ $value->keterangan_kontainer }}">
                                            <input type="hidden" name="jumlah_slot" value="{{ $value->jumlah_slot }}">
                                            <input type="hidden" name="id_jenis_kontainer" value="{{ $value->id_jenis_kontainer }}">
                                            <input type="hidden" name="id_lokasi" value="{{ $value->id_lokasi }}">
                                            <input type="hidden" name="mode" value="edit">
                                            <input type="hidden" name="route" value="kontainer">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitedit" name="submit" value="EDIT">
                                        </form>
                                        <form action="/kontainer_delete" method="post" enctype="multipart/form-data">                                          
                                            {{ csrf_field() }}  
                                            <input type="hidden" name="id" value="{{ $value->id_kontainer }}">
                                            <input type="hidden" name="name" value="{{ $value->nama_kontainer }}">
                                            <input type="hidden" name="keterangan" value="{{ $value->keterangan_kontainer }}">
                                            <input type="hidden" name="jumlah_slot" value="{{ $value->jumlah_slot }}">
                                            <input type="hidden" name="id_jenis_kontainer" value="{{ $value->id_jenis_kontainer }}">
                                            <input type="hidden" name="id_lokasi" value="{{ $value->id_lokasi }}">
                                            <input type="hidden" name="mode" value="delete">
                                            <input type="hidden" name="route" value="kontainer">
                                            <input type="hidden" name="_method" value="POST">
                                            <input type="submit" class="btn btn-link btn-xs" id="submitdel" name="submit" value="DELETE">
                                        </form></small>
                                        </div>
                                    </td>  
                                </tr>
                                @endforeach
                            </tbody>
                          </table>
                          
                        </div> 
                         <div class="col-sm-12" align="center" >
                            <div style="text-align: center;">{{  $listkontainer->links() }}</div>
                           
                        </div>                          

                    </div> 
    
                </div>
            </div>
        </div>

        </div>  
    </div>
@endsection