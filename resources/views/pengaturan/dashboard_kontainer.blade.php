<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')
<meta name="csrf-token" content="<?php echo csrf_token() ?>">

@section('content')

<script type="text/javascript">
$(document).ready(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });  

    $('#refreshBtn').click(function() {        
        $.ajax({
            type:'GET',
            url:"{{url('refresh_captcha')}}",
            success:function(data){
                $(".captcha span").html(data.captcha);
            }
        });
    });

   
}); 


$(document).on("click", ".open-ModalLokasiHapus", function () {
     var hapusid = $(this).data('id');
     var hapusname = $(this).data('name');
    
     $(".modal-footer #id").val( hapusid );
     $(".modal-footer #name").val( hapusname );

    document.getElementById("hapusname").innerHTML = hapusname;

});

$(document).on("click", ".open-ModalLokasiEdit", function () {
     var editid = $(this).data('id');
     var editname = $(this).data('name');
     var editketerangan = $(this).data('keterangan');
    
     $(".modal-body #id").val( editid );
     $(".modal-body #name").val( editname );
     $(".modal-body #keterangan").val( editketerangan );

    document.getElementById("id").innerHTML = editid;
    document.getElementById("name").innerHTML = editname;
    document.getElementById("keterangan").innerHTML = keterangan;


});

$(document).on("click", ".open-ModalKontainerTambah", function () {
     var id = $(this).data('id');
     var name = $(this).data('name');
    
     $(".modal-body #id_lokasi").val( id );
     $(".modal-body #name_lokasi").val( name );

    document.getElementById("id").innerHTML = id;
    document.getElementById("name").innerHTML = name;

});

$(document).on("click", ".open-ModalKontainerEdit", function () {
     var id = $(this).data('id');
     var name = $(this).data('name');
     var keterangan = $(this).data('keterangan');
     var id_lokasi = $(this).data('id_lokasi');
    
     var jenis = $(this).data('jenis');
    

     $(".modal-body #id").val( id );
     $(".modal-body #id_lokasi").val( id_lokasi );
     $(".modal-body #name").val( name );
     $(".modal-body #keterangan").val( keterangan );
     $(".modal-body #id_jenis_kontainer").val( jenis );

    document.getElementById("id").innerHTML = id;
    document.getElementById("name").innerHTML = name;
    document.getElementById("keterangan").innerHTML = keterangan;
});

$(document).on("click", ".open-ModalKontainerHapus", function () {
     var hapusid = $(this).data('id');
     var hapusname = $(this).data('name');
    
     $(".modal-footer #id").val( hapusid );
     $(".modal-footer #name").val( hapusname );

    document.getElementById("hapusnamekontainer").innerHTML = hapusname;

});


$(document).on("click", ".open-ModalPosisiTambah", function () {
     var id = $(this).data('id');
     var name = $(this).data('name');
    
     $(".modal-body #id_kontainer").val( id );
     $(".modal-body #name_kontainer").val( name );

    document.getElementById("id").innerHTML = id;
    document.getElementById("name").innerHTML = name;

});

$(document).on("click", ".open-ModalPosisiEdit", function () {
     var id = $(this).data('id');
     var name = $(this).data('name');
     var keterangan = $(this).data('keterangan');
     var id_kontainer = $(this).data('id_kontainer');
    
    

     $(".modal-body #id").val( id );
     $(".modal-body #id_kontainer").val( id_kontainer );
     $(".modal-body #name").val( name );
     $(".modal-body #keterangan").val( keterangan );

    document.getElementById("id").innerHTML = id;
    document.getElementById("name").innerHTML = name;
    document.getElementById("keterangan").innerHTML = keterangan;
});

$(document).on("click", ".open-ModalPosisiHapus", function () {
     var hapusid = $(this).data('id');
     var hapusname = $(this).data('name');
    
     $(".modal-footer #id").val( hapusid );
     $(".modal-footer #name").val( hapusname );

    document.getElementById("hapusnameposisi").innerHTML = hapusname;

});

</script>

      
<div class="col-sm-12" align="left">          
    <ul class="nav nav-tabs bg-purple">
      <li class="nav-item"><a class="nav-link active" href="#">Dashboard Container</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ route('jenis_kontainer') }}">Jenis Container</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ route('kontainer') }}">Container</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ route('posisi_kontainer') }}">Posisi</a></li>
      <li class="nav-item"><a class="nav-link" href="{{ route('lokasi_kontainer') }}">Lokasi</a></li>
    </ul>
    <div class="panel panel-default">
        <div class="row">    
            <div class="col-sm-12">
                <button type="submit" class="card-title btn btn-link pull-right" data-toggle="modal" data-target="#ModalLokasi" data-backdrop="static" data-keyboard="false">
                    <i class="fa fa-plus-circle"></i> Tambah Lokasi
                </button>
            </div>
        </div>
        <div class="row">    
        
        @foreach($listlokasikontainer as $key => $lokasi)
        <div class="row">    
            <div class="col-sm-12">
               <a href="#demo_{{$lokasi->id_lokasi}}" class="btn btn-link" data-toggle="collapse"> <h5><strong>{{$lokasi->nama_lokasi}}</strong></h5> </a>
            </div>
        </div>
            <div id="demo_{{$lokasi->id_lokasi}}" class="collapse col-md-12 col-sm-12 col-xs-12">           
                <div class="card card-stats">
                    <div class="form-group row" align="center">
                        <div class="col-sm-11">

                            <h5><strong>{{$lokasi->nama_lokasi}}</strong></h5>
                        </div> 
                        <div class="col-sm-1">
                            
                            <button style="margin-right: -30px;margin-top:-10px" type="submit" class="open-ModalLokasiEdit card-title btn btn-link pull-right" data-toggle="modal" data-target="#ModalLokasiEdit" data-backdrop="static" data-keyboard="false" data-id="{{$lokasi->id_lokasi}}" data-name="{{$lokasi->nama_lokasi}}"  data-keterangan="{{$lokasi->keterangan_lokasi}}" rel="tooltip" data-title="Ubah data {{ $lokasi->nama_lokasi }}">
                                <i class="fa fa-gear"></i>
                            </button>
                        </div>
                        <hr>
                        <?php 
                            $listkontainer = App\Models\MS_Kontainer::where('id_lokasi', $lokasi->id_lokasi)->get();
                            if(count($listkontainer) > 0 ) {
                        ?>
                        @foreach($listkontainer as $key => $kontainer)

                        <?php 
                            $jenis_kontainer = App\Models\MS_Jenis_Kontainer::getbyID($kontainer->id_jenis_kontainer);
                        ?>
                        <div class="col-md-3 col-sm-12 col-xs-12">           
                            <div class="card card-stats">
                                <div class="form-group row" align="left">
                                    <div class="col-sm-10">
                                        <h6>{{ $jenis_kontainer->nama_jenis_kontainer }} :: {{ $kontainer->nama_kontainer }}</h6>
                                        <hr>
                                    </div>

                                    <div class="col-sm-2">
                                        <button style="margin-left: -15px;margin-top:-10px" type="submit" class="open-ModalKontainerEdit  btn btn-link " data-toggle="modal"   data-target="#ModalKontainerEdit" data-backdrop="static" data-keyboard="false" data-id="{{$kontainer->id_kontainer}}" data-name="{{$kontainer->nama_kontainer}}" data-keterangan="{{$kontainer->keterangan_kontainer}}" data-id_lokasi="{{$kontainer->id_lokasi}}" data-jenis="{{$kontainer->id_jenis_kontainer}}" rel="tooltip" data-title="Ubah Kontainer {{ $kontainer->nama_kontainer }}">
                                                <i class="fa fa-gear"></i>
                                        </button>
                                    </div>

                                    <?php 
                                    $listposisi = App\Models\MS_Posisi_Kontainer::where('id_kontainer', $kontainer->id_kontainer)->get();
                                    if(count($listposisi) > 0 ) {
                                    ?>
                                    @foreach($listposisi as $key => $posisi)
                                   
                                    <div class="col-md-12 col-sm-12 col-xs-12">           
                                        <div class="card card-stats">
                                            <div class="form-group row" align="center" >
                                                <div class="col-sm-10" style="line-height: 50px;">
                                                    <h7>{{ $posisi->label_posisi_kontainer }}</h7>
                                                </div>
                                                <div class="col-sm-2">
                                                    <button style="margin-left: -15px;margin-top:-10px;margin-bottom:-10px" type="submit" class="open-ModalPosisiEdit  btn btn-link " data-toggle="modal"   data-target="#ModalPosisiEdit" data-backdrop="static" data-keyboard="false" data-id="{{$posisi->id_posisi_kontainer}}" data-name="{{$posisi->label_posisi_kontainer}}" data-keterangan="{{$posisi->keterangan_posisi_kontainer}}" data-id_kontainer="{{$posisi->id_kontainer}}" rel="tooltip" data-title="Ubah Label {{ $posisi->label_posisi_kontainer }}">
                                                        <i class="fa fa-gear"></i>
                                                    </button>
                                                    <button style="margin-left: -15px;margin-top:-10px;margin-bottom:-10px" type="submit" class="open-ModalPosisiHapus btn btn-link " data-toggle="modal" data-target="#ModalPosisiHapus" data-backdrop="static" data-keyboard="false" data-id="{{ $posisi->id_posisi_kontainer }}" data-name="{{ $posisi->label_posisi_kontainer }}" rel="tooltip" data-title="Hapus Label {{ $posisi->label_posisi_kontainer }}">
                                                        <i class="fa fa-minus-circle"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                     @endforeach
                                    <?php 
                                        }
                                    ?>
                                    
                                   
                                    <div class="col-sm-10">
                                        <button style="margin-left: -30px;margin-bottom:-10px" type="submit" class="open-ModalPosisiTambah btn btn-link " data-toggle="modal" data-target="#ModalPosisiTambah" data-backdrop="static" data-keyboard="false" data-id="{{$kontainer->id_kontainer}}" data-name="{{$kontainer->nama_kontainer}}" rel="tooltip" data-title="Tambah Slot di Kontainer {{ $kontainer->nama_kontainer }}">
                                            <i class="fa fa-plus-circle"></i>
                                        </button>
                                    </div>
                                    <div class="col-sm-2">
                                        
                                        <button style="margin-left: -15px;margin-bottom:-10px" type="submit" class="open-ModalKontainerHapus btn btn-link " data-toggle="modal" data-target="#ModalKontainerHapus" data-backdrop="static" data-keyboard="false" data-id="{{ $kontainer->id_kontainer }}" data-name="{{ $kontainer->nama_kontainer }}" rel="tooltip" data-title="Hapus Kontainer {{ $kontainer->nama_kontainer }}">
                                            <i class="fa fa-minus-circle"></i>
                                        </button>
                                    </div>  
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <?php 
                            }
                        ?>
                    </div>

                    <div class="form-group row" align="left">
                        <div class="col-sm-11">
                            <button style="margin-left: -30px;margin-bottom:-10px" type="submit" class="open-ModalKontainerTambah card-title btn btn-link " data-toggle="modal" data-target="#ModalKontainerTambah" data-backdrop="static" data-keyboard="false" data-id="{{$lokasi->id_lokasi}}" data-name="{{$lokasi->nama_lokasi}}" rel="tooltip" data-title="Tambah Kontainer di {{ $lokasi->nama_lokasi }}">
                                <i class="fa fa-plus-circle"></i>
                            </button>
                        </div> 
                        <div class="col-sm-1">
                            
                            <button style="margin-left: 55px;margin-bottom:-10px" type="submit" class="open-ModalLokasiHapus card-title btn btn-link " data-toggle="modal" data-target="#ModalLokasiHapus" data-backdrop="static" data-keyboard="false" data-id="{{$lokasi->id_lokasi}}" data-name="{{$lokasi->nama_lokasi}}" rel="tooltip" data-title="Hapus lokasi {{ $lokasi->nama_lokasi }}">
                                <i class="fa fa-minus-circle"></i>
                            </button>
                        </div>
                    </div>    
                </div>
            </div>
        @endforeach           
        
        </div>

    </div>  
</div>



<!-- Modal -->
<div class="modal fade" id="ModalLokasi" tabindex="-1" role="dialog" aria-labelledby="LokasiModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    
    <form method="POST" action="{{ route('lokasi_kontainer_save') }}">
        <input type="hidden" name="route" value="dashboard_kontainer">
    {{ csrf_field() }} 
    @csrf
    <div class="modal-body">
        <div class="card card-stats">
          <div class="card-header card-header-icon">
            <div class="card-icon">
              <i class="fa fa-building"></i>
            </div>
            <h3 class="card-title text-primary"><strong>Tambah Lokasi</strong></h3>                
          </div>
          <hr>
          <div class="col-sm-12">
                                               
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Nama Lokasi
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Keterangan
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 ">
                        <input type="text" class="form-control" id="keterangan" name="keterangan">
                      </div>
                    </div>
                </div>                  
          </div>
        </div>
     </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
    </form>

  </div>
</div>
</div>


  <!-- POPUP EDIT LOKASI -->
<div class="modal fade" id="ModalLokasiEdit" tabindex="-1" role="dialog" aria-labelledby="EditLokasiModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">

        <form method="POST" action="{{ route('lokasi_kontainer_save') }}">
        {{ csrf_field() }} 
        @csrf
        <div class="modal-body">
            <div class="card card-stats">
                <div class="card-header card-header-icon">
                    <div class="card-icon">
                      <i class="fa fa-building"></i>
                    </div>
                    <h3 class="card-title text-primary"><strong>Ubah Lokasi</strong></h3>                
                  </div>
                  <hr>

            <input type="hidden" name="id" id="id"  />
                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        Nama Lokasi
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        Keterangan Lokasi
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="keterangan" name="keterangan">
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="mode" value="edit">
            <input type="hidden" name="route" value="dashboard_kontainer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>

        </form>
    </div>
  </div>
</div>

  <!-- POPUP HAPUS LOKASI -->
<div class="modal fade" id="ModalLokasiHapus" tabindex="-1" role="dialog" aria-labelledby="HapusLokasiModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS LOKASI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus Lokasi <span id="hapusname"></span> ?       

      </div>
      <div class="modal-footer">
       
        <form>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
        </form>

        <form action="/lokasi_kontainer_delete" method="post" enctype="multipart/form-data">                                          
            {{ csrf_field() }}  
            <input type="hidden" name="id" id="id"  />
            <input type="hidden" name="name" id="name"  />
            <input type="hidden" name="mode" value="delete">
            <input type="hidden" name="route" value="dashboard_kontainer">
            <input type="hidden" name="_method" value="POST">
            <button type="submit" class="btn btn-success" id="submit" name="submit">
                <i class="fa fa-trash"></i>&emsp;Ya, Hapus
            </button>
        </form>



      </div>
    </div>
  </div>
</div>


<!-- POPUP TAMBAH KONTAINER -->
<div class="modal fade" id="ModalKontainerTambah" tabindex="-1" role="dialog" aria-labelledby="TambahKontainerModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    
    <form method="POST" action="{{ route('kontainer_save') }}">
        <input type="hidden" name="route" value="dashboard_kontainer">

    {{ csrf_field() }} 
    @csrf
    <div class="modal-body">
        <div class="card card-stats">
          <div class="card-header card-header-icon">
            <div class="card-icon">
              <i class="fa fa-cubes"></i>
            </div>
            <h3 class="card-title text-primary"><strong>Tambah Kontainer</strong></h3>                
          </div>
          <hr>
          <div class="col-sm-12">
                                               
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Nama Kontainer
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Lokasi
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="hidden" id="id_lokasi" name="id_lokasi">
                        <input type="text" class="form-control" id="name_lokasi" name="name_lokasi" disabled="true">
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4" align="left">
                                Jenis Kontainer
                        </div>
                        <div class="col-md-8">

                            <select class="form-control input-sm" name="id_jenis_kontainer" id="id_jenis_kontainer">
                                <option value="0"  selected="true">Pilih Jenis</option>

                            @foreach ($listjeniskontainer as $jenis_kontainer)
                                <option value="{{ $jenis_kontainer->id_jenis_kontainer }}" >{{ str_replace('_',' ',$jenis_kontainer->nama_jenis_kontainer) }}</option>
                            @endforeach
                            </select>


                        </div>
                    </div>  
                </div>    
                <!--div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Jumlah Slot
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" id="jumlah_slot" name="jumlah_slot">
                      </div>
                    </div>
                </div-->
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Keterangan
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 ">
                        <input type="text" class="form-control" id="keterangan" name="keterangan">
                      </div>
                    </div>
                </div>                  
          </div>
        </div>
     </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
    </form>

  </div>
</div>
</div>


<!-- POPUP EDIT KONTAINER -->
<div class="modal fade" id="ModalKontainerEdit" tabindex="-1" role="dialog" aria-labelledby="EditKontainerModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    
    <form method="POST" action="{{ route('kontainer_save') }}">

                       
    {{ csrf_field() }} 
    @csrf
    <div class="modal-body"> 
        <input type="hidden" id="id" name="id">
        <input type="hidden" id="id_lokasi" name="id_lokasi">
        <div class="card card-stats">
          <div class="card-header card-header-icon">
            <div class="card-icon">
              <i class="fa fa-cubes"></i>
            </div>
            <h3 class="card-title text-primary"><strong>Ubah Kontainer</strong></h3>                
          </div>
          <hr>
          <div class="col-sm-12">
                                               
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Nama Kontainer
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">                        
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-4" align="left">
                                Jenis Kontainer
                        </div>
                        <div class="col-md-8">

                            <select class="form-control input-sm" name="id_jenis_kontainer" id="id_jenis_kontainer">
                                <option value="0"  selected="true">Pilih Jenis</option>

                            @foreach ($listjeniskontainer as $jenis_kontainer)
                                <option value="{{ $jenis_kontainer->id_jenis_kontainer }}" 
                                    >{{ str_replace('_',' ',$jenis_kontainer->nama_jenis_kontainer) }}</option>
                            @endforeach
                            </select>


                        </div>
                    </div>  
                </div>    
                <!--div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Jumlah Slot
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" id="jumlah_slot" name="jumlah_slot">
                      </div>
                    </div>
                </div-->
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Keterangan
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 ">
                        <input type="text" class="form-control" id="keterangan" name="keterangan">
                      </div>
                    </div>
                </div>                  
          </div>
        </div>
     </div>
    <div class="modal-footer">
            <input type="hidden" name="mode" value="edit">
            <input type="hidden" name="route" value="dashboard_kontainer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
    </form>

  </div>
</div>
</div>



  <!-- POPUP HAPUS KONTAINER -->
<div class="modal fade" id="ModalKontainerHapus" tabindex="-1" role="dialog" aria-labelledby="HapusKontainerModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS KONTAINER</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus Kontainer <span id="hapusnamekontainer"></span> ?       

      </div>
      <div class="modal-footer">
       
        <form>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
        </form>

        <form action="/kontainer_delete" method="post" enctype="multipart/form-data">                                          
            {{ csrf_field() }}  
            <input type="hidden" name="id" id="id"  />
            <input type="hidden" name="name" id="name"  />
            <input type="hidden" name="mode" value="delete">
            <input type="hidden" name="route" value="dashboard_kontainer">
            <input type="hidden" name="_method" value="POST">
            <button type="submit" class="btn btn-success" id="submit" name="submit">
                <i class="fa fa-trash"></i>&emsp;Ya, Hapus
            </button>
        </form>



      </div>
    </div>
  </div>
</div>



<!-- POPUP TAMBAH POSISI -->
<div class="modal fade" id="ModalPosisiTambah" tabindex="-1" role="dialog" aria-labelledby="TambahPosisiModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    
    <form method="POST" action="{{ route('posisi_kontainer_save') }}">
        <input type="hidden" name="route" value="dashboard_kontainer">

    {{ csrf_field() }} 
    @csrf
    <div class="modal-body">
        <div class="card card-stats">
          <div class="card-header card-header-icon">
            <div class="card-icon">
              <i class="fa fa-cubes"></i>
            </div>
            <h3 class="card-title text-primary"><strong>Tambah Posisi Kontainer</strong></h3>                
          </div>
          <hr>
          <div class="col-sm-12">
                                               
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Label Posisi
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Kontainer
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">
                        <input type="hidden" id="id_kontainer" name="id_kontainer">
                        <input type="text" class="form-control" id="name_kontainer" name="name_kontainer" disabled="true">
                      </div>
                    </div>
                </div>                
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Keterangan
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 ">
                        <input type="text" class="form-control" id="keterangan" name="keterangan">
                      </div>
                    </div>
                </div>                  
          </div>
        </div>
     </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
    </form>

  </div>
</div>
</div>



<!-- POPUP EDIT POSISI -->
<div class="modal fade" id="ModalPosisiEdit" tabindex="-1" role="dialog" aria-labelledby="EditPosisiModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    
    <form method="POST" action="{{ route('posisi_kontainer_save') }}">

                       
    {{ csrf_field() }} 
    @csrf
    <div class="modal-body"> 
        <input type="hidden" id="id" name="id">
        <input type="hidden" id="id_kontainer" name="id_kontainer">
        <div class="card card-stats">
          <div class="card-header card-header-icon">
            <div class="card-icon">
              <i class="fa fa-cubes"></i>
            </div>
            <h3 class="card-title text-primary"><strong>Ubah Label</strong></h3>                
          </div>
          <hr>
          <div class="col-sm-12">
                                               
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Label Posisi
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8">                        
                        <input type="text" class="form-control" id="name" name="name">
                      </div>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <div class="row">
                      <div class="col-md-4 col-sm-4 col-xs-4">
                        Keterangan
                      </div>
                      <div class="col-md-8 col-sm-8 col-xs-8 ">
                        <input type="text" class="form-control" id="keterangan" name="keterangan">
                      </div>
                    </div>
                </div>                  
          </div>
        </div>
     </div>
    <div class="modal-footer">
            <input type="hidden" name="mode" value="edit">
            <input type="hidden" name="route" value="dashboard_kontainer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
      <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
    </form>

  </div>
</div>
</div>


  <!-- POPUP HAPUS POSISI -->
<div class="modal fade" id="ModalPosisiHapus" tabindex="-1" role="dialog" aria-labelledby="HapusPosisiModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS POSISI</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus <span id="hapusnameposisi"></span> ?       

      </div>
      <div class="modal-footer">
       
        <form>
            <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
        </form>

        <form action="/posisi_kontainer_delete" method="post" enctype="multipart/form-data">                                          
            {{ csrf_field() }}  
            <input type="hidden" name="id" id="id"  />
            <input type="hidden" name="name" id="name"  />
            <input type="hidden" name="mode" value="delete">
            <input type="hidden" name="route" value="dashboard_kontainer">
            <input type="hidden" name="_method" value="POST">
            <button type="submit" class="btn btn-success" id="submit" name="submit">
                <i class="fa fa-trash"></i>&emsp;Ya, Hapus
            </button>
        </form>



      </div>
    </div>
  </div>
</div>
@endsection