<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/manajemenAset">Pengiriman Tanpa Permintaan</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Input Pengiriman Tanpa Permintaan</u></li>
  </ol>
</nav>

<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form method="POST" action="{{ route('simpanAsetKeluar') }}" enctype="multipart/form-data">
      <div class="card card-profile">
        <div class="card-header card-header-primary">
          <h4 class="card-title"><strong>Input Pengiriman Tanpa Permintaan</strong></h4>
        </div>
        <div class="card-body" align="left">
        </div>
      </div>

      <div class="card card-profile">
        <div class="card-body" align="left">

          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <!-- <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
              ID Pengiriman
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="text" name="id_pengiriman" id="id-pengiriman" class="form-control">
              </div>
            </div>
            <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
              ID Alokasi
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="text" name="id_lokasi" id="id-lokasi" class="form-control">
              </div>
            </div> -->
          </div>

          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
                No. Pengiriman
              </div>
              <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
                <div class="input-group">
                  <input type="text" name="id_permintaan" id="id-permintaan" class="form-control">
                </div>
              </div>
            <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
                Alokasi untuk
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                  <select class="form-control input-sm" name="kode_kanim_alokasi" id="kode-kanim-alokasi">
                    <option value="" selected="selected">- Pilih Kantor Imigrasi -</option>
                    @foreach ($list_kantor_imigrasi as $kanim)
                      <option value="{{ $kanim->kode_kanim }}">{{ $kanim->nama_kanim }}</option>
                    @endforeach
                  </select>
                <input type="text" class="form-control" name="lokasi_keluar_lain_alokasi" id="lokasi-keluar-lain-alokasi" placeholder="Masukkan lokasi satuan kerja lain">
              </div>
            </div>
                <!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" id="tidak-menemukan-alokasi">
                  Tidak menemukan satuan kerja yang dituju?<br><a id="klik-disini-alokasi" style="cursor: pointer;"><u class="text-primary">Klik disini</u></a> untuk mengisi secara manual.
                </div> -->
          </div>
          
          <hr>
          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
              Nama Kurir
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="text" name="nama_kurir" id="nama-kurir" class="form-control">
              </div>
            </div>
            <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
              Nomor Resi
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="text" name="nomor_resi" id="nomor-resi" class="form-control">
              </div>
            </div>
          </div>

          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
              Alamat Kurir
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <textarea name="alamat_kurir" id="alamat-kurir" class="form-control" rows="3"></textarea> 
              </div>
            </div>
            <div class="form-group col-lg-2 col-md-2 col-sm-2 col-xs-2">
              Biaya Resi
            </div>
            <div class="form-group col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="text" name="biaya_resi" id="biaya-resi" class="form-control">
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <div class="pull-left">
                <strong>Rincian Item Pengiriman Tanpa Permintaan</strong>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <div class="pull-right">
                <h5>Total item terpilih: <strong><span class="badge badge-primary" id="count-terpilih">0</span></strong></h5>
              </div>
            </div>
            <input type="hidden" id="index-item-aset-keluar" name="index_item_aset_keluar" value="">
            <input type="hidden" id="count-item-aset-keluar" name="count_item_aset_keluar" value="0">
          </div>
          <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="table-responsive">
              <table class="table">
                <colgroup>
                  <col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;"><col style="width:2%;">
                </colgroup>
                <tbody>
                  @foreach($list_kategori_aset as $key => $value)
                  <tr id="kategori-aset-{{ $value->id_kategori_aset }}">
                    <td colspan="50" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;">
                      <input type="hidden" id="check-kategori-aset-{{ $value->id_kategori_aset }}" value="0">
                      <button type="button" class="btn btn-md btn-link btn-primary" style="margin-right: 3px; padding: 0px;" id="btn-exp-col-kategori-aset-{{ $value->id_kategori_aset }}"><i class="fa fa-caret-right"></i></button>
                      &emsp;
                      <strong class="text-primary">{{ $value->nama_kategori_aset }}</strong>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div> -->
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                &emsp;Dokumen Pengiriman &emsp;
                <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-dokumen" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
              </div>
              <input type="hidden" id="count-dokumen" name="count_dokumen" value="0" />
              <input type="hidden" id="last-dokumen-id" value="0" />
            </div>
            <div id="dokumen-aset-keluar">

            </div>
          </div>
        </div>
      </div>

      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row pull-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Kirim</button>
          </div>
        </div>
      </div>
      {{ csrf_field() }}
    </form>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
    $("select").select2();
  });
  $(document).ready(function(){
    $("[id^=jenis-aset_]").hide();
    $("[id^=aset_]").hide();
    $("[id^=satuan-aset_]").hide();
    $("#div-alokasi-aset").hide();
    $("#div-peminjaman-aset").hide();
    $("#lokasi-keluar-lain-alokasi").hide();
    $("#lokasi-keluar-lain-peminjaman").hide();
    moment.locale('id');

      // window.onbeforeunload = function () {
      //   var msg = "Apakah Anda akan meninggalkan halaman ini? Data akan hilang sebelum Anda menyimpannya.";
      //   return msg;
      // };

    $(document).on("change", "#jns-aset-keluar", function (e) {
      if ($('#jns-aset-keluar').val() == 'Alokasi'){
        $('#div-alokasi-aset').fadeIn().delay(500);
        $('#div-peminjaman-aset').fadeOut().delay(500);

        // $('select[name="options"]')
        $('#kode-kanim-peminjaman').find('option:contains("Pilih")').attr("selected",true);
        // $('#pemberi-aset-peminjaman').value("");
        // $('#penerima-aset-peminjaman').value("");
        $('#tanggal-aset-kembali').value("");
        $('#lokasi-keluar-lain-peminjaman').value("");

      } else if ($('#jns-aset-keluar').val() == 'Peminjaman'){
        $('#div-peminjaman-aset').fadeIn().delay(500);
        $('#div-alokasi-aset').fadeOut().delay(500);

        $('#kode-kanim-alokasi').find('option:contains("Pilih")').attr("selected",true);
        // $('#pemberi-aset-alokasi').value("");
        // $('#penerima-aset-alokasi').value("");
        $('#lokasi-keluar-lain-alokasi').value("");
      }
    });

    $(document).on("click", "#klik-disini-alokasi", function (e) {
      $('#kode-kanim-alokasi').find('option:contains("Pilih")').attr("selected",true);
      $('#kode-kanim-alokasi').fadeOut().delay(500);
      $("#lokasi-keluar-lain-alokasi").fadeIn().delay(500);
      $('#tidak-menemukan-alokasi').fadeOut().delay(500);
    });

    $(document).on("click", "#klik-disini-peminjaman", function (e) {
      $('#kode-kanim-peminjaman').find('option:contains("Pilih")').attr("selected",true);
      $('#kode-kanim-peminjaman').fadeOut().delay(500);
      $("#lokasi-keluar-lain-peminjaman").fadeIn().delay(500);
      $('#tidak-menemukan-peminjaman').fadeOut().delay(500);
    });

    $(document).on("click", "[id^=btn-exp-col-kategori-aset-]", function (e) {
      e.preventDefault();
      var targetId = e.currentTarget.id.split('-').pop();
      if ($('#btn-exp-col-kategori-aset-' + targetId).html().indexOf("fa-caret-right") != -1) {
        if($('#check-kategori-aset-' + targetId).val() == 0) {
          $.ajax({
             type:"GET",
             url:"{{url('api/get-jenis-aset-list')}}?kategori_aset=" + targetId,
             success:function(res){
              if(res){
                $('#kategori-aset-' + targetId).after('<tr id="jenis-aset_' + targetId + '-00"><td colspan="50"></td></tr>');
                $.each(res,function(index,Obj){
                  var newRowJenisAset = '<tr id="jenis-aset_' + targetId + '-' + Obj.id_jenis_aset + '"><td colspan="2"></td><td colspan="48" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><button type="button" class="btn btn-md btn-link btn-primary" id="btn-exp-col-jenis-aset_' + targetId + '-' + Obj.id_jenis_aset + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-caret-right"></i></button>&emsp; <strong class="text-primary">' + Obj.nama_jenis_aset + '</strong><input type="hidden" id="check-jenis-aset_' + targetId + '-' + Obj.id_jenis_aset + '" value="0"></td></tr>';
                  $('#kategori-aset-' + targetId).after(newRowJenisAset);
                });
                $('#check-kategori-aset-' + targetId).val(1);
              } else {
                // console.log('why');
              }
             }, error:function(x) {
              alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
              console.log(x);
             }
          });
        }        
        $('[id^=jenis-aset_' + targetId + '-]').fadeIn().delay(500);
        $('#btn-exp-col-kategori-aset-' + targetId).html('<i class="fa fa-caret-down"></i>');
      } else {
        $('[id^=jenis-aset_' + targetId + '-]').fadeOut().delay(500);
        $('[id^=aset_' + targetId + '-]').fadeOut().delay(500);
        $('[id^=satuan-aset_' + targetId + '-]').fadeOut().delay(500);
        $('#btn-exp-col-kategori-aset-' + targetId).html('<i class="fa fa-caret-right"></i>');
        $('[id^=btn-exp-col-jenis-aset_' + targetId + '-]').html('<i class="fa fa-caret-right"></i>');
        $('[id^=btn-exp-col-aset_' + targetId + '-]').html('<i class="fa fa-caret-right"></i>');
        $('[id^=thead-satuan-aset_' + targetId + '-]').fadeOut().delay(500);
      }
    });

    $(document).on("click", "[id^=btn-exp-col-jenis-aset_]", function (e) {
      e.preventDefault();
      var target = e.currentTarget.id.split('_').pop();
      var targetKategoriAsetId = target.split('-')[0];
      var targetJenisAsetId = target.split('-')[1];

      if ($('#btn-exp-col-jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).html().indexOf("fa-caret-right") != -1) {
        if ($('#check-jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).val() == 0) {
          $.ajax({
             type:"GET",
             url:"{{url('api/get-aset-list')}}?kategori_aset=" + targetKategoriAsetId +  "&jenis_aset=" + targetJenisAsetId,
             success:function(res){
              if(res){
                if (res.length != 0) {
                  $('#jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).after('<tr id="aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-00"><td colspan="50"></td></tr>');
                  $.each(res,function(index, Obj){
                    var newRowAset = '<tr id="aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + Obj.id_aset + '"><td colspan="4"></td><td colspan="18" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><button type="button" class="btn btn-md btn-link btn-primary" id="btn-exp-col-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + Obj.id_aset + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-caret-right"></i></button>&emsp; <strong class="text-primary">' + Obj.nama_merek + ' ' + Obj.tipe_aset + '</strong></td><td colspan="4" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;">total: <strong class="text-primary">' + (Obj.total_stok_aset?Obj.total_stok_aset:"0") + '</strong></td><td colspan="4" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;">tersedia: <strong class="text-success">' + (Obj.jumlah_stok_tersedia?Obj.jumlah_stok_tersedia:"0") + '</strong></td><td colspan="4" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;">terpakai: <strong class="text-danger">' + (Obj.total_stok_aset - Obj.jumlah_stok_tersedia) + '</strong></td><td colspan="16" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><button type="button" class="btn btn-sm btn-primary btn-secondary" id="btn-gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + Obj.id_aset + '">Lihat Gambar</button><img src="' + Obj.link_gambar.replace('public', '/storage') + '" id="gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + Obj.id_aset + '" alt="' + Obj.nama_gambar + '" class="gambar-aset-thumbnails"><input type="hidden" id="check-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + Obj.id_aset + '" value="0"></td></tr><tr id="thead-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + Obj.id_aset + '-" style="display: none;"><td colspan="6"></td><td colspan="4" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><strong class="text-info">SATUAN #</strong></td><td colspan="14" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><strong class="text-info">FIELD ASET & ISIANNYA </strong></td><td colspan="8" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><strong class="text-info">SUMBER PENGADAAN</strong></td><td colspan="10" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><strong class="text-info">STATUS TERAKHIR</strong></td><td colspan="6" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><strong class="text-info">KETERANGAN</strong></td><td colspan="2" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><strong class="text-info">PILIH</strong></td></tr>';
                    $('#jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).after(newRowAset);
                  });
                  $('[id^=gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-]').hide();
                  $('#check-jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).val(1);
                } else {
                  var newRowAset = '<tr id="aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-0"><td colspan="4"></td><td colspan="46" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><i>Tidak ada data.</i></td></tr></tr><tr id="aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-00"><td colspan="50"></td></tr>';
                    $('#jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).after(newRowAset);
                    $('#check-jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).val(1); 
                  
                }
              }
             }, error:function(x) {
              alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
              console.log(x);
             }
          });
        }
        $('[id^=aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-]').fadeIn().delay(500);        
        $('#btn-exp-col-jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).html('<i class="fa fa-caret-down"></i>');
      } else {
        $('[id^=aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-]').fadeOut().delay(500);
        $('[id^=satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-]').fadeOut().delay(500);
        $('#btn-exp-col-jenis-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId).html('<i class="fa fa-caret-right"></i>');
        $('[id^=btn-exp-col-aset_' + targetKategoriAsetId + '-]').html('<i class="fa fa-caret-right"></i>');
        $('[id^=thead-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-]').fadeOut().delay(500);
      }
    });

    $(document).on("click", "[id^=btn-exp-col-aset_]", function (e) {
      e.preventDefault();
      var target = e.currentTarget.id.split('_').pop();
      var targetKategoriAsetId = target.split('-')[0];
      var targetJenisAsetId = target.split('-')[1];
      var targetAsetId = target.split('-')[2];

      if ($('#btn-exp-col-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).html().indexOf("fa-caret-right") != -1) {
        if ($('#check-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).val() == 0) {
          $.ajax({
             type:"GET",
             url:"{{url('api/get-satuan-aset-list')}}?aset=" + targetAsetId + "&jenis_aset=" + targetJenisAsetId,
             success:function(res){
              if(res){
                console.log(res.list_satuan_aset);
                if (res.list_satuan_aset.length != 0) {
                  var idx_satuan = 0;
                  var prev_idx_satuan = 0;
                  var prev_kode_grup = 0;
                  var grup_field_isian_aset = '';
                  var id_grup_satuan = '';
                  var last_idx_list = parseInt(res.list_satuan_aset.length) - 1;
                  var flag_grup = 0;
                  
                  $('#satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-00-').after('<tr id="aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-00"><td colspan="50"></td></tr>');

                  $.each(res.list_satuan_aset,function(index, Obj){
                    var status_satuan_aset = '-';
                    var keterangan_satuan_aset = '-';
                    var sumber_pengadaan = '-';
                    var select_satuan_aset = '';
                    if (Obj.asa_id_aset_keluar != null) {
                      if (Obj.ake_jenis_aset_keluar == 'Alokasi')
                      {
                        status_satuan_aset = '<i class="text-success">Sudah dialokasikan ke ';
                        if (Obj.ake_kode_kanim) {
                          status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                          // keterangan_satuan_aset = 'Penerima: ' + Obj.penerima_aset;
                        } else {
                          status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                        }
                        status_satuan_aset = status_satuan_aset + ' pada tanggal ' + moment(Obj.ake_tanggal_aset_keluar).format('Do MMMM YYYY') + '.</i>';
                      }
                      else
                      {
                        status_satuan_aset = '<i class="text-success">Aset dipinjamkan kepada ';
                        if (Obj.ake_kode_kanim) {
                          status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                          // keterangan_satuan_aset = 'Penerima: ' + Obj.penerima_aset;
                        } else {
                          status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                        }
                        status_satuan_aset = status_satuan_aset + ' dan harus dikembalikan pada tanggal ' + moment(Obj.ake_tanggal_aset_kembali).format('Do MMMM YYYY') + '.</i>'
                      }
                    } else {
                      if (Obj.asa_id_lokasi) {
                        status_satuan_aset = 'Belum dialokasikan dan masih tersimpan di ' + Obj.asa_nama_lokasi;
                        if (Obj.asa_id_kontainer) {
                          status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_nama_kontainer;
                          if(Obj.asa_id_posisi_kontainer) {
                            status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_label_posisi_kontainer;
                          }
                        } 
                      }
                      select_satuan_aset = '<div class="form-check"><label class="form-check-label"><input class="form-check-input" type="checkbox" value="' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '" id="select-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '"><span class="form-check-sign"><span class="check"></span></span></label></div>';
                    }
                      
                    if (Obj.p_kode_pengadaan) {
                      sumber_pengadaan = Obj.p_nama_pengadaan + ' Tahun ' + Obj.p_tahun_pengadaan;
                    }

                    if (prev_kode_grup == 0) {
                      //THIS IS THE SHIT (CATAT ASET KELUAR)
                      var newRowSatuanAset = '<tr id="satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-"><td colspan="6"></td><td colspan="4" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-satuan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">#' + Obj.asa_kode_grup_satuan + '</td><td colspan="14" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-field-isian-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-"><i>' + Obj.asa_field_aset + '</id>: <strong style="color: blue;">' + Obj.asa_isian_aset  + '</strong></td><td colspan="8" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-sumber-pengadaan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + sumber_pengadaan + '</td><td colspan="10" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-status_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + status_satuan_aset + '</td><td colspan="6" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-keterangan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + keterangan_satuan_aset + '</td><td colspan="2" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-select-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + select_satuan_aset + '</td></tr>';
                      if (idx_satuan == 0) {
                        $('#thead-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-').after(newRowSatuanAset);
                      } else {
                        $('#satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + prev_idx_satuan + '-').after(newRowSatuanAset);
                      }
                      prev_idx_satuan = Obj.asa_id_satuan;
                      idx_satuan++;
                      id_grup_satuan = targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-';
                      if (flag_grup == 1) grup_field_isian_aset = grup_field_isian_aset + '<hr>';
                      grup_field_isian_aset = grup_field_isian_aset + '<i>' + Obj.asa_field_aset + '</i>: <strong style="color: blue;">' + Obj.asa_isian_aset  + '</strong>';
                    } else if (Obj.asa_kode_grup_satuan == prev_kode_grup) {
                      grup_field_isian_aset = grup_field_isian_aset + '<hr><i>' + Obj.asa_field_aset + '</i>: <strong style="color: blue;">' + Obj.asa_isian_aset  + '</strong>';

                      if(last_idx_list == index) {
                        $('#td-field-isian-aset_' + id_grup_satuan).html(grup_field_isian_aset);
                        grup_field_isian_aset = '';
                        id_grup_satuan = '';
                        flag_grup = 0;
                      }
                      flag_grup = 1;
                    } else {
                      $('#td-field-isian-aset_' + id_grup_satuan).html(grup_field_isian_aset);
                      grup_field_isian_aset = '';
                      id_grup_satuan = '';
                      flag_grup = 0;

                      var newRowSatuanAset = '<tr id="satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-"><td colspan="6"></td><td colspan="4" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-satuan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">#' + Obj.asa_kode_grup_satuan + '</td><td colspan="14" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-field-isian-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-"><i>' + Obj.asa_field_aset + '</i>: <strong style="color: blue;">' + Obj.asa_isian_aset  + '</strong></td><td colspan="8" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-sumber-pengadaan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + sumber_pengadaan + '</td><td colspan="10" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-status_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + status_satuan_aset + '</td><td colspan="6" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-keterangan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + keterangan_satuan_aset + '</td><td colspan="2" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;" id="td-select-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-">' + select_satuan_aset + '</td></tr>';
                      if (idx_satuan == 0) {
                        $('#thead-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-').after(newRowSatuanAset);
                      } else {
                        $('#satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + prev_idx_satuan + '-').after(newRowSatuanAset);
                      }
                      prev_idx_satuan = Obj.asa_id_satuan;
                      idx_satuan++;
                      id_grup_satuan = targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + Obj.asa_id_satuan + '-';
                      if (flag_grup == 1) grup_field_isian_aset = grup_field_isian_aset + '<hr>';
                      grup_field_isian_aset = grup_field_isian_aset + '<i>' + Obj.asa_field_aset + '</i>: <strong style="color: blue;">' + Obj.asa_isian_aset  + '</strong>';
                    }
                    prev_kode_grup = Obj.asa_kode_grup_satuan;
                  });
                  $('#satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + prev_idx_satuan + '-').after('<tr id="satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + (prev_idx_satuan + 1) + '-"><td colspan="50"></td></tr>');
                  $('#check-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).val(1);
                }
                else {
                  var newRowSatuanAset = '<tr id="satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-0-"><td colspan="6"></td><td colspan="44" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px;"><i>Tidak ada data.</i></td></tr><tr id="satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-00-"><td colspan="50"></td></tr>';
                  $('#thead-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).after(newRowSatuanAset);
                  $('#check-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).val(1); 
                }
              }
             }, error:function(x) {
              alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
              console.log(x);
             }
          });
        }
        $('[id^=thead-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + ']').fadeIn().delay(500);
        $('[id^=satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-]').fadeIn().delay(500);        
        $('#btn-exp-col-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).html('<i class="fa fa-caret-down"></i>');
      } else {
        $('[id^=satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-]').fadeOut().delay(500);        
        $('#btn-exp-col-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).html('<i class="fa fa-caret-right"></i>');
        $('[id^=thead-satuan-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + ']').fadeOut().delay(500);
      }
    });

    $(document).on("click", "[id^=btn-gambar-aset_]", function (e) {
      var target = e.currentTarget.id.split('_').pop();
      var targetKategoriAsetId = target.split('-')[0];
      var targetJenisAsetId = target.split('-')[1];
      var targetAsetId = target.split('-')[2];

      if ($('#btn-gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).html().indexOf("Lihat Gambar") != -1) {
        $('#gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).fadeIn().delay(500);
        $('#btn-gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).html('Tutup Gambar');
      } else {
        $('#gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).fadeOut().delay(500);
        $('#btn-gambar-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId).html('Lihat Gambar');
      }
    });

    $(document).on("change", "[id^=select-satuan-aset_]", function (e) {
      var target = e.currentTarget.id.split('_').pop();
      var targetKategoriAsetId = target.split('-')[0];
      var targetJenisAsetId = target.split('-')[1];
      var targetAsetId = target.split('-')[2];
      var targetSatuanId = target.split('-')[3];

      var index_item_aset_keluar = $('#index-item-aset-keluar').val();
      var count_item_aset_keluar = $('#count-item-aset-keluar').val();

      if (this.checked) {
        index_item_aset_keluar = index_item_aset_keluar + targetAsetId + "-" + targetSatuanId + '~';
        $('#index-item-aset-keluar').val(index_item_aset_keluar);

        count_item_aset_keluar = parseInt(count_item_aset_keluar) + 1;
        $('#count-item-aset-keluar').val(count_item_aset_keluar);
        $('#count-terpilih').html(count_item_aset_keluar);


        $('#td-satuan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').addClass('table-primary');
        $('#td-field-isian-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').addClass('table-primary');
        $('#td-sumber-pengadaan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').addClass('table-primary');
        $('#td-status_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').addClass('table-primary');
        $('#td-keterangan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').addClass('table-primary');
        $('#td-select-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').addClass('table-primary');
      } else {
        var new_item_aset_keluar = index_item_aset_keluar.replace(targetAsetId + "-" + targetSatuanId + '~', '');
        $('#index-item-aset-keluar').val(new_item_aset_keluar);

        count_item_aset_keluar = parseInt(count_item_aset_keluar) - 1;
        $('#count-item-aset-keluar').val(count_item_aset_keluar);
        $('#count-terpilih').html(count_item_aset_keluar);


        $('#td-satuan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').removeClass('table-primary');
        $('#td-field-isian-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').removeClass('table-primary');
        $('#td-sumber-pengadaan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').removeClass('table-primary');
        $('#td-status_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').removeClass('table-primary');
        $('#td-keterangan_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').removeClass('table-primary');
        $('#td-select-aset_' + targetKategoriAsetId + '-' + targetJenisAsetId + '-' + targetAsetId + '-' + targetSatuanId + '-').removeClass('table-primary');
      }
    });

   $(document).on("click", ".btn-tambah-dokumen", function (e) {
      e.preventDefault();
      var current = $("#last-dokumen-id").val();
      var next = parseInt(current) + 1;
      $('#dokumen-aset-keluar').append('<div id="dokumen-' + (next) + '" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="form-group row"><div class="col-md-3 col-sm-3 col-xs-3"><div class="row"><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-sm btn-link btn-danger" id="btn-hapus-dokumen-' + next + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div><div class="col-md-10 col-sm-10 col-xs-10"><input type="text" class="form-control" placeholder="Nama dokumen" name="nama_dokumentasi~' + (next) + '"> </div></div></div><div class="col-md-5 col-sm-5 col-xs-5"><div class="input-group"><input type="text" class="form-control" id="url-dokumen-aset-keluar-tambah-' + (next) + '" readonly="" style="background-color: white;" placeholder="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB"><div class="input-group-append"><div class="btn btn-primary btn-sm"><span><i class="fa fa-upload"></i></span><input type="file" id="file-dokumen-aset-keluar-tambah-' + (next) + '" rel="tooltip" data-original-title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." class="upload" name="id_dokumentasi~' + (next) + '" /></div></div></div></div><div class="col-md-4 col-sm-4 col-xs-4"><div class="input-group"><input type="date" name="tanggal_dokumentasi~' + (next) + '"class="form-control" placeholder="Tanggal Dokumentasi" rel="tooltip" title="Tanggal Dokumentasi" data-original-title="Tanggal Dokumentasi"><div class="input-group-append"><div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div></div></div></div></div><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><input type="text" class="form-control" placeholder="Keterangan dokumen" name="keterangan_dokumentasi~' + (next) + '"></div></div><br><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><div class="radio"><label class="radio-inline">Apakah ada dokumen secara fisik?</label>&emsp;<br><label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="Y"> Ya </label>&emsp;<label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="N"> Tidak </label></div></div><div id="form-penyimpanan-dokumentasi-' + (next) + '" class="col-md-8 col-sm-8 col-xs-8">Disimpan di<div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~lokasi~' + (next) + '" id="tambah-lokasi-dropdown-' + (next) + '"><option value=""  selected="true">Pilih Lokasi</option></select></div><div class="col-md-4 col-sm-4  col-xs-4"><select class="form-control" name="dokumentasi~kontainer~' + (next) + '" id="tambah-kontainer-dropdown-' + (next) + '"><option value="" selected="true">Pilih kontainer...</option></select></div><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~posisi_kontainer~' + (next) + '" id="tambah-posisi-kontainer-dropdown-' + (next) + '"><option value="" selected>Pilih posisi kontainer...</option></select></div></div></div></div></div></div></div>');
      var count_dokumen = $("#count-dokumen").val();
      $("#count-dokumen").val(parseInt(count_dokumen)+1);
      $("#last-dokumen-id").val(parseInt(next));
      $("#form-penyimpanan-dokumentasi-" + next).hide();                  
    });

    $(document).on("click", "[id^=btn-hapus-dokumen-]", function (e) {
      var targetId = e.currentTarget.id.split('-').pop();
      $('#dokumen-' + targetId).remove();
      var current = $("#count-dokumen").val();
      var next = parseInt(current) - 1;
      $("#count-dokumen").val(next);
    });

    $(document).on("change", "[id^=file-dokumen-aset-keluar-tambah-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $("#url-dokumen-aset-keluar-tambah-" + targetId).val(this.value.replace(/C:\\fakepath\\/i, ''));
      });

      $(document).on("click", ".showmore", function (e) {
        e.preventDefault();
        $(this).parent().parent().next().slideToggle("fast");
        $(this).css("display", "none");
      });

      $('[id^=form-penyimpanan-dokumentasi-]').hide();
      $(document).on('change', '[class^="dokumentasi-fisik-"]', function (e) {
          var targetClass = $(e.currentTarget).attr('class').split(' ').pop();
          var targetClassId = targetClass.split('-').pop();
          e.preventDefault();
          if (this.value == "Y") { //check value if it is domicilio
              $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).show(750); //than show
          } else {
              $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).hide(750); //else hide
              $('#tambah-lokasi-dropdown-' + targetClassId).val('');
              $('#tambah-kontainer-dropdown-' + targetClassId).val('');
              $('#tambah-posisi-kontainer-dropdown-' + targetClassId).val('');
          }
      });

      $(document).on("change", "[id^=tambah-lokasi-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        var lokasi = e.target.value;

        if(lokasi){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kontainer-list')}}?lokasi="+lokasi,
             success:function(res){
              if(res){

                  $('#tambah-kontainer-dropdown-' + targetId).empty();
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
                  $('#tambah-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $("#tambah-kontainer-dropdown-" + targetId).append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
                  });

              }else{
                 $("#tambah-kontainer-dropdown-" + targetId).empty();
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
              }
             }
          });
        }
        else{
          $("#tambah-kontainer-dropdown-" + targetId).empty();
          $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
        }

      });

      $(document).on("change", "[id^=tambah-kontainer-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        var kontainer = e.target.value;

        if(kontainer){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
             success:function(res){
              if(res){

                  $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $("#tambah-posisi-kontainer-dropdown-" + targetId).append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
                  });

              }else{
                 $("#tambah-posisi-kontainer-dropdown-" + targetId).empty();
              }
             }
          });
        }
        else{
          $("#tambah-posisi-kontainer-dropdown-" + targetId).empty();
        }
      });
  });
</script>
@endsection