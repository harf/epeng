<script src="/js/core/jquery.min.js" type="text/javascript"></script>
@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/pengiriman">Pengiriman Tanpa Permintaan</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Informasi Pengiriman #{{ $id_pengiriman }}</u></li>
  </ol>
</nav>
<div class="row">  
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <div class="card card-profile">
      <div class="card-header card-header-primary">
        <h4 class="card-title"><strong>Detail Pengiriman #{{ $id_pengiriman }}</strong></h4>
      </div>
      <div class="card-body" align="left">
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
            <div class="row col-lg-12 col-md-12 col-sm-12"><a href="#"><h4><strong class="text-primary">#{{ $pengiriman->id_pengiriman }}</strong></h4></a></div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">ID Permintaan: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $pengiriman->id_permintaan }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">ID Alokasi: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $pengiriman->id_alokasi }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Nama Kurir: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $pengiriman->nama_kurir }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Alamat Kurir: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $pengiriman->alamat_kurir }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Nomor Resi: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $pengiriman->nomor_resi }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Biaya Kirim: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $pengiriman->biaya_kirim }}</strong></div>
            </div>
            <!-- 
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Status Terakhir: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">Sudah dialokasikan ke Kantor Imigrasi Kelas I Khusus Batam</strong></div>
            </div> -->
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Status Per Tanggal: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $pengiriman->created_at }}</strong></div>
            </div>
          </div>

        </div>
      </div>
    </div>
    
    
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row col-lg-12 col-md-12 col-sm-12">
          <div class="row col-lg-12 col-md-12 col-sm-12">
            <strong class="text-primary"><u>RINCIAN ASET YANG DIKIRIM #{{ $id_pengiriman }}</u></strong>
          </div>
          <div class="form-group row col-lg-12 col-md-12 col-sm-12">
            <table class="table table-bordered table-hover" style="vertical-align: middle; text-align: center;">
              <colgroup>
                <col style="width:3%;">
                <col style="width:20%;">
                <col style="width:20%;">
                <col style="width:15%;">
                <col style="width:20%;">
                <col style="width:20%;">
              </colgroup>
              <thead class="thead-primary-lighter">
                <tr>
                  <th><strong>No.</strong></th>
                  <th><strong>ID Aset</strong></th>
                  <th><strong>ID Jenis Aset</strong></th>
                  <th><strong>ID Merek</strong></th>
                  <th><strong>Tipe Aset</strong></th>
                  <th><strong>Keterangan Aset</strong></th>
                </tr>
              </thead>
              <tbody id="tbody-aset-terpilih">
                <i style="display: none">{{ $index = 1 }}</i>
                  @if(count($aset_satuan_log) != 0)
                    @foreach($aset_satuan_log as $key => $log)
                      <tr>
                        <td>{{ $index }}.</td>
                        <td style="text-align: left;">{{ $log->log }}</td>
                        <td style="text-align: left;">{{ $log->keterangan_log }}</td>
                        <td>{{ strftime( "%d %B %Y", strtotime($log->created_at)) }}</td>
                        <td>{{ $log->created_by }}</td>
                        <td></td>
                      </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="6">
                        <center><i>Tidak ada data.</i></center>
                      </td>
                    </tr>
                  @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row pull-right">
            <a href="/pengirimanPusat" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Kembali</a>
            <a href="#" class="btn btn-primary" data-dismiss="modal" aria-label="Close" onclick="return confirm('Apakah Anda yakin?')"><i class="fa fa-check-circle"></i>&emsp;Terima</a>
          </div>
        </div>
      </div>
  </div>
</div>



<script type="text/javascript">
  $(document).ready(function(){
    
    $('.kode-html-input').css("display", "none");

    $(document).on("click", "[id^=btn-edit-field-isian]", function (e) {
      e.preventDefault();
      if($('#flag-editing-field-isian').text() == "0") {
        $('.input-field-isian').prop('readonly', false);
        $('#btn-edit-field-isian').html('<i class="fa fa-ban"></i>');
        $('#flag-editing-field-isian').text("1");
        $('#div-button-field-isian').slideToggle();
        $('#btn-simpan-field-isian').prop('disabled', false);
      } else if($('#flag-editing-field-isian').text() == "1") {
        $('.input-field-isian').prop('readonly', true);
        $('#btn-edit-field-isian').html('<i class="fa fa-gear"></i>');
        $('#flag-editing-field-isian').text("0");
        $('#btn-simpan-field-isian').prop('disabled', true);
        $('#div-button-field-isian').slideToggle();
      }
    });

    $(document).on("click", "[id^=btn-edit-informasi-aset]", function (e) {
      e.preventDefault();
      if($('#flag-editing-informasi-aset').text() == "0") {
        $('.kode-html-input').css("display", "");
        $('.kode-html-show').css("display", "none");
        $('.input-informasi-aset').prop('readonly', false);
        $('#btn-edit-informasi-aset').html('<i class="fa fa-ban"></i>');
        $('#flag-editing-informasi-aset').text("1");
        $('#div-button-informasi-aset').slideToggle();
        $('#btn-simpan-informasi-aset').prop('disabled', false);
      } else if($('#flag-editing-informasi-aset').text() == "1") {
        $('.kode-html-input').css("display", "none");
        $('.kode-html-show').css("display", "");
        $('.input-informasi-aset').prop('readonly', true);
        $('#btn-edit-informasi-aset').html('<i class="fa fa-gear"></i>');
        $('#flag-editing-informasi-aset').text("0");
        $('#btn-simpan-informasi-aset').prop('disabled', true);
        $('#div-button-informasi-aset').slideToggle();
      }
    });
  });
</script>
@endsection