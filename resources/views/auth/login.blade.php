<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.app')
@section('content')

<div class="container" style="margin-top: 50px">
    <div class="form-group row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <center>
                        <h3 class="text-primary">L O G   I N</h3>
                        <h4 class="text-secondary">Aplikasi e-Pengelolaan BMN (<strong class="text-primary">e-Peng</strong>)</h4>
                        <hr>
                    </center>
                </div>
                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                    @csrf

                    @if(session()->has('login_error'))
                        <div class="alert alert-danger">
                          {{ session()->get('login_error') }}
                        </div>
                    @endif

                    @if (!empty($user_message))
                        <div class="alert alert-success">
                          <strong>{{ $user_message }}</strong>
                        </div>
                    @endif
                    <div class="form-group row">
                        <label for="identity" class="col-sm-4 col-form-label text-md-right">{{ __('NIP atau Email') }}</label>

                        <div class="col-md-6">
                            <input id="identity" type="identity" class="form-control" name="identity" value="{{ old('identity') }}" required autofocus>

                            @if ($errors->has('identity'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('identity') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                {{ __('Login') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection