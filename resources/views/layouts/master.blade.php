<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    e-Peng
  </title>
  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
  
  <!-- CSS Files -->
  <link href="/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
  <link href="/vendor/bootstrap/css/datepicker.css" rel="stylesheet">
  <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" /> -->
  <link rel="stylesheet" type="text/css" href="{{url('/css/sweetalert2.min.css')}}">
  <link rel="stylesheet" href="/fontawesome/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> -->
  <link rel="stylesheet" href="/datetimepicker/material-datetime-picker.css"> 
  <link rel="stylesheet" type="text/css" href="/css/datatables.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/pace.css"/>
  <link href="/css/select2.css" rel="stylesheet" />
  <link href="/css/site.css" rel="stylesheet" />
  <style type="text/css">
    a[data-toggle="collapse"] {
        position: relative;
    }

    .dropdown-toggle::after {
        display: block;
        position: absolute;
        top: 50%;
        right: 20px;
        transform: translateY(-50%);
    }
     
    ul ul a {
        font-size: 0.9em !important;
        margin-left: 20px !important;
    }

    .nav-item:hover{
        background-color: #fffbce;
    }

    .menu-active {
      background-color: #fff7a0;
      margin: 10px;
      transition: background-color 0.3s;
      border-radius: 10px;
    }

    .menu-active p {
      color: #2349a0;
    }

    .menu-active i {
      background-color: white;
    }

    .menu-active:hover {
      background-color: #fffbce;
    }

  </style>

  <script src="/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="{{url('js/mask/jquery.mask.js')}}"></script>
  <script type="text/javascript" src="{{url('js/sweetalert2.min.js')}}"></script>
</head>

<body class="">
  <div class="wrapper" id="page-wrap">
    <div class="sidebar" data-color="purple" data-background-color="blue">
      <div class="logo" style="background-color: #1a3677;">
        <a href="/home" class="simple-text logo-normal" style="color: #fff763; font: bold 25px Helvetica">
          <img src="/img/logo-imigrasi.png" style="width: 2.3em;margin-right: 10px;"><strong style="text-transform: lowercase!important;">e</strong> - P <strong style="text-transform: lowercase!important;">e n g</strong>
        </a>
      </div>
      <div class="sidebar-wrapper" style="background-color: white;">
        <ul class="nav">
          <li class="nav-item">
            <a class="nav-link" href="{{ url('home') }}">
              <i class="fa fa-home" style="width: 40px; height: 40px; position: relative; top: -5px; background-color: #2349a0; color: #fff763; border-radius: 100px; padding-top: 3px; font-size: 20px;"></i>
              <p style="color: #2349a0;">{{ __('Beranda') }}</p>
            </a>
          </li>

          <?php
            $auth = App\User::getUser();
            $listmenu = App\Models\Ms_Menu::getMenuByID($auth->role);
          ?>

          @foreach ($listmenu as $menu)
            <?php
            $listchild = App\Models\Ms_Menu::getChild($menu->id_menu);
            if(count($listchild) > 0 ) {
            ?>
              <li class="nav-item">                
                <a class="nav-link " href="#pageSubmenu_{{ $menu->id_menu }}" data-toggle="collapse" aria-expanded="false">
                  <i class="{{ $menu->icon }}" style="width: 40px; height: 40px; position: relative; top: -5px; background-color: #2349a0; color: #fff699; border-radius: 100px; padding-top: 3px; font-size: 20px;"></i> 
                  <p>{{ $menu->title }}<span class="caret"></span></p>
                </a>
                <ul class="collapse list-unstyled" id="pageSubmenu_{{ $menu->id_menu }}">
                  @foreach ($listchild as $child)
                    <li style="margin-left: 50px;" class="nav-item ">
                        <a class="nav-link" href="{{ url($child->route) }}" style="position: relative; left: -10px;">
                          <i class="{{ $child->icon }}" style="color: #2349a0;"></i>
                          <p style="word-break: break-all;">{{ $child->title }}</p>
                        </a>
                    </li>
                  @endforeach
                </ul>
              </li>
            <?php
            } else {
            ?>
              <li class="nav-item ">
                <a class="nav-link" href="{{ url($menu->route) }}">
                  <i class="{{ $menu->icon }}" style="width: 40px; height: 40px; position: relative; top: -5px; background-color: #2349a0; color: #fff699; border-radius: 100px; padding-top: 3px; font-size: 20px;"></i>
                  <p>{{ $menu->title }}</p>
                </a>
              </li>
            <?php
            }         
            ?>            
          @endforeach

          <li class="nav-item ">
            <a class="nav-link" href="{{ route('clearcache') }}">
              <i class="fa fa-fire-extinguisher" style="width: 40px; height: 40px; position: relative; top: -5px; background-color: #2349a0; color: #fff699; border-radius: 100px; padding-top: 3px; font-size: 20px;"></i>
              <p>{{ __('Clear Cache') }}</p>
            </a>
          </li>

          
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <a class="navbar-brand" href="{{ route('home') }}"><strong class="text-primary">Aplikasi e-Pengelolaan BMN</strong></a>
            </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav list-unstyled">
              <li class="nav-item dropdown" style="margin-right: 50px;">
                <a id="navbarDropdown" class="nav-link dropdown-toggle  freelancer-color a-nav-item" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                  <i class="fa fa-user i-nav-item"></i>&emsp;
                  <strong>{{ $auth->name }}</strong> (<i>{{$auth->role}}</i>) &emsp; <span class="caret">&emsp;&emsp;&emsp;</span>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <ul class="nav dropdown-list list-unstyled">
                    <li>
                    <a href="{{ route('logout') }}"
                      onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="padding:15px 10px;">
                      <i class="fa fa-power-off"></i> &emsp; {{ __('Keluar') }}
                    </a>
                    </li>
                  </ul>
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    <input type="hidden" name="token" value={{ $auth->role }}>
                    @csrf
                  </form>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content" id="main-content">
        <div class="container-fluid">
          @yield('content')
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <div class="copyright float-right">
            Dikembangkan oleh <strong class="text-primary">Direktorat Sistem dan Teknologi Informasi Keimigrasian</strong> &copy; 
            <script>
              document.write(new Date().getFullYear())
            </script>
          </div>
        </div>
      </footer>
    </div>
  </div>

    <!-- JAVASCRIPT Files -->

  <script src="/js/core/popper.min.js" type="text/javascript"></script>
  <script src="/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <script src="/js/plugins/chartist.min.js"></script>
  <script src="/js/plugins/bootstrap-notify.js"></script>
  <script src="/js/material-dashboard.min.js?v=2.1.0" type="text/javascript"></script>
  <script defer src="/fontawesome/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>
  <script src="/js/polyfill.js" type="text/javascript"></script>
  <script src="/js/moment.js" type="text/javascript"></script>
  <script src="/js/moment-with-locales.js" type="text/javascript"></script>
  <script src="/js/rome.standalone.js" type="text/javascript"></script>
  <script src="/js/select2.js" type="text/javascript"></script>
  <script>
    paceOptions = {
        initialRate:0.7,
        minTime:1750,
        maxProgressPerFrame:1,
        ghostTime: 1000,
        ajax: false
    }
  </script>
  <script src="/js/pace.js" type="text/javascript"></script>

<script type="text/javascript" src="/js/datatables.min.js"></script>


  <script type="text/javascript">
    $(document).ready(function(){
      var protocol = window.location.protocol;
      var hostname = window.location.hostname;
      var port = window.location.port;
      var path = window.location.pathname;
      var firstPath = path.split("/");

      var url = protocol + '//' + hostname + ':' + port + '/' + firstPath[1];

      $('.nav-item a[href="'+ url +'"]').parent().addClass('menu-active');
      $('.nav-item').filter(function() { return this.href == url; }).parent().addClass('menu-active');
      $('.nav-item a[href="'+ url +'"]').parent().parent().prev().removeClass("collapsed");
      $('.nav-item a[href="'+ url +'"]').parent().parent().prev().attr("aria-expanded","true");
      $('.nav-item a[href="'+ url +'"]').parent().parent().slideDown(500).addClass('show');
    });
    $(document).ready(function(){
      $("select").select2();
    });
  </script>

</body>
@stack('script-footer')
</html>