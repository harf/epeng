<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8" />
      <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
      <link rel="icon" type="image/png" href="../assets/img/favicon.png">
      <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      <title>
        Aplikasi e-Pengelolaan BMN (e-Peng)
      </title>
      <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
      
      <!-- CSS Files -->
      <link href="/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
      <link href="/vendor/bootstrap/css/datepicker.css" rel="stylesheet">
      <link href="/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
      <!-- <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" /> -->

      <link rel="stylesheet" href="/fontawesome/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css"> -->
      <link rel="stylesheet" href="/datetimepicker/material-datetime-picker.css"> 
      <link rel="stylesheet" type="text/css" href="/css/datatables.min.css"/>

      <link href="/css/site.css" rel="stylesheet" />
      <style type="text/css">
        a[data-toggle="collapse"] {
            position: relative;
        }

        .dropdown-toggle::after {
            display: block;
            position: absolute;
            top: 50%;
            right: 20px;
            transform: translateY(-50%);
        }
         
        ul ul a {
            font-size: 0.9em !important;
            margin-left: 20px !important;
        }

        .nav-item:hover{
            background-color: #fcedff;
        }

        .menu-active {
          background-color: #f6bcff;
          margin: 10px;
          transition: background-color 0.3s;
          border-radius: 10px;
        }

        .menu-active p {
          color: #9f34af;
        }

        .menu-active i {
          background-color: white;
        }

        .menu-active:hover {
          background-color: #e04af7;
        }

      </style>
    </head>

    <body class="">
        <div class="wrapper ">
            <div class="content">
            @guest
                <div class="container-fluid">
                  @yield('content')
                </div>
            @endguest
          </div>
        </div>
    </body>
      <footer class="footer">
        <div class="container-fluid">
            <div class="copyright float-right">
                Dikembangkan oleh <strong class="text-primary">Direktorat Sistem dan Teknologi Informasi Keimigrasian</strong> &copy; 
                <script>
                  document.write(new Date().getFullYear())
                </script>
            </div>
        </div>
    </footer>
</html>
