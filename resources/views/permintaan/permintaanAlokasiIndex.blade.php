<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')
@section('content')
  @if($message_text)
    <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" @if($message_id == 1) style="background: rgba(76, 175, 80, 0.6);" @else style="background: rgba(255, 119, 119, 0.6);" @endif>
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title"><small>PESAN SISTEM</small></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-2 col-sm-2 col-xs-2" style="text-align: center;">
                <center>
                  @if($message_id == 1)
                    <h1 class="text-success"><i class="fa fa-check-circle"></i></h1>
                  @else
                    <h1 class="text-danger"><i class="fa fa-times-circle"></i></h1>
                  @endif
                </center>
              </div>
              <div class="col-md-10 col-sm-10 col-xs-10">
                <blockquote>
                  @if($message_id == 1)
                    <h4 class="text-success"><strong>Data {{ $message_source }} berhasil {{ $message_crud }}.</strong></h4>
                  @else
                    <h4 class="text-danger"><strong>Data {{ $message_source }} gagal {{ $message_crud }}.</strong></h4>
                    <footer><small>Error: {{ $message_text }}</small></footer>
                  @endif
                </blockquote>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">Tutup</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
      $(window).on('load',function(){
        $('#modalMessage').modal('show');
      });
    </script>
  @endif
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-5">
              <h3 class="card-title text-primary">
                <small>
                  <strong>
                    <i class="fa fa-stack-overflow"></i>&emsp;Alokasi Permintaan
                  </strong>
                </small>
              </h3>
            </div>
            <!-- <div class="col-md-7">
              <div class="pull-right">
                <a href="{{ route('catatAsetMasuk') }}" class="card-title btn btn-success"><i class="fa fa-arrow-circle-down"></i>&emsp;1. Catat Aset Masuk</a>
                <i class="fa fa-angle-double-right"></i>
                <a href="{{ route('catatAsetKeluar') }}" class="card-title btn btn-warning"><i class="fa fa-arrow-circle-up"></i>&emsp;2. Catat Aset Keluar</a>
                <i class="fa fa-angle-double-right"></i>
                <a href="{{ route('catatAsetInformasi') }}" class="card-title btn btn-info"><i class="fa fa-info-circle"></i>&emsp;3. Catat Informasi Satuan Aset</a>
              </div>
            </div> -->
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-10">
              <i><small>Halaman {{ $page_number }}/{{ceil($count_list_permintaan/$page_size)}} - menampilkan @if($is_filtered == 0 && $is_searched == 0) seluruh Permintaan @else @if($is_filtered != 0) <u>{{ ($filter1_name)?$filter1_name:'seluruh permintaan' }}</u> @endif @if($is_searched != 0) @if($is_filtered != 0) > @endif permintaan yang memiliki keyword "<u>{{ $keyword }}</u>" @endif @endif sebanyak @if($list_permintaan) <strong>{{ count($list_permintaan) }}/{{ ($count_list_permintaan)?$count_list_permintaan:0 }} data</strong>@else<strong>0 data</strong>@endif.</small></i>
            </div>
          </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
          <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
            </div>
            
            <div class="col-lg-3 col-md-3 col-sm-3">
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
            </div>

            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-search"></i>&emsp;</span>
                </div>
                <input type="text" class="form-control" id="keyword-search" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter untuk mencari data" @if($is_searched != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" value="{{ $keyword }}" @endif>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-eye"></i>&emsp;</span>
                </div>
                <input type="hidden" id="page-number" value="{{ $page_number }}">
                <select class="form-control input-sm" name="page_size" id="page-size">
                  <option value="15"  selected="true">15</option>
                  <option value="30" @if($page_size == 30) selected="true" @endif>30</option>
                  <option value="60" @if($page_size == 60) selected="true" @endif>60</option>
                  <option value="120" @if($page_size == 120) selected="true" @endif>120</option>
                  <option value="240" @if($page_size == 240) selected="true" @endif>240</option>
                  <option value="480" @if($page_size == 480) selected="true" @endif>240</option>
                  <option value="960" @if($page_size == 960) selected="true" @endif>960</option>
                </select>
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm">&emsp;data</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <br>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card card-profile">
        <div class="card-body">
          <!-- <div class="table-responsive"> -->
          <table class="table table-bordered">
            <colgroup>
              <col style="width:3%;">
              <col style="width:13%;">
              <col style="width:13%;">
              <col style="width:13%;">
              <col style="width:21%;">
              <col style="width:22%;">
              <col style="width:12%;">
            </colgroup>
            <thead class="thead-primary" style="text-align: center;">
              <tr>
                <th><strong>NO.</strong></th>
                <th><strong>NO. SURAT</strong></th>
                <th><strong>TANGGAL SURAT</strong></th>
                <th><strong>TANGGAL PERMINTAAN</strong></th>
                <th><strong>KANTOR IMIGRASI</strong></th>
                <th><strong>PEMOHON</strong></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              @if($list_permintaan)
                <i style="display: none">{{ $index = (($page_number-1) * $page_size) + 1 }}</i>
                @foreach($list_permintaan as $key => $permintaan)
                  <tr>
                    <td align="center">{{ $index }}.</td>
                    <td>{{ $permintaan->no_surat_permintaan }}</td>
                    <td align="center">{{ strftime( "%d %B %Y", strtotime($permintaan->tanggal_surat_permintaan)) }}</td>
                    <td align="center">{{ strftime( "%d %B %Y", strtotime($permintaan->tanggal_permintaan)) }}</td>
                    <td>{{ $permintaan->kantor_imigrasi }}</td>
                    <td>{{ $permintaan->nip_pemohon }} - {{ $permintaan->nama_pemohon }}</td>
                    <td align="center">
                      <a href="permintaanAlokasiAdd/{{ $permintaan->id_permintaan }}/add" class="card-title btn btn-primary"><i class="fa fa-arrow-circle-right"></i>&emsp;Proses</a>
                    </td>
                  </tr>
                  <i style="display: none">{{ $index++ }}</i>
                @endforeach
              @else
                <tr>
                  <td colspan="7">
                    <center><i>Tidak ada data.</i></center>
                  </td>
                </tr>
              @endif
            </tbody>
          </table>
          <!-- </div> -->
        </div>
      </div>
    </div>
  </div>
  <br>
  <hr>
  <div class="row">
    <div class="pull-right">
      <nav aria-label="...">
        <ul class="pagination">
          @if($page_number == 1)
            <li class="page-item disabled">
              <span class="page-link">Sebelumnya</span>
            </li>
          @else
            <li class="page-item">
              <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter1={{ $filter1 }}&filter2={{ $filter2 }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number-1 }}">Sebelumnya</a>
            </li>  
          @endif
          @for($i = 0; $i < $count_list_permintaan/$page_size; $i++)
            <li class="page-item @if($page_number == $i+1) active @endif">
              @if($page_number == $i+1)
                <span class="page-link">
                  {{ $i+1 }}
                  <span class="sr-only">(current)</span>
                </span>
              @else
                <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter1={{ $filter1 }}&filter2={{ $filter2 }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $i+1 }}">{{ $i+1 }}</a>
              @endif
            </li>
          @endfor
          @if($page_number == ceil($count_list_permintaan/$page_size))
            <li class="page-item disabled">
              <span class="page-link">Selanjutnya</span>
            </li>
          @else
            <li class="page-item">
              <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number+1 }}">Selanjutnya</a>
            </li> 
          @endif
        </ul>
      </nav>
    </div>
  </div>

<script type="text/javascript"> 
  $(document).ready(function(){
    $("select").select2();
  });
  $(document).ready(function(){
    $("[id^=hidden-sumber-expandable-row-]").hide();
    $("[id^=hidden-satuan-expandable-row-]").hide();
    $("[id^=sumber-tr-]").hide();
    $("[id^=new-satuan-row-]").hide();
    moment.locale('id');

    $(document).on("change", "#id-jenis-aset", function(e) {
      e.preventDefault();
      var targetId = e.target.value;

      var keyword = $("#keyword-search").val();
      var is_searched = 0;
      if (keyword == '') {
        keyword = "null";
        is_searched = 0;
      } else {
        is_searched = 1;
      }

      var page_size = $("#page-size").val();
      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      if (targetId != 0) window.location.href = baseUrl + "?is_filtered=1&is_searched=" + is_searched + "&filter=" + targetId + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=1";
      else window.location.href = baseUrl;
    });

    $(document).on("keypress", "#keyword-search", function(e) {
      if(e.which === 13){
        e.preventDefault();
        var keyword = $("#keyword-search").val();
        var filter = $("#id-jenis-aset").val();

        var is_filtered = 0;
        if (filter == 0) {
          is_filtered = 0;
          jenis_aset = null;
        } else {
          is_filtered = 1;
          jenis_aset = filter;
        }

        var page_size = $("#page-size").val();
        var page_number = $("#page-number").val();

        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        
        if (keyword.replace(" ", "") != '') window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=1&filter=" + filter + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=" + page_number;
        else if (is_filtered != 0) window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=0&filter=" + filter + "&keyword=null" + "&page_size=" + page_size + "&page_number=1";
        else window.location.href = baseUrl;
      }
    });

    $(document).on("change", "#page-size", function(e) {
      e.preventDefault();
      var targetId = e.target.value;
      var filter = $("#id-jenis-aset").val();
      var keyword = $("#keyword-search").val();

      var is_filtered = 0;
      if (filter == 0) is_filtered = 0;
      else is_filtered = 1;

      var is_searched = 0;
      if (keyword == '') {
        keyword = "null";
        is_searched = 0;
      } else {
        is_searched = 1;
      }

      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=" + is_searched + "&filter=" + filter + "&keyword=" + keyword + "&page_size=" + targetId + "&page_number=1";
    });
  });
</script>
@endsection