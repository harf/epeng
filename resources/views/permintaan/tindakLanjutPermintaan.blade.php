<script src="/js/core/jquery.min.js" type="text/javascript"></script>
@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page"><u>Tindak Lanjut Permintaan</u></li>
  </ol>
</nav>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card card-profile">
      <div class="card-header card-header-primary">
        <h4 class="card-title"><strong>Tindak Lanjut Permintaan</strong></h4>
      </div>
      <div class="card-body" align="left">
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="card card-profile">
          <div class="card-body" align="left">
            <h4>Daftar satuan aset yang pernah diminta namun belum ada tindak lanjutnya</h4>
            <div class="form-group row">
              <table class="table table-bordered table-responsive" style="vertical-align: middle; text-align: center;">
                <colgroup>
                  <col style="width:3%;">
                  <col style="width:3%;">
                  <col style="width:64%;">
                  <col style="width:30%;">
                </colgroup>
                <thead class="thead-primary">
                  <tr>
                    <th rowspan="2"><strong></strong></th>
                    <th rowspan="2"><strong>No.</strong></th>
                    <th rowspan="2"><strong>Aset</strong></th>
                    <th rowspan="2" rel="tooltip" title="Jumlah Satuan Aset yang Belum Ditindaklanjuti"><strong>Jumlah</strong></th>
                  </tr>
                </thead>
                <tbody>
                  <i style="display: none">{{ $index = 1 }}</i>
                  @foreach($list_aset_informasi as $key => $aset)
                    <tr>
                      <i id="flag-tr-pre-exp-{{ $aset->id_aset }}" style="display: none">0</i>
                      <i id="id-jenis-aset-{{ $aset->id_aset }}" style="display: none">{{ $aset->id_jenis_aset }}</i>
                      <td id="td-pre-exp-{{ $aset->id_aset}}" style="cursor: pointer;"><button type="button" id="btn-pre-exp-{{ $aset->id_aset }}" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button></td>
                      <td>{{ $index }}.</td>
                      <td style="text-align: left;" id="aset-pre-{{ $aset->id_aset }}"><span class="badge badge-primary" style="background-color: #2349a0; color: #fff763; text-transform: uppercase;"><strong id="nama-jenis-aset-{{ $aset->id_aset }}">{{ $aset->nama_jenis_aset }}</strong></span> - {{ $aset->nama_merek . ' ' . $aset->tipe_aset }}</td>
                      <td>{{ $aset->jumlah_satuan_aset }}</td>
                    </tr>
                    <i style="display: none">{{ $index++ }}</i>
                    <tr id="tr-pre-exp-{{ $aset->id_aset }}">
                      <td colspan="5">
                        <div class="row" id="hidden-div-tr-pre-exp-{{ $aset->id_aset }}">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                              <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"></div>
                              <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                <table class="table table-bordered table-responsive" style="vertical-align: middle;">
                                  <colgroup>
                                    <col style="width:15%;">
                                    <col style="width:40%;">
                                    <col style="width:42%;">
                                    <col style="width:3%;">
                                    <col style="">
                                  </colgroup>
                                  <thead class="thead-primary-lighter" style="vertical-align: middle; text-align: center;">
                                    <tr>
                                      <th><strong>Satuan #</strong></th>
                                      <th><strong>Field & Isian Satuan Aset</strong></th>
                                      <th><strong>Status</strong></th>
                                      <th><strong></strong></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <i style="display: none">{{ $index2 = 1 }}</i>
                                    @foreach($aset->grup_satuan as $key2 => $grup)
                                      <tr id="tr-grup-{{ $grup->kode_grup_satuan }}">
                                        <td id="kode-grup-pre_{{ $aset->id_aset }}-{{ $grup->kode_grup_satuan }}"><center>#{{ $grup->kode_grup_satuan }}</center></td>
                                        <td id="satuan-pre_{{ $aset->id_aset }}-{{ $grup->kode_grup_satuan }}">
                                          <ul class="list-group">
                                            @foreach($grup->satuan_aset as $key3 => $satuan)
                                              <li class="list-group-item">- <i>{{ $satuan->field_aset}}</i>: <strong class="text-primary">{{ $satuan->isian_aset }}</strong></li>
                                            @endforeach
                                          </ul>
                                        </td>
                                        <td><span class="badge badge-success" rel="tooltip" title='Dialokasikan pada tanggal {{ strftime( "%d %B %Y", strtotime($grup->tanggal_aset_keluar)) }}'><strong>ALLOCATED</strong></span><br><i class="text-success"><strong>{{ $grup->nama_kanim }}</strong></i></td>
                                        <i id="flag-td-isian-pre_{{ $aset->id_aset }}-{{ $grup->kode_grup_satuan }}" style="display: none">0</i>
                                        <td id="td-btn-isian-pre_{{ $aset->id_aset }}-{{ $grup->kode_grup_satuan }}">
                                          <button type="button" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-pencil"></i></button>
                                        </td>
                                      </tr>
                                      <i style="display: none">{{ $index2++ }}</i>
                                    @endforeach
                                  </tbody>
                                </table>
                              </div>
                            </div>
                        </div>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
        <div class="card card-profile">
          <div class="card-body" align="left">
            <h4>Daftar Satuan Aset yang Akan Ditindaklanjuti</h4>
            <form method="POST" action="{{ route('simpanAsetInformasi') }}" enctype="multipart/form-data">
              <div class="form-group row">
                <table class="table table-bordered" style="vertical-align: middle; text-align: center;">
                  <colgroup>
                    <col style="width:3%;">
                    <col style="width:3%;">
                    <col style="width:51%;">
                    <col style="width:40%;">
                    <col style="width:3%;">
                  </colgroup>
                  <tbody id="tbody-aset-terpilih">
                    <tr id="no-data">
                      <td colspan="5"><i>Belum ada aset yang terpilih.</i></td>
                      <input type="hidden" id="grup-satuan-field" name="grup_satuan_field">
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="form-group row pull-right">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
                <button type="submit" class="btn btn-primary" id="btn-catat-aset-informasi" disabled="disabled"><i class="fa fa-check-circle"></i>&emsp;Simpan Informasi Tindak Lanjut (<strong><span id="count-terpilih">0</span></strong>)</button>
              </div>
              {{ csrf_field() }}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"> 
  var pathjs = "/js/core/jquery.min.js";
  $.getScript(pathjs);
  $(document).ready(function(){
    $("select").select2();
  });

  $(document).ready(function(){
    $("[id^=tr-pre-exp-]").hide();
    $("[id^=hidden-div-tr-pre-exp-]").hide();
    $("#grup-satuan-field").val('');

    $(document).on("click", "[id^=td-pre-exp-]", function (e) {
      e.preventDefault();
      var targetId = e.currentTarget.id.split('-').pop();

      if ($("#flag-tr-pre-exp-" + targetId).text() == "0") {
        $("#btn-pre-exp-" + targetId).html('<i class="fa fa-minus-circle"></i>');
        $("#tr-pre-exp-" + targetId).attr("style", "").fadeIn().delay(500);
        $("#flag-tr-pre-exp-" + targetId).text("1");
      } else if ($("#flag-tr-pre-exp-" + targetId).text() == "1") {
        $("#btn-pre-exp-" + targetId).html('<i class="fa fa-plus-circle"></i>');
        $("#tr-pre-exp-" + targetId).attr("style", "").fadeOut().delay(500);
        $("#flag-tr-pre-exp-" + targetId).text("0");
      }
      $('#hidden-div-tr-pre-exp-' + targetId).slideToggle();   
    });

    $(document).on("click", "[id^=td-post-exp-]", function (e) {
      e.preventDefault();
      var targetId = e.currentTarget.id.split('-').pop();
      if ($("#flag-tr-post-exp-" + targetId).text() == "0") {
        $("#btn-post-exp-" + targetId).html('<i class="fa fa-plus-circle"></i>');
        $("#tr-post-exp-" + targetId).attr("style", "").fadeOut().delay(500);
        $("#tr-post-isian-" + targetId).attr("style", "").fadeOut().delay(500);
        $("#flag-tr-post-exp-" + targetId).text("1");
      } else if ($("#flag-tr-post-exp-" + targetId).text() == "1") {
        $("#btn-post-exp-" + targetId).html('<i class="fa fa-minus-circle"></i>');
        $("#tr-post-exp-" + targetId).attr("style", "").fadeIn().delay(500);
        $("#tr-post-isian-" + targetId).attr("style", "").fadeIn().delay(500);
        $("#flag-tr-post-exp-" + targetId).text("0");
      }
      $('#hidden-div-tr-post-exp-' + targetId).slideToggle();
    });

    $(document).on("click", "[id^=btn-post-trash__]", function (e) {
      e.preventDefault();
      var targetId = e.currentTarget.id.split('__').pop();
      var parentId = targetId.split('-')[0];
      var childId = targetId.split('-')[1];

      $("#tr-post-" + childId).remove();
      $("#tr-post-isian-" + childId).remove();

      var count_terpilih = parseInt($('#count-terpilih').text());
      $('#count-terpilih').text(count_terpilih - 1);
      $('#tr-grup-' + childId).children('td, th').css('background-color','white');
      $('#td-btn-isian-pre_' + parentId + '-' + childId).html('<button type="button" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-pencil"></i></button>');
      $('#flag-td-isian-pre_' + parentId + '-' + childId).text("0");

      if(count_terpilih - 1 == 0) {
        $('#no-data').css('display', '');
        $('#btn-catat-aset-informasi').attr('disabled', 'disabled');
      }

      var grup_satuan_field = $('#grup-satuan-field').val();
      var new_grup_satuan_field = grup_satuan_field.replace(targetId + '~', '');
      $('#grup-satuan-field').val(new_grup_satuan_field);
    });

    $(document).on("click", "[id^=td-btn-isian-pre_]", function (e) {
      e.preventDefault();
      var targetId = e.currentTarget.id.split('_').pop();
      var parentId = targetId.split('-')[0];
      var childId = targetId.split('-')[1];

      if ($('#flag-td-isian-pre_' + parentId + '-' + childId).text() == "0") {
        var id_jenis_aset = $('#id-jenis-aset-' + parentId).text();
        var nama_jenis_aset = $('#nama-jenis-aset-' + parentId).text();
        var div_list_field = '';
        var flag_list_field = 0;
        var list_field_id = '';

        $.ajax({
           type:"GET",
           async:false,
           url:"{{url('api/get-field-aset-informasi-list')}}?jenis_aset="+parseInt(id_jenis_aset)+"&kode_grup_satuan="+parseInt(childId),
           success:function(res){
            // console.log(res);
            if(res){
              if (res.list_field.length != 0) {
                $.each(res.list_field,function(index,Obj){
                  div_list_field = div_list_field + '<div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-4 col-sm-4 col-xs-4" style="display: inline-block; align-self: center; text-align: left;">' + (index+1) + '. ' + Obj.nama_field_info + '</div><div class="col-md-7 col-sm-7 col-xs-7">' + Obj.kode_html_input + '</div></div>';

                    list_field_id = list_field_id + Obj.id_field_info + '_';
                });
                flag_list_field = 1;
              } else {
                alert('Belum ada Parameter Field Aset Informasi untuk jenis ' + nama_jenis_aset + '. Silahkan menghubungi Administrator Aplikasi MISTIK.');
                flag_list_field = 0;
              }
            }
           }, error:function(x) {
            alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
            console.log(x);
           }
        });

        if (flag_list_field == 1) {
          var aset = $('#aset-pre-' + parentId).html();
          var satuanAset = $('#satuan-pre_' + parentId + '-' + childId).html();
          var kodeGrupAset = $('#kode-grup-pre_' + parentId + '-' + childId).text();
          var count_terpilih = parseInt($('#count-terpilih').text());

          $('#tbody-aset-terpilih').append('<tr id="tr-post-' + childId + '"><td id="td-post-exp-' + childId + '" style="cursor: pointer;"><i id="flag-tr-post-exp-' + childId + '" style="display: none;">0</i><button type="button" id="btn-post-exp-' + childId + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-minus-circle"></i></button></td><td>' + (count_terpilih + 1) + '.</td><td style="text-align: left;">Satuan <strong class="text-primary">' + kodeGrupAset + '</strong><br>' + aset + '</td><td style="text-align: left;">' + satuanAset + '</td><td><button type="button" id="btn-post-trash__' + parentId + '-' + childId + '-' + list_field_id + '" class="btn btn-md btn-link btn-danger" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></td></tr><tr id="tr-post-isian-' + childId + '"><td colspan="5"><div class="row" id="hidden-div-tr-post-exp-' + childId + '"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">' + div_list_field + '</div></div></tr>'); 

          $('#count-terpilih').text(count_terpilih + 1);
          $('#tr-grup-' + childId).children('td, th').css('background-color','#fffbce');
          $('#td-btn-isian-pre_' + parentId + '-' + childId).html('<i class="fa fa-check-circle text-success"></i>');
          $('#flag-td-isian-pre_' + parentId + '-' + childId).text("1");
          $('#no-data').css('display', 'none');
          $('#btn-catat-aset-informasi').removeAttr('disabled');
          var grup_satuan_field = $('#grup-satuan-field').val();
          grup_satuan_field = grup_satuan_field + parentId + '-' + childId + '-' + list_field_id + '~';
          $('#grup-satuan-field').val(grup_satuan_field);
        } else if (flag_list_field == 0) {
          $('#flag-td-isian-pre_' + parentId + '-' + childId).text("0");
          alert('Tidak dapat mengisi aset informasi untuk satuan aset yang dimaksud karena belum ada Parameter Field Aset Informasi untuk jenis ' + nama_jenis_aset + '. Silahkan menghubungi Administrator Aplikasi MISTIK.');          
        }
      }
    });
  });
</script>
@endsection