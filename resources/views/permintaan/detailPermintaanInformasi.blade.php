<script src="/js/core/jquery.min.js" type="text/javascript"></script>
@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/konfirmasi">Manajemen Permintaan</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Informasi Permintaan #{{ $id_permintaan }}</u></li>
  </ol>
</nav>
<div class="row">  
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <div class="card card-profile">
      <div class="card-header card-header-primary">
        <h4 class="card-title"><strong>Informasi Satuan Item Permintaan #{{ $id_permintaan }}</strong></h4>
      </div>
      <div class="card-body" align="left">
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
            <div class="row col-lg-12 col-md-12 col-sm-12">
                <a href="#"><h4><strong class="text-primary">Nomor Surat : {{ $list_permintaan->no_surat_permintaan }}</strong></h4></a>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Tanggal Surat </div>
              <div class="col-lg-1 col-md-1 col-sm-1">: </div>
              <div class="col-lg-7 col-md-7 col-sm-7"><strong class="text-primary">{{ strftime( "%d %B %Y", strtotime($list_permintaan->tanggal_surat_permintaan)) }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Deskripsi </div>
              <div class="col-lg-1 col-md-1 col-sm-1">: </div>
              <div class="col-lg-7 col-md-7 col-sm-7"><strong class="text-primary">{{ $list_permintaan->deskripsi_permintaan }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Kantor Imigrasi </div>
              <div class="col-lg-1 col-md-1 col-sm-1">: </div>
              <div class="col-lg-7 col-md-7 col-sm-7"><strong class="text-primary">{{ $list_permintaan->kantor_imigrasi }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">NIP Pemohon </div>
              <div class="col-lg-1 col-md-1 col-sm-1">: </div>
              <div class="col-lg-7 col-md-7 col-sm-7"><strong class="text-primary">{{ $list_permintaan->nip_pemohon }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Nama Pemohon </div>
              <div class="col-lg-1 col-md-1 col-sm-1">: </div>
              <div class="col-lg-7 col-md-7 col-sm-7"><strong class="text-primary">{{ $list_permintaan->nama_pemohon }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Pangkat/Golongan Pemohon </div>
              <div class="col-lg-1 col-md-1 col-sm-1">: </div>
              <div class="col-lg-7 col-md-7 col-sm-7"><strong class="text-primary">{{ $list_permintaan->pangkat_golongan }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Jabatan Pemohon </div>
              <div class="col-lg-1 col-md-1 col-sm-1">: </div>
              <div class="col-lg-7 col-md-7 col-sm-7"><strong class="text-primary">{{ $list_permintaan->jabatan_pemohon }}</strong></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row col-lg-12 col-md-12 col-sm-12">
          <div class="row col-lg-12 col-md-12 col-sm-12">
            <strong class="text-primary"><u>RINCIAN INFORMASI PERMINTAAN ITEM #{{ $id_permintaan }}</u></strong>
          </div>
          <div class="form-group row col-lg-12 col-md-12 col-sm-12">
            <table class="table table-bordered table-hover" style="vertical-align: middle; text-align: center;">
              <colgroup>
                <col style="width:3%;">
                <col style="width:15%;">
                <col style="width:30%;">
                <col style="width:12%;">
                <col style="width:40%;">
              </colgroup>
              <thead class="thead-primary-lighter">
                <tr>
                  <th><strong>No.</strong></th>
                  <th><strong>ID Item</strong></th>
                  <th><strong>Tipe Item</strong></th>
                  <th><strong>Qty Item</strong></th>
                  <th><strong>Keterangan</strong></th>
                </tr>
              </thead>
              <tbody id="tbody-aset-terpilih">
                <i style="display: none">{{ $index = 1 }}</i>
                  @if(count($list_permintaan_item) != 0)
                    @foreach($list_permintaan_item as $key => $permintaan_item)
                      <tr>
                        <td align="center">{{ $index }}.</td>
                        <td style="text-align: center;">{{ $permintaan_item->id_aset }}</td>
                        <td style="text-align: left;">{{ $permintaan_item->nama_aset }}</td>
                        <td style="text-align: center;">{{ $permintaan_item->jumlah }}</td>
                        <td style="text-align: left;">{{ $permintaan_item->keterangan }}</td>
                      </tr>
                      <i style="display: none">{{ $index++ }}</i>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="5">
                        <center><i>Tidak ada data.</i></center>
                      </td>
                    </tr>
                  @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row col-lg-12 col-md-12 col-sm-12">
          <div class="row col-lg-12 col-md-12 col-sm-12">
            <strong class="text-primary"><u>RINCIAN INFORMASI STOK ASET #{{ $id_permintaan }}</u></strong>
          </div>
          <div class="form-group row col-lg-12 col-md-12 col-sm-12">
            <table class="table table-bordered table-hover" style="vertical-align: middle; text-align: center;">
              <colgroup>
                <col style="width:3%;">
                <col style="width:15%;">
                <col style="width:42%;">
                <col style="width:40%;">
              </colgroup>
              <thead class="thead-primary-lighter">
                <tr>
                  <th><strong>No.</strong></th>
                  <th><strong>ID Aset</strong></th>
                  <th><strong>Tipe Aset</strong></th>
                  <th><strong>Stok</strong></th>
                </tr>
              </thead>
              <tbody id="tbody-aset-terpilih">
                <i style="display: none">{{ $index = 1 }}</i>
                  @if(count($list_aset_stok) != 0)
                    @foreach($list_aset_stok as $key => $aset_stok)
                      <tr>
                        <td align="center">{{ $index }}.</td>
                        <td style="text-align: left;">{{ $aset_stok->id_aset }}</td>
                        <td style="text-align: left;">{{ $aset_stok->tipe_aset }}</td>
                        <td style="text-align: center;">{{ $aset_stok->jumlah_stok }}</td>
                      </tr>
                      <i style="display: none">{{ $index++ }}</i>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="5">
                        <center><i>Tidak ada data.</i></center>
                      </td>
                    </tr>
                  @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="form-group row pull-right">
          <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Konfirmasi</button>
          <button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
        </div>
      </div>
    </div>
    {{ csrf_field() }}

    
  </div>
</div>
@endsection

