<script src="/js/core/jquery.min.js" type="text/javascript"></script>
@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/manajemenAset">Manajemen Aset</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Informasi Aset #{{ $kode_grup_satuan }}</u></li>
  </ol>
</nav>
<div class="row">  
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <div class="card card-profile">
      <div class="card-header card-header-primary">
        <h4 class="card-title"><strong>Informasi Satuan Aset #{{ $kode_grup_satuan }}</strong></h4>
      </div>
      <div class="card-body" align="left">
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row">
          <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
            <div class="row col-lg-12 col-md-12 col-sm-12"><a href="#"><h4><strong class="text-primary">{{ $aset_satuan->nama_merek }} {{ $aset_satuan->tipe_aset }}</strong></h4></a></div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Kode Satuan Aset: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">#{{ $kode_grup_satuan }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Kategori Aset: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $aset_satuan->nama_kategori_aset }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Jenis Aset: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $aset_satuan->nama_jenis_aset }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Merek: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ $aset_satuan->nama_merek }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Sumber Pengadaan: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ ($aset_satuan->nama_pengadaan)?$aset_satuan->nama_pengadaan:'Non Pengadaan' }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Tahun Pengadaan: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ ($aset_satuan->tahun_pengadaan)?$aset_satuan->tahun_pengadaan:'-' }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Harga Satuan: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">{{ ($aset_satuan->harga_satuan)?$aset_satuan->harga_satuan:'-' }}</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Status Terakhir: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">Sudah dialokasikan ke Kantor Imigrasi Kelas I Khusus Batam</strong></div>
            </div>
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-4 col-md-4 col-sm-4">Status Per Tanggal: </div>
              <div class="col-lg-8 col-md-8 col-sm-8"><strong class="text-primary">03 Maret 2019</strong></div>
            </div>
          </div>
          <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
            <div style="border:1px solid black; width: 100%; height: 300px;">
              <img src="{{ str_replace('public', '/storage', $gambar->link_gambar) }}" alt="{{ $gambar->nama_gambar }}" class="gambar-aset-thumbnails">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <form method="POST" action="{{ route('ubahFieldIsianAset') }}" enctype="multipart/form-data">
          <div class="row col-lg-12 col-md-12 col-sm-12">
            <div class="col-lg-11 col-md-11 col-sm-11">
              <strong class="text-primary"><u>FIELD & ISIANNYA SATUAN ASET</u></strong>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1" style="text-align: right;">
              <i id="flag-editing-field-isian" style="display: none;">0</i>
              <button class="btn btn-link btn-sm btn-primary" id="btn-edit-field-isian"><i class="fa fa-gear"></i></button>
            </div>
          </div>
          {{ $id_satuan = '' }}
          @foreach($field_isian_list as $key => $field_isian)
            <div class="row col-lg-12 col-md-12 col-sm-12">
              <div class="col-lg-2 col-md-2 col-sm-2">
              </div>
              <div class="col-lg-3 col-md-3 col-sm-3" style="display: inline-block; align-self: center;">
                <i class="fa fa-arrow-right"></i>&emsp;{{ $field_isian->field_aset }}
              </div>
              <div class="col-lg-5 col-md-5 col-sm-5">
                <input class="form-control input-field-isian" type="text" name="isian_field~{{ $field_isian->id_satuan }}" value="{{ $field_isian->isian_aset }}" readonly="readonly">
              </div>
            </div>
            <i style="display: none;">{{ $id_satuan = $id_satuan . $field_isian->id_satuan . '-' }}</i>
            <hr>
          @endforeach
          <input type="hidden" name="id_satuan" value="{{ $id_satuan }}">
          <input type="hidden" name="kode_grup_satuan" value="{{ $kode_grup_satuan }}">
          <div class="form-group row pull-right" id="div-button-field-isian" style="display: none;">
            <button type="submit" class="btn btn-primary" id="btn-simpan-field-isian" disabled="disabled"><i class="fa fa-check-circle"></i>&emsp;Simpan Perubahan Field & Isian Aset</button>
          </div>
          {{ csrf_field() }}
        </form>
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row col-lg-12 col-md-12 col-sm-12">
          <div class="col-lg-11 col-md-11 col-sm-11">
            <strong class="text-primary"><u>RINCIAN INFORMASI SATUAN ASET</u></strong>
          </div>
          @if(count($informasi_list) != 0)
            <div class="col-lg-1 col-md-1 col-sm-1" style="text-align: right;">
              <i id="flag-editing-informasi-aset" style="display: none;">0</i>
              <button class="btn btn-link btn-sm btn-primary" id="btn-edit-informasi-aset"><i class="fa fa-gear"></i></button>
            </div>
          @endif
        </div>
        @if(count($informasi_list) != 0)
          <form method="POST" action="{{ route('ubahAsetInformasi') }}" enctype="multipart/form-data">
            {{ $id_aset_informasi = '' }}
            @foreach($informasi_list as $key => $informasi)
              <div class="row col-lg-12 col-md-12 col-sm-12">
                <div class="col-lg-2 col-md-2 col-sm-2">
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3" style="display: inline-block; align-self: center;">
                  <i class="fa fa-arrow-right"></i>&emsp;{{ $informasi->nama_field_info }}
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5">
                  <div class="kode-html-show">
                    {!! $informasi->kode_html_show !!}
                  </div>
                  <div class="kode-html-input">
                    {!! $informasi->kode_html_input !!}
                  </div>
                </div>
              </div>
              <i style="display: none;">{{ $id_aset_informasi = $id_aset_informasi . $informasi->id_aset_informasi . '-' }}</i>
              <hr>
            @endforeach
            <input type="hidden" name="id_aset_informasi" value="{{ $id_aset_informasi }}">
            <input type="hidden" name="kode_grup_satuan" value="{{ $kode_grup_satuan }}">
            <div class="form-group row pull-right" id="div-button-informasi-aset" style="display: none;">
              <button type="submit" class="btn btn-primary" id="btn-simpan-informasi-aset" disabled="disabled"><i class="fa fa-check-circle"></i>&emsp;Simpan Perubahan Rincian Informasi</button>
            </div>
            {{ csrf_field() }}
          </form>
        @elseif(count($informasi_list) == 0)
          <center>
            <i>Belum ada rincian informasi aset lebih lanjut untuk satuan aset #{{ $kode_grup_satuan }} ini.<br>Anda dapat menambahkannya dengan pergi ke halaman <strong class="text-primary">Manajemen Aset</strong> lalu pilih <strong class="text-primary">Catat Informasi Satuan Aset</strong> atau dengan cukup mengklik <a href="/manajemenAset/catatAsetInformasi" class="text-primary"><u>link</u></a> ini.</i>
          </center>
        @endif
      </div>
    </div>
    <div class="card card-profile">
      <div class="card-body" align="left">
        <div class="row col-lg-12 col-md-12 col-sm-12">
          <div class="row col-lg-12 col-md-12 col-sm-12">
            <strong class="text-primary"><u>AKTIVITAS PERGERAKAN SATUAN ASET #{{ $kode_grup_satuan }}</u></strong>
          </div>
          <div class="form-group row col-lg-12 col-md-12 col-sm-12">
            <table class="table table-bordered table-hover" style="vertical-align: middle; text-align: center;">
              <colgroup>
                <col style="width:3%;">
                <col style="width:47%;">
                <col style="width:20%;">
                <col style="width:15%;">
                <col style="width:15%;">
              </colgroup>
              <thead class="thead-primary-lighter">
                <tr>
                  <th><strong>No.</strong></th>
                  <th><strong>Aktivitas Pergerakan</strong></th>
                  <th><strong>Keterangan Pergerakan</strong></th>
                  <th><strong>Tanggal Aktivitas</strong></th>
                  <th><strong>Aktivitas Oleh</strong></th>
                </tr>
              </thead>
              <tbody id="tbody-aset-terpilih">
                <i style="display: none">{{ $index = 1 }}</i>
                  @if(count($aset_satuan_log) != 0)
                    @foreach($aset_satuan_log as $key => $log)
                      <tr>
                        <td>{{ $index }}.</td>
                        <td style="text-align: left;">{{ $log->log }}</td>
                        <td style="text-align: left;">{{ $log->keterangan_log }}</td>
                        <td>{{ strftime( "%d %B %Y", strtotime($log->created_at)) }}</td>
                        <td>{{ $log->created_by }}</td>
                      </tr>
                    @endforeach
                  @else
                    <tr>
                      <td colspan="5">
                        <center><i>Tidak ada data.</i></center>
                      </td>
                    </tr>
                  @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    
    $('.kode-html-input').css("display", "none");

    $(document).on("click", "[id^=btn-edit-field-isian]", function (e) {
      e.preventDefault();
      if($('#flag-editing-field-isian').text() == "0") {
        $('.input-field-isian').prop('readonly', false);
        $('#btn-edit-field-isian').html('<i class="fa fa-ban"></i>');
        $('#flag-editing-field-isian').text("1");
        $('#div-button-field-isian').slideToggle();
        $('#btn-simpan-field-isian').prop('disabled', false);
      } else if($('#flag-editing-field-isian').text() == "1") {
        $('.input-field-isian').prop('readonly', true);
        $('#btn-edit-field-isian').html('<i class="fa fa-gear"></i>');
        $('#flag-editing-field-isian').text("0");
        $('#btn-simpan-field-isian').prop('disabled', true);
        $('#div-button-field-isian').slideToggle();
      }
    });

    $(document).on("click", "[id^=btn-edit-informasi-aset]", function (e) {
      e.preventDefault();
      if($('#flag-editing-informasi-aset').text() == "0") {
        $('.kode-html-input').css("display", "");
        $('.kode-html-show').css("display", "none");
        $('.input-informasi-aset').prop('readonly', false);
        $('#btn-edit-informasi-aset').html('<i class="fa fa-ban"></i>');
        $('#flag-editing-informasi-aset').text("1");
        $('#div-button-informasi-aset').slideToggle();
        $('#btn-simpan-informasi-aset').prop('disabled', false);
      } else if($('#flag-editing-informasi-aset').text() == "1") {
        $('.kode-html-input').css("display", "none");
        $('.kode-html-show').css("display", "");
        $('.input-informasi-aset').prop('readonly', true);
        $('#btn-edit-informasi-aset').html('<i class="fa fa-gear"></i>');
        $('#flag-editing-informasi-aset').text("0");
        $('#btn-simpan-informasi-aset').prop('disabled', true);
        $('#div-button-informasi-aset').slideToggle();
      }
    });
  });
</script>
@endsection