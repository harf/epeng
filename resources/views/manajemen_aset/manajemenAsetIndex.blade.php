<script src="/js/core/jquery.min.js" type="text/javascript"></script>

<?php
    use Carbon\Carbon;
    setlocale (LC_TIME, 'id_ID');
?>

@extends('layouts.master')
@section('content')
    @if($message_text)
    <div class="modal fade" id="modalMessage" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" @if($message_id == 1) style="background: rgba(76, 175, 80, 0.6);" @else style="background: rgba(255, 119, 119, 0.6);" @endif>
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"><small>PESAN SISTEM</small></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-2" style="text-align: center;">
                            <center>
                                @if($message_id == 1)
                                    <h1 class="text-success"><i class="fa fa-check-circle"></i></h1>
                                @else
                                    <h1 class="text-danger"><i class="fa fa-times-circle"></i></h1>
                                @endif
                            </center>
                        </div>

                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <blockquote>
                                @if($message_id == 1)
                                    <h4 class="text-success"><strong>Data {{ $message_source }} berhasil {{ $message_crud }}.</strong></h4>
                                @else
                                    <h4 class="text-danger"><strong>Data {{ $message_source }} gagal {{ $message_crud }}.</strong></h4>
                                    <footer><small>Error: {{ $message_text }}</small></footer>
                                @endif
                            </blockquote>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal" aria-label="Close">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
      $(window).on('load',function(){
        $('#modalMessage').modal('show');
      });
    </script>
    @endif

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-5">
                            <h3 class="card-title text-primary">
                                <small>
                                    <strong>
                                        <i class="fa fa-cubes"></i>&emsp;Dashboard Stok
                                    </strong>
                                </small>
                            </h3>
                        </div>

                        <div class="col-md-7">
                            <div class="pull-right">
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="row">
                        <div class="col-lg-10 col-md-10 col-sm-10">
                            <i><small>Halaman {{ $page_number }}/{{ceil($count_list_aset/$page_size)}} - menampilkan @if($is_filtered == 0 && $is_searched == 0) seluruh Aset @else Aset @if($is_filtered != 0) <u>{{ $filter_name }}</u> @endif @if($is_searched != 0) @if($is_filtered != 0) > @endif "<u>{{ $keyword }}</u>" @endif @endif sebanyak @if($list_aset) <strong>{{ count($list_aset) }}/{{ ($count_list_aset)?$count_list_aset:0 }} data</strong> @else <strong>0 data</strong>@endif.</small></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4">

                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-filter"></i>&emsp;</span>
                                </div>

                                <select class="form-control input-sm" name="id_jenis_aset" id="id-jenis-aset" rel="tooltip" title="Filter data berdasarkan Jenis Aset SIMKIM" @if($is_filtered != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" @endif>
                                    <option value="0"  selected="true">Semua Jenis Aset</option>

                                    @if($list_jenis_aset)
                                        @foreach ($list_jenis_aset as $jenis_aset)
                                            @if($jenis_aset->id_jenis_aset == $filter)
                                                <option value="{{ $jenis_aset->id_jenis_aset }}" selected="true">
                                                    {{ $jenis_aset->nama_jenis_aset }}
                                                </option>
                                            @else
                                                <option value="{{ $jenis_aset->id_jenis_aset }}">
                                                    {{ $jenis_aset->nama_jenis_aset }}
                                                </option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-search"></i>&emsp;</span>
                                </div>
                                <input type="text" class="form-control" id="keyword-search" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter untuk mencari data" rel="tooltip" title="Cari data Aset SIMKIM" @if($is_searched != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" value="{{ $keyword }}" @endif>
                            </div>
                        </div>

                        <div class="col-lg-2 col-md-2 col-sm-2">
                            <div class="input-group input-group-sm mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-eye"></i>&emsp;</span>
                                </div>

                                <input type="hidden" id="page-number" value="{{ $page_number }}">
                                <select class="form-control input-sm" name="page_size" id="page-size" rel="tooltip" title="Tampilkan banyaknya jumlah data dalam satu halaman">
                                    <option value="15"  selected="true">15</option>
                                    <option value="30" @if($page_size == 30) selected="true" @endif>30</option>
                                    <option value="60" @if($page_size == 60) selected="true" @endif>60</option>
                                    <option value="120" @if($page_size == 120) selected="true" @endif>120</option>
                                    <option value="240" @if($page_size == 240) selected="true" @endif>240</option>
                                    <option value="480" @if($page_size == 480) selected="true" @endif>240</option>
                                    <option value="960" @if($page_size == 960) selected="true" @endif>960</option>
                                </select>

                                <div class="input-group-prepend">
                                    <span class="input-group-text text-primary" id="inputGroup-sizing-sm">&emsp;data</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card card-profile">
                <div class="card-body">
                <!-- <div class="table-responsive"> -->
                    <table class="table table-bordered">
                        <colgroup>
                            <col style="width:3%;">
                            <col style="width:3%;">
                            <col style="width:30%;">
                            <col style="width:10%;">
                            <col style="width:10%;">
                            <col style="width:5%;">
                            <col style="width:5%;">
                            <col style="width:10%;">
                            <!-- <col style="width:20%;"> -->
                        </colgroup>
                        <thead class="thead-primary" style="text-align: center;">
                            <tr>
                                <th rowspan="2"></th>
                                <th rowspan="2"><strong>NO.</strong></th>
                                <th rowspan="2"><strong>ASET</strong></th>
                                <th rowspan="2"><strong>KATEGORI ASET</strong></th>
                                <th rowspan="2"><strong>JENIS ASET</strong></th>
                                <th colspan="3"><strong>STOK TERAKHIR</strong></th>
                            </tr>
                            <tr class="thead-primary-lighter">
                              <th>Total</th>
                              <th>Satuan</th>
                              <th>Per Tanggal</th>
                            </tr>
                        </thead>

                        <tbody>
                            @if($list_aset)
                                <i style="display: none">{{ $index = (($page_number-1) * $page_size) + 1 }}</i>
                                
                                @foreach($list_aset as $key => $aset)
                                    <tr>
                                        <td id="sumber-expandable-td-{{ $aset->id_aset }}" style="cursor: pointer;"><button type="button" id="btn-sumber-exp-{{ $aset->id_aset }}"class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button></td>
                                        <td>{{ $index }}.</td>
                                        <td>
                                            <a href="/aset/{{ $aset->id_aset }}/detail" rel="tooltip" title="Lihat Rincian Aset"><u><strong id="nama-aset-{{ $aset->id_aset }}">{{ $aset->nama_merek . ' ' . $aset->tipe_aset }}</strong></u></a>
                                        </td>
                                        <td>{{ $aset->nama_kategori_aset }}</td>
                                        <td>{{ $aset->nama_jenis_aset }}</td>
                                        <td>{{ ($aset->jumlah_stok)?$aset->jumlah_stok:'-' }}</td>
                                        <td>{{ ($aset->satuan_stok)?$aset->satuan_stok:'-' }}</td>
                                        <td class="text-primary">{{ strftime( "%d %B %Y", strtotime($aset->tanggal_stok)) }}</td>
                                    </tr>

                                    <tr id="sumber-tr-{{ $aset->id_aset }}">
                                        <td colspan="8" style="padding-left: 30px;">
                                            <div id="hidden-sumber-expandable-row-{{ $aset->id_aset }}" style="border: 1px solid #e8e8e8; border-radius: 10px; padding: 0px; background-color: #fcf7ff;">
                                                <div class="row col-lg-12 col-md-12 col-sm-12">
                                                    <div class="form-group row col-lg-12 col-md-12 col-sm-12">Sumber Stok Aset {{ $aset->nama_merek . ' ' . $aset->tipe_aset }}:</div>
                                                    <div class="row col-lg-12 col-md-12 col-sm-12" style="margin: 0px;">
                                                        <input type="hidden" id="check-sumber-stok-{{ $aset->id_aset }}" value="0">
                                                        <table class="table table-bordered" style="margin: 0px;">
                                                            <colgroup>
                                                                <col style="width:3%;">
                                                                <col style="width:3%;">
                                                                <col style="width:67%;">
                                                                <col style="width:15%;">
                                                                <col style="width:12%;">
                                                            </colgroup>

                                                            <thead class="thead-primary" style="text-align:center;">
                                                                <tr>
                                                                    <th><strong></strong></th>
                                                                    <th><strong>No.</strong></th>
                                                                    <th><strong>Pengadaan</strong></th>
                                                                    <th><strong>Tahun Pengadaan</strong></th>
                                                                    <th><strong>Jumlah</strong></th>
                                                                </tr>
                                                            </thead>

                                                            <tbody id="sumber-stok-aset-{{ $aset->id_aset }}"></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <br />
                                            </div>
                                        </td>
                                    </tr>

                                    <i style="display: none">{{ $index++ }}</i>
                                @endforeach
                                @else
                                    <tr>
                                        <td colspan="8">
                                            <center><i>Tidak ada data.</i></center>
                                        </td>
                                    </tr>
                                @endif
                        </tbody>
                    </table>
                <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
    <br>
    <hr>

    <div class="row">
        <div class="pull-right">
            <nav aria-label="...">
                <ul class="pagination">
                    @if($page_number == 1)
                        <li class="page-item disabled">
                            <span class="page-link">Sebelumnya</span>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter={{ $filter }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number-1 }}">Sebelumnya</a>
                        </li>  
                    @endif

                    @for($i = 0; $i < $count_list_aset/$page_size; $i++)
                        <li class="page-item @if($page_number == $i+1) active @endif">
                            @if($page_number == $i+1)
                                <span class="page-link">
                                    {{ $i+1 }}
                                    <span class="sr-only">(current)</span>
                                </span>
                            @else
                                <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter={{ $filter }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $i+1 }}">{{ $i+1 }}</a>
                            @endif
                        </li>
                    @endfor

                    @if($page_number == ceil($count_list_aset/$page_size))
                        <li class="page-item disabled">
                            <span class="page-link">Selanjutnya</span>
                        </li>
                    @else
                        <li class="page-item">
                            <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter={{ $filter }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number+1 }}">Selanjutnya</a>
                        </li> 
                    @endif
                </ul>
            </nav>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            $("select").select2();
            $("[id^=hidden-sumber-expandable-row-]").hide();
            $("[id^=hidden-satuan-expandable-row-]").hide();
            $("[id^=sumber-tr-]").hide();
            $("[id^=new-satuan-row-]").hide();
            moment.locale('id');
        

            $(document).on("click", "[id^=sumber-expandable-td-]", function (e) {
                e.preventDefault();
                var targetId = e.currentTarget.id.split('-').pop();

                if($('#btn-sumber-exp-' + targetId).length){
            
                    $('#sumber-expandable-td-' + targetId).html('<button type="button" id="btn-sumber-col-' + targetId + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-minus-circle"></i></button>');

                    if($('#check-sumber-stok-' + targetId).val() == 0) {
                        $.ajax({
                            type:"GET",
                            url:"{{url('api/get-sumber-pengadaan-list')}}?id_aset="+targetId,
                            success:function(res){
                                if(res){
                                // console.log(res);
                                    if (res.list_sumber_aset.length != 0) {
                                        $.each(res.list_sumber_aset,function(index,Obj){
                                            var newRowSumber = '<tr><td id="satuan-expandable-td_' + targetId + '-' + Obj.kode_pengadaan + '" style="cursor: pointer;"><button type="button" id="btn-satuan-exp-' + targetId + '-' + Obj.kode_pengadaan +'" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button></td><td>' + (index+1) +'.</td><td>' + Obj.nama_pengadaan + '</td><td>' + Obj.tahun_pengadaan + '</td><td style="text-align: center;">' + Obj.jumlah_aset_masuk + '</td></tr><tr id="new-satuan-row-' + targetId + '-' + Obj.kode_pengadaan + '"><td colspan="6" style="padding-left: 40px;"><div id="hidden-satuan-expandable-row-' + targetId + '-' + Obj.kode_pengadaan + '" style="border: 1px solid rgb(232, 232, 232); border-radius: 10px; padding: 0px; background-color: white;"><div class="row col-lg-12 col-md-12 col-sm-12"><div class="form-group row col-lg-12 col-md-12 col-sm-12">Satuan Aset:</div><div class="row col-lg-12 col-md-12 col-sm-12" style="margin: 0px;"><input id="check-satuan-aset-' + targetId + '-' + Obj.kode_pengadaan + '" value="0" type="hidden"><table class="table table-bordered" style="margin: 0px; margin-bottom: 10px; vertical-align: middle;"><colgroup><col style="width:8%;"><col style="width:12%;"><col style="width:25%;"><col style="width:10%;"><col style="width:15%;"><col style="width:28%;"><col style="width:2%;"></colgroup><thead class="thead-primary-lighter"><tr style="text-align:center;"><th><strong>Satuan #</strong></th><th><strong>Field Satuan Aset</strong></th><th><strong>Isian Field Satuan Aset</strong></th><th><strong>Harga Satuan (Rp.)</strong></th><th><strong>Status Terakhir</strong></th><th><strong>Rincian Informasi Satuan Aset</strong></th><th><strong></strong></th></tr></thead><tbody id="satuan-aset-' + targetId + '-' + Obj.kode_pengadaan + '"></tbody></table></div></div></div></td></tr>';
                                            $(newRowSumber).appendTo($("#sumber-stok-aset-" + targetId));
                                            $('#check-sumber-stok-' + targetId).val(1);
                                            $("[id^=hidden-satuan-expandable-row-" + targetId + "]").hide();
                                            $("[id^=new-satuan-row-" + targetId + "-]").hide();
                                        });
                                    } else {
                                        // console.log('plis');
                                        var emptyRowSumber = '<tr><td colspan="6"><i>Tidak ada data.</i></td></tr>';
                                        $(emptyRowSumber).appendTo($("#sumber-stok-aset-" + targetId));
                                        $('#check-sumber-stok-' + targetId).val(0);
                                    }
                                }
                            }, error:function(x) {
                                alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
                                console.log(x);
                            }
                        });
                    }
                    $("#sumber-tr-" + targetId).attr("style", "").fadeIn().delay(500);

                } else {
                    $('#sumber-expandable-td-' + targetId).html('<button type="button" id="btn-sumber-exp-' + targetId + '"class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>');
                    $('#check-sumber-stok-' + targetId).val(1);

                    $("#sumber-tr-" + targetId).attr("style", "").fadeOut().delay(500);
                }

                $('#hidden-sumber-expandable-row-' + targetId).slideToggle();   
            });



            $(document).on("click", "[id^=satuan-expandable-td_]", function (e) {
                e.preventDefault();
                var target = e.currentTarget.id.split('_').pop();
                var targetParent = target.split('-')[0];
                var targetChild = target.split('-').pop();

                if($('#btn-satuan-exp-' + targetParent + '-' + targetChild).length){
                    $('#satuan-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-satuan-col-' + targetParent + '-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-minus-circle"></i></button>');
                    if($('#check-satuan-aset-' + targetParent + '-' + targetChild).val() == 0) {
                        var last_kode_grup = 0;
                        var count_grup_satuan = 0;
                        var index_col = 0;
                        $.ajax({
                            type:"GET",
                            url:"{{url('api/get-satuan-aset-pengadaan-list')}}?id_aset=" + targetParent + "&kode_pengadaan=" + targetChild,
                            success:function(res){
                                if(res){
                                    var last_index_list = parseInt(res.list_satuan_aset_pengadaan.length) - 1;

                                    if(res.list_satuan_aset_pengadaan.length != 0) {
                                        $.each(res.list_satuan_aset_pengadaan,function(index,Obj){
                                            var status_satuan_aset = '-';
                                            console.log(Obj);

                                            if (Obj.asa_id_aset_keluar) {

                                                if (Obj.ake_jenis_aset_keluar == 'Alokasi'){

                                                    status_satuan_aset = '<span class="badge badge-success" rel="tooltip" title="Dialokasikan pada tanggal ' + moment(Obj.ake_tanggal_aset_keluar).format('Do MMMM YYYY') + '"><strong>ALLOCATED</strong></span><br><i class="text-success"><strong>';
                                                    
                                                    if (Obj.ake_kode_kanim) {
                                                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                                                    } else {
                                                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                                                    }
                                                    status_satuan_aset = status_satuan_aset + '</strong></i>';
                                                } else {
                                                    status_satuan_aset = '<i class="text-success">Aset dipinjamkan kepada ';

                                                    if (Obj.ake_kode_kanim) {
                                                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                                                    } else {
                                                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                                                    }
                                                    status_satuan_aset = status_satuan_aset + ' dan harus dikembalikan pada tanggal ' + moment(Obj.ake_tanggal_aset_kembali).format('Do MMMM YYYY') + '.</i>'
                                                }
                                            } else {
                                                var tersimpan_di = '';

                                                if (Obj.asa_id_lokasi) {
                                                    tersimpan_di = Obj.asa_nama_lokasi;

                                                    if (Obj.asa_id_kontainer) {
                                                        tersimpan_di = tersimpan_di + ' >> ' + Obj.asa_nama_kontainer;
                        
                                                        if(Obj.asa_id_posisi_kontainer) {
                                                            tersimpan_di = tersimpan_di + ' >> ' + Obj.asa_label_posisi_kontainer;
                                                        }
                                                    } 
                                                    status_satuan_aset = '<span class="badge badge-default" rel="tooltip" title="Masih tersimpan di ' + tersimpan_di + '"><strong>UNALLOCATED</strong></span>';
                                                }
                                            }

                                            var aset_informasi = '';

                                            if(Obj.list_aset_informasi.length != 0) {
                                                $.each(Obj.list_aset_informasi,function(index2,Obj2){
                                                    aset_informasi = aset_informasi + '- ' + Obj2.nama_field_info + ': ' + Obj2.kode_html_show + '<br>'; 
                                                });
                                            } else {
                                                var aset_informasi = '<i class="text-warning">Tidak ada data.</i>';
                                            }

                                            if (last_kode_grup == 0) {
                                                count_grup_satuan = count_grup_satuan + 1;
                                                var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle;">' + aset_informasi + '</td><td id="aksi-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '"><a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-link btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a></td></tr>';
                                                $(newRowSatuan).appendTo($("#satuan-aset-" + targetParent + '-' + targetChild));
                                                index_col = index_col + 1;
                                            } else if (Obj.asa_kode_grup_satuan == last_kode_grup) {
                                                count_grup_satuan = count_grup_satuan + 1;
                                                var newRowSatuan = '<tr><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td></tr>';
                                                $(newRowSatuan).appendTo($("#satuan-aset-" + targetParent + '-' + targetChild));

                                                if(index == last_index_list) {
                                                    $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                                                    $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                                                    $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;' });
                                                    $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;' });                      
                                                    $("#aksi-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                                                    last_kode_grup = '';                    
                                                    count_grup_satuan = 0;
                                                    index = 0;
                                                }
                                            } else {
                                                $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                                                $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                                                $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;' });
                                                $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;' });
                                                $("#aksi-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });

                                                last_kode_grup = '';                    
                                                count_grup_satuan = 0;
                                                count_grup_satuan = count_grup_satuan + 1;
                                                var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle;">'+ aset_informasi +'</td><td id="aksi-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '"><a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-link btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a></td></tr>';
                                                $(newRowSatuan).appendTo($("#satuan-aset-" + targetParent + '-' + targetChild));
                                                index_col = index_col + 1;
                                            }

                                            $('#check-satuan-aset-' + targetParent + '-' + targetChild).val(1);
                                            last_kode_grup = Obj.asa_kode_grup_satuan;
                                        });
                                    } else {
                                        var emptyRowSatuan = '<tr><td colspan="6"><i>Tidak ada data.</i></td></tr>';
                                        $(emptyRowSatuan).appendTo($('#satuan-expandable-td_' + targetParent + '-' + targetChild));
                                        $('#check-satuan-aset-' + targetParent + '-' + targetChild).val(0);
                                    }
                                }
                            }, error:function(x) {
                                alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
                                console.log(x);
                            }
                        });
                    } else { 
            
                    }
                    $("#new-satuan-row-" + targetParent + "-" + targetChild).attr("style", "").fadeIn().delay(500);
                } else {
                    $('#satuan-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-satuan-exp-' + targetParent + '-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>');
                    $('#check-satuan-aset-' + targetParent + '-' + targetChild).val(1);
                    $("#new-satuan-row-" + targetParent + "-" + targetChild).attr("style", "").fadeOut().delay(500);
                }
                $('#hidden-satuan-expandable-row-' + targetParent + '-' + targetChild).slideToggle();
            });

            $(document).on("change", "#id-jenis-aset", function(e) {
                e.preventDefault();
                var targetId = e.target.value;

                var keyword = $("#keyword-search").val();
                var is_searched = 0;

                if (keyword == '') {
                    keyword = "null";
                    is_searched = 0;
                } else {
                    is_searched = 1;
                }

                var page_size = $("#page-size").val();
                var page_number = $("#page-number").val();

                var getUrl = window.location;
                var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
              
                if (targetId != 0) window.location.href = baseUrl + "?is_filtered=1&is_searched=" + is_searched + "&filter=" + targetId + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=1";
                else window.location.href = baseUrl;
            });

            $(document).on("keypress", "#keyword-search", function(e) {
                if(e.which === 13){
                    e.preventDefault();
                    var keyword = $("#keyword-search").val();
                    var filter = $("#id-jenis-aset").val();

                    var is_filtered = 0;
                    if (filter == 0) {
                        is_filtered = 0;
                        jenis_aset = null;
                    } else {
                        is_filtered = 1;
                        jenis_aset = filter;
                    }

                    var page_size = $("#page-size").val();
                    var page_number = $("#page-number").val();

                    var getUrl = window.location;
                    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
                
                    if (keyword.replace(" ", "") != '') window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=1&filter=" + filter + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=" + page_number;
                    else if (is_filtered != 0) window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=0&filter=" + filter + "&keyword=null" + "&page_size=" + page_size + "&page_number=1";
                    else window.location.href = baseUrl;
                }
            });

            $(document).on("change", "#page-size", function(e) {
                e.preventDefault();
                var targetId = e.target.value;
                var filter = $("#id-jenis-aset").val();
                var keyword = $("#keyword-search").val();

                var is_filtered = 0;
                if (filter == 0) is_filtered = 0;
                else is_filtered = 1;

                var is_searched = 0;
                if (keyword == '') {
                    keyword = "null";
                    is_searched = 0;
                } else {
                    is_searched = 1;
                }

                var page_number = $("#page-number").val();
                var getUrl = window.location;
                var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
              
                window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=" + is_searched + "&filter=" + filter + "&keyword=" + keyword + "&page_size=" + targetId + "&page_number=1";
            });
        });
    </script>
@endsection