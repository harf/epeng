<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/pengadaan">Paket Pengadaan</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Tambah</u></li>
  </ol>
</nav>
<div class="row">
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <form method="POST" action="{{ route('simpanPengadaan') }}" enctype="multipart/form-data">
      <div class="card card-profile">
        <div class="card-header card-header-primary">
          <h4 class="card-title"><strong>Tambah Paket Pengadaan Baru</strong></h4>
        </div>
        <div class="card-body" align="left">
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Nama Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" name="nama_pengadaan">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Kategori Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
               <select class="form-control input-sm" name="id_kategori_pengadaan" id="id_kategori_pengadaan">
                  <option value=""  selected="true">Pilih Kategori Pengadaan</option>
                  @foreach ($list_kategori_pengadaan as $kategori)
                    <option value="{{ $kategori->id_kategori_pengadaan }}">{{ $kategori->nama_kategori_pengadaan }}</option>
                  @endforeach
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Nomor Pengadaan
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="nomor_pengadaan">
              </div>
            </div>
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Tahun Pengadaan
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <select class="form-control" name="tahun_pengadaan">
                  <option value=""  selected="true">Pilih Tahun Pengadaan</option>
                  @foreach ($list_tahun_pengadaan as $tahun)
                    <option value="{{ $tahun }}">{{ $tahun }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Nilai Anggaran
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="nilai_anggaran">
              </div>
            </div>
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Kode Anggaran
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="kode_anggaran">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Nama PPK
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" name="nama_ppk">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Tanggal Kontrak
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="date" name="tanggal_mulai_kontrak" id="tanggal_mulai_kontrak" class="form-control">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1">
              s.d.
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="date" name="tanggal_selesai_kontrak" id="tanggal_selesai_kontrak" name="tanggal_selesai_kontrak" class="form-control">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Penyedia
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <select class="form-control" name="id_penyedia">
                <option value=""  selected="true">Pilih Pihak Penyedia</option>
                @foreach ($list_penyedia as $penyedia)
                  <option value="{{ $penyedia->id_penyedia }}">{{ $penyedia->nama_penyedia }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Keterangan Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <textarea class="form-control" name="keterangan_pengadaan" rows="3"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="row">
                  <strong>&emsp;&emsp; Rincian Item Pengadaan &emsp;</strong>
                  <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-item" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
                </div>
                <input type="hidden" id="count-item" name="count_item" value="0" />
                <input type="hidden" id="last-item-id" value="0" />
                <input type="hidden" id="index-item" name="index_item" value="" />
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 text-secondary">
                <i>Tidak ada item yang dicari?<br><a href="#"><u>Klik disini</u></a> untuk menambahkan item baru.</i>
              </div>
            </div>
            <br>
            <div id="rincian-item-pengadaan">
              
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                &emsp;Dokumen Pengadaan &emsp;
                <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-dokumen" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
              </div>
              <input type="hidden" id="count-dokumen" name="count_dokumen" value="0" />
              <input type="hidden" id="last-dokumen-id" value="0" />
            </div>
            <div id="dokumen-pengadaan">

            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row pull-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Simpan Paket Pengadaan</button>
          </div>
        </div>
      </div>
      {{ csrf_field() }}
    </form>
  </div>
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
</div>

<script type="text/javascript"> 
  $(document).ready(function(){
      $("#last-item-id").val(parseInt(0));
      $("#count-item").val(parseInt(0));
      $("#index-item").val('');


      $(document).on("keypress", "[type='number']", function (e) {
        e.preventDefault();
      });

      $(document).on("change", "[id^=file-dokumen-pengadaan-tambah-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $("#url-dokumen-pengadaan-tambah-" + targetId).val(this.value.replace(/C:\\fakepath\\/i, ''));
      });

      $(document).on("click", ".showmore", function (e) {
        e.preventDefault();
        $(this).parent().parent().next().slideToggle("fast");
        $(this).css("display", "none");
      });

      $('[id^=form-penyimpanan-dokumentasi-]').hide();
      $(document).on('change', '[class^="dokumentasi-fisik-"]', function (e) {
          var targetClass = $(e.currentTarget).attr('class').split(' ').pop();
          var targetClassId = targetClass.split('-').pop();
          e.preventDefault();
          if (this.value == "Y") { //check value if it is domicilio
              $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).show(750); //than show
          } else {
              $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).hide(750); //else hide
              $('#tambah-lokasi-dropdown-' + targetClassId).val('');
              $('#tambah-kontainer-dropdown-' + targetClassId).val('');
              $('#tambah-posisi-kontainer-dropdown-' + targetClassId).val('');
          }
      });

      $(document).on("change", "[id^=tambah-kategori-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var kategori_aset = e.target.value;

        if(kategori_aset){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-jenis-aset-list')}}?kategori_aset="+kategori_aset,
             success:function(res){
              if(res){
                  $('#tambah-jenis-aset-dropdown-' + targetId).empty();
                  $('#tambah-spek-aset-kiri-' + targetId).empty();
                  $('#tambah-spek-aset-kanan-' + targetId).empty();
                  $('#tambah-jenis-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Jenis Aset</option>');
                  $('#tambah-tipe-aset-dropdown-' + targetId).empty();
                  $('#tambah-tipe-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Tipe Aset</option>');
                  $('#div-btn-exp-col-' + targetId).empty();
                  $('#foto-item-' + targetId).empty();
                  $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');

                  $.each(res,function(index,Obj){
                      $("#tambah-jenis-aset-dropdown-" + targetId).append('<option value="'+Obj.id_jenis_aset+'">'+Obj.nama_jenis_aset+'</option>');
                  });

              }else{
                 $("#tambah-jenis-aset-dropdown-" + targetId).empty();
                 $('#tambah-spek-aset-kiri-' + targetId).empty();
                 $('#tambah-spek-aset-kanan-' + targetId).empty();
                 $('#div-btn-exp-col-' + targetId).empty();
                 $('#foto-item-' + targetId).empty();
                $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
              }
             }
          });
        }
        else{
          $("#tambah-jenis-aset-dropdown-" + targetId).empty();
          $('#tambah-spek-aset-kiri-' + targetId).empty();
          $('#tambah-spek-aset-kanan-' + targetId).empty();
          $('#div-btn-exp-col-' + targetId).empty();
          $('#foto-item-' + targetId).empty();
          $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
        }
      });

      $(document).on("change", "[id^=tambah-jenis-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var jenis_aset = e.target.value;
        var merek_aset =  $('#tambah-merek-aset-dropdown-' + targetId).val();

        if(merek_aset) {
          if(jenis_aset){
            $.ajax({
               type:"GET",
               url:"{{url('api/get-tipe-aset-list')}}?jenis_aset="+jenis_aset+"&merek_aset="+merek_aset,
               success:function(res){
                if(res){
                    $('#tambah-tipe-aset-dropdown-' + targetId).empty();
                    $('#tambah-spek-aset-kiri-' + targetId).empty();
                    $('#tambah-spek-aset-kanan-' + targetId).empty();
                    $('#tambah-tipe-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Tipe Aset</option>');
                    $('#div-btn-exp-col-' + targetId).empty();
                    $('#foto-item-' + targetId).empty();
                    $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');

                    $.each(res,function(index,Obj){
                        $("#tambah-tipe-aset-dropdown-" + targetId).append('<option value="'+Obj.id_aset+'">'+Obj.tipe_aset+'</option>');
                    });

                }else{
                   $("#tambah-tipe-aset-dropdown-" + targetId).empty();
                   $('#tambah-spek-aset-kiri-' + targetId).empty();
                   $('#tambah-spek-aset-kanan-' + targetId).empty();
                   $('#div-btn-exp-col-' + targetId).empty();
                   $('#foto-item-' + targetId).empty();
                  $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
                }
               }
            });
          }
          else{
            $("#tambah-tipe-aset-dropdown-" + targetId).empty();
            $('#tambah-spek-aset-kiri-' + targetId).empty();
            $('#tambah-spek-aset-kanan-' + targetId).empty();
            $('#div-btn-exp-col-' + targetId).empty();
            $('#foto-item-' + targetId).empty();
            $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
          }
        }
      });

      $(document).on("change", "[id^=tambah-merek-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var jenis_aset = $('#tambah-jenis-aset-dropdown-' + targetId).val();
        var merek_aset = e.target.value;

        if(jenis_aset) {
          if(merek_aset){
            $.ajax({
               type:"GET",
               url:"{{url('api/get-tipe-aset-list')}}?jenis_aset="+jenis_aset+"&merek_aset="+merek_aset,
               success:function(res){
                if(res){
                    $('#tambah-tipe-aset-dropdown-' + targetId).empty();
                    $('#tambah-spek-aset-kiri-' + targetId).empty();
                    $('#tambah-spek-aset-kanan-' + targetId).empty();
                    $('#tambah-tipe-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Tipe Aset</option>');
                    $('#div-btn-exp-col-' + targetId).empty();
                    $('#foto-item-' + targetId).empty();
                    $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');

                    $.each(res,function(index,Obj){
                        $("#tambah-tipe-aset-dropdown-" + targetId).append('<option value="'+Obj.id_aset+'">'+Obj.tipe_aset+'</option>');
                    });

                }else{
                   $("#tambah-tipe-aset-dropdown-" + targetId).empty();
                   $('#tambah-spek-aset-kiri-' + targetId).empty();
                   $('#tambah-spek-aset-kanan-' + targetId).empty();
                   $('#div-btn-exp-col-' + targetId).empty();
                   $('#foto-item-' + targetId).empty();
                   $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
                }
               }
            });
          }
          else{
            $("#tambah-tipe-aset-dropdown-" + targetId).empty();
            $('#tambah-spek-aset-kiri-' + targetId).empty();
            $('#tambah-spek-aset-kanan-' + targetId).empty();
            $('#div-btn-exp-col-' + targetId).empty();
            $('#foto-item-' + targetId).empty();
            $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
          }
        }
      });

      $(document).on("change", "[id^=tambah-tipe-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var jenis_aset = $('#tambah-jenis-aset-dropdown-' + targetId).val();
        var tipe_aset = e.target.value;
        var nama_jenis_aset = $('#tambah-jenis-aset-dropdown-' + targetId).find('option:selected').text();
        var nama_merek_aset = $('#tambah-merek-aset-dropdown-' + targetId).find('option:selected').text();
        var nama_tipe_aset = e.target.options[e.target.selectedIndex].text;

        
        $('#foto-item-' + targetId).empty();
        $('#tambah-spek-aset-kiri-' + targetId).empty();
        $('#tambah-spek-aset-kanan-' + targetId).empty();
        
        if(jenis_aset) {
          if(tipe_aset){
            $.ajax({
               type:"GET",
               url:"{{url('api/get-spesifikasi-aset-list')}}?jenis_aset="+jenis_aset+"&tipe_aset="+tipe_aset,
               success:function(res){
                if(res){
                    var list_field = res.list_field;
                    var list_spesifikasi = res.list_spesifikasi;
                    var list_gambar = res.list_gambar;

                    var length_list_field = list_field.length;

                    var length_list_field = res.list_field.length;
                    var length_list_kiri = Math.round(length_list_field/2);
                    for (var i = 0; i < length_list_kiri; i++)
                    {
                      var idx = i + 1;
                      if (list_spesifikasi[i].isian_field){
                        $('#tambah-spek-aset-kiri-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[i].nama_field_spek +'</div><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[i].nama_field_spek +'" value="'+ list_spesifikasi[i].isian_field +'" readonly="readonly" disabled="disabled" style="background-color: white;"></div></div>');
                      } else {
                        $('#tambah-spek-aset-kiri-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">'+ list_field[i].nama_field_spek +'</div><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[i].nama_field_spek +'"></div></div>');
                      }
                    }

                    var length_list_kanan = length_list_field - length_list_kiri;
                    for (var j = length_list_kiri; j < length_list_field; j++)
                    {
                      var idx = j + 1;
                      if (list_spesifikasi[i].isian_field){
                        $('#tambah-spek-aset-kanan-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[j].nama_field_spek +'</div><div class="col-md-7 col-sm-7 col-xs-7"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[j].nama_field_spek +'" value="'+ list_spesifikasi[j].isian_field +'" readonly="readonly" disabled="disabled" style="background-color: white;"></div></div>');
                      } else {
                        $('#tambah-spek-aset-kanan-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[j].nama_field_spek +'</div><div class="col-md-7 col-sm-7 col-xs-7"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[j].nama_field_spek +'"></div></div>');
                      }
                    }

                    $('#foto-item-' + targetId).empty();
                    if (list_gambar.length != 0) {
                      for (var k = 0; k < list_gambar.length; k++) {
                        $('#foto-item-' + targetId).append('<div class="col-md-3 gambar-aset-thumbnails-div" style="margin-bottom: 3px;"><img src="' + list_gambar[k].link_gambar.replace('public', '/storage') +'" alt="' + list_gambar[k].nama_gambar + '" class="gambar-aset-thumbnails"></div>');
                      }
                    }

                    $('#div-btn-exp-col-' + targetId).empty();
                    $('#div-btn-exp-col-' + targetId).append('<button title="Lihat lebih rinci Spesifikasi Item" type="button" class="btn btn-sm btn-secondary" id="btn-expand-collapse-spek-' + targetId + '" style="border-radius: 0px;"><i class="fa fa-arrows-v"></i></button>');

                    $('#title-item-pengadaan-' + targetId).html(nama_jenis_aset + ' ' + nama_merek_aset + ' ' + nama_tipe_aset);
                }else{
                   $('#tambah-spek-aset-kiri-' + targetId).empty();
                   $('#tambah-spek-aset-kanan-' + targetId).empty();
                   $('#foto-item-' + targetId).empty();
                   $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
                }
               }
            });
          }
          else{
            $('#tambah-spek-aset-kiri-' + targetId).empty();
            $('#tambah-spek-aset-kanan-' + targetId).empty();
            $('#foto-item-' + targetId).empty();
            $('#title-item-pengadaan-' + targetId).html('<i><small>item belum terpilih.</small></i>');
          }
        }
      });

      $("[id^=div-spesifikasi-item-]").hide();
      $(document).on("click", "[id^=btn-expand-collapse-spek-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $('#div-spesifikasi-item-' + targetId).slideToggle();                
      });

      $(document).on("click", ".btn-tambah-item", function (e) {
        e.preventDefault();
        var current = $("#last-item-id").val();
        var next = parseInt(current) + 1;
        var count_item = $("#count-item").val();

        $('#rincian-item-pengadaan').append('<div id="item-pengadaan-' + next + '" style="border: 1px solid #a7b7d6; border-radius: 10px; padding: 10px; margin-bottom: 30px; background-color: #f9fbff;"><div class="form-group row"><div class="col-md-3 col-sm-3 col-xs-3"><div class="row"><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-sm btn-link btn-danger" rel="tooltip" title="Klik untuk menghapus Item Pengadaan #' + next +'" id="btn-hapus-item-' + next + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div><div class="col-md-10 col-sm-10 col-xs-10"><h5><i>Item</i> Pengadaan #' + parseInt(next) + '</h5></div></div></div><div class="col-md-5 col-sm-5 col-xs-5"><h5 id="title-item-pengadaan-' + next + '" class="text-primary"><i><small>item belum terpilih.</small></i></h5></div><div class="col-md-3 col-sm-3 col-xs-3"><h5 class="text-primary">Jumlah: <span class="badge badge-primary" id="jumlah-item-pengadaan-' + next + '">1</span> <small id="unit-item-pengadaan-' + next + '"></small></h5></div><div class="col-md-1 col-sm-1 col-xs-1"><div class="pull-right"><button title="Lihat lebih rinci Detail Item Pengadaan #' + next + '" type="button" class="btn btn-sm btn-primary" id="btn-expand-collapse-detail-' + next + '" style="border-radius: 0px;"><i class="fa fa-arrows-v"></i></button></div></div></div><div class="row col-md-12 col-sm-12 col-xs-12" id="div-detail-item-' + next + '"><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11 div-shadow-hover" style="border: 1px solid #d3d3d3; border-radius: 10px; padding: 10px; margin-bottom: 10px; background-color: #fffefc;"><div class="row col-md-12 col-sm-12 col-xs-12"><strong><i class="text-primary">SPESIFIKASI ITEM PENGADAAN #' + next + '</i></strong></div><hr><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="kategori_aset~' + next + '" id="tambah-kategori-aset-dropdown-' + next + '"><option value="" selected>Pilih kategori aset...</option>@foreach ($list_kategori_aset as $kategori_aset)<option value="{{ $kategori_aset->id_kategori_aset }}">{{ $kategori_aset->nama_kategori_aset }}</option>@endforeach</select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="jenis_aset~' + next + '" id="tambah-jenis-aset-dropdown-' + next + '"><option value="" selected>Pilih jenis aset...</option></select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="merek_aset~' + next + '" id="tambah-merek-aset-dropdown-' + next + '"><option value="" selected>Pilih merek aset...</option>@foreach ($list_merek_aset as $merek_aset)<option value="{{ $merek_aset->id_merek }}">{{ $merek_aset->nama_merek }}</option>@endforeach</select></div><div class="col-md-5 col-sm-5 col-xs-5"><select class="form-control" name="tipe_aset~' + next + '" id="tambah-tipe-aset-dropdown-' + next + '"><option value="" selected>Pilih tipe aset...</option></select></div><div class="col-md-1 col-sm-1 col-xs-1" id="div-btn-exp-col-' + next + '"></div></div><br><div class="row" id="div-spesifikasi-item-' + next + '"><div class="row col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;"><div class="row col-md-12 col-sm-12 col-xs-12" id="foto-item-' + next + '"></div></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-6 col-sm-6 col-xs-6" id="tambah-spek-aset-kiri-' + next + '"></div><div class="col-md-6 col-sm-6 col-xs-6" id="tambah-spek-aset-kanan-' + next + '"></div></div></div></div></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11 div-shadow-hover" style="border: 1px solid #d3d3d3; border-radius: 10px; padding: 10px; margin-bottom: 10px; background-color: #fffefc;"><div class="row col-md-12 col-sm-12 col-xs-12"><strong><i class="text-primary">SATUAN ITEM PENGADAAN #' + next + '</i></strong></div><hr><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Harga satuan aset (Rupiah)" name="harga_satuan~' + next + '"></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="number" min="1" class="form-control" placeholder="Jumlah" name="jumlah_stok~' + next + '" id="jumlah-stok-' + next + '" value="1"><input type="hidden" id="last-jumlah-stok-' + next + '" value="1" /></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="text" class="form-control" placeholder="Satuan" id="satuan-stok-' + next + '" name="satuan_stok~' + next + '"></div></div><br></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div></div></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11 div-shadow-hover" style="border: 1px solid #d3d3d3; border-radius: 10px; padding: 10px; margin-bottom: 10px; background-color: #fffefc;"><div class="row col-md-12 col-sm-12 col-xs-12"><strong><i class="text-primary">RINCIAN SATUAN ITEM PENGADAAN #' + next + '</i></strong><input type="hidden" name="count_satuan~' + next + '" id="count-satuan-' + next + '" value="1" /><input type="hidden" name="index_satuan~' + next + '" id="index-satuan-' + next + '" value="1~" /></u></div><hr><div id="div-satuan-item-' + next + '"><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="satuan-item-' + next + '-1"><div id="field-isian-satuan_' + next + '-1" class="col-md-9 col-sm-9 col-xs-9"><div class="row"><input type="hidden" id="last-id-field-isian-satuan-' + next +'-1" value="1" /><input type="hidden" id="index-field-isian-satuan-' + next +'-1" name="index_field_isian_satuan~' + next +'-1" value="1~"/><div class="col-md-2 col-sm-2 col-xs-2"><i>Satuan 1</i></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="text" class="form-control" placeholder="Field Satuan 1 (1)" id="field-aset_' + next + '-1-1" name="field_aset~' + next + '-1-1"></div><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Isian dari Field Satuan 1 (1)" id="isian-aset_' + next + '-1" name="isian_aset~' + next + '-1-1"></div><div class="col-md-1 col-sm-1 col-xs-1"><button type="button"   rel="tooltip" title="Klik untuk menambah Field Satuan dan Isiannya" class="btn btn-sm btn-link btn-info" id="btn-tambah-field-isian-satuan_' + next + '-1" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button></div></div></div><div class="col-md-3 col-sm-3 col-xs-3" style="border-left: 1px solid #a7b7d6;"><i class="fa fa-map-marker"></i> Satuan ini disimpan di: <select class="form-control" name="satuan~lokasi~' + next + '-1" id="tambah-satuan-lokasi-dropdown_' + next + '-1"><option value="" selected="true">Pilih lokasi </option>@foreach ($list_lokasi as $lokasi)<option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>@endforeach</select><select class="form-control" name="satuan~kontainer~' + next + '-1" id="tambah-satuan-kontainer-dropdown_' + next + '-1"><option value="" selected="true">Pilih kontainer...</option></select><select class="form-control" name="satuan~posisi_kontainer~' + (next) + '-1" id="tambah-satuan-posisi-kontainer-dropdown_' + (next) + '-1"><option value="" selected>Pilih posisi kontainer...</option></select></div></div></div></div></div></div>');

        $("select").select2({width: '100%'});
        var current_index_item = $("#index-item").val();
        $("#index-item").val(current_index_item + next + "~");
        $("#count-item").val(parseInt(count_item)+1);
        $("#last-item-id").val(parseInt(next));           
      });

      $("[id^=div-detail-item-]").hide();
      $(document).on("click", "[id^=btn-expand-collapse-detail-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        $('#div-detail-item-' + targetId).slideToggle();                
      });

      $(document).on("change", "[id^=satuan-stok-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        $('#unit-item-pengadaan-' + targetId).text($('#satuan-stok-' + targetId).val());
      });
      
      $(document).on("click", "[id^=btn-hapus-item-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $('#item-pengadaan-' + targetId).remove();
        var current = $("#count-item").val();
        var next = parseInt(current) - 1;
        $("#count-item").val(next);

        var current_index_item = $("#index-item").val();
        var new_index_item = current_index_item.replace(targetId + '~', '');
        $("#index-item").val(new_index_item);
      });

      $(document).on("change", "[id^=jumlah-stok-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        var last_jumlah = $("#last-jumlah-stok-" + targetId).val();
        var current_jumlah = $("#jumlah-stok-" + targetId).val();
        var count_satuan = $("#count-satuan-" + targetId).val();

        if(parseInt(current_jumlah) > parseInt(last_jumlah)) {
          var next_satuan_id = parseInt(count_satuan) + 1;
          $('#div-satuan-item-' + targetId).append('<hr id="hr-satuan-item-' + targetId + '-' + next_satuan_id + '"><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="satuan-item-' + targetId + '-' + next_satuan_id +'"><div id="field-isian-satuan_' + targetId + '-' + next_satuan_id + '" class="col-md-9 col-sm-9 col-xs-9"><div class="row"><input type="hidden" id="last-id-field-isian-satuan-' + targetId + '-' + next_satuan_id + '" value="1" /><input type="hidden" id="index-field-isian-satuan-' + targetId + '-' + next_satuan_id + '" name="index_field_isian_satuan~' + targetId + '-' + next_satuan_id + '" value="1~" /><div class="col-md-2 col-sm-2 col-xs-2"><i>Satuan '+ next_satuan_id +'</i></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="text" class="form-control" placeholder="Field Satuan ' + next_satuan_id + ' (1)" id="field-aset_' + targetId + '-' + next_satuan_id + '-1" name="field_aset~' + targetId + '-' + next_satuan_id + '-1"></div><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Isian dari Field Satuan ' + next_satuan_id + ' (1)" id="isian-aset_' + targetId + '-' + next_satuan_id + '-1" name="isian_aset~' + targetId + '-' + next_satuan_id + '-1"></div><div class="col-md-1 col-sm-1 col-xs-1"><button type="button" rel="tooltip" title="Klik untuk menambah Field Satuan dan Isiannya" class="btn btn-sm btn-link btn-info" id="btn-tambah-field-isian-satuan_' + targetId + '-' + next_satuan_id + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button></div></div></div><div class="col-md-3 col-sm-3 col-xs-3" style="border-left: 1px solid #a7b7d6;"><i class="fa fa-map-marker"></i> Satuan ini disimpan di: <select class="form-control" name="satuan~lokasi~' + targetId + '-' + next_satuan_id +'" id="tambah-satuan-lokasi-dropdown_' + targetId + '-' + next_satuan_id +'"><option value="" selected="true">Pilih lokasi </option>@foreach ($list_lokasi as $lokasi)<option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>@endforeach</select><select class="form-control" name="satuan~kontainer~' + targetId + '-' + next_satuan_id +'" id="tambah-satuan-kontainer-dropdown_' + targetId + '-' + next_satuan_id +'"><option value="" selected="true">Pilih kontainer...</option></select><select class="form-control" name="satuan~posisi_kontainer~' + targetId + '-' + next_satuan_id +'" id="tambah-satuan-posisi-kontainer-dropdown_' + targetId + '-' + next_satuan_id +'"><option value="" selected>Pilih posisi kontainer...</option></select></div></div>');
          $("select").select2({width: '100%'});
          $("#last-jumlah-stok-" + targetId).val(parseInt(last_jumlah) + 1);
          $("#count-satuan-" + targetId).val(next_satuan_id);
          $("#total-satuan-" + targetId).html(next_satuan_id);
          var current_index_satuan = $("#index-satuan-" + targetId).val();
          $("#index-satuan-" + targetId).val(current_index_satuan + next_satuan_id + "~");
        } else if(parseInt(current_jumlah) < parseInt(last_jumlah)) {
          $('#hr-satuan-item-' + targetId + '-' + count_satuan).remove();
          $('#satuan-item-' + targetId + '-' + count_satuan).remove();
          var next_satuan_id = parseInt(count_satuan) - 1;
          $("#last-jumlah-stok-" + targetId).val(next_satuan_id);
          $("#count-satuan-" + targetId).val(next_satuan_id);
          $("#total-satuan-" + targetId).html(next_satuan_id);

          var current_index_satuan = $("#index-satuan-" + targetId).val();
          var new_index_satuan = current_index_satuan.replace(count_satuan + '~', '');
          $("#index-satuan-" + targetId).val(new_index_satuan);
        }
        $("#jumlah-item-pengadaan-" + targetId).text($("#jumlah-stok-" + targetId).val());
      });

      $(document).on("change", "[id^=tambah-satuan-lokasi-dropdown_]", function (e) {
        e.preventDefault();
        var target = e.currentTarget.id.split('_').pop();
        var targetParent = target.split('-')[0];
        var targetChild = target.split('-').pop();
        var lokasi = e.target.value;

        if(lokasi){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kontainer-list')}}?lokasi="+lokasi,
             success:function(res){
              if(res){

                  $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
              }
             }
          });
        }
        else{
          $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
          $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
        }

      });


      $(document).on("change", "[id^=tambah-satuan-kontainer-dropdown_]", function (e) {
        e.preventDefault();
        var target = e.currentTarget.id.split('_').pop();
        var targetParent = target.split('-')[0];
        var targetChild = target.split('-').pop();
        var kontainer = e.target.value;

        if(kontainer){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
             success:function(res){
              if(res){
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
              }
             }
          });
        }
        else{
          $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
        }
      });

      $(document).on("click", "[id^=btn-tambah-field-isian-satuan_]", function (e) {
        e.preventDefault();
        var target = e.currentTarget.id.split('_').pop();
        var targetParent = target.split('-')[0];
        var targetChild = target.split('-').pop();

        var last_id_field_isian_satuan = parseInt($("#last-id-field-isian-satuan-" + targetParent + "-" + targetChild).val()) + 1;


        $('#field-isian-satuan_' + targetParent + '-' + targetChild).append('<div class="row" id="row-field-isian-satuan-' + targetParent + '-' + targetChild + '-' + last_id_field_isian_satuan + '"><div class="col-md-2 col-sm-2 col-xs-2"></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="text" class="form-control" placeholder="Field Satuan ' + targetChild + ' (' + last_id_field_isian_satuan + ')" id="field-aset_' + targetParent + '-' + targetChild + '-' + last_id_field_isian_satuan + '" name="field_aset~' + targetParent + '-' + targetChild + '-' + last_id_field_isian_satuan + '"></div><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Isian dari Field Satuan ' + targetChild + ' (' + last_id_field_isian_satuan + ')" id="isian-aset_' + targetParent + '-' + targetChild + '-' + last_id_field_isian_satuan + '" name="isian_aset~' + targetParent + '-' + targetChild + '-' + last_id_field_isian_satuan + '"></div><div class="col-md-1 col-sm-1 col-xs-1"><button type="button"   rel="tooltip" title="Klik untuk menghapus Field Satuan dan Isiannya di baris ini" class="btn btn-sm btn-link btn-warning" id="btn-hapus-field-isian-satuan_' + targetParent + '-' + targetChild + '-' + last_id_field_isian_satuan + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div></div>');

        $("#last-id-field-isian-satuan-" + targetParent + "-" + targetChild).val(last_id_field_isian_satuan);

        var current_index_field_isian = $("#index-field-isian-satuan-" + targetParent + '-' + targetChild).val();
        $("#index-field-isian-satuan-" + targetParent + '-' + targetChild).val(current_index_field_isian + last_id_field_isian_satuan + "~");
      });

      $(document).on("click", "[id^=btn-hapus-field-isian-satuan_]", function (e) {
        e.preventDefault();
        var target = e.currentTarget.id.split('_').pop();
        var targetParent = target.split('-')[0];
        var targetChild = target.split('-')[1];
        var targetGrandChild = target.split('-')[2];


        $('#row-field-isian-satuan-' + targetParent + '-' + targetChild + '-' + targetGrandChild).remove();

        var current_index_field_isian = $("#index-field-isian-satuan-" + targetParent + '-' + targetChild).val();
        var new_index_field_isian = current_index_field_isian.replace(targetGrandChild + '~', '');
        $("#index-field-isian-satuan-" + targetParent + '-' + targetChild).val(new_index_field_isian);
      }); 

      $(document).on("click", ".btn-tambah-dokumen", function (e) {
        e.preventDefault();
        var current = $("#last-dokumen-id").val();
        var next = parseInt(current) + 1;
        $('#dokumen-pengadaan').append('<div id="dokumen-' + (next) + '" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="form-group row"><div class="col-md-3 col-sm-3 col-xs-3"><div class="row"><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-sm btn-link btn-danger" id="btn-hapus-dokumen-' + next + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div><div class="col-md-10 col-sm-10 col-xs-10"><input type="text" class="form-control" placeholder="Nama dokumen" name="nama_dokumentasi~' + (next) + '"> </div></div></div><div class="col-md-5 col-sm-5 col-xs-5"><div class="input-group"><input type="text" class="form-control" id="url-dokumen-pengadaan-tambah-' + (next) + '" readonly="" style="background-color: white;" placeholder="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB"><div class="input-group-append"><div class="btn btn-primary btn-sm"><span><i class="fa fa-upload"></i></span><input type="file" id="file-dokumen-pengadaan-tambah-' + (next) + '" rel="tooltip" data-original-title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." class="upload" name="id_dokumentasi~' + (next) + '" /></div></div></div></div><div class="col-md-4 col-sm-4 col-xs-4"><div class="input-group"><input type="date" name="tanggal_dokumentasi~' + (next) + '"class="form-control" placeholder="Tanggal Dokumentasi" rel="tooltip" title="Tanggal Dokumentasi" data-original-title="Tanggal Dokumentasi"><div class="input-group-append"><div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div></div></div></div></div><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><input type="text" class="form-control" placeholder="Keterangan dokumen" name="keterangan_dokumentasi~' + (next) + '"></div></div><br><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><div class="radio"><label class="radio-inline">Apakah ada dokumen secara fisik?</label>&emsp;<br><label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="Y"> Ya </label>&emsp;<label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="N"> Tidak </label></div></div><div id="form-penyimpanan-dokumentasi-' + (next) + '" class="col-md-8 col-sm-8 col-xs-8">Disimpan di<div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~lokasi~' + (next) + '" id="tambah-lokasi-dropdown-' + (next) + '"><option value=""  selected="true">Pilih Lokasi</option>@foreach ($list_lokasi as $lokasi)<option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>@endforeach</select></div><div class="col-md-4 col-sm-4  col-xs-4"><select class="form-control" name="dokumentasi~kontainer~' + (next) + '" id="tambah-kontainer-dropdown-' + (next) + '"><option value="" selected="true">Pilih kontainer...</option></select></div><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~posisi_kontainer~' + (next) + '" id="tambah-posisi-kontainer-dropdown-' + (next) + '"><option value="" selected>Pilih posisi kontainer...</option></select></div></div></div></div></div></div></div>');
        var count_dokumen = $("#count-dokumen").val();
        $("#count-dokumen").val(parseInt(count_dokumen)+1);
        $("#last-dokumen-id").val(parseInt(next));
        $("#form-penyimpanan-dokumentasi-" + next).hide();                  
      });

      $(document).on("click", "[id^=btn-hapus-dokumen-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $('#dokumen-' + targetId).remove();
        var current = $("#count-dokumen").val();
        var next = parseInt(current) - 1;
        $("#count-dokumen").val(next);
      });
  });
</script>
@endsection