<script src="/js/core/jquery.min.js" type="text/javascript"></script>

<?php
  use Carbon\Carbon;
  setlocale(LC_TIME, 'id_ID');
?>

<script type="text/javascript">
  $(document).ready(function(){
    $('.form-penyimpanan').hide();
    $(".dokumentasi-fisik").change(function () { //use change event
        if (this.value == "yadisimpan") { //check value if it is domicilio
            $('.form-penyimpanan').stop(true,true).show(750); //than show
        } else {
            $('.form-penyimpanan').stop(true,true).hide(750); //else hide
        }
    });

    $('.form-penyimpanan-sourcecode').hide();
    $(".sourcecode-fisik").change(function () { //use change event
        if (this.value == "yadisimpan") { //check value if it is domicilio
            $('.form-penyimpanan-sourcecode').stop(true,true).show(750); //than show
        } else {
            $('.form-penyimpanan-sourcecode').stop(true,true).hide(750); //else hide
        }
    });

    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });
  });

  $(document).ready(function(){
    var showChar = 105;
    var ellipsestext = "...";
    var moretext = "lebih rinci";
    var lesstext = "lebih ringkas";
    $('.more').each(function() {
      var content = $(this).html();
      var textcontent = $(this).text();

      if (textcontent.length > showChar) {

        var c = textcontent.substr(0, showChar);

        var html = '<span class="container" style="padding-left: 0px;"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';
        $(this).html(html);
        $(this).after('<a href="" class="morelink badge badge-dark" align="right">' + moretext + '</a>');
      }

    });

    $(".morelink").click(function() {
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
        $(this).prev().children('.morecontent').fadeToggle(500, function(){
          $(this).prev().fadeToggle(500);
        });
         
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
        $(this).prev().children('.container').fadeToggle(500, function(){
          $(this).next().fadeToggle(500);
        });
      }
      return false;
    });

    $("a.showmore").click(function(e) {
      e.preventDefault();
      $(this).parent().next().slideToggle("fast");
      $(this).css("display", "none");
    });
  });
  
  function readImageSStambah(input) {
    alert("HEY");
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#ss-tambah-preview')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
  }

  function readImageSSubah(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#ss-ubah-preview')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
  }
  
</script>

@extends('layouts.master')


@section('content')
<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card card-profile">
      <div class="card-header card-header-primary">
        <h3 class="card-title"><strong>{{ $pengadaan->nama_pengadaan }} Tahun {{ $pengadaan->tahun_pengadaan }}</strong></h3>
      </div>
      <div class="card-body">
        <div class="row" align="left">
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="form-group row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                Kode Anggaran
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                {{ $pengadaan->kode_anggaran }}
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                Nilai Anggaran
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                Rp {{ $pengadaan->nilai_anggaran }}
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                Nama PPK
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                {{ $pengadaan->nama_ppk }}
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="form-group row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                Penyedia
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                {{ $pengadaan->nama_penyedia }}
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                Kategori Pengadaan
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                {{ $pengadaan->nama_kategori_pengadaan }}
              </div>
            </div>
            <div class="form-group row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                Tanggal Kontrak
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                {{ strftime( "%d %B %Y", strtotime($pengadaan->tanggal_mulai_kontrak)) }} - {{ strftime( "%d %B %Y", strtotime($pengadaan->tanggal_selesai_kontrak)) }}
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
            <div class="form-group row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                Keterangan Pengadaan
              </div>
              <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary more">
                {{ $pengadaan->keterangan_pengadaan }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="card card-profile">
  <div class="card-body" align="left">
    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h4 class="card-title"><strong>Dokumen Pengadaan</strong></h4>
    </div>
    <br>
    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table class="table table-border table-hover">
        <colgroup>
          <col style="width:5%;">
          <col style="width:30%;">
          <col style="width:15%;">
          <col style="width:20%;">
          <col style="width:30%;">
        </colgroup>
        <thead class="thead-light">
          <tr>
            <th>NO.</th>
            <th>DOKUMEN</th>
            <th>TANGGAL DOKUMEN</th>
            <th>KETERANGAN DOKUMEN</th>
            <th>DOKUMEN FISIK DISIMPAN DI</th>
          </tr>
        </thead>
        <tbody>
          @if(count($list_dokumentasi_pengadaan) != 0)
            @foreach($list_dokumentasi_pengadaan as $key => $value)
              <tr>
                <td>{{ ($key + 1) }}.</td>
                <td><u><a href="/downloadDokumentasi/{{ $value->id_dokumentasi }}">{{ $value->nama_dokumentasi }} <i class="fa fa-download"></i></a></u></td>
                <td>{{ strftime( "%d %B %Y", strtotime($value->tanggal_dokumentasi)) }}</td>
                <td>{{ $value->keterangan_dokumentasi }}</td>
                <td>
                  {{ $value->id_lokasi ? $value->nama_lokasi : ''  }} {{ $value->id_kontainer ? ' >> ' . $value->nama_kontainer : '' }} {{ $value->id_posisi_kontainer ?' >> ' . $value->label_posisi_kontainer : '' }}
                </td>
              </tr>
            @endforeach
          @else
            <tr>
              <td colspan="5"><center><i>Tidak ada data.</i></center></td>
            </tr>
          @endif
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="card card-profile">
  <div class="card-body" align="left">
    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <h4 class="card-title"><strong>Rincian Item Pengadaan</strong></h4>
    </div>
    <br>
    <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <table class="table table-bordered table-sm" id="table-item-pengadaan">
        <colgroup>
          <col style="width:3%;">
          <col style="width:5%;">
          <col style="width:20%;">
          <col style="width:30%;">
          <col style="width:20%;">
          <col style="width:15%;">
        </colgroup>
        <thead class="thead-light">
          <tr>
            <th></th>
            <th><strong>NO.</strong></th>
            <th colspan="2"><strong>ITEM Pengadaan</strong></th>
            <th><strong>JENIS ASET</strong></th>
            <th><strong>KATEGORI ASET </strong></th>
          </tr>
        </thead>
        <tbody>
          @foreach ($list_item_pengadaan as $key => $value)
            <tr>
              <td id="aset-expandable-td_{{ $value->kode_pengadaan }}-{{ $value->id_aset }}" style="cursor: pointer;">
                <button type="button" id="btn-aset-exp-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
              </td>
              <td>{{ ($key + 1) }}.<input type="hidden" id="check-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}" value="0"></td>
              <td><img src="{{ str_replace('public', '/storage', $value->link_gambar) }}" alt="{{ $value->nama_gambar }}" class="gambar-aset-thumbnails"></td>
              <td>{{ $value->nama_merek }} {{ $value->tipe_aset }}</td>
              <td>{{ $value->nama_jenis_aset }}</td>
              <td>{{ $value->nama_kategori_aset }}</td>
            </tr>
            <tr id="tr-satuan-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}">
              <td colspan="7">
                <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="hidden-satuan-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}">
                  <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    Satuan Item Pengadaan:
                  </div>
                  <hr>
                  <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <table class="table table-bordered table-sm">
                      <colgroup>
                        <col style="width:7%;">
                        <col style="width:15%;">
                        <col style="width:23%;">
                        <col style="width:15%;">
                        <col style="width:25%;">
                        <col style="width:15%;">
                      </colgroup>
                      <thead class="thead-light" style="vertical-align: middle; text-align: center;">
                        <tr>
                          <th>Satuan #</th>
                          <th>Field Aset</th>
                          <th>Isian Field Aset</th>
                          <th>Harga</th>
                          <th>Status Saat Ini</th>
                          <th>Keterangan Aset</th>
                        </tr>
                      </thead>
                      <tbody id="tbody-satuan-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}">
                       
                      </tbody>
                    </table>
                  </div>
                </div>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){

    $("[id^=tr-satuan-aset-]").hide();
    $("[id^=check-aset-]").val(0);

    $(document).on("click", "[id^=aset-expandable-td_]", function (e) {
      e.preventDefault();
      var target = e.currentTarget.id.split('_').pop();
      var targetParent = target.split('-')[0];
      var targetChild = target.split('-')[1];

      if($('#btn-aset-exp-' + targetParent + '-' + targetChild).length) {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-col-' + targetParent +'-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-minus-circle"></i></button>');

        if($('#check-aset-' + targetParent + '-' + targetChild).val() == 0) {
          var last_kode_grup = 0;
          var count_grup_satuan = 0;
          var index_col = 0;
          $.ajax({
            type:"GET",
            url:"{{url('api/get-satuan-item-pengadaan')}}?kode_pengadaan=" + targetParent + "&id_aset=" + targetChild,
            success:function(res){
              if(res.list_satuan_item_pengadaan.length){
                var last_index_list = parseInt(res.list_satuan_item_pengadaan.length) - 1;
                $.each(res.list_satuan_item_pengadaan,function(index,Obj){
                  console.log(res.list_satuan_item_pengadaan);
                  var status_satuan_aset = '-';
                  var keterangan_satuan_aset = '<a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-link btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a>';
                  
                  if (Obj.asa_id_aset_keluar) {
                    if (Obj.ake_jenis_aset_keluar == 'Alokasi')
                    {
                      status_satuan_aset = '<i class="text-success">Sudah dialokasikan ke ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' pada tanggal ' + moment(Obj.ake_tanggal_aset_keluar).format('Do MMMM YYYY') + '.</i>';
                    }
                    else
                    {
                      status_satuan_aset = '<i class="text-success">Aset dipinjamkan kepada ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                        // keterangan_satuan_aset = 'Penerima: ' + Obj.penerima_aset;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' dan harus dikembalikan pada tanggal ' + moment(Obj.ake_tanggal_aset_kembali).format('Do MMMM YYYY') + '.</i>'
                    }
                  } else {
                    if (Obj.asa_id_lokasi) {
                      status_satuan_aset = 'Belum dialokasikan dan masih tersimpan di ' + Obj.asa_nama_lokasi;
                      if (Obj.asa_id_kontainer) {
                        status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_nama_kontainer;
                        if(Obj.asa_id_posisi_kontainer) {
                          status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_label_posisi_kontainer;
                        }
                      } 
                    }
                  }

                  if(Obj.asa_kode_aset_informasi) {
                    var keterangan_satuan_aset = '<a href="/manajemenAset/' + Obj.asa_kode_grup_satuan + '/informasi" class="btn btn-sm btn-primary" style="border: 1px solid #ced6e2;" rel="tooltip" title="Lihat Informasi Satuan Aset #' + Obj.asa_kode_grup_satuan +' "><i class="fa fa-eye"></i></a>';
                  }

                  if (last_kode_grup == 0) {
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + keterangan_satuan_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                    index_col = index_col + 1;
                  } else if (Obj.asa_kode_grup_satuan == last_kode_grup) {
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));

                    if(index == last_index_list)
                    {
                      $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      last_kode_grup = '';                    
                      count_grup_satuan = 0;
                      index = 0;
                    }
                  } else {
                    $("#satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#harga-satuan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#status-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                      $("#keterangan-aset-col-" + targetParent + '-' + targetChild + '-' + last_kode_grup).attr({ 'rowspan': count_grup_satuan, 'style': 'vertical-align : middle;text-align:center;' });
                    last_kode_grup = '';                    
                    count_grup_satuan = 0;
                    count_grup_satuan = count_grup_satuan + 1;
                    var newRowSatuan = '<tr><td id="satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" rowspan="' + count_grup_satuan + '" style="vertical-align: middle; text-align: center;"><i>#' + Obj.asa_kode_grup_satuan + '</i></td><td style="vertical-align: middle; text-align: center;"> ' + Obj.asa_field_aset + '</td><td style="vertical-align: middle; text-align: center;">' + Obj.asa_isian_aset + '</td><td id="harga-satuan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + parseFloat(Obj. asm_harga_satuan ).toFixed(2) + '</td><td id="status-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + status_satuan_aset + '</td><td id="keterangan-aset-col-' + targetParent + '-' + targetChild + '-' + Obj.asa_kode_grup_satuan + '" style="vertical-align: middle; text-align: center;">' + keterangan_satuan_aset + '</td></tr>';
                    $(newRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                    index_col = index_col + 1;
                  }
                  $('#check-satuan-aset-' + targetParent + '-' + targetChild).val(1);
                  last_kode_grup = Obj.asa_kode_grup_satuan;
                });
                $('#check-aset-' + targetParent + '-' + targetChild).val(1);
              } else {
                var emptyRowSatuan = '<tr><td colspan="6"><i>Tidak ada data.</i></td></tr>';
                $(emptyRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                $('#check-aset-' + targetParent + '-' + targetChild).val(0);
              }
           }, error:function(x) {
            alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
            console.log(x);
           }
          });
        } else {
          $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "").fadeIn().delay(500);
        }
      } else {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-exp-' + targetParent + '-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>');
        $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "display: none").fadeOut().delay(500);
      }
      $('#tr-satuan-aset-' + targetParent + '-' + targetChild).slideToggle(); 
    }); 
  });
</script>

@endsection