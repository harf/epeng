<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="/manajemenAset">Persediaan</a></li>
        <li class="breadcrumb-item active" aria-current="page"><u>Input Stok Awal</u></li>
    </ol>
</nav>

<div class="row">
    <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
    </div>

    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <form method="POST" action="{{ route('simpanPersediaan') }}" enctype="multipart/form-data">
            <div class="card card-profile">
                <div class="card-header card-header-primary">
                    <h4 class="card-title"><strong>Tambah Input Stok Awal</strong></h4>
                </div>

                <div class="card-body" align="left">
                </div>
            </div>
            
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Nama Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" name="nama_pengadaan">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Kategori Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
               <select class="form-control input-sm" name="id_kategori_pengadaan" id="id_kategori_pengadaan">
                  <option value=""  selected="true">Pilih Kategori Pengadaan</option>
                  
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Nomor Pengadaan
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="nomor_pengadaan">
              </div>
            </div>
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Tahun Pengadaan
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <select class="form-control" name="tahun_pengadaan">
                  <option value=""  selected="true">Pilih Tahun Pengadaan</option>
                  
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Nilai Anggaran
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="nilai_anggaran">
              </div>
            </div>
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Kode Anggaran
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="kode_anggaran">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Nama PPK
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" name="nama_ppk">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Tanggal Kontrak
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="date" name="tanggal_mulai_kontrak" id="tanggal_mulai_kontrak" class="form-control">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1">
              s.d.
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="date" name="tanggal_selesai_kontrak" id="tanggal_selesai_kontrak" name="tanggal_selesai_kontrak" class="form-control">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Penyedia
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <select class="form-control" name="id_penyedia">
                <option value=""  selected="true">Pilih Pihak Penyedia</option>
                
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Keterangan Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <textarea class="form-control" name="keterangan_pengadaan" rows="3"></textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                <div class="row">
                  <strong>&emsp;&emsp; Rincian Item Pengadaan &emsp;</strong>
                  <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-item" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
                </div>
                <input type="hidden" id="count-item" name="count_item" value="0" />
                <input type="hidden" id="last-item-id" value="0" />
                <input type="hidden" id="index-item" name="index_item" value="" />
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
              </div>
              <div class="col-md-3 col-sm-3 col-xs-3 text-secondary">
                <i>Tidak ada item yang dicari?<br><a href="#"><u>Klik disini</u></a> untuk menambahkan item baru.</i>
              </div>
            </div>
            <br>
            <div id="rincian-item-pengadaan">
              
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                &emsp;Dokumen Pengadaan &emsp;
                <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-dokumen" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
              </div>
              <input type="hidden" id="count-dokumen" name="count_dokumen" value="0" />
              <input type="hidden" id="last-dokumen-id" value="0" />
            </div>
            <div id="dokumen-pengadaan">

            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row pull-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-check-circle"></i>&emsp;Simpan Paket Pengadaan</button>
          </div>
        </div>
      </div>
      {{ csrf_field() }}
    </form>
  </div>
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
</div>

@endsection