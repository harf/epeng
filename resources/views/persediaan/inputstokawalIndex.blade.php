<script src="/js/core/jquery.min.js" type="text/javascript"></script>

<?php
  use Carbon\Carbon;
  \Carbon\Carbon::setLocale('id');
  setlocale(LC_TIME, 'id_ID');
?>

@extends('layouts.master')
@section('content')
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <div class="row">
            <div class="col-md-6">
              <h3 class="card-title text-primary">
                <small>
                  <strong>
                    <i class="fa fa-clipboard"></i>&emsp;Paket Pengadaan
                  </strong>
                </small>
              </h3>
            </div>
            <div class="col-md-6">
              <a href="{{ route('pengadaanAdd') }}" class="card-title btn btn-primary pull-right">Tambah Pengadaaan</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
          <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-10">
              <i><small>Halaman {{ $page_number }}/{{ceil($count_list_pengadaan/$page_size)}} - menampilkan @if($is_filtered == 0 && $is_searched == 0) seluruh Pengadaan @else @if($is_filtered != 0) <u>{{ ($filter1_name)?$filter1_name:'seluruh pengadaan' }} ({{ ($filter2_name)?$filter2_name:'Seluruh Waktu' }})</u> @endif @if($is_searched != 0) @if($is_filtered != 0) > @endif pengadaan yang memiliki keyword "<u>{{ $keyword }}</u>" @endif @endif sebanyak @if($list_pengadaan) <strong>{{ count($list_pengadaan) }}/{{ ($count_list_pengadaan)?$count_list_pengadaan:0 }} data</strong>@else<strong>0 data</strong>@endif.</small></i>
            </div>
          </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
          <div class="row">
            <div class="col-lg-2 col-md-2 col-sm-2">
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-filter"></i>&emsp;</span>
                </div>
                <select class="form-control input-sm" name="id_kategori_pengadaan" id="id-kategori-pengadaan" @if($filter1 != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" @endif>
                  <option value="0"  selected="true">Semua Kategori Pengadaan</option>
                  @if($list_kategori_pengadaan)
                    @foreach ($list_kategori_pengadaan as $kat_peng)
                      @if($kat_peng->id_kategori_pengadaan == $filter1)
                        <option value="{{ $kat_peng->id_kategori_pengadaan }}" selected="true">
                          {{ $kat_peng->nama_kategori_pengadaan }}
                        </option>
                      @else
                        <option value="{{ $kat_peng->id_kategori_pengadaan }}">
                          {{ $kat_peng->nama_kategori_pengadaan }}
                        </option>
                      @endif
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <div class="input-group input-group-sm mb-3">
                <select class="form-control input-sm" name="tahun_pengadaan" id="tahun-pengadaan" @if($filter2 != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" @endif>
                  <option value="0"  selected="true">Seluruh Waktu</option>
                  @if($list_tahun_pengadaan)
                    @foreach ($list_tahun_pengadaan as $tahun_peng)
                      @if($tahun_peng == $filter2)
                        <option value="{{ $tahun_peng }}" selected="true">
                          {{ $tahun_peng }}
                        </option>
                      @else
                        <option value="{{ $tahun_peng }}">
                          {{ $tahun_peng }}
                        </option>
                      @endif
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-search"></i>&emsp;</span>
                </div>
                <input type="text" class="form-control" id="keyword-search" aria-label="Small" aria-describedby="inputGroup-sizing-sm" placeholder="Enter untuk mencari data" @if($is_searched != 0) style="border: 1px solid #2349a0; border-radius: 5px; background-color: #fffddd;" value="{{ $keyword }}" @endif>
              </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2">
              <div class="input-group input-group-sm mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm"><i class="fa fa-eye"></i>&emsp;</span>
                </div>
                <input type="hidden" id="page-number" value="{{ $page_number }}">
                <select class="form-control input-sm" name="page_size" id="page-size">
                  <option value="15"  selected="true">15</option>
                  <option value="30" @if($page_size == 30) selected="true" @endif>30</option>
                  <option value="60" @if($page_size == 60) selected="true" @endif>60</option>
                  <option value="120" @if($page_size == 120) selected="true" @endif>120</option>
                  <option value="240" @if($page_size == 240) selected="true" @endif>240</option>
                  <option value="480" @if($page_size == 480) selected="true" @endif>240</option>
                  <option value="960" @if($page_size == 960) selected="true" @endif>960</option>
                </select>
                <div class="input-group-prepend">
                  <span class="input-group-text text-primary" id="inputGroup-sizing-sm">&emsp;data</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <br>
  @if($list_pengadaan)
    @foreach($list_pengadaan as $key => $value)
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="card card-stats">
            <div class="card-header card-header-icon">
              <div class="card-icon card-icon-2">
                <strong class="text"><small style="font-size: 15px;">#</small><small style="font-size: 28px;">{{ $value->kode_pengadaan }}</small><br><small style="font-size: 17px; margin-left: -3px;"><span class="badge badge-pill badge-light" style="margin-top: 20px; border: 1px solid;"><strong>{{ $value->tahun_pengadaan }}</strong></span></small></strong>
              </div>
              <h4 class="card-title text-primary" align="left"><strong>{{ $value->nama_pengadaan }}</strong></h4>
              <p class="card-category" align="left">Nomor Pengadaan: {{ $value->nomor_pengadaan }}</p>
              <hr>
              <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" align="center" style="text-align: center;">
                <a href="#" class="showmore btn-link">Lihat deskripsi singkat paket pengadaan...</a>
              </div>
              <div class="row" align="left" style="display: none;">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                      Kode Anggaran
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                      {{ $value->kode_anggaran }}
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                      Nilai Anggaran
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                      Rp {{ $value->nilai_anggaran }}
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                      Nama PPK
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                      {{ $value->nama_ppk }}
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                      Penyedia
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                      {{ $value->nama_penyedia }}
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                      Kategori Pengadaan
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                      {{ $value->nama_kategori_pengadaan }}
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                      Tanggal Kontrak
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary">
                      {{ strftime( "%d %B %Y", strtotime($value->tanggal_mulai_kontrak)) }} - {{ strftime( "%d %B %Y", strtotime($value->tanggal_selesai_kontrak)) }}
                    </div>
                  </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                  <div class="form-group row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-gray">
                      Keterangan Pengadaan
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-primary more">
                      {{ $value->keterangan_pengadaan }}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="stats">
                  <i>terakhir diubah tanggal {{ strftime( "%d %B %Y pukul %H:%M:%S", strtotime(($value->updated_at)?$value->updated_at:$value->created_at)) }}</i>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">           
                <button type="button" rel="tooltip" title="Hapus Paket Pengadaan" class="open-modalHapusPengadaan btn btn-danger btn-link btn-sm pull-right" data-toggle="modal" data-target=".modalHapusPengadaan" data-backdrop="static" data-keyboard="false" data-pengadaan_kode_pengadaan="{{ $value->kode_pengadaan }}" data-pengadaan_nama_pengadaan="{{ $value->nama_pengadaan }}" data-pengadaan_tahun_pengadaan="{{ $value->tahun_pengadaan }}">
                  <i class="fa fa-trash"></i>
                </button>
                <button type="button" rel="tooltip" title="Ubah Paket Pengadaan" class="btn btn-info btn-link btn-sm pull-right" onclick="location.href='pengadaan/{{ $value->kode_pengadaan }}/ubah'">
                  <i class="fa fa-gear"></i>
                </button>            
                <button type="button" rel="tooltip" title="Lihat Lebih Rinci Paket Pengadaan" class="btn btn-success btn-link btn-sm pull-right" onclick="location.href='pengadaan/{{ $value->kode_pengadaan }}'">
                  <i class="fa fa-eye"></i>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br>
      <br>
    @endforeach
  @else
    <div class="col-lg-12 col-md-12 col-sm-12"><center><i>Tidak ada data.</i></center></div>
  @endif
  <br>
  <hr>
  <div class="row">
    <div class="pull-right">
      <nav aria-label="...">
        <ul class="pagination">
          @if($page_number == 1)
            <li class="page-item disabled">
              <span class="page-link">Sebelumnya</span>
            </li>
          @else
            <li class="page-item">
              <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter1={{ $filter1 }}&filter2={{ $filter2 }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number-1 }}">Sebelumnya</a>
            </li>  
          @endif
          @for($i = 0; $i < $count_list_pengadaan/$page_size; $i++)
            <li class="page-item @if($page_number == $i+1) active @endif">
              @if($page_number == $i+1)
                <span class="page-link">
                  {{ $i+1 }}
                  <span class="sr-only">(current)</span>
                </span>
              @else
                <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter1={{ $filter1 }}&filter2={{ $filter2 }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $i+1 }}">{{ $i+1 }}</a>
              @endif
            </li>
          @endfor
          @if($page_number == ceil($count_list_pengadaan/$page_size))
            <li class="page-item disabled">
              <span class="page-link">Selanjutnya</span>
            </li>
          @else
            <li class="page-item">
              <a class="page-link" href="?is_filtered={{ $is_filtered }}&is_searched={{ $is_searched }}&filter1={{ $filter1 }}&filter2={{ $filter2 }}&keyword={{ $keyword }}&page_size={{ $page_size }}&page_number={{ $page_number+1 }}">Selanjutnya</a>
            </li> 
          @endif
        </ul>
      </nav>
    </div>
  </div>

<!-- POPUP HAPUS PENGADAAN -->
<div class="modal fade modalHapusPengadaan" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><strong class="text-align">HAPUS PAKET PENGADAAN</strong></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin akan menghapus <i class="text-primary" id="hapus_pengadaan_nama_pengadaan"></i> Tahun <i class="text-primary" id="hapus_pengadaan_tahun_pengadaan"></i>?
      </div>
      <div class="modal-footer">
        <form action="/pengadaanDelete" method="post" enctype="multipart/form-data">    
          <input type="hidden" name="kode_pengadaan" id="hapus_pengadaan_kode_pengadaan"  />
          <input type="hidden" name="_method" value="POST">
          <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Tidak</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-trash"></i>&emsp;Ya, Hapus</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $("select").select2();
  });
  $(document).ready(function(){
    var showChar = 105;
    var ellipsestext = "...";
    var moretext = "lebih rinci";
    var lesstext = "lebih ringkas";
    $('.more').each(function() {
      var content = $(this).html();
      var textcontent = $(this).text();

      if (textcontent.length > showChar) {

        var c = textcontent.substr(0, showChar);

        var html = '<span class="container" style="padding-left: 0px;"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';
        $(this).html(html);
        $(this).after('<a href="" class="morelink badge badge-dark" align="right">' + moretext + '</a>');
      }

    });

    $(".morelink").click(function() {
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
        $(this).prev().children('.morecontent').fadeToggle(500, function(){
          $(this).prev().fadeToggle(500);
        });
         
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
        $(this).prev().children('.container').fadeToggle(500, function(){
          $(this).next().fadeToggle(500);
        });
      }
      return false;
    });

    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function() {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);
    });

    $("a.showmore").click(function(e) {
      e.preventDefault();
      $(this).parent().next().slideToggle("fast");
      $(this).css("display", "none");
    });

    $(document).on("click", ".open-modalHapusPengadaan", function () {
      var hapus_pengadaan_kode_pengadaan = $(this).data('pengadaan_kode_pengadaan');
      var hapus_pengadaan_nama_pengadaan = $(this).data('pengadaan_nama_pengadaan');
      var hapus_pengadaan_tahun_pengadaan = $(this).data('pengadaan_tahun_pengadaan');
      
      $(".modal-footer #hapus_pengadaan_kode_pengadaan").val(hapus_pengadaan_kode_pengadaan);
      $(".modal-body #hapus_pengadaan_nama_pengadaan").html(hapus_pengadaan_nama_pengadaan);
      $(".modal-body #hapus_pengadaan_tahun_pengadaan").html(hapus_pengadaan_tahun_pengadaan);
    });

    $(document).on("change", "#id-kategori-pengadaan", function(e) {
      e.preventDefault();
      var targetId = e.target.value;

      var keyword = $("#keyword-search").val();
      var is_searched = 0;
      if (keyword == '') {
        keyword = "null";
        is_searched = 0;
      } else {
        is_searched = 1;
      }

      var tahun_pengadaan = $("#tahun-pengadaan").val();
      var page_size = $("#page-size").val();
      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      if (targetId != 0) window.location.href = baseUrl + "?is_filtered=1&is_searched=" + is_searched + "&filter1=" + targetId +  "&filter2=" + tahun_pengadaan + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=1";
      else window.location.href = baseUrl;
    });

    $(document).on("change", "#tahun-pengadaan", function(e) {
      e.preventDefault();
      var targetId = e.target.value;

      var keyword = $("#keyword-search").val();
      var is_searched = 0;
      if (keyword == '') {
        keyword = "null";
        is_searched = 0;
      } else {
        is_searched = 1;
      }

      var id_kategori_pengadaan = $("#id-kategori-pengadaan").val();
      var page_size = $("#page-size").val();
      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      if (targetId != 0) window.location.href = baseUrl + "?is_filtered=1&is_searched=" + is_searched + "&filter1=" + id_kategori_pengadaan +  "&filter2=" + targetId + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=1";
      else window.location.href = baseUrl;
    });

    $(document).on("keypress", "#keyword-search", function(e) {
      if(e.which === 13){
        e.preventDefault();
        var keyword = $("#keyword-search").val();
        var filter1 = $("#id-kategori-pengadaan").val();
        var filter2 = $("#tahun-pengadaan").val();

        var is_filtered = 0;
        if (filter1 == 0 || filter2 == 0) {
          is_filtered = 0;
        } else {
          is_filtered = 1;
        }

        var page_size = $("#page-size").val();
        var page_number = $("#page-number").val();

        var getUrl = window.location;
        var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
        
        if (keyword.replace(" ", "") != '') window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=1&filter1=" + filter1 + "&filter2=" + filter2 + "&keyword=" + keyword + "&page_size=" + page_size + "&page_number=" + page_number;
        else if (is_filtered != 0) window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=0&filter1=" + filter1 + "&filter2=" + filter2 + "&keyword=null" + "&page_size=" + page_size + "&page_number=1";
        else window.location.href = baseUrl;
      }
    });

    $(document).on("change", "#page-size", function(e) {
      e.preventDefault();
      var targetId = e.target.value;
      var filter1 = $("#id-kategori-pengadaan").val();
      var filter2 = $("#tahun-pengadaan").val();
      var keyword = $("#keyword-search").val();

      var is_filtered = 0;
      if (filter1 == 0 || filter2 == 0) is_filtered = 0;
      else is_filtered = 1;

      var is_searched = 0;
      if (keyword == '') {
        keyword = "null";
        is_searched = 0;
      } else {
        is_searched = 1;
      }

      var page_number = $("#page-number").val();

      var getUrl = window.location;
      var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1];
      
      window.location.href = baseUrl + "?is_filtered=" + is_filtered + "&is_searched=" + is_searched + "&filter1=" + filter1 + "&filter2=" + filter2 + "&keyword=" + keyword + "&page_size=" + targetId + "&page_number=1";
    });
  });
</script>
@endsection