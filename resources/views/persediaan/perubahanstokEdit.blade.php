<script src="/js/core/jquery.min.js" type="text/javascript"></script>

@extends('layouts.master')

@section('content')
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/pengadaan">Paket Pengadaan</a></li>
    <li class="breadcrumb-item active" aria-current="page"><u>Ubah</u></li>
  </ol>
</nav>
<div class="row">
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
  <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
    <form method="POST" action="{{ route('simpanPengadaan') }}" enctype="multipart/form-data">
      <div class="card card-profile">
        <div class="card-header card-header-primary">
          <h4 class="card-title"><strong>Ubah Paket Pengadaan</strong></h4>
        </div>
        <div class="card-body" align="left">
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Nama Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" name="nama_pengadaan" value="{{ $pengadaan->nama_pengadaan }}">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Kategori Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
               <select class="form-control input-sm" name="id_kategori_pengadaan" id="id_kategori_pengadaan">
                  <option value="">Pilih Kategori Pengadaan</option>
                  @foreach ($list_kategori_pengadaan as $kategori)
                    @if($kategori->id_kategori_pengadaan == $pengadaan->id_kategori_pengadaan)
                      <option value="{{ $kategori->id_kategori_pengadaan }}" selected="true">{{ $kategori->nama_kategori_pengadaan }}</option>
                    @else
                      <option value="{{ $kategori->id_kategori_pengadaan }}">{{ $kategori->nama_kategori_pengadaan }}</option>
                    @endif
                  @endforeach
              </select>
            </div>
          </div>
          <div class="row">
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Nomor Pengadaan
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="nomor_pengadaan" value="{{ $kategori->nomor_pengadaan }}">
              </div>
            </div>
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Tahun Pengadaan
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <select class="form-control" name="tahun_pengadaan">
                  <option value="">Pilih Tahun Pengadaan</option>
                  @foreach ($list_tahun_pengadaan as $tahun)
                    @if($tahun == $pengadaan->tahun_pengadaan)
                      <option value="{{ $tahun }}" selected="true">{{ $tahun }}</option>
                    @else
                      <option value="{{ $tahun }}">{{ $tahun }}</option>
                    @endif
                  @endforeach
                </select>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Nilai Anggaran
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="nilai_anggaran" value="{{ $pengadaan->nilai_anggaran }}">
              </div>
            </div>
            <div class="form-group row col-md-6 col-sm-6 col-xs-6">
              <div class="col-md-6 col-sm-6 col-xs-6">
                Kode Anggaran
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <input type="text" class="form-control" name="kode_anggaran" value="{{ $pengadaan->kode_anggaran }}">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Nama PPK
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <input type="text" class="form-control" name="nama_ppk" value="{{ $pengadaan->nama_ppk }}">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Tanggal Kontrak
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="date" name="tanggal_mulai_kontrak" id="tanggal_mulai_kontrak" class="form-control" value="{{ $pengadaan->tanggal_mulai_kontrak }}">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-1">
              s.d.
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
              <div class="input-group">
                <input type="date" name="tanggal_selesai_kontrak" id="tanggal_selesai_kontrak" name="tanggal_selesai_kontrak" class="form-control" value="{{ $pengadaan->tanggal_selesai_kontrak }}">
                <div class="input-group-append">
                  <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Penyedia
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <select class="form-control" name="id_penyedia">
                <option value="">Pilih Pihak Penyedia</option>
                @foreach ($list_penyedia as $penyedia)
                  @if($penyedia->id_penyedia == $pengadaan->id_penyedia)
                    <option value="{{ $penyedia->id_penyedia }}" selected="true">{{ $penyedia->nama_penyedia }}</option>
                  @else
                    <option value="{{ $penyedia->id_penyedia }}">{{ $penyedia->nama_penyedia }}</option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-md-3 col-sm-3 col-xs-3">
              Keterangan Pengadaan
            </div>
            <div class="col-md-9 col-sm-9 col-xs-9">
              <textarea class="form-control" name="keterangan_pengadaan" rows="3">{{ $pengadaan->keterangan_pengadaan }}</textarea>
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div>
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-3">
                &emsp;Dokumen Pengadaan &emsp;
                <button type="button" class="btn btn-sm btn-link btn-success btn-tambah-dokumen" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
              </div>
              <input type="hidden" id="count-dokumen" name="count_dokumen" value="0" />
              <input type="hidden" id="last-dokumen-id" value="0" />
            </div>
            <div id="dokumen-pengadaan">
              @foreach($list_dokumentasi_pengadaan as $dokumentasi)
                <div id="dokumen-{{ $dokumentasi->id_dokumentasi }}" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;">
                  <div class="form-group row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                      <div class="row">
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <button type="button" class="btn btn-sm btn-link btn-danger" id="btn-hapus-dokumen-{{ $dokumentasi->id_dokumentasi }}" style="margin-right: 3px; padding: 0px;">
                            <i class="fa fa-trash"></i>
                          </button>
                        </div>
                        <div class="col-md-10 col-sm-10 col-xs-10">
                          <input type="text" class="form-control" placeholder="Nama dokumen" name="nama_dokumentasi~{{ $dokumentasi->id_dokumentasi }}" value="{{ $dokumentasi->nama_dokumentasi }}">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-5 col-sm-5 col-xs-5">
                      <div class="input-group">
                        <input type="text" class="form-control" id="url-dokumen-aset-tambah-{{ $dokumentasi->id_dokumentasi }}" readonly="" style="background-color: white;" placeholder="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB" value="{{ $dokumentasi->link_dokumentasi }}">
                        <div class="input-group-append">
                          <div class="btn btn-primary btn-sm">
                            <span><i class="fa fa-upload"></i></span>
                            <input type="file" id="file-dokumen-aset-tambah-{{ $dokumentasi->id_dokumentasi }}" rel="tooltip" data-original-title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." class="upload" name="id_dokumentasi~{{ $dokumentasi->id_dokumentasi }}" />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-4">
                      <div class="input-group">
                        <input type="date" name="tanggal_dokumentasi~{{ $dokumentasi->id_dokumentasi }}" class="form-control" placeholder="Tanggal Dokumentasi" rel="tooltip" title="Tanggal Dokumentasi" data-original-title="Tanggal Dokumentasi" value="{{ $dokumentasi->tanggal_dokumentasi }}">
                        <div class="input-group-append">
                          <div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-md-11 col-sm-11 col-xs-11">
                      <input type="text" class="form-control" placeholder="Keterangan dokumen" name="keterangan_dokumentasi~{{ $dokumentasi->id_dokumentasi }}" value="{{ $dokumentasi->keterangan_dokumentasi }}">
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-1 col-sm-1 col-xs-1"></div>
                    <div class="col-md-11 col-sm-11 col-xs-11">
                      <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                          <div class="radio">
                            <label class="radio-inline">Apakah ada dokumen secara fisik?</label>&emsp;<br>
                            <label class="radio-inline">
                              <input type="radio" class="dokumentasi-fisik-{{ $dokumentasi->id_dokumentasi }}" name="is_document_stored{{ $dokumentasi->id_dokumentasi }}" value="Y"
                              @if($dokumentasi->id_lokasi)
                                checked="checked"
                              @endif
                              > Ya 
                            </label>&emsp;
                            <label class="radio-inline">
                              <input type="radio" class="dokumentasi-fisik-{{ $dokumentasi->id_dokumentasi }}" name="is_document_stored{{ $dokumentasi->id_dokumentasi }}" value="N"> Tidak 
                            </label>
                          </div>
                        </div>
                        <div id="form-penyimpanan-dokumentasi-{{ $dokumentasi->id_dokumentasi }}" class="col-md-8 col-sm-8 col-xs-8">
                          Disimpan di
                          <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-4">
                              <select class="form-control" name="dokumentasi~lokasi~{{ $dokumentasi->id_dokumentasi }}" id="tambah-lokasi-dropdown-{{ $dokumentasi->id_dokumentasi }}">
                                <option value=""  selected="true">Pilih Lokasi</option>
                                @foreach ($list_lokasi as $lokasi)
                                  @if($lokasi->id_lokasi == $dokumentasi->id_lokasi)
                                    <option value="{{ $lokasi->id_lokasi }}" selected="true">{{ $lokasi->nama_lokasi }}</option>
                                  @else
                                    <option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>
                                  @endif
                                @endforeach
                              </select>
                            </div>
                            <div class="col-md-4 col-sm-4  col-xs-4">
                              <select class="form-control" name="dokumentasi~kontainer~{{ $dokumentasi->id_dokumentasi }}" id="tambah-kontainer-dropdown-{{ $dokumentasi->id_dokumentasi }}">
                                <option value="" selected="true">Pilih kontainer...</option>
                                @if ($dokumentasi->id_kontainer)
                                  <option value="{{ $dokumentasi->id_kontainer }}" selected="true">{{ $dokumentasi->nama_kontainer }}</option>
                                @endif
                              </select>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4">
                              <select class="form-control" name="dokumentasi~posisi_kontainer~{{ $dokumentasi->id_dokumentasi }}" id="tambah-posisi-kontainer-dropdown-{{ $dokumentasi->id_dokumentasi }}">
                                <option value="" selected="true">Pilih posisi kontainer...</option>
                                @if($dokumentasi->id_posisi_kontainer)
                                  <option value="{{ $dokumentasi->id_posisi_kontainer }}" selected="true">{{ $dokumentasi->label_posisi_kontainer }}</option>
                                @endif
                              </select>
                            </div>
                          </div>
                        </div> 
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h4 class="card-title"><strong>Rincian Item Pengadaan</strong></h4>
          </div>
          <br>
          <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <table class="table table-bordered table-sm" id="table-item-pengadaan">
              <colgroup>
                <col style="width:3%;">
                <col style="width:5%;">
                <col style="width:20%;">
                <col style="width:30%;">
                <col style="width:20%;">
                <col style="width:15%;">
              </colgroup>
              <thead class="thead-light">
                <tr>
                  <th></th>
                  <th><strong>NO.</strong></th>
                  <th colspan="2"><strong>ITEM Pengadaan</strong></th>
                  <th><strong>JENIS ASET</strong></th>
                  <th><strong>KATEGORI ASET </strong></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($list_item_pengadaan as $key => $value)
                  <tr>
                    <td id="aset-expandable-td_{{ $value->kode_pengadaan }}-{{ $value->id_aset }}" style="cursor: pointer;">
                      <button type="button" id="btn-aset-exp-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>
                    </td>
                    <td>{{ ($key + 1) }}.<input type="hidden" id="check-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}" value="0"></td>
                    <td><img src="{{ str_replace('public', '/storage', $value->link_gambar) }}" alt="{{ $value->nama_gambar }}" class="gambar-aset-thumbnails"></td>
                    <td>{{ $value->nama_merek }} {{ $value->tipe_aset }}</td>
                    <td>{{ $value->nama_jenis_aset }}</td>
                    <td>{{ $value->nama_kategori_aset }}</td>
                  </tr>
                  <tr id="tr-satuan-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}">
                    <td colspan="7">
                      <div class="form-group row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="hidden-satuan-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}">
                        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          Satuan Item Pengadaan:
                        </div>
                        <hr>
                        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12">
                          <table class="table table-bordered table-hover table-sm">
                            <colgroup>
                              <col style="width:7%;">
                              <col style="width:15%;">
                              <col style="width:23%;">
                              <col style="width:15%;">
                              <col style="width:25%;">
                              <col style="width:15%;">
                            </colgroup>
                            <thead class="thead-light">
                              <tr>
                                <th>Satuan #</th>
                                <th>Field Aset</th>
                                <th>Isian Field Aset</th>
                                <th>Harga</th>
                                <th>Status Saat Ini</th>
                                <th>Keterangan Aset</th>
                              </tr>
                            </thead>
                            <tbody id="tbody-satuan-aset-{{ $value->kode_pengadaan }}-{{ $value->id_aset }}">
                             
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="card card-profile">
        <div class="card-body" align="left">
          <div class="form-group row pull-right">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i>&emsp;Batal</button>
            <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i>&emsp;Simpan Perubahan Paket Pengadaan</button>
          </div>
        </div>
      </div>
      {{ csrf_field() }}
    </form>
  </div>
  <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
  </div>
</div>

<script type="text/javascript"> 
  $(document).ready(function(){
      window.onbeforeunload = function () {
        var msg = "Apakah Anda akan meninggalkan halaman ini? Data akan hilang sebelum Anda menyimpannya.";
        return msg;
      };


      $(document).on("keypress", "[type='number']", function (e) {
        e.preventDefault();
      });

      $(document).on("change", "[id^=file-dokumen-pengadaan-tambah-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $("#url-dokumen-pengadaan-tambah-" + targetId).val(this.value.replace(/C:\\fakepath\\/i, ''));
      });

      $(document).on("click", ".showmore", function (e) {
        e.preventDefault();
        $(this).parent().parent().next().slideToggle("fast");
        $(this).css("display", "none");
      });

      $('[id^=form-penyimpanan-dokumentasi-]').hide();
      $(document).on('change', '[class^="dokumentasi-fisik-"]', function (e) {
          var targetClass = $(e.currentTarget).attr('class').split(' ').pop();
          var targetClassId = targetClass.split('-').pop();
          e.preventDefault();
          if (this.value == "Y") { //check value if it is domicilio
              $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).show(750); //than show
          } else {
              $('#form-penyimpanan-dokumentasi-' + targetClassId).stop(true,true).hide(750); //else hide
              $('#tambah-lokasi-dropdown-' + targetClassId).val('');
              $('#tambah-kontainer-dropdown-' + targetClassId).val('');
              $('#tambah-posisi-kontainer-dropdown-' + targetClassId).val('');
          }
      });

      $(document).on("change", "[id^=tambah-lokasi-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        var lokasi = e.target.value;

        if(lokasi){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kontainer-list')}}?lokasi="+lokasi,
             success:function(res){
              if(res){

                  $('#tambah-kontainer-dropdown-' + targetId).empty();
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
                  $('#tambah-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $("#tambah-kontainer-dropdown-" + targetId).append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
                  });

              }else{
                 $("#tambah-kontainer-dropdown-" + targetId).empty();
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
              }
             }
          });
        }
        else{
          $("#tambah-kontainer-dropdown-" + targetId).empty();
          $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
        }

      });

      $(document).on("change", "[id^=tambah-kontainer-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        var kontainer = e.target.value;

        if(kontainer){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
             success:function(res){
              if(res){

                  $('#tambah-posisi-kontainer-dropdown-' + targetId).empty();
                  $('#tambah-posisi-kontainer-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $("#tambah-posisi-kontainer-dropdown-" + targetId).append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
                  });

              }else{
                 $("#tambah-posisi-kontainer-dropdown-" + targetId).empty();
              }
             }
          });
        }
        else{
          $("#tambah-posisi-kontainer-dropdown-" + targetId).empty();
        }
      });

      $(document).on("change", "[id^=tambah-kategori-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var kategori_aset = e.target.value;

        if(kategori_aset){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-jenis-aset-list')}}?kategori_aset="+kategori_aset,
             success:function(res){
              if(res){
                  $('#tambah-jenis-aset-dropdown-' + targetId).empty();
                  $('#tambah-spek-aset-kiri-' + targetId).empty();
                  $('#tambah-spek-aset-kanan-' + targetId).empty();
                  $('#tambah-jenis-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Jenis Aset</option>');
                  $('#tambah-tipe-aset-dropdown-' + targetId).empty();
                  $('#tambah-tipe-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Tipe Aset</option>');
                  $('#div-btn-exp-col-' + targetId).empty();
                  $('#foto-item-' + targetId).empty();

                  $.each(res,function(index,Obj){
                      $("#tambah-jenis-aset-dropdown-" + targetId).append('<option value="'+Obj.id_jenis_aset+'">'+Obj.nama_jenis_aset+'</option>');
                  });

              }else{
                 $("#tambah-jenis-aset-dropdown-" + targetId).empty();
                 $('#tambah-spek-aset-kiri-' + targetId).empty();
                 $('#tambah-spek-aset-kanan-' + targetId).empty();
                 $('#div-btn-exp-col-' + targetId).empty();
                 $('#foto-item-' + targetId).empty();
              }
             }
          });
        }
        else{
          $("#tambah-jenis-aset-dropdown-" + targetId).empty();
          $('#tambah-spek-aset-kiri-' + targetId).empty();
          $('#tambah-spek-aset-kanan-' + targetId).empty();
          $('#div-btn-exp-col-' + targetId).empty();
          $('#foto-item-' + targetId).empty();
        }
      });

      $(document).on("change", "[id^=tambah-jenis-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var jenis_aset = e.target.value;
        var merek_aset =  $('#tambah-merek-aset-dropdown-' + targetId).val();

        if(merek_aset) {
          if(jenis_aset){
            $.ajax({
               type:"GET",
               url:"{{url('api/get-tipe-aset-list')}}?jenis_aset="+jenis_aset+"&merek_aset="+merek_aset,
               success:function(res){
                if(res){
                    $('#tambah-tipe-aset-dropdown-' + targetId).empty();
                    $('#tambah-spek-aset-kiri-' + targetId).empty();
                    $('#tambah-spek-aset-kanan-' + targetId).empty();
                    $('#tambah-tipe-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Tipe Aset</option>');
                    $('#div-btn-exp-col-' + targetId).empty();
                    $('#foto-item-' + targetId).empty();

                    $.each(res,function(index,Obj){
                        $("#tambah-tipe-aset-dropdown-" + targetId).append('<option value="'+Obj.id_aset+'">'+Obj.tipe_aset+'</option>');
                    });

                }else{
                   $("#tambah-tipe-aset-dropdown-" + targetId).empty();
                   $('#tambah-spek-aset-kiri-' + targetId).empty();
                   $('#tambah-spek-aset-kanan-' + targetId).empty();
                   $('#div-btn-exp-col-' + targetId).empty();
                   $('#foto-item-' + targetId).empty();
                }
               }
            });
          }
          else{
            $("#tambah-tipe-aset-dropdown-" + targetId).empty();
            $('#tambah-spek-aset-kiri-' + targetId).empty();
            $('#tambah-spek-aset-kanan-' + targetId).empty();
            $('#div-btn-exp-col-' + targetId).empty();
            $('#foto-item-' + targetId).empty();
          }
        }
      });

      $(document).on("change", "[id^=tambah-merek-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var jenis_aset = $('#tambah-jenis-aset-dropdown-' + targetId).val();
        var merek_aset = e.target.value;

        if(jenis_aset) {
          if(merek_aset){
            $.ajax({
               type:"GET",
               url:"{{url('api/get-tipe-aset-list')}}?jenis_aset="+jenis_aset+"&merek_aset="+merek_aset,
               success:function(res){
                if(res){
                    $('#tambah-tipe-aset-dropdown-' + targetId).empty();
                    $('#tambah-spek-aset-kiri-' + targetId).empty();
                    $('#tambah-spek-aset-kanan-' + targetId).empty();
                    $('#tambah-tipe-aset-dropdown-' + targetId).append('<option value="" disable="true" selected="true">Pilih Tipe Aset</option>');
                    $('#div-btn-exp-col-' + targetId).empty();
                    $('#foto-item-' + targetId).empty();

                    $.each(res,function(index,Obj){
                        $("#tambah-tipe-aset-dropdown-" + targetId).append('<option value="'+Obj.id_aset+'">'+Obj.tipe_aset+'</option>');
                    });

                }else{
                   $("#tambah-tipe-aset-dropdown-" + targetId).empty();
                   $('#tambah-spek-aset-kiri-' + targetId).empty();
                   $('#tambah-spek-aset-kanan-' + targetId).empty();
                   $('#div-btn-exp-col-' + targetId).empty();
                   $('#foto-item-' + targetId).empty();
                }
               }
            });
          }
          else{
            $("#tambah-tipe-aset-dropdown-" + targetId).empty();
            $('#tambah-spek-aset-kiri-' + targetId).empty();
            $('#tambah-spek-aset-kanan-' + targetId).empty();
            $('#div-btn-exp-col-' + targetId).empty();
            $('#foto-item-' + targetId).empty();
          }
        }
      });

      $(document).on("change", "[id^=tambah-tipe-aset-dropdown-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();      
        var jenis_aset = $('#tambah-jenis-aset-dropdown-' + targetId).val();
        var tipe_aset = e.target.value;
        
        $('#tambah-spek-aset-kiri-' + targetId).empty();
        $('#tambah-spek-aset-kanan-' + targetId).empty();
        $('#foto-item-' + targetId).empty();

        if(jenis_aset) {
          if(tipe_aset){
            $.ajax({
               type:"GET",
               url:"{{url('api/get-spesifikasi-aset-list')}}?jenis_aset="+jenis_aset+"&tipe_aset="+tipe_aset,
               success:function(res){
                if(res){
                    var list_field = res.list_field;
                    var list_spesifikasi = res.list_spesifikasi;
                    var list_gambar = res.list_gambar;

                    var length_list_field = list_field.length;

                    var length_list_field = res.list_field.length;
                    var length_list_kiri = Math.round(length_list_field/2);
                    for (var i = 0; i < length_list_kiri; i++)
                    {
                      var idx = i + 1;
                      if (list_spesifikasi[i].isian_field){
                        $('#tambah-spek-aset-kiri-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[i].nama_field_spek +'</div><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[i].nama_field_spek +'" value="'+ list_spesifikasi[i].isian_field +'" readonly="readonly" disabled="disabled" style="background-color: white;"></div></div>');
                      } else {
                        $('#tambah-spek-aset-kiri-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">'+ list_field[i].nama_field_spek +'</div><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[i].nama_field_spek +'"></div></div>');
                      }
                    }

                    var length_list_kanan = length_list_field - length_list_kiri;
                    for (var j = length_list_kiri; j < length_list_field; j++)
                    {
                      var idx = j + 1;
                      if (list_spesifikasi[j].isian_field){
                        $('#tambah-spek-aset-kanan-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[j].nama_field_spek +'</div><div class="col-md-7 col-sm-7 col-xs-7"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[j].nama_field_spek +'" value="'+ list_spesifikasi[j].isian_field +'" readonly="readonly" disabled="disabled" style="background-color: white;"></div></div>');
                      } else {
                        $('#tambah-spek-aset-kanan-' + targetId).append('<div class="row"><div class="col-md-5 col-sm-5 col-xs-5">' + idx + '. ' + list_field[j].nama_field_spek +'</div><div class="col-md-7 col-sm-7 col-xs-7"><input type="text" class="form-control" placeholder="Masukkan '+ list_field[j].nama_field_spek +'"></div></div>');
                      }
                    }

                    $('#foto-item-' + targetId).empty();
                    if (list_gambar.length != 0) {
                      for (var k = 0; k < list_gambar.length; k++) {
                        $('#foto-item-' + targetId).append('<div class="col-md-3 gambar-aset-thumbnails-div" style="margin-bottom: 3px;"><img src="' + list_gambar[k].link_gambar.replace('public', '/storage') +'" alt="' + list_gambar[k].nama_gambar + '" class="gambar-aset-thumbnails"></div>');
                      }
                    }

                    $('#div-btn-exp-col-' + targetId).empty();
                    $('#div-btn-exp-col-' + targetId).append('<button title="Lihat lebih rinci Spesifikasi Item" type="button" class="btn btn-sm btn-default" id="btn-expand-collapse-spek-' + targetId + '" style="border-radius: 0px;"><i class="fa fa-arrows-v"></i></button>');
                }else{
                   $('#tambah-spek-aset-kiri-' + targetId).empty();
                   $('#tambah-spek-aset-kanan-' + targetId).empty();
                   $('#foto-item-' + targetId).empty();
                }
               }
            });
          }
          else{
            $('#tambah-spek-aset-kiri-' + targetId).empty();
            $('#tambah-spek-aset-kanan-' + targetId).empty();
            $('#foto-item-' + targetId).empty();
          }
        }
      });

      $("[id^=div-spesifikasi-item-]").hide();
      $(document).on("click", "[id^=btn-expand-collapse-spek-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $('#div-spesifikasi-item-' + targetId).slideToggle();                
      });

      $(document).on("click", ".btn-tambah-item", function (e) {
        e.preventDefault();
        var current = $("#last-item-id").val();
        var next = parseInt(current) + 1;
        var count_item = $("#count-item").val();

        $('#rincian-item-pengadaan').append('<div id="item-pengadaan-' + next + '" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="form-group row"><div class="col-md-3 col-sm-3 col-xs-3"><div class="row"><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-sm btn-link btn-danger" id="btn-hapus-item-' + next + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div><div class="col-md-10 col-sm-10 col-xs-10"><i>Item</i> Pengadaan #' + parseInt(next) + '</div></div></div></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11 div-shadow-hover" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="row col-md-12 col-sm-12 col-xs-12">•&emsp;<u class="text-primary">Spesifikasi Item Pengadaan</u></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="kategori_aset~' + next + '" id="tambah-kategori-aset-dropdown-' + next + '"><option value="" selected>Pilih kategori aset...</option>@foreach ($list_kategori_aset as $kategori_aset)<option value="{{ $kategori_aset->id_kategori_aset }}">{{ $kategori_aset->nama_kategori_aset }}</option>@endforeach</select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="jenis_aset~' + next + '" id="tambah-jenis-aset-dropdown-' + next + '"><option value="" selected>Pilih jenis aset...</option></select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="merek_aset~' + next + '" id="tambah-merek-aset-dropdown-' + next + '"><option value="" selected>Pilih merek aset...</option>@foreach ($list_merek_aset as $merek_aset)<option value="{{ $merek_aset->id_merek }}">{{ $merek_aset->nama_merek }}</option>@endforeach</select></div><div class="col-md-5 col-sm-5 col-xs-5"><select class="form-control" name="tipe_aset~' + next + '" id="tambah-tipe-aset-dropdown-' + next + '"><option value="" selected>Pilih tipe aset...</option></select></div><div class="col-md-1 col-sm-1 col-xs-1" id="div-btn-exp-col-' + next + '"></div></div><br><div class="row" id="div-spesifikasi-item-' + next + '"><div class="row col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;"><div class="row col-md-12 col-sm-12 col-xs-12" id="foto-item-' + next + '"></div></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-6 col-sm-6 col-xs-6" id="tambah-spek-aset-kiri-' + next + '"></div><div class="col-md-6 col-sm-6 col-xs-6" id="tambah-spek-aset-kanan-' + next + '"></div></div></div></div></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11 div-shadow-hover" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="row col-md-12 col-sm-12 col-xs-12">•&emsp;<u class="text-primary">Satuan Item Pengadaan</u></div><div class="row col-md-12 col-sm-12 col-xs-12"><div class="col-md-6 col-sm-6 col-xs-6"><input type="text" class="form-control" placeholder="Harga satuan aset (Rupiah)" name="harga_satuan~' + next + '"></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="number" min="1" class="form-control" placeholder="Jumlah" name="jumlah_stok~' + next + '" id="jumlah-stok-' + next + '" value="1"><input type="hidden" id="last-jumlah-stok-' + next + '" value="1" /></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="text" class="form-control" placeholder="Satuan" name="satuan_stok~' + next + '"></div></div><br><div style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12""><i>Rincian Satuan:</i><input type="hidden" name="count_satuan~' + next + '" id="count-satuan-' + next + '" value="1" /><input type="hidden" name="index_satuan~' + next + '" id="index-satuan-' + next + '" value="1~" /></div><br><div id="div-satuan-item-' + next + '"><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="satuan-item-' + next + '-1"><div class="col-md-1 col-sm-1 col-xs-1"><i>Satuan 1</i></div><div class="col-md-2 col-sm-2 col-xs-2"><input type="text" class="form-control" placeholder="Field Satuan" id="field-aset-' + next + '-1" name="field_aset~' + next + '-1"></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="text" class="form-control" placeholder="Isian dari Field Satuan" id="isian-aset-' + next + '-1" name="isian_aset~' + next + '-1"></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="satuan~lokasi~' + next + '-1" id="tambah-satuan-lokasi-dropdown_' + next + '-1"><option value="" selected="true">Disimpan di? Pilih lokasi </option>@foreach ($list_lokasi as $lokasi)<option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>@endforeach</select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="satuan~kontainer~' + next + '-1" id="tambah-satuan-kontainer-dropdown_' + next + '-1"><option value="" selected="true">Pilih kontainer...</option></select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="satuan~posisi_kontainer~' + (next) + '-1" id="tambah-satuan-posisi-kontainer-dropdown_' + (next) + '-1"><option value="" selected>Pilih posisi kontainer...</option></select></div></div></div><hr><div><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-md-8 col-sm-8 col-xs-8"></div><div class="col-md-4 col-sm-4 col-xs-4"><a href="#" class="showmore btn-link text-default" align="right"><i><small>Klik untuk melihat bagaimana cara mengisi satuan item...</small></i></a></div></div><div style="display: none;"><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12"><span><i><small><u>Cara mengisi Rincian Satuan Item</u></small></i></span></div><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12""><div class="col-md-3 col-sm-3 col-xs-3"><small>Satuan #x.</small></div><div class="col-md-3 col-sm-3 col-xs-3"><small>((... <i>Field</i> Satuan ...))</small></div><div class="col-md-6 col-sm-6 col-xs-6"><small>((... Isian dari <i>Field</i> Satuan ...))</small></div></div><div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12"><div class="col-md-3 col-sm-3 col-xs-3"><strong class="text-primary"><small>Satuan #x.</small></strong></div><div class="col-md-3 col-sm-3 col-xs-3"><strong class="text-primary"><small>Nomor BMN</small></strong></div><div class="col-md-6 col-sm-6 col-xs-6"><strong class="text-primary"><small>013.06.0199.409272.000.KP 2017</small></strong></div></div></div></div></div></div>');

        var current_index_item = $("#index-item").val();
        $("#index-item").val(current_index_item + next + "~");
        $("#count-item").val(parseInt(count_item)+1);
        $("#last-item-id").val(parseInt(next));           
      });

      $(document).on("click", ".btn-tambah-dokumen", function (e) {
        e.preventDefault();
        var current = $("#last-dokumen-id").val();
        var next = parseInt(current) + 1;
        $('#dokumen-pengadaan').append('<div id="dokumen-' + (next) + '" style="border: 1px solid #e6e6e6; border-radius: 10px; padding: 10px; margin-bottom: 10px;"><div class="form-group row"><div class="col-md-3 col-sm-3 col-xs-3"><div class="row"><div class="col-md-2 col-sm-2 col-xs-2"><button type="button" class="btn btn-sm btn-link btn-danger" id="btn-hapus-dokumen-' + next + '" style="margin-right: 3px; padding: 0px;"><i class="fa fa-trash"></i></button></div><div class="col-md-10 col-sm-10 col-xs-10"><input type="text" class="form-control" placeholder="Nama dokumen" name="nama_dokumentasi~' + (next) + '"> </div></div></div><div class="col-md-5 col-sm-5 col-xs-5"><div class="input-group"><input type="text" class="form-control" id="url-dokumen-pengadaan-tambah-' + (next) + '" readonly="" style="background-color: white;" placeholder="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB"><div class="input-group-append"><div class="btn btn-primary btn-sm"><span><i class="fa fa-upload"></i></span><input type="file" id="file-dokumen-pengadaan-tambah-' + (next) + '" rel="tooltip" data-original-title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." title="Format .jpeg/.pdf/.zip. Ukuran maksimum: 100 MB." class="upload" name="id_dokumentasi~' + (next) + '" /></div></div></div></div><div class="col-md-4 col-sm-4 col-xs-4"><div class="input-group"><input type="date" name="tanggal_dokumentasi~' + (next) + '"class="form-control" placeholder="Tanggal Dokumentasi" rel="tooltip" title="Tanggal Dokumentasi" data-original-title="Tanggal Dokumentasi"><div class="input-group-append"><div class="text-primary">&emsp;<i class="fa fa-calendar"></i></div></div></div></div></div><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><input type="text" class="form-control" placeholder="Keterangan dokumen" name="keterangan_dokumentasi~' + (next) + '"></div></div><br><div class="row"><div class="col-md-1 col-sm-1 col-xs-1"></div><div class="col-md-11 col-sm-11 col-xs-11"><div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><div class="radio"><label class="radio-inline">Apakah ada dokumen secara fisik?</label>&emsp;<br><label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="Y"> Ya </label>&emsp;<label class="radio-inline"><input type="radio" class="dokumentasi-fisik-' + (next) + '" name="is_document_stored' + (next) + '" value="N"> Tidak </label></div></div><div id="form-penyimpanan-dokumentasi-' + (next) + '" class="col-md-8 col-sm-8 col-xs-8">Disimpan di<div class="row"><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~lokasi~' + (next) + '" id="tambah-lokasi-dropdown-' + (next) + '"><option value=""  selected="true">Pilih Lokasi</option>@foreach ($list_lokasi as $lokasi)<option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>@endforeach</select></div><div class="col-md-4 col-sm-4  col-xs-4"><select class="form-control" name="dokumentasi~kontainer~' + (next) + '" id="tambah-kontainer-dropdown-' + (next) + '"><option value="" selected="true">Pilih kontainer...</option></select></div><div class="col-md-4 col-sm-4 col-xs-4"><select class="form-control" name="dokumentasi~posisi_kontainer~' + (next) + '" id="tambah-posisi-kontainer-dropdown-' + (next) + '"><option value="" selected>Pilih posisi kontainer...</option></select></div></div></div></div></div></div></div>');
        var count_dokumen = $("#count-dokumen").val();
        $("#count-dokumen").val(parseInt(count_dokumen)+1);
        $("#last-dokumen-id").val(parseInt(next));
        $("#form-penyimpanan-dokumentasi-" + next).hide();                  
      });

      $(document).on("click", "[id^=btn-hapus-item-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $('#item-pengadaan-' + targetId).remove();
        var current = $("#count-item").val();
        var next = parseInt(current) - 1;
        $("#count-item").val(next);

        var current_index_item = $("#index-item").val();
        var new_index_item = current_index_item.replace(targetId + '~', '');
        $("#index-item").val(new_index_item);
      });

      $(document).on("click", "[id^=btn-hapus-dokumen-]", function (e) {
        var targetId = e.currentTarget.id.split('-').pop();
        $('#dokumen-' + targetId).remove();
        var current = $("#count-dokumen").val();
        var next = parseInt(current) - 1;
        $("#count-dokumen").val(next);
      });

      $(document).on("change", "[id^=jumlah-stok-]", function (e) {
        e.preventDefault();
        var targetId = e.currentTarget.id.split('-').pop();
        var last_jumlah = $("#last-jumlah-stok-" + targetId).val();
        var current_jumlah = $("#jumlah-stok-" + targetId).val();
        var count_satuan = $("#count-satuan-" + targetId).val();

        if(parseInt(current_jumlah) > parseInt(last_jumlah)) {
          var next_satuan_id = parseInt(count_satuan) + 1;
          $('#div-satuan-item-' + targetId).append('<div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12" id="satuan-item-' + targetId + '-' + next_satuan_id + '"><div class="col-md-1 col-sm-1 col-xs-1"><i>Satuan ' + next_satuan_id + '</i></div><div class="col-md-2 col-sm-2 col-xs-2"><input type="text" class="form-control" placeholder="Field Satuan" id="field-aset-' + targetId + '-' + next_satuan_id + '" name="field_aset~' + targetId + '-' + next_satuan_id + '"></div><div class="col-md-3 col-sm-3 col-xs-3"><input type="text" class="form-control" placeholder="Isian dari Field Satuan" id="isian-aset-' + targetId + '-' + next_satuan_id + '" name="isian_aset~' + targetId + '-' + next_satuan_id + '"></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="satuan~lokasi~' + targetId + '-' + next_satuan_id + '" id="tambah-satuan-lokasi-dropdown_' + targetId + '-' + next_satuan_id + '"><option value="" selected="true">Disimpan di? Pilih lokasi</option>@foreach ($list_lokasi as $lokasi)<option value="{{ $lokasi->id_lokasi }}">{{ $lokasi->nama_lokasi }}</option>@endforeach</select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="satuan~kontainer~' + targetId + '-' + next_satuan_id + '" id="tambah-satuan-kontainer-dropdown_' + targetId + '-' + next_satuan_id + '"><option value="" selected="true">Pilih kontainer...</option></select></div><div class="col-md-2 col-sm-2 col-xs-2"><select class="form-control" name="satuan~posisi_kontainer~' + targetId + '-' + next_satuan_id + '" id="tambah-satuan-posisi-kontainer-dropdown_' + targetId + '-' + next_satuan_id + '"><option value="" selected>Pilih posisi kontainer...</option></select></div></div>');
          $("#last-jumlah-stok-" + targetId).val(parseInt(last_jumlah) + 1);
          $("#count-satuan-" + targetId).val(next_satuan_id);
          $("#total-satuan-" + targetId).html(next_satuan_id);

          var current_index_satuan = $("#index-satuan-" + targetId).val();
          $("#index-satuan-" + targetId).val(current_index_satuan + next_satuan_id + "~");

        } else if(parseInt(current_jumlah) < parseInt(last_jumlah)) {
          $('#satuan-item-' + targetId + '-' + count_satuan).remove();
          var next_satuan_id = parseInt(count_satuan) - 1;
          $("#last-jumlah-stok-" + targetId).val(next_satuan_id);
          $("#count-satuan-" + targetId).val(next_satuan_id);
          $("#total-satuan-" + targetId).html(next_satuan_id);
          
          var current_index_satuan = $("#index-satuan-" + targetId).val();
          var new_index_satuan = current_index_satuan.replace(next_satuan_id + '~', '');
          $("#index-satuan-" + targetId).val(new_index_satuan);
        }
      });

      $(document).on("change", "[id^=tambah-satuan-lokasi-dropdown_]", function (e) {
        e.preventDefault();
        var target = e.currentTarget.id.split('_').pop();
        var targetParent = target.split('-')[0];
        var targetChild = target.split('-').pop();
        var lokasi = e.target.value;

        if(lokasi){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-kontainer-list')}}?lokasi="+lokasi,
             success:function(res){
              if(res){

                  $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="" disable="true" selected="true">Pilih Kontainer</option>');
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="'+Obj.id_kontainer+'">'+Obj.nama_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
              }
             }
          });
        }
        else{
          $('#tambah-satuan-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
          $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
        }

      });

      $(document).on("change", "[id^=tambah-satuan-kontainer-dropdown_]", function (e) {
        e.preventDefault();
        var target = e.currentTarget.id.split('_').pop();
        var targetParent = target.split('-')[0];
        var targetChild = target.split('-').pop();
        var kontainer = e.target.value;

        if(kontainer){
          $.ajax({
             type:"GET",
             url:"{{url('api/get-posisi-kontainer-list')}}?kontainer="+kontainer,
             success:function(res){
              if(res){
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="" disable="true" selected="true">Pilih Posisi Kontainer</option>');

                  $.each(res,function(index,Obj){
                      $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).append('<option value="'+Obj.id_posisi_kontainer+'">'+Obj.label_posisi_kontainer+'</option>');
                  });

              }else{
                  $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
              }
             }
          });
        }
        else{
          $('#tambah-satuan-posisi-kontainer-dropdown_' + targetParent + '-' + targetChild).empty();
        }
      });

      $("[id^=tr-satuan-aset-]").hide();

    $(document).on("click", "[id^=aset-expandable-td_]", function (e) {
      e.preventDefault();
      var target = e.currentTarget.id.split('_').pop();
      var targetParent = target.split('-')[0];
      var targetChild = target.split('-')[1];
      // alert(targetParent + ' ' + targetChild); 

      if($('#btn-aset-exp-' + targetParent + '-' + targetChild).length) {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-col-' + targetParent +'-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-minus-circle"></i></button>');

        if($('#check-aset-' + targetParent + '-' + targetChild).val() == 0) {
          $.ajax({
            type:"GET",
            url:"{{url('api/get-satuan-item-pengadaan')}}?kode_pengadaan=" + targetParent + "&id_aset=" + targetChild,
            success:function(res){
              if(res.list_satuan_item_pengadaan.length){
                $.each(res.list_satuan_item_pengadaan,function(index,Obj){
                  var status_satuan_aset = '-';
                  var keterangan_satuan_aset = '-';
                  if (Obj.asa_id_aset_keluar) {
                    if (Obj.ake_jenis_aset_keluar == 'Alokasi')
                    {
                      status_satuan_aset = '<i class="text-success">Sudah dialokasikan ke ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' pada tanggal ' + moment(Obj.ake_tanggal_aset_keluar).format('Do MMMM YYYY') + '.</i>';
                    }
                    else
                    {
                      status_satuan_aset = '<i class="text-success">Aset dipinjamkan kepada ';
                      if (Obj.ake_kode_kanim) {
                        status_satuan_aset = status_satuan_aset + Obj.ake_nama_kanim;
                      } else {
                        status_satuan_aset = status_satuan_aset + Obj.ake_lokasi_keluar_lain;
                      }
                      status_satuan_aset = status_satuan_aset + ' dan harus dikembalikan pada tanggal ' + moment(Obj.ake_tanggal_aset_kembali).format('Do MMMM YYYY') + '.</i>'
                    }
                  } else {
                    if (Obj.asa_id_lokasi) {
                      status_satuan_aset = 'Belum dialokasikan dan masih tersimpan di ' + Obj.asa_nama_lokasi;
                      if (Obj.asa_id_kontainer) {
                        status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_nama_kontainer;
                        if(Obj.asa_id_posisi_kontainer) {
                          status_satuan_aset = status_satuan_aset + ' >> ' + Obj.asa_label_posisi_kontainer;
                        }
                      } 
                    }
                  }

                  var newRowSatuan = '<tr><td><i>Satuan #' + (index + 1) + '</i></td><td> ' + Obj.asa_field_aset + '</td><td>' + Obj.asa_isian_aset + '</td><td>' + parseFloat(Obj. asm_harga_satuan).toFixed(2) + '</td><td>' + status_satuan_aset + '</td><td>' + keterangan_satuan_aset + '</td></tr>';
                  $(newRowSatuan).appendTo($("#tbody-satuan-aset-" + targetParent + "-" + targetChild));
                  $('#check-aset-' + targetParent + '-' + targetChild).val(1);
                });
              } else {
                // console.log('test oy');
                var emptyRowSatuan = '<tr><td colspan="6"><i>Tidak ada data.</i></td></tr>';
                $(emptyRowSatuan).appendTo($('#tbody-satuan-aset-' + targetParent + '-' + targetChild));
                $('#check-aset-' + targetParent + '-' + targetChild).val(0);
              }
           }, error:function(x) {
            alert('Pengambilan data error! Silahkan coba refresh halaman kembali.');
            console.log(x);
           }
          });
        } else {
          $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "");
        }
      } else {
        $('#aset-expandable-td_' + targetParent + '-' + targetChild).html('<button type="button" id="btn-aset-exp-' + targetParent + '-' + targetChild + '" class="btn btn-md btn-link btn-default" style="margin-right: 3px; padding: 0px;"><i class="fa fa-plus-circle"></i></button>');
        $('#hidden-satuan-aset-' + targetParent + '-' + targetChild).attr("style", "display: none");
      }
      $('#tr-satuan-aset-' + targetParent + '-' + targetChild).slideToggle(); 
    }); 
  });
</script>
@endsection